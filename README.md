# ManagementByFlutter

#### 介绍
{**以下是 Gitee 平台说明，您可以替换此简介**
Gitee 是 OSCHINA 推出的基于 Git 的代码托管平台（同时支持 SVN）。专为开发者提供稳定、高效、安全的云端软件开发协作平台
无论是个人、团队、或是企业，都能够用 Gitee 实现代码托管、项目管理、协作开发。企业项目请看 [https://gitee.com/enterprises](https://gitee.com/enterprises)}

#### 软件架构
软件架构说明

#### problem

[同一界面刷新复用解决办法](https://juejin.cn/post/7345105816242683943)

#### 页面跳转

1. 初始界面 SplashScreen
if(登录过) => HomeScreen else => WelcomeScreen
2. 欢迎界面 WelcomeScreen 
if(登录) => SignInScreen else => SignUpScreen
3. 注册界面 SignUpScreen
if(登录) => SignInScreen
if(注册) => VerifyCodeScreen 
{'isNewPassword': 是否修改密码, 'data': 手机号/邮箱, 'pwd': 密码}
if(isNewPassword) => CreatePwdScreen else WelcomeScreen
4. 验证码界面 VerifyCodeScreen
if(创建账号) => WelcomeScreen
if(修改密码) => CreateNewPwdScreen
{'account': 账号, 'code' : 验证码}
5. 登录界面 SignInScreen
if(第一次登录) => EditProfileScreen else => HomeScreen
if(注册) => SignUpScreen
6. 详情界面 ProfileScreen
if(点击公司) => WorkSpaceScreen
if(修改信息) => EditProfileScreen
if(点击安全) => SecurityScreen
if(点击登出) => WelcomeScreen
7. 主界面 homeScreen
if(公司id==0) => jCompanyScreen
{'isJoin' : 是否加入公司 }
8. 加入公司界面 jCompanyScreen
if(加入成功) => HomeScreen
9. 安全界面 SecurityScreen
if(修改密码) => VerifiedScreen
{'isNewPassword': 是否修改密码, 'data': 手机号/邮箱, 'pwd': 密码}

thanks to @Baker for the right answer. 
However, if you have a list and in viewModel and want to update that list, 
just use the list.refresh() when the list updated

RxList<Models> myList = <Models>[].obs;
when add or insert data act like this:

myList.add(newItem);
myList.refresh();

#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
