import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:management/const/consts.dart';
import 'package:management/const/init.dart';
import 'package:management/data/network/api/api.dart';
import 'package:management/data/network/entity/demand/response/ListLogDemandOrTaskResponse.dart'
    as log;
import 'package:management/element/loading.dart';
import 'package:management/element/toast.dart';
import 'package:management/main.dart';
import 'package:timelines/timelines.dart';

class MyLogSheet {
  MyLogSheet._();

  static var _listLogDemand = <log.Data>[];

  static showLogSheet(num demandId) {
    LoadingElement.loadingDialog(true);
    MyHttpClient.getListLogDemandOrTask(demandId.toString()).then((value) {
      if (value.code == 200) {
        _listLogDemand = value.data!;
        _realSeeDemandLog();
      } else {
        Get.back();
        Utils.toastError(value.msg);
      }
    }).onError((error, stackTrace) {
      Get.back();
      Utils.toastError(error);
    });
  }

  static void _realSeeDemandLog() {
    Get.back();
    Get.bottomSheet(
        Flexible(
          child: Padding(
            padding: const EdgeInsets.all(24.0),
            child: Timeline(
              theme: TimelineThemeData(
                  direction: Axis.vertical,
                  color: MyColor.primary[300],
                  nodePosition: 0.3),
              children: List.generate(_listLogDemand.length, (int index) {
                return TimelineTile(
                  node: TimelineNode(
                    indicator: DotIndicator(
                      child: CircleAvatar(
                        radius: 13,
                        backgroundColor:
                            Theme.of(Get.context!).scaffoldBackgroundColor,
                        backgroundImage: NetworkImage(
                            _listLogDemand[index].companyUser?.avatar ??
                                GlobalInit.headImage),
                      ),
                    ),
                    startConnector: index != 0
                        ? DecoratedLineConnector(
                            decoration: BoxDecoration(
                              gradient: LinearGradient(
                                begin: Alignment.topCenter,
                                end: Alignment.bottomCenter,
                                colors: [
                                  MyColor.gradientBlue[0]!,
                                  MyColor.gradientBlue[1]!
                                ],
                              ),
                            ),
                          )
                        : null,
                    endConnector: index == _listLogDemand.length - 1
                        ? null
                        : DecoratedLineConnector(
                            decoration: BoxDecoration(
                              gradient: LinearGradient(
                                begin: Alignment.topCenter,
                                end: Alignment.bottomCenter,
                                colors: [
                                  MyColor.gradientBlue[1]!,
                                  MyColor.gradientBlue[0]!
                                ],
                              ),
                            ),
                          ),
                  ),
                  contents: Container(
                    margin: const EdgeInsets.only(left: 24),
                    padding: const EdgeInsets.all(8),
                    child: GestureDetector(
                      onTap: () =>
                          _jumpToNextDemandByLog(_listLogDemand[index]),
                      child: Card(
                        child: Column(
                          children: [
                            Text(_logText1(_listLogDemand[index]),
                                style: Theme.of(Get.context!)
                                    .textTheme
                                    .bodyMedium
                                    ?.copyWith(
                                      fontWeight: FontWeight.w400,
                                    )),
                            Text(_listLogDemand[index].relateDemandName ?? '',
                                style: Theme.of(Get.context!)
                                    .textTheme
                                    .bodyMedium
                                    ?.copyWith(
                                        fontWeight: FontWeight.w400,
                                        color: MyColor.grayScale[500])),
                          ],
                        ),
                      ),
                    ),
                  ),
                  oppositeContents: Center(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(_listLogDemand[index].createTime ?? '',
                          style: Theme.of(Get.context!)
                              .textTheme
                              .bodyMedium
                              ?.copyWith(
                                  fontWeight: FontWeight.w400,
                                  color: MyColor.grayScale[500])),
                    ),
                  ),
                );
              }),
            ),
          ),
        ),
        isDismissible: false,
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(top: Radius.circular(20))),
        backgroundColor: Theme.of(Get.context!).scaffoldBackgroundColor);
  }

  static String _logText1(log.Data listLogDemand) {
    if (listLogDemand.companyUser == null) {
      return '';
    }
    if (listLogDemand.companyUser?.nickName == null) {
      return '';
    }
    return '用户  ${listLogDemand.companyUser!.nickName!}  ${listLogDemand.content!}';
  }

  static void _jumpToNextDemandByLog(log.Data listLogDemand) {
    if (listLogDemand.content == null || listLogDemand.id == null) return;
    if (listLogDemand.content!.startsWith('创建')) {
      Get.toNamed(RouteConfig.demandOrTaskScreen,
          preventDuplicates: false,
          arguments: {'demandId': listLogDemand.relateDemandId});
    }
  }
}
