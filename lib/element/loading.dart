import 'package:flutter/material.dart';
import 'package:flutter_svg_provider/flutter_svg_provider.dart';
import 'package:get/get.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';
import 'package:lottie/lottie.dart';
import 'package:management/const/consts.dart';
import 'package:management/generated/assets.dart';

class LoadingElement {
  LoadingElement._();
  static Widget loadingWidget(bool loadingState) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 44, vertical: 250),
      child: Wrap(children: [
        SizedBox(
          width: 340,
          height: 300,
          child: Card(
              shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(24)),
              ),
              child: loadingState
                  ? Lottie.asset(Assets.loading,
                      repeat: true,
                      fit: BoxFit.scaleDown,
                      width: 300,
                      height: 300)
                  : Stack(
                      fit: StackFit.expand,
                      alignment: AlignmentDirectional.center,
                      children: [
                        Positioned(
                            bottom: 160,
                            child: Text(
                              MyText.loadingFailure.tr,
                              style: Theme.of(Get.context!)
                                  .textTheme
                                  .titleLarge
                                  ?.copyWith(
                                      color: MyColor.error,
                                      fontStyle: FontStyle.italic),
                            )),
                        const Image(
                          image: Svg(
                            Assets.imagesLoadingFailure,
                          ),
                          fit: BoxFit.contain,
                          width: 300,
                          height: 300,
                        ),
                      ],
                    )),
        ),
      ]),
    );
  }

  static void loadingDialog(dynamic canBack) {
    if (canBack is bool) {
      Get.defaultDialog(
          title: "",
          backgroundColor: Colors.transparent,
          barrierDismissible: false,
          onWillPop: (() async {
            return canBack ?? false;
          }),
          content: LoadingAnimationWidget.flickr(
              leftDotColor: MyColor.blue,
              rightDotColor: MyColor.error,
              size: 50));
    } else {
      Get.defaultDialog(
          title: "",
          backgroundColor: Colors.transparent,
          barrierDismissible: false,
          onWillPop: canBack,
          content: LoadingAnimationWidget.flickr(
              leftDotColor: MyColor.blue,
              rightDotColor: MyColor.error,
              size: 50));
    }
  }
}
