import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:management/const/consts.dart';

class Utils {
  static toastError(dynamic msg) {
    Get.snackbar(MyText.error.tr, msg.toString(),
        duration: const Duration(seconds: 1, milliseconds: 500),
        backgroundGradient: LinearGradient(colors: [
          MyColor.gradientRed[0]!,
          MyColor.gradientRed[1]!,
          MyColor.gradientRed[0]!
        ]),
        snackPosition: SnackPosition.TOP,
        margin:
            const EdgeInsets.only(left: 20, top: 300, right: 20, bottom: 20));
  }

  static toastWarning(dynamic msg) {
    Get.snackbar(MyText.warning.tr, msg.toString(),
        duration: const Duration(seconds: 1, milliseconds: 500),
        backgroundGradient: LinearGradient(colors: [
          MyColor.gradientYellow[0]!,
          MyColor.gradientYellow[1]!,
          MyColor.gradientYellow[0]!
        ]),
        snackPosition: SnackPosition.TOP,
        margin:
            const EdgeInsets.only(left: 20, top: 300, right: 20, bottom: 20));
  }

  static toastNotification(dynamic msg) {
    Get.snackbar(MyText.notifications.tr, msg.toString(),
        duration: const Duration(seconds: 3, milliseconds: 500),
        backgroundGradient: Get.isDarkMode
            ? LinearGradient(colors: [
                MyColor.gradientGreen[0]!,
                MyColor.gradientGreen[1]!,
                MyColor.gradientGreen[0]!
              ])
            : LinearGradient(colors: [
                MyColor.gradientBlue[0]!,
                MyColor.gradientBlue[1]!,
                MyColor.gradientBlue[0]!
              ]),
        snackPosition: SnackPosition.TOP,
        margin: const EdgeInsets.all(20));
  }

  static toast(dynamic title, dynamic msg) {
    Get.snackbar(title.toString(), msg.toString(),
        duration: const Duration(seconds: 2, milliseconds: 500),
        snackPosition: SnackPosition.BOTTOM,
        margin: const EdgeInsets.all(20));
  }
}
