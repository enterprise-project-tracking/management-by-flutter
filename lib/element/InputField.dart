import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:management/const/consts.dart';
import 'package:management/const/icon.dart';

class MyTextField extends StatefulWidget {
  const MyTextField(
      {super.key,
      this.hintText,
      this.prefixIcon,
      this.errorText,
      this.suffixIcon,
      this.prefixText,
      this.inputType,
      this.isDark = false,
      this.onSubmitted,
      this.onChanged,
      this.controller,
      this.readOnly = false,
      this.autoFocus = false,
      this.onTap,
      this.labelText,
      this.onTapSuffix,
      this.expands = false});

  final String? hintText;
  final IconData? prefixIcon;
  final IconData? suffixIcon;
  final String? errorText;
  final String? prefixText;
  // s -> secret, e -> Email, n->Number, p->Phone
  final String? inputType;
  final bool readOnly;
  final bool autoFocus;
  final bool isDark;
  final String? labelText;
  final bool expands;
  final TextEditingController? controller;
  final ValueChanged<String>? onSubmitted;
  final ValueChanged<String>? onChanged;
  final void Function()? onTapSuffix;
  final void Function()? onTap;

  @override
  State<StatefulWidget> createState() => _MyTextFieldState();
}

class _MyTextFieldState extends State<MyTextField> {
  Color _backColor = MyColor.grayScale[50]!;
  Color _iconColor = MyColor.grayScale[500]!;
  bool _isShowText = false;
  @override
  Widget build(BuildContext context) {
    return Focus(
      onFocusChange: (focus) => {
        if (focus)
          {
            _backColor = Get.isDarkMode ? MyColor.dark2 : MyColor.backBlue,
            _iconColor = MyColor.primary
          }
        else
          {
            _backColor =
                Get.isDarkMode ? MyColor.dark3 : MyColor.grayScale[50]!,
            _iconColor = MyColor.grayScale[500]!
          },
        setState(() => {})
      },
      child: TextField(
        onTap: widget.onTap,
        onSubmitted: widget.onSubmitted,
        onChanged: widget.onChanged,
        readOnly: widget.onTapSuffix == null ? widget.readOnly : false,
        autofocus: widget.autoFocus,
        controller: widget.controller,
        expands: widget.expands,
        minLines: widget.expands ? null : 1,
        decoration: InputDecoration(
            prefixIcon: widget.prefixIcon == null
                ? null
                : Icon(
                    widget.prefixIcon,
                    size: 20,
                  ),
            hintText: widget.hintText,
            errorText: widget.errorText,
            prefixText: widget.prefixText,
            suffix: _suffix(),
            filled: true,
            counterText: ' ',
            labelStyle: Theme.of(Get.context!).textTheme.bodyMedium?.copyWith(
                color: MyColor.grayScale[500], fontWeight: FontWeight.w400),
            floatingLabelStyle: Theme.of(Get.context!)
                .textTheme
                .bodyMedium
                ?.copyWith(color: MyColor.primary, fontWeight: FontWeight.w700),
            labelText: widget.labelText,
            fillColor: _backColor),
        style: TextStyle(
            fontSize: MyDimension.medium,
            fontWeight: FontWeight.w600,
            color: Get.isDarkMode ? MyColor.white : MyColor.grayScale[900]!,
            letterSpacing: 0.2,
            height: 1.4),
        maxLines: widget.expands ? null : 1,
        maxLength: widget.expands ? null : 40,
        cursorWidth: widget.onTapSuffix != null ? 0 : 2,
        obscureText: _isShowText,
        keyboardType:
            widget.onTapSuffix == null ? _keyBordType() : TextInputType.none,
      ),
    );
  }

  Widget? _suffix() {
    if (widget.suffixIcon == null) return null;

    if (widget.suffixIcon == IconBold.Iconly_Bold_Hide) {
      return GestureDetector(
        child: Icon(
          widget.suffixIcon,
          size: 20,
          color: _iconColor,
        ),
        onTap: () {
          _isShowText = !_isShowText;
          setState(() {});
        },
      );
    }
    return GestureDetector(
      onTap: widget.onTapSuffix,
      child: Icon(
        widget.suffixIcon,
        size: 20,
        color: _iconColor,
      ),
    );
  }

  TextInputType? _keyBordType() {
    switch (widget.inputType) {
      case 's':
        return TextInputType.visiblePassword;
      case 'n':
        return TextInputType.number;
      case 'p':
        return TextInputType.phone;
      case 'e':
        return TextInputType.emailAddress;
    }
    return null;
  }
}
