import 'package:flutter/material.dart';

class MyDelegate extends SliverPersistentHeaderDelegate {
  final Widget? child;
  final double height;

  MyDelegate({
    required this.child,
    required this.height,
  });

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return SizedBox.expand(
      child: Material(
        elevation: overlapsContent ? 5 : 0,
        color: Theme.of(context).scaffoldBackgroundColor,
        shadowColor: const Color(0x1404060f),
        child: child,
      ),
    );
  }

  @override
  double get maxExtent => height;

  @override
  double get minExtent => height;

  @override
  bool shouldRebuild(covariant SliverPersistentHeaderDelegate oldDelegate) =>
      true;
}
