import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:management/const/consts.dart';

class DemandStatusSheet {
  DemandStatusSheet._();

  static var listDemandState = {
    0: MyText.notStarted.tr,
    1: MyText.inProgress.tr,
    2: MyText.completed.tr,
    3: MyText.testing.tr
  };

  static void showSheet(int value) {
    Get.bottomSheet(
        Wrap(
            children: List<Widget>.generate(5, (index) {
          return Container(
            padding: const EdgeInsets.symmetric(vertical: 2),
            width: double.infinity,
            height: 60,
            child: TextButton(
              onPressed: () {
                if (index == 4) {
                  Get.back();
                  return;
                }
                value = index;
                Get.back();
              },
              style: Theme.of(Get.context!).textButtonTheme.style,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    index == 4 ? MyText.cancel : listDemandState[index]!,
                    style: Theme.of(Get.context!).textTheme.bodyLarge?.copyWith(
                        color: index == 4
                            ? MyColor.error
                            : MyColor.grayScale[500]),
                  ),
                ],
              ),
            ),
          );
        })),
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(top: Radius.circular(20))),
        backgroundColor: Theme.of(Get.context!).scaffoldBackgroundColor);
  }
}
