import 'package:flutter/material.dart';
import 'package:management/const/consts.dart';

class AppTheme {
  // 2.亮色主题
  static final ThemeData lightTheme = ThemeData(
    brightness: Brightness.light,
    colorScheme: ColorScheme.fromSeed(
        seedColor: MyColor.primary[500]!,
        primary: MyColor.primary,
        secondary: MyColor.second,
        error: MyColor.error,
        background: MyColor.white),
    inputDecorationTheme: InputDecorationTheme(
      contentPadding: const EdgeInsets.symmetric(horizontal: 20, vertical: 18),
      fillColor: MyColor.grayScale[50],
      focusColor: MyColor.backBlue,
      suffixIconColor: MyColor.primary,
      hintStyle: TextStyle(
        fontSize: MyDimension.medium,
        fontWeight: FontWeight.w400,
        color: MyColor.grayScale[500]!,
        letterSpacing: 0.2,
        height: 1.4,
      ),
      enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(12),
          borderSide: BorderSide(width: 1.0, color: MyColor.grayScale[50]!)),
      focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(12),
          borderSide: const BorderSide(width: 1.0, color: MyColor.primary)),
      errorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(12),
          borderSide: const BorderSide(width: 1.0, color: MyColor.error)),
      focusedErrorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(12),
          borderSide: const BorderSide(width: 1.0, color: MyColor.error)),
    ),
    textTheme: TextTheme(
      headlineLarge: TextStyle(
          fontSize: MyDimension.heading1,
          height: 1.1,
          fontWeight: FontWeight.w700,
          color: MyColor.grayScale[900]),
      headlineMedium: TextStyle(
          fontSize: MyDimension.heading2,
          height: 1.1,
          fontWeight: FontWeight.w700,
          color: MyColor.grayScale[900]),
      headlineSmall: TextStyle(
          fontSize: MyDimension.heading3,
          height: 1.1,
          fontWeight: FontWeight.w700,
          color: MyColor.grayScale[900]),
      titleLarge: TextStyle(
          fontSize: MyDimension.heading4,
          height: 1.2,
          fontWeight: FontWeight.w700,
          color: MyColor.grayScale[900]),
      titleMedium: TextStyle(
          fontSize: MyDimension.heading5,
          height: 1.2,
          fontWeight: FontWeight.w700,
          color: MyColor.grayScale[900]),
      titleSmall: TextStyle(
          fontSize: MyDimension.heading6,
          height: 1.2,
          fontWeight: FontWeight.w700,
          color: MyColor.grayScale[900]),
      bodyLarge: TextStyle(
          fontSize: MyDimension.large,
          height: 1.4,
          letterSpacing: 0.2,
          wordSpacing: 0.2,
          fontWeight: FontWeight.w600,
          color: MyColor.grayScale[900]),
      bodyMedium: TextStyle(
          fontSize: MyDimension.medium,
          height: 1.4,
          letterSpacing: 0.2,
          wordSpacing: 0.2,
          fontWeight: FontWeight.w600,
          color: MyColor.grayScale[900]),
      bodySmall: TextStyle(
          fontSize: MyDimension.small,
          letterSpacing: 0.2,
          wordSpacing: 0.2,
          fontWeight: FontWeight.w600,
          color: MyColor.grayScale[900]),
    ),
    appBarTheme: AppBarTheme(
        backgroundColor: Colors.transparent,
        elevation: 0,
        titleTextStyle: TextStyle(
            fontSize: MyDimension.heading4,
            height: 1.2,
            fontWeight: FontWeight.w700,
            color: MyColor.grayScale[900]),
        toolbarHeight: 48,
        iconTheme: IconThemeData(size: 28, color: MyColor.grayScale[900]),
        actionsIconTheme:
            const IconThemeData(size: 28, color: MyColor.grayScale)),
    textButtonTheme: TextButtonThemeData(
      style: ButtonStyle(
          side: MaterialStateProperty.all(
              BorderSide(color: MyColor.grayScale[200]!)),
          shape: MaterialStateProperty.all(const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(16))))),
    ),
    elevatedButtonTheme: ElevatedButtonThemeData(
        style: ButtonStyle(
      alignment: Alignment.center,
      textStyle: MaterialStateProperty.all(const TextStyle(
          fontWeight: FontWeight.w700,
          fontSize: MyDimension.large,
          letterSpacing: 0.2,
          height: 1.4,
          color: MyColor.white)),
      shape: MaterialStateProperty.all(const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(100)))),
      shadowColor: MaterialStateProperty.all(MyColor.primary),
      elevation: MaterialStateProperty.resolveWith((states) {
        if (states.contains(MaterialState.disabled)) {
          return 0;
        }
        return 5;
      }),
      backgroundColor: MaterialStateProperty.resolveWith((states) {
        if (states.contains(MaterialState.disabled)) {
          return MyColor.disButton;
        }
        return MyColor.primary;
      }),
    )),
    checkboxTheme: const CheckboxThemeData(
        side: BorderSide(width: 3, color: MyColor.primary),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(8)),
        )),
    bottomNavigationBarTheme: BottomNavigationBarThemeData(
        elevation: 0,
        showSelectedLabels: true,
        showUnselectedLabels: true,
        backgroundColor: MyColor.white,
        unselectedItemColor: MyColor.grayScale[500],
        selectedItemColor: MyColor.primary,
        unselectedIconTheme:
            IconThemeData(size: 24, color: MyColor.grayScale[500]),
        selectedIconTheme:
            const IconThemeData(size: 24, color: MyColor.primary),
        unselectedLabelStyle: TextStyle(
            fontWeight: FontWeight.w500,
            fontSize: 10,
            height: 1.2,
            wordSpacing: 0.2,
            color: MyColor.grayScale[500]),
        selectedLabelStyle: const TextStyle(
            fontWeight: FontWeight.w500,
            fontSize: 10,
            height: 1.2,
            wordSpacing: 0.2,
            color: MyColor.primary)),
    floatingActionButtonTheme: const FloatingActionButtonThemeData(
        elevation: 0,
        backgroundColor: MyColor.primary,
        iconSize: 40,
        shape: CircleBorder()),
    iconButtonTheme: IconButtonThemeData(
        style: ButtonStyle(
      iconColor: MaterialStateProperty.all(MyColor.grayScale[900]),
    )),
    listTileTheme: ListTileThemeData(
      iconColor: MyColor.grayScale[800],
      minLeadingWidth: 20,
    ),
    chipTheme: const ChipThemeData(
      backgroundColor: Colors.transparent,
      side: BorderSide(color: MyColor.primary, width: 2.0),
      labelStyle: TextStyle(
          fontWeight: FontWeight.w600,
          fontSize: 14,
          height: 1.4,
          wordSpacing: 0.2,
          color: MyColor.primary),
      selectedColor: MyColor.primary,
    ),
    cardTheme: const CardTheme(
      color: MyColor.white
    ),
    sliderTheme: SliderThemeData(
      
    ),
    scaffoldBackgroundColor: MyColor.white,
    useMaterial3: false,
  );

  // 3.暗黑主题
  static final ThemeData darkTheme = ThemeData(
    colorScheme: ColorScheme.fromSwatch()
        .copyWith(background: MyColor.black, brightness: Brightness.dark),
    // colorScheme: ColorScheme.fromSeed(
    //   seedColor: MyColor.primary,
    //   primary: MyColor.primary,
    //   secondary: MyColor.second,
    //   error: MyColor.error,
    //   background: MyColor.black,
    // ),
    inputDecorationTheme: InputDecorationTheme(
      contentPadding: const EdgeInsets.symmetric(horizontal: 20, vertical: 18),
      fillColor: MyColor.dark2,
      focusColor: MyColor.backBlue,
      hintStyle: TextStyle(
          fontSize: MyDimension.medium,
          fontWeight: FontWeight.w400,
          color: MyColor.grayScale[500]!,
          letterSpacing: 0.2,
          height: 1.4),
      enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(12),
          borderSide: const BorderSide(width: 1.0, color: MyColor.dark2)),
      focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(12),
          borderSide: const BorderSide(width: 1.0, color: MyColor.primary)),
      errorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(12),
          borderSide: const BorderSide(width: 1.0, color: MyColor.error)),
      focusedErrorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(12),
          borderSide: const BorderSide(width: 1.0, color: MyColor.error)),
    ),
    textTheme: const TextTheme(
      headlineLarge: TextStyle(
          fontSize: MyDimension.heading1,
          height: 1.1,
          fontWeight: FontWeight.w700,
          color: MyColor.white),
      headlineMedium: TextStyle(
          fontSize: MyDimension.heading2,
          height: 1.1,
          fontWeight: FontWeight.w700,
          color: MyColor.white),
      headlineSmall: TextStyle(
          fontSize: MyDimension.heading3,
          height: 1.1,
          fontWeight: FontWeight.w700,
          color: MyColor.white),
      titleLarge: TextStyle(
          fontSize: MyDimension.heading4,
          height: 1.2,
          fontWeight: FontWeight.w700,
          color: MyColor.white),
      titleMedium: TextStyle(
          fontSize: MyDimension.heading5,
          height: 1.2,
          fontWeight: FontWeight.w700,
          color: MyColor.white),
      titleSmall: TextStyle(
          fontSize: MyDimension.heading6,
          height: 1.2,
          fontWeight: FontWeight.w700,
          color: MyColor.white),
      bodyLarge: TextStyle(
          fontSize: MyDimension.large,
          height: 1.4,
          letterSpacing: 0.2,
          wordSpacing: 0.2,
          fontWeight: FontWeight.w600,
          color: MyColor.white),
      bodyMedium: TextStyle(
          fontSize: MyDimension.medium,
          height: 1.4,
          letterSpacing: 0.2,
          wordSpacing: 0.2,
          fontWeight: FontWeight.w600,
          color: MyColor.white),
      bodySmall: TextStyle(
          fontSize: MyDimension.small,
          letterSpacing: 0.2,
          wordSpacing: 0.2,
          fontWeight: FontWeight.w600,
          color: MyColor.white),
    ),
    appBarTheme: const AppBarTheme(
        backgroundColor: Colors.transparent,
        elevation: 0,
        titleTextStyle: TextStyle(
            fontSize: MyDimension.heading4,
            height: 1.2,
            fontWeight: FontWeight.w700,
            color: MyColor.white),
        toolbarHeight: 48,
        iconTheme: IconThemeData(size: 28, color: MyColor.white),
        actionsIconTheme: IconThemeData(size: 28, color: MyColor.white)),
    textButtonTheme: TextButtonThemeData(
      style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all(MyColor.dark2),
          side:
              MaterialStateProperty.all(const BorderSide(color: MyColor.dark3)),
          shape: MaterialStateProperty.all(const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(16))))),
    ),
    elevatedButtonTheme: ElevatedButtonThemeData(
        style: ButtonStyle(
      alignment: Alignment.center,
      textStyle: MaterialStateProperty.all(const TextStyle(
          fontWeight: FontWeight.w700,
          fontSize: MyDimension.large,
          letterSpacing: 0.2,
          height: 1.4,
          color: MyColor.white)),
      shape: MaterialStateProperty.all(const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(100)))),
      shadowColor: MaterialStateProperty.all(MyColor.primary),
      elevation: MaterialStateProperty.resolveWith((states) {
        if (states.contains(MaterialState.disabled)) {
          return 0;
        }
        return 5;
      }),
      backgroundColor: MaterialStateProperty.resolveWith((states) {
        if (states.contains(MaterialState.disabled)) {
          return MyColor.disButton;
        }
        return MyColor.primary;
      }),
    )),
    checkboxTheme: const CheckboxThemeData(
        side: BorderSide(width: 3, color: MyColor.primary),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(8)),
        )),
    bottomNavigationBarTheme: BottomNavigationBarThemeData(
        elevation: 0,
        backgroundColor: MyColor.dark1,
        showSelectedLabels: true,
        showUnselectedLabels: true,
        unselectedItemColor: MyColor.grayScale[500],
        selectedItemColor: MyColor.primary,
        unselectedIconTheme: IconThemeData(color: MyColor.grayScale[500]),
        selectedIconTheme: const IconThemeData(color: MyColor.primary),
        unselectedLabelStyle: TextStyle(
            fontWeight: FontWeight.w500,
            fontSize: 10,
            height: 1.2,
            wordSpacing: 0.2,
            color: MyColor.grayScale[500]),
        selectedLabelStyle: const TextStyle(
            fontWeight: FontWeight.w500,
            fontSize: 10,
            height: 1.2,
            wordSpacing: 0.2,
            color: MyColor.primary)),
    floatingActionButtonTheme: const FloatingActionButtonThemeData(
      elevation: 0,
      backgroundColor: MyColor.primary,
      iconSize: 40,
      shape: CircleBorder(),
    ),
    iconButtonTheme: IconButtonThemeData(
        style: ButtonStyle(
      iconColor: MaterialStateProperty.all(MyColor.white),
    )),
    listTileTheme: const ListTileThemeData(
      iconColor: MyColor.white,
      minLeadingWidth: 20,
    ),
    chipTheme: const ChipThemeData(
      backgroundColor: MyColor.black,
      side: BorderSide(color: MyColor.primary, width: 2.0),
      labelStyle: TextStyle(
          fontWeight: FontWeight.w600,
          fontSize: 14,
          height: 1.4,
          wordSpacing: 0.2,
          color: MyColor.primary),
      selectedColor: MyColor.primary,
    ),
    cardTheme: const CardTheme(
        color: MyColor.dark2
    ),
    scaffoldBackgroundColor: MyColor.black,
    useMaterial3: false,
  );
}
