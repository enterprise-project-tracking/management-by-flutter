import 'package:get/get.dart';

import 'package:flutter/material.dart';
import 'package:management/const/consts.dart';

enum CompanyMemberPermissions { president, leader, member, none }

class MyText {
  MyText._();
  static const String login = 'login';
  static const String welcomeTitle = 'Let`s you in';
  static const String continueWithQQ = 'Continue with QQ';
  static const String continueWithWechat = 'Continue with Wechat';
  static const String or = 'or';
  static const String continueWithPassword = 'Sign in with password';
  static const String notAccount = 'Don’t have an account?';
  static const String hasAccount = 'Already have an account?';
  static const String signUp = 'Sign up';
  static const String signIn = 'Sign in';
  static const String createAccent = 'Create your\nAccount';
  static const String mail = 'Email';
  static const String phoneNumber = 'PhoneNUmber';
  static const String loginAccent = 'Login to your\nAccount';
  static const String errorTextEP = "Please enter the correct information";
  static const String errSomeThing = 'Something is Wrong';
  static const String resendFailure = 'Resend Failure';
  static const String errSomeThingEx = 'Please try again';
  static const String password = 'Password';
  static const String rememberMe = 'Remember me';
  static const String forgetPassword = 'Forgot the password?';
  static const String howToVerify =
      'Select which contact details should we use to reset your password';
  static const String viaSMS = 'via SMS:';
  static const String viaEmail = 'via Email:';
  static const String sendTo = 'Send To ';
  static const String pleaseEnterYourPhoneNumber = 'Enter your phoneNumber';
  static const String pleaseEnterYourEmailAddress = 'Enter your email';
  static const String pleaseEnterYourEmailOrPhone =
      'Enter your email or phoneNumber';
  static const String pleaseEnterYourOldPwd = 'Enter your old Pwd';
  static const String hasSendCodeTo = 'Verification code has been sent to ';
  static const String reSend = 'Resend';
  static const String submit = 'Submit';
  static const String createNewPwd = 'Create New Password';
  static const String continues = 'continue';
  static const String createNewSuccess = 'Your account is ready to use';
  static const String appName = 'Taska';
  static const String home = 'Home';
  static const String project = 'Project';
  static const String manage = 'Manage';
  static const String profile = 'Profile';
  static const String projects = 'Projects';
  static const String tasks = 'Tasks';
  static const String devices = 'Devices';
  static const String workspace = 'Workspace';
  static const String editProfile = 'Edit Profile';
  static const String notifications = 'Notifications';
  static const String notification = 'Notification';
  static const String security = 'Security';
  static const String help = 'Help';
  static const String darkTheme = 'Dark Theme';
  static const String logout = 'Logout';
  static const String logoutSure = 'Are you sure you want to log out ?';
  static const String logoutYes = 'Yes, Logout';
  static const String cancel = 'Cancel';
  static const String confirm = 'Confirm';
  static const String changePwd = 'Change Password';
  static const String searchForPeople = 'Search For People';
  static const String searchForDevice = 'Search For Device';
  static const String loadingFailure = 'Loading Failure';
  static const String sendSuccess = 'Sent successfully';
  static const String signUpSuccess = 'SignUp successfully';
  static const String signInSuccess = 'SignIn successfully';
  static const String error = 'Error';
  static const String delete = 'Delete';
  static const String deleteSuccess = 'Delete Successfully';
  static const String warning = 'Warning';
  static const String getUserDataFailure = 'Get UserData Failure';
  static const String update = 'Update';
  static const String fire = 'fire';
  static const String upDateSuccess = 'Update Successful';
  static const String pmuibs =
      'Please modify your information before submitting';
  static const String clickToCpy = 'Click to copy';
  static const String defaultName = 'Default Name';
  static const String unknownPosition = 'Unknown Position';
  static const String companyName = 'Company Name';
  static const String companyEmail = 'Company Email';
  static const String companyPhone = 'Company Phone';
  static const String companyCorporate = 'Corporate';
  static const String companyNumSize = 'Number of employees';
  static const String createTime = 'Creation time';
  static const String companyType = 'Industry type';
  static const String joinCompany = 'Join Your Company';
  static const String createCompany = 'Create New Company';
  static const String createCompanyGroup = 'Create company group';
  static const String deleteCompanyGroup = 'Delete company group';
  static const String changeCompanyGroup = 'Change company group';
  static const String mobileCompanyGroup = 'Mobile company group';
  static const String mobileEmployeeGroup = 'Mobile employee group';
  static const String psagtmt = 'Please select a group to move to';
  static const String createNewDevice = 'Create new device';
  static const String changeEmployee = 'Change employee';
  static const String changeDevice = 'Change device';
  static const String deleteDevice = 'Delete device';
  static const String deleteEmployee = 'Fire an employee';
  static const String corjCompanyHint = 'Create or Join Company';
  static const String pleaseEnterInvitationCode =
      'Please enter your invitation code';
  static const String pleaseEnterYourCompanyName =
      'Please enter your company name';
  static const String pleaseEnterYourCompanyInfo =
      'Please enter your company information';
  static const String pwdNotNull = 'password can not be blank';
  static const String pwdTwoEnterNotSame =
      'The new passwords entered twice are inconsistent';
  static const String companyGroupName = 'Company group name';
  static const String picFromCamera = 'Take a picture';
  static const String picFromAlbum = 'Select picture from album';
  static const String ppeo = 'Please perform effective operations';
  static const String moveAToB = 'Are you sure you want to move A to B';
  static const String inDemand = 'In demand';
  static const String inDevelopment = 'In development';
  static const String paid = 'Paid';
  static const String stopped = 'Stopped';
  static const String all = 'All';
  static const String xDayLeft = 'x Days Left';
  static const String createProject = 'Create Project';
  static const String projectName = 'project name';
  static const String pex = 'Please enter x';
  static const String pspxd = 'Please select project x date';
  static const String start = 'start';
  static const String end = 'end';
  static const String estimatedCost = 'estimated cost';
  static const String projectDescription = 'project description';
  static const String projectStatus = 'project status';
  static const String addMember = 'Add Member';
  static const String member = 'Member';
  static const String charge = 'Charge';
  static const String setCharge = 'Set Charge';
  static const String addConferenceResolutions = 'Add Conference Resolutions';
  static const String addProjectPlanningPaper = 'Add Project Planning Paper';
  static const String xCantBeNull = 'x can`t be null';
  static const String startTimeAfterEndTime =
      'Start time is later than end time';
  static const String projectPlanningPaper = 'Project Planning Paper';
  static const String deleteProject = 'Delete project';
  static const String notStarted = 'Not started';
  static const String inProgress = 'In progress';
  static const String completed = 'Completed';
  static const String testing = 'Testing';
  static const String dueDate = 'Due date';
  static const String finalProject = 'Final project';
  static const String viewProjectResults = 'View project results';
  static const String setLegalPerson = 'Set legal person';
  static const String setApplyCompanyUser = 'Set apply company user';
  static const String projectOutcome = 'project outcome';
  static const String applyCompanyUser = 'Apply company user';
  static const String legalPerson = 'Legal person';
  static const String loading = 'Loading...';
  static const String files = 'Files';
  static const String addDemand = 'Add Demand Task';
  static const String taskTitle = 'Task title';
  static const String demandDescription = 'demand description';
  static const String addAttachments = 'Add Attachments';
  static const String demandStatus = 'demand status';
  static const String demandPriority = 'demand priority';
  static const String low = 'low';
  static const String high = 'high';
  static const String common = 'common';
  static const String urgency = 'urgency';
  static const String deleteDemand = 'Delete demand';
  static const String taskDetail = 'Task Detail';
  static const String status = 'Status';
  static const String attachments = 'Attachments';
  static const String name = 'Name';
  static const String priority = 'Priority';
  static const String subTask = 'Sub-Task';
  static const String seeAll = 'See All';
  static const String operationLog = 'Operation log';
  static const String setting = 'Setting';
  static const String workloadType = 'Workload Type';
  static const String language = 'Language';
  static const String daily = 'Daily';
  static const String weekly = 'WeekLy';
  static const String monthly = 'Monthly';
  static const String recentFinishProject = 'Projects due soon';
  static const String hidden = 'Hidden';
  static const String show = 'Show';
  static const String myTask = 'My Task';
  static const String myWorkLog = 'My WorkLog';
  static const String workTime = 'WorkTime';
  static const String xContent = '@x work content';
  static const String content = 'Work content';
  static const String summary = 'Summary';
  static const String workPlan = 'WorkPlan';
  static const String morning = 'Morning';
  static const String afternoon = 'Afternoon';
  static const String notAllowDeleteWorkLog =
      'Do not allow deletion of logs created more than 24 hours ago';
  static const String createWorkLog = 'Create WorkLog';
  static const String reportTime = 'Report Time';
  static const String deleteWorkLog = 'Delete workLog';
  static const String changeWorkLog = 'Change workLog';
  static const String nickName = 'Nick name';
  static const String realName = 'Real name';
  static const String trind = 'This requirement is not described';
}

// TextTranslation By GetX
class Messages extends Translations {
  @override
  Map<String, Map<String, String>> get keys => {
        'zh_CN': {
          MyText.login: '登录',
          MyText.welcomeTitle: '选择登录方式',
          MyText.continueWithQQ: '使用 QQ 登录',
          MyText.continueWithWechat: '使用 微信 登录',
          MyText.or: '或',
          MyText.continueWithPassword: '使用账号密码登录',
          MyText.notAccount: '若您没有账号',
          MyText.hasAccount: '已经拥有账号?',
          MyText.signUp: '注册',
          MyText.signIn: '登录',
          MyText.createAccent: '创建新的\n账号',
          MyText.mail: '邮箱',
          MyText.phoneNumber: '手机号码',
          MyText.loginAccent: '请输入您的\n账号密码',
          MyText.errorTextEP: '请输入正确的信息',
          MyText.password: '密码',
          MyText.rememberMe: '记录账号密码',
          MyText.forgetPassword: '忘记密码',
          MyText.howToVerify: '选择验证方式',
          MyText.viaSMS: '通过手机号码验证',
          MyText.viaEmail: '通过邮箱验证',
          MyText.sendTo: '发送至 ',
          MyText.pleaseEnterYourPhoneNumber: '请输入您的电话号码',
          MyText.pleaseEnterYourEmailAddress: '请输入您的电子邮箱',
          MyText.pleaseEnterYourOldPwd: '请输入您的旧密码',
          MyText.hasSendCodeTo: '已经发送验证码至 ',
          MyText.reSend: '重新发送',
          MyText.resendFailure: '发送失败',
          MyText.submit: '提交',
          MyText.createNewPwd: '创建新的密码',
          MyText.continues: '继续',
          MyText.createNewSuccess: '密码修改成功',
          MyText.home: '主页',
          MyText.project: '项目',
          MyText.manage: '管理',
          MyText.profile: '设置',
          MyText.projects: '项目数',
          MyText.tasks: '任务数',
          MyText.devices: '设备数',
          MyText.workspace: '公司',
          MyText.editProfile: '个人信息',
          MyText.notifications: '通知',
          MyText.notification: '通知',
          MyText.security: '安全',
          MyText.help: '帮助',
          MyText.darkTheme: '暗夜模式',
          MyText.logout: '登出',
          MyText.logoutSure: '您确定要登出吗 ?',
          MyText.logoutYes: '确定',
          MyText.cancel: '取消',
          MyText.changePwd: '修改密码',
          MyText.searchForPeople: '搜索人员',
          MyText.searchForDevice: '搜索设备',
          MyText.loadingFailure: '加载失败',
          MyText.errSomeThing: '发生错误',
          MyText.errSomeThingEx: '请再试一次',
          MyText.sendSuccess: '发送成功',
          MyText.signUpSuccess: '注册成功',
          MyText.signInSuccess: '登录成功',
          MyText.error: '错误',
          MyText.delete: '删除',
          MyText.deleteSuccess: '删除成功',
          MyText.confirm: '确认',
          MyText.warning: '警告',
          MyText.getUserDataFailure: '获取用户信息失败',
          MyText.update: '更新',
          MyText.fire: '解雇',
          MyText.upDateSuccess: '更新成功',
          MyText.pmuibs: '请修改您的信息后再提交',
          MyText.defaultName: '默认昵称',
          MyText.unknownPosition: '未知位置',
          // 公司信息
          MyText.companyName: '公司名称',
          MyText.companyEmail: '公司邮箱',
          MyText.companyPhone: '公司电话',
          MyText.companyNumSize: '公司人数',
          MyText.companyCorporate: '法人代表',
          MyText.createTime: '创建时间',
          MyText.companyType: '行业类型',
          MyText.joinCompany: '加入您的公司',
          MyText.clickToCpy: '点击复制',
          MyText.corjCompanyHint: '加入或创建公司',
          MyText.createCompany: '创建新的公司',
          MyText.pleaseEnterInvitationCode: '请输入您的邀请码',
          MyText.pleaseEnterYourCompanyInfo: '请输入您的公司信息',
          MyText.pleaseEnterYourCompanyName: '请输入您的公司名',
          MyText.pleaseEnterYourEmailOrPhone: '请输入您的登录账号',
          MyText.pwdNotNull: '密码不能为空',
          MyText.createCompanyGroup: '创建新用户组',
          MyText.deleteCompanyGroup: '删除用户组',
          MyText.changeCompanyGroup: '修改用户组',
          MyText.mobileCompanyGroup: '移动分组',
          MyText.mobileEmployeeGroup: '移动员工分组',
          MyText.psagtmt: '请选择要移动到的分组',
          MyText.changeDevice: '修改设备信息',
          MyText.createNewDevice: '添加新设别',
          MyText.changeEmployee: '修改雇员分组',
          MyText.deleteEmployee: '解雇此员工',
          MyText.deleteDevice: '删除此设备',
          MyText.pwdTwoEnterNotSame: '两次输入的新密码不一致',
          MyText.companyGroupName: '用户组名',
          MyText.picFromCamera: '拍摄一张图片',
          MyText.picFromAlbum: '从相册选择一张图片',
          MyText.ppeo: '请进行有效操作',
          MyText.moveAToB: '确定将@A移动到@B吗？',
          MyText.inDemand: '需求中',
          MyText.inDevelopment: '开发中',
          MyText.paid: '已交付',
          MyText.stopped: '已停止',
          MyText.all: '全部',
          MyText.xDayLeft: '还剩 @x 天',
          MyText.createProject: '创建项目',
          MyText.projectName: '项目名称',
          MyText.pex: '请输入@x',
          MyText.pspxd: '请选择项目@x日期',
          MyText.start: '开始',
          MyText.end: '截止',
          MyText.estimatedCost: '预估成本',
          MyText.projectDescription: '项目描述',
          MyText.addMember: '添加成员',
          MyText.member: '成员',
          MyText.charge: '项目负责人',
          MyText.setCharge: '设置项目负责人',
          MyText.addConferenceResolutions: '添加决议文件',
          MyText.addProjectPlanningPaper: '添加项目计划文件',
          MyText.xCantBeNull: '@x不能为空',
          MyText.startTimeAfterEndTime: '开始时间晚于结束时间',
          MyText.projectPlanningPaper: '项目计划文件',
          MyText.deleteProject: '删除项目',
          MyText.notStarted: '未开始',
          MyText.inProgress: '进行中',
          MyText.completed: '已完成',
          MyText.testing: '测试中',
          MyText.dueDate: '预计时间',
          MyText.finalProject: '结题项目',
          MyText.viewProjectResults: '查看项目结题',
          MyText.setLegalPerson: '设置法人',
          MyText.setApplyCompanyUser: '申请人',
          MyText.projectOutcome: '项目成果',
          MyText.applyCompanyUser: '申请人',
          MyText.legalPerson: '法人',
          MyText.loading: '加载中...',
          MyText.files: '文件',
          MyText.addDemand: '添加需求任务',
          MyText.taskTitle: '任务名',
          MyText.demandDescription: '项目描述',
          MyText.addAttachments: '关联文件',
          MyText.demandStatus: '需求状态',
          MyText.demandPriority: '需求优先级',
          MyText.low: '低',
          MyText.common: '一般',
          MyText.high: '高',
          MyText.urgency: '紧急',
          MyText.deleteDemand: '删除需求',
          MyText.taskDetail: '任务详情',
          MyText.status: '状态',
          MyText.attachments: '相关文件',
          MyText.name: '名称',
          MyText.priority: '优先级',
          MyText.subTask: '子任务',
          MyText.seeAll: '查看全部',
          MyText.operationLog: '操作日志',
          MyText.setting: '设置',
          MyText.workloadType: '工作日志类型',
          MyText.language: '语言',
          MyText.daily: '日报',
          MyText.weekly: '周报',
          MyText.monthly: '月报',
          MyText.recentFinishProject: '即将截止的工程',
          MyText.hidden: '隐藏',
          MyText.show: '显示',
          MyText.myTask: '我的任务',
          MyText.myWorkLog: '我的日志',
          MyText.workTime: '工时',
          MyText.xContent: '@x的工作内容',
          MyText.content: '工作内容',
          MyText.summary: '总结',
          MyText.workPlan: '工作计划',
          MyText.morning: '上午',
          MyText.afternoon: '下午',
          MyText.notAllowDeleteWorkLog: '不允许删除创建超过24小时的日志',
          MyText.createWorkLog: '创建工作日志',
          MyText.reportTime: '上传日期',
          MyText.deleteWorkLog: '删除工作日志',
          MyText.changeWorkLog: '修改工作日志',
          MyText.nickName: '昵称',
          MyText.realName: '真实姓名',
          MyText.trind: '点击输入内容'
        },
        'en_US': {
          MyText.login: 'login',
          MyText.welcomeTitle: 'Let`s you in',
          MyText.continueWithQQ: 'Continue with QQ',
          MyText.continueWithWechat: 'Continue with Wechat',
          MyText.or: 'Or',
          MyText.continueWithPassword: 'Sign in with password',
          MyText.notAccount: 'Don’t have an account?',
          MyText.hasAccount: 'Already have an account?',
          MyText.signUp: 'Sign up',
          MyText.signIn: 'Sign in',
          MyText.createAccent: 'Create your\nAccount',
          MyText.mail: 'Email',
          MyText.phoneNumber: 'PhoneNUmber',
          MyText.loginAccent: 'Login to your\nAccount',
          MyText.errorTextEP: "Please enter the correct information",
          MyText.password: 'Password',
          MyText.rememberMe: 'Remember me',
          MyText.forgetPassword: 'Forgot the password?',
          MyText.howToVerify:
              'Select which contact details should we use to reset your password',
          MyText.viaSMS: 'via SMS:',
          MyText.viaEmail: 'via Email:',
          MyText.sendTo: 'Send To ',
          MyText.pleaseEnterYourPhoneNumber: 'Enter your phoneNumber',
          MyText.pleaseEnterYourEmailAddress: 'Enter your email',
          MyText.pleaseEnterYourOldPwd: 'Enter your old Pwd',
          MyText.hasSendCodeTo: 'Verification code has been sent to ',
          MyText.reSend: 'Resend',
          MyText.resendFailure: 'Resend Failure',
          MyText.submit: 'Submit',
          MyText.createNewPwd: 'Create New Password',
          MyText.continues: 'continue',
          MyText.createNewSuccess: 'Your account is ready to use',
          MyText.home: 'Home',
          MyText.project: 'Project',
          MyText.manage: 'Manage',
          MyText.profile: 'Profile',
          MyText.projects: 'Projects',
          MyText.tasks: 'Tasks',
          MyText.devices: 'Devices',
          MyText.workspace: 'Workspace',
          MyText.editProfile: 'Edit Profile',
          MyText.notifications: 'Notifications',
          MyText.notification: 'Notification',
          MyText.security: 'Security',
          MyText.help: 'Help',
          MyText.darkTheme: 'Dark Theme',
          MyText.logout: 'Logout',
          MyText.logoutSure: 'Are you sure you want to log out ?',
          MyText.logoutYes: 'Yes, Logout',
          MyText.cancel: 'Cancel',
          MyText.changePwd: 'changePwd',
          MyText.searchForPeople: 'Search For People',
          MyText.searchForDevice: 'Search For Device',
          MyText.loadingFailure: 'Loading Failure',
          MyText.errSomeThing: 'Something is Wrong',
          MyText.errSomeThingEx: 'Please try again',
          MyText.sendSuccess: 'Sent successfully',
          MyText.signUpSuccess: 'SignUp successfully',
          MyText.signInSuccess: 'SignIn successfully',
          MyText.error: 'Error',
          MyText.delete: 'Delete',
          MyText.deleteSuccess: 'Delete Successfully',
          MyText.confirm: 'Confirm',
          MyText.warning: 'Warning',
          MyText.getUserDataFailure: 'Get UserData Failure',
          MyText.update: 'Update',
          MyText.fire: 'Fire',
          MyText.upDateSuccess: 'Update Successful',
          MyText.pmuibs: 'Please modify your information before submitting',
          MyText.defaultName: 'Default Name',
          MyText.unknownPosition: 'Unknown Position',
          // 公司信息
          MyText.companyName: 'Company Name',
          MyText.companyEmail: 'Company Email',
          MyText.companyPhone: 'Company Phone',
          MyText.companyNumSize: 'Number of employees',
          MyText.companyCorporate: 'Corporate',
          MyText.createTime: 'Creation time',
          MyText.companyType: 'Industry type',
          MyText.joinCompany: 'Join Your Company',
          MyText.clickToCpy: 'Click to copy',
          MyText.corjCompanyHint: 'Create or Join Company',
          MyText.createCompany: 'Create New Company',
          MyText.pleaseEnterInvitationCode: 'Please enter your invitation code',
          MyText.pleaseEnterYourCompanyInfo:
              'Please enter your company information',
          MyText.pleaseEnterYourCompanyName: 'Please enter your company name',
          MyText.pleaseEnterYourEmailOrPhone: 'Enter your email or phoneNumber',
          MyText.pwdNotNull: 'password can not be blank',
          MyText.createCompanyGroup: 'Create company group',
          MyText.deleteCompanyGroup: 'Delete company group',
          MyText.changeCompanyGroup: 'Change company group',
          MyText.mobileCompanyGroup: 'Mobile company group',
          MyText.mobileEmployeeGroup: 'Mobile employee group',
          MyText.psagtmt: 'Please select a group to move to',
          MyText.changeDevice: 'Change device',
          MyText.deleteDevice: 'Delete device',
          MyText.createNewDevice: 'Create new device',
          MyText.changeEmployee: 'Change employee',
          MyText.deleteEmployee: 'Fire an employee',
          MyText.pwdTwoEnterNotSame:
              'The new passwords entered twice are inconsistent',
          MyText.companyGroupName: 'Company group name',
          MyText.picFromCamera: 'Take a picture',
          MyText.picFromAlbum: 'Select picture from album',
          MyText.ppeo: 'Please perform effective operations',
          MyText.moveAToB: 'Are you sure you want to move @A to @B ?',
          MyText.inDemand: 'In demand',
          MyText.inDevelopment: 'In development',
          MyText.paid: 'Paid',
          MyText.stopped: 'Stopped',
          MyText.all: 'All',
          MyText.xDayLeft: '@x Days Left',
          MyText.createProject: 'Create Project',
          MyText.projectName: 'Project name',
          MyText.pex: 'Please enter @x',
          MyText.pspxd: 'Please select project @x date',
          MyText.start: 'Start',
          MyText.end: 'End',
          MyText.estimatedCost: 'Estimated cost',
          MyText.projectDescription: 'Project description',
          MyText.addMember: 'Add Member',
          MyText.member: 'Member',
          MyText.charge: 'Charge',
          MyText.setCharge: 'Set Charge',
          MyText.addConferenceResolutions: 'Add Conference Resolutions',
          MyText.addProjectPlanningPaper: 'Add Project Planning Paper',
          MyText.xCantBeNull: '@x can`t be null value',
          MyText.startTimeAfterEndTime: 'Start time is later than end time',
          MyText.projectPlanningPaper: 'Project Planning Paper',
          MyText.deleteProject: 'Delete project',
          MyText.notStarted: 'Not started',
          MyText.inProgress: 'In progress',
          MyText.completed: 'Completed',
          MyText.testing: 'Testing',
          MyText.dueDate: 'Due date',
          MyText.finalProject: 'Final project',
          MyText.viewProjectResults: 'View project results',
          MyText.setLegalPerson: 'Set legal person',
          MyText.setApplyCompanyUser: 'Set apply company user',
          MyText.applyCompanyUser: 'Apply company user',
          MyText.legalPerson: 'Legal person',
          MyText.projectOutcome: 'Project outcome',
          MyText.loading: 'Loading...',
          MyText.files: 'Files',
          MyText.addDemand: 'Add Demand Task',
          MyText.taskTitle: 'Task title',
          MyText.demandDescription: 'Demand description',
          MyText.demandStatus: 'Demand status',
          MyText.demandPriority: 'Demand priority',
          MyText.addAttachments: 'Add attachments',
          MyText.low: 'Low',
          MyText.common: 'Common',
          MyText.high: 'High',
          MyText.urgency: 'Urgency',
          MyText.deleteDemand: 'Delete demand',
          MyText.taskDetail: 'Task Detail',
          MyText.status: 'Status',
          MyText.attachments: 'Attachments',
          MyText.name: 'Name',
          MyText.priority: 'Priority',
          MyText.subTask: 'Sub-Task',
          MyText.seeAll: 'See All',
          MyText.operationLog: 'Operation log',
          MyText.setting: 'Setting',
          MyText.workloadType: 'Workload Type',
          MyText.language: 'Language',
          MyText.daily: 'Daily',
          MyText.weekly: 'Weekly',
          MyText.monthly: 'Monthly',
          MyText.recentFinishProject: 'Projects due soon',
          MyText.hidden: 'Hidden',
          MyText.show: 'Show',
          MyText.myTask: 'My Task',
          MyText.myWorkLog: 'My WorkLog',
          MyText.workTime: 'WorkTime',
          MyText.xContent: '@x work content',
          MyText.content: 'Work content',
          MyText.summary: 'Summary',
          MyText.workPlan: 'WorkPlan',
          MyText.morning: 'Morning',
          MyText.afternoon: 'Afternoon',
          MyText.notAllowDeleteWorkLog:
              'Do not allow deletion of logs created more than 24 hours ago',
          MyText.createWorkLog: 'Create WorkLog',
          MyText.reportTime: 'Report Time',
          MyText.deleteWorkLog: 'Delete workLog',
          MyText.changeWorkLog: 'Change workLog',
          MyText.nickName: 'Nick name',
          MyText.realName: 'Real name',
          MyText.trind: 'This requirement is not described'
        },
      };
}

class MyDimension {
  MyDimension._();
  static const double heading1 = 48;
  static const double heading2 = 40;
  static const double heading3 = 32;
  static const double heading4 = 24;
  static const double heading5 = 20;
  static const double heading6 = 18;

  static const double xLarge = 18;
  static const double large = 16;
  static const double medium = 14;
  static const double small = 12;
  static const double xSmall = 10;
}

class MyColor {
  MyColor._();
  static const int _primaryValue = 0xff246BFD;
  static const MaterialAccentColor primary = MaterialAccentColor(
    _primaryValue,
    <int, Color>{
      500: Color(0xff246BFD),
      400: Color(0xff5089FD),
      300: Color(0xff7CA6FE),
      200: Color(0xffA7C4FE),
      100: Color(0xffE9F0FF)
    },
  );

  static const int _secondValue = 0xffFFD300;
  static const MaterialAccentColor second = MaterialAccentColor(
    _secondValue,
    <int, Color>{
      500: Color(0xffFFD300),
      400: Color(0xffFFDC33),
      300: Color(0xffFFE566),
      200: Color(0xffFFED99),
      100: Color(0xffFFFBE6)
    },
  );

  static const int _grayValue = 0xff212121;
  static const MaterialAccentColor grayScale = MaterialAccentColor(
    _grayValue,
    <int, Color>{
      900: Color(0xff212121),
      800: Color(0xff424242),
      700: Color(0xff616161),
      600: Color(0xff757575),
      500: Color(0xff9E9E9E),
      400: Color(0xffBDBDBD),
      300: Color(0xffE0E0E0),
      200: Color(0xffEEEEEE),
      100: Color(0xffF5F5F5),
      50: Color(0xffFAFAFA),
    },
  );

  // ############
  // gradient color from 0 to 1
  // ############
  static const int _gradientBlueValue = 0xff246BFD;
  static const MaterialAccentColor gradientBlue = MaterialAccentColor(
    _gradientBlueValue,
    <int, Color>{
      1: Color(0xff246BFD),
      0: Color(0xff6F9EFF),
    },
  );

  static const int _gradientGreenValue = 0xff4ADE80;
  static const MaterialAccentColor gradientGreen = MaterialAccentColor(
    _gradientGreenValue,
    <int, Color>{
      1: Color(0xff4ADE80),
      0: Color(0xff73FFA6),
    },
  );

  static const int _gradientYellowValue = 0xffFACC15;
  static const MaterialAccentColor gradientYellow = MaterialAccentColor(
    _gradientYellowValue,
    <int, Color>{
      1: Color(0xffFACC15),
      0: Color(0xffFFE580),
    },
  );

  static const int _gradientRedValue = 0xffFF4D67;
  static const MaterialAccentColor gradientRed = MaterialAccentColor(
    _gradientRedValue,
    <int, Color>{
      1: Color(0xffFF4D67),
      0: Color(0xffFF8A9B),
    },
  );

  static const Color success = Color(0xff4ADE80);
  static const Color info = Color(0xff246BFD);
  static const Color warning = Color(0xffFACC15);
  static const Color error = Color(0xffF75555);
  static const Color disabled = Color(0xffD8D8D8);
  static const Color disButton = Color(0xff476EBE);

  static const Color dark1 = Color(0xff181A20);
  static const Color dark2 = Color(0xff1F222A);
  static const Color dark3 = Color(0xff35383F);

  static const Color white = Color(0xffffffff);
  static const Color black = Color(0xff000000);
  static const Color red = Color(0xffF54336);
  static const Color pink = Color(0xffEA1E61);
  static const Color purple = Color(0xff9D28AC);
  static const Color deepPurple = Color(0xff673AB3);
  static const Color indigo = Color(0xff3F51B2);
  static const Color blue = Color(0xff1A96F0);
  static const Color lightBlue = Color(0xff00A9F1);
  static const Color cyan = Color(0xff00BCD3);
  static const Color teal = Color(0xff009689);
  static const Color green = Color(0xff4AAF57);
  static const Color lightGreen = Color(0xff8BC255);
  static const Color lime = Color(0xffCDDC4C);
  static const Color yellow = Color(0xffFFEB4F);
  static const Color amber = Color(0xffFFC02D);
  static const Color orange = Color(0xffFF981F);
  static const Color deepOrange = Color(0xffFF5726);
  static const Color brown = Color(0xff7A5548);
  static const Color blueGray = Color(0xff607D8A);
  static const Color transparent = Color(0x00000000);

  // backgroundColor
  static const Color backBlue = Color(0xffF6FAFD);
  static const Color backRed = Color(0xffFFF5F5);
  static const Color backYellow = Color(0xffFFFEE0);
  static const Color backPurple = Color(0xffFCF4FF);
}
