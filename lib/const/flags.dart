class SPFlag {
  SPFlag._();
  // 用于保存登录信息
  static const String accessToken = 'access_token';
  static const String refreshToken = 'refresh_token';
  static const String idToken = 'id_token';
  static const String tokenType = 'token_type';
  static const String expiresIn = 'expires_in';
  static const String accountPhone = 'accountPhone';
  static const String accountEmail = 'accountEmail';
  static const String password = 'password';
  // 个人信息存储
  static const String realName = 'realName';
  static const String nickName = 'nickName';
  // 是否加入公司
  static const String companyId = 'companyId';
}
