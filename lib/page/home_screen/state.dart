
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:management/page/ModuleMain/main_screen/view.dart';
import 'package:management/page/ModuleManage/manage_screen/view.dart';
import 'package:management/page/ModuleProject/project_screen/view.dart';
import 'package:management/page/ModuleSetting/profile_screen/view.dart';

class HomeScreenState {
  var currentPage = 0.obs;
  bool isFillUserData = false;
  String? myCompanyId = '0';

  final List<Widget> widgetOptions = <Widget>[
    const MainScreenPage(),
    const ProjectScreenPage(),
    const ManageScreenPage(),
    const ProfileScreenPage()
  ];

  HomeScreenState() {
    ///Initialize variables
  }
}
