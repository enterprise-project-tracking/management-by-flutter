import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:management/const/consts.dart';
import 'package:management/const/icon.dart';
import 'package:management/generated/assets.dart';

import 'logic.dart';

class HomeScreenPage extends StatelessWidget {
  const HomeScreenPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final logic = Get.put(HomeScreenLogic());
    final state = Get
        .find<HomeScreenLogic>()
        .state;

    return Obx(() {
      return Scaffold(
          floatingActionButton: FloatingActionButton(
            onPressed: () => logic.addWindow(),
            child: const Icon(Icons.add, color: MyColor.white,),
          ),
          floatingActionButtonLocation: FloatingActionButtonLocation
              .miniCenterDocked,
          body: state.widgetOptions[state.currentPage.value],
          bottomNavigationBar: SizedBox(height: 60,
            child: BottomNavigationBar(
              onTap: (index) => logic.changePage(index),
              currentIndex: state.currentPage.value.toInt(),
              type: BottomNavigationBarType.fixed,
              items: [
                BottomNavigationBarItem(
                    icon: const Icon(IconLight.Iconly_Light_Outline_Home),
                    activeIcon: const Icon(IconBold.Iconly_Bold_Home),
                    label: MyText.home.tr),
                BottomNavigationBarItem(
                    icon: const Icon(IconLight.Iconly_Light_Outline_Document),
                    activeIcon: const Icon(IconBold.Iconly_Bold_Document),
                    label: MyText.project.tr),
                BottomNavigationBarItem(
                    icon: const Icon(IconLight.Iconly_Light_Outline_Profile),
                    activeIcon: const Icon(IconBold.Iconly_Bold_Profile),
                    label: MyText.manage.tr),
                BottomNavigationBarItem(
                    icon: const Icon(IconLight.Iconly_Light_Outline_Setting),
                    activeIcon: const Icon(IconBold.Iconly_Bold_Setting),
                    label: MyText.profile.tr)
              ],
            ),
          ));
    });
  }
}
