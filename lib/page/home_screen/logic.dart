import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:management/const/consts.dart';
import 'package:management/const/flags.dart';
import 'package:management/const/init.dart';
import 'package:management/data/network/api/api.dart';
import 'package:management/main.dart';
import 'state.dart';

class HomeScreenLogic extends GetxController {
  final HomeScreenState state = HomeScreenState();

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
    String? refreshToken = GlobalInit.getSpData(SPFlag.refreshToken);
    // 需要手动刷新下accessToken
    MyHttpClient.getRefreshByToken(refreshToken!).then((value) {
      if (value.code == 200) {
        GlobalInit.saveSpData(SPFlag.expiresIn, value.data!.expiresIn!);
        GlobalInit.saveSpData(SPFlag.accessToken, value.data!.accessToken!);
        MyHttpClient.getMyCompanyInfo().then((value) {
          state.myCompanyId = value.data?.id.toString();
          _showCompanySheet();
        });
      }
    });
  }

  // state.myCompanyId = GlobalInit.getSpData(SPFlag.companyId)
  void addWindow() {
    Get.toNamed(RouteConfig.createProjectScreen);
  }

  void changePage(int index) {
    state.currentPage.value = index;
    update();
  }

  void _joinCompany() {
    Get.toNamed(RouteConfig.jCompanyScreen, arguments: {'isJoin': true});
  }

  void _createCompany() {
    Get.toNamed(RouteConfig.jCompanyScreen, arguments: {'isJoin': false});
  }

  void _showCompanySheet() {
    if (state.myCompanyId == '0' || state.myCompanyId!.isEmpty) {
      Get.bottomSheet(
          Wrap(
            children: <Widget>[
              Container(
                padding: const EdgeInsets.symmetric(vertical: 2),
                width: double.infinity,
                height: 60,
                child: TextButton(
                  onPressed: () => _createCompany(),
                  style: Theme.of(Get.context!).textButtonTheme.style,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        MyText.createCompany.tr,
                        style: Theme.of(Get.context!).textTheme.bodyLarge,
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                padding: const EdgeInsets.symmetric(vertical: 2),
                width: double.infinity,
                height: 60,
                child: TextButton(
                  onPressed: () => _joinCompany(),
                  style: Theme.of(Get.context!).textButtonTheme.style,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        MyText.joinCompany.tr,
                        style: Theme.of(Get.context!).textTheme.bodyLarge,
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                padding:
                    const EdgeInsets.symmetric(horizontal: 24, vertical: 5),
                width: double.infinity,
                height: 60,
                child: TextButton(
                  onPressed: () => Get.back(),
                  style: Theme.of(Get.context!).textButtonTheme.style,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        MyText.cancel.tr,
                        style: Theme.of(Get.context!)
                            .textTheme
                            .bodyLarge
                            ?.copyWith(color: MyColor.error),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
          isDismissible: false,
          backgroundColor: Theme.of(Get.context!).scaffoldBackgroundColor);
    }
  }
}
