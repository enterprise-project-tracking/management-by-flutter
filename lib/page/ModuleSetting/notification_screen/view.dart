import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:management/const/consts.dart';

import 'logic.dart';

class NotificationScreenPage extends StatelessWidget {
  const NotificationScreenPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final logic = Get.put(NotificationScreenLogic());
    final state = Get.find<NotificationScreenLogic>().state;

    return Scaffold(
      appBar: AppBar(
        title: Text(MyText.setting.tr),
      ),
      body: Container(
        padding: const EdgeInsets.symmetric(vertical: 24),
        child: Flexible(
            child: ListView.builder(
          itemBuilder: (context, index) {
            return Obx(() {
              return Visibility(
                visible:
                    state.roleId.value == CompanyMemberPermissions.president,
                replacement: Center(
                  child: Text(
                    "Nothing you can set",
                    style: Theme.of(context)
                        .textTheme
                        .titleSmall
                        ?.copyWith(color: MyColor.red),
                  ),
                ),
                child: ListTile(
                  title: Text(
                    state.listSetting[index],
                    style: Theme.of(context).textTheme.bodyLarge?.copyWith(
                          fontSize: 18,
                        ),
                  ),
                  trailing: Text(
                      state.listWorkLoadTypeMap[state.workLoadType.value] ??
                          MyText.loading),
                  onTap: () => logic.changeWorkLoadType(),
                ),
              );
            });
          },
          itemCount: state.listSetting.length,
        )),
      ),
    );
  }
}
