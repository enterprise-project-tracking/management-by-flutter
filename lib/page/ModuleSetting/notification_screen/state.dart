import 'package:get/get.dart';
import 'package:management/const/consts.dart';

class NotificationScreenState {
  var listSetting = [
    MyText.workloadType.tr,
    // MyText.language.tr
  ];

  var roleId = 2.obs;
  var workLoadType = 0.obs;

  var listWorkLoadTypeMap = {
    0: MyText.loading.tr,
    1: MyText.daily.tr,
    2: MyText.weekly.tr,
    3: MyText.monthly.tr
  };

  NotificationScreenState() {
    ///Initialize variables
  }
}
