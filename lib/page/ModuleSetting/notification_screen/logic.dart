import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:management/const/consts.dart';
import 'package:management/data/network/api/api.dart';
import 'package:management/element/loading.dart';
import 'package:management/element/toast.dart';

import 'state.dart';

class NotificationScreenLogic extends GetxController {
  final NotificationScreenState state = NotificationScreenState();

  @override
  void onInit() {
    super.onInit();
    _initData(false);
  }

  Future<void> refreshSettingPage() async {
    LoadingElement.loadingDialog(true);
    _initData(true);
  }

  void _initData(bool isRefresh) async {
    await MyHttpClient.getMyCompanyInfo().then((value) {
      if (value.code == 200) {
        state.roleId.value = value.data?.roleId?.toInt() ?? 2;
      } else {
        Get.back();
        Utils.toastError(value.msg);
        return;
      }
    }).onError((error, p) {
      Get.back();
      Utils.toastError(error);
      return;
    });
    await MyHttpClient.getWorkLoadType().then((value) {
      if (value.code == 200) {
        state.workLoadType.value = value.data?.workloadType?.toInt() ?? 1;
        refresh();
        if (isRefresh) Get.back();
      } else {
        Get.back();
        Utils.toastError(value.msg);
        return;
      }
    }).onError((error, p) {
      Get.back();
      Utils.toastError(error);
      return;
    });
  }

  void changeWorkLoadType() {
    Get.bottomSheet(
        Wrap(
            children: List<Widget>.generate(4, (index) {
          return Container(
            padding: const EdgeInsets.symmetric(vertical: 2),
            width: double.infinity,
            height: 60,
            child: TextButton(
              onPressed: () {
                if (index == 3) {
                  Get.back();
                  return;
                }
                _realChangeWorkLoadType(index + 1);
                Get.back();
              },
              style: Theme.of(Get.context!).textButtonTheme.style,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    index == 3
                        ? MyText.cancel
                        : state.listWorkLoadTypeMap[index + 1]!,
                    style: Theme.of(Get.context!).textTheme.bodyLarge?.copyWith(
                        color: index == 3
                            ? MyColor.error
                            : MyColor.grayScale[500]),
                  ),
                ],
              ),
            ),
          );
        })),
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(top: Radius.circular(20))),
        backgroundColor: Theme.of(Get.context!).scaffoldBackgroundColor);
  }

  void _realChangeWorkLoadType(int index) {
    MyHttpClient.changeWordLoadType(index).then((value) {
      if (value.code == 200) {
        refreshSettingPage();
      } else {
        Utils.toastError(value.msg);
      }
    }).onError((error, p) {
      Utils.toastError(error);
    });
  }

  String getText() {
    return state.listWorkLoadTypeMap[state.workLoadType.value] ?? '';
  }
}
