import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:management/const/consts.dart';
import 'package:management/const/icon.dart';
import 'package:management/element/InputField.dart';

import 'logic.dart';

class EditProfileScreenPage extends StatelessWidget {
  const EditProfileScreenPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final logic = Get.put(EditProfileScreenLogic());
    final state = Get.find<EditProfileScreenLogic>().state;

    return Scaffold(
      appBar: AppBar(title: Text(MyText.editProfile.tr)),
      body: Obx(() {
        return Container(
          padding: const EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            children: [
              const SizedBox(
                height: 24,
              ),
              MyTextField(
                isDark: Get.isDarkMode,
                prefixIcon: IconBold.Iconly_Bold_2_User,
                hintText: state.nickNameHint.value,
                labelText: MyText.nickName.tr,
                onChanged: (value) {
                  state.nickName = value;
                },
              ),
              MyTextField(
                isDark: Get.isDarkMode,
                prefixIcon: IconLight.Iconly_Light_Outline_2_User,
                hintText: state.realNameHint.value,
                labelText: MyText.realName.tr,
                onChanged: (value) {
                  state.realName = value;
                },
              ),
              MyTextField(
                prefixIcon: IconLight.Iconly_Light_Outline_Call,
                isDark: Get.isDarkMode,
                hintText: state.phoneNumber.value,
                labelText: MyText.phoneNumber.tr,
              ),
              MyTextField(
                prefixIcon: IconLight.Iconly_Light_Outline_Message,
                isDark: Get.isDarkMode,
                hintText: state.email.value,
                labelText: MyText.mail.tr,
              ),
              SizedBox(
                width: double.infinity,
                height: 55,
                child: ElevatedButton(
                    style: Theme.of(Get.context!)
                        .elevatedButtonTheme
                        .style
                        ?.copyWith(
                            elevation: WidgetStateProperty.all(0),
                            backgroundColor: Get.isDarkMode
                                ? WidgetStateProperty.all(MyColor.dark3)
                                : WidgetStateProperty.all(
                                    MyColor.primary[200])),
                    onPressed: () => logic.updateInformation(),
                    child: Text(MyText.update.tr)),
              ),
            ],
          ),
        );
      }),
    );
  }
}
