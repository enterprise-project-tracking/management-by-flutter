import 'package:get/get.dart';
import 'package:management/const/consts.dart';
import 'package:management/const/flags.dart';
import 'package:management/const/init.dart';
import 'package:management/data/network/api/api.dart';
import 'package:management/element/toast.dart';
import 'package:management/main.dart';
import 'package:management/page/ModuleSetting/profile_screen/logic.dart';

import 'state.dart';

class EditProfileScreenLogic extends GetxController {
  final EditProfileScreenState state = EditProfileScreenState();

  @override
  void onInit() {
    super.onInit();
    _initData();
  }

  void _initData() {
    MyHttpClient.getUserData().then((value) {
      if (value.code == 200) {
        state.realNameHint.value = value.data!.realName!;
        state.nickNameHint.value = value.data!.nickName!;
        state.email.value = value.data!.email!;
        state.phoneNumber.value = value.data!.phone;
      } else {
        Utils.toastError(MyText.getUserDataFailure.tr);
      }
    });
  }

  void updateInformation() {
    if (state.realName.isEmpty || state.nickName.isEmpty) {
      Utils.toastError(MyText.pmuibs.tr);
      return;
    }
    MyHttpClient.updateUserData(
            {"nickName": state.nickName, "realName": state.realName})
        .then((value) async {
      if (value.code == 200) {
        GlobalInit.saveSpData(SPFlag.nickName, state.nickName);
        GlobalInit.saveSpData(SPFlag.realName, state.realName);
        Utils.toastNotification(MyText.upDateSuccess.tr);

        // MyHttpClient.getMyCompanyInfo().then((value){
        //   // 没有加入公司
        //   if(value.data?.id == 0)
        //     {
        //       // Get.
        //     }
        // });

        // 跳转并刷新详情页
        Get.offNamed(RouteConfig.homeScreen);
        Get.find<ProfileScreenLogic>().refreshProfileScreen();
      } else {
        Utils.toastError(MyText.errSomeThing.tr);
      }
    });
  }
}
