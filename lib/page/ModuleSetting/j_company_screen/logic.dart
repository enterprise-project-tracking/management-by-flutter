import 'package:get/get.dart';
import 'package:management/const/consts.dart';
import 'package:management/const/flags.dart';
import 'package:management/const/init.dart';
import 'package:management/data/network/api/api.dart';
import 'package:management/element/toast.dart';
import 'package:management/main.dart';

import 'state.dart';

class JCompanyScreenLogic extends GetxController {
  final JCompanyScreenState state = JCompanyScreenState();

  @override
  void onInit() {
    super.onInit();
    _initData();
  }

  void chooseCompanyType(int index) {
    state.companyType.value = state.listCompanyType.value[index].name!;
    if (index <= 9) {
      state.companyTypeId = "0$index";
      return;
    }
    state.companyTypeId = index.toString();
  }

  void _initData() {
    MyHttpClient.getCompanyType().then((value) {
      if (value.code == 200) {
        state.listCompanyType.value.addAll(value.data!);
        state.listCompanyType.refresh();
        state.companyTypeLength.value = state.listCompanyType.length;
      } else {
        Utils.toastError(MyText.errSomeThing.tr);
      }
    });
  }

  void nameOrCode(String value) {
    if (state.isJoin) {
      state.joinCode = value;
    } else {
      state.companyName = value;
    }
  }

  void chooseNum(int index) {
    state.employeesNum.value = state.listEmployees[index];
    state.employeeId = index.toString();
  }

  void saveJoin() {
    MyHttpClient.joinCompany(state.joinCode).then((value) {
      if (value.code == 200) {
        Utils.toastNotification(value.msg);
        String? refreshToken = GlobalInit.getSpData(SPFlag.refreshToken);
        // 加入公司后需要手动刷新下accessToken
        MyHttpClient.getRefreshByToken(refreshToken!).then((value) {
          if (value.code == 200) {
            GlobalInit.saveSpData(SPFlag.expiresIn, value.data!.expiresIn!);
            GlobalInit.saveSpData(SPFlag.accessToken, value.data!.accessToken!);
            _saveCompanyId();
          }
        }).onError((error, stackTrace) {
          Utils.toastError(error);
        });
      } else {
        Utils.toastError(value.msg);
      }
    });
  }

  void _saveCompanyId() {
    MyHttpClient.getMyCompanyInfo().then((value) {
      if (value.code == 200) {
        GlobalInit.saveSpData(SPFlag.companyId, value.data!.id.toString());
        if (GlobalInit.getCompanyId() != "0") {
          Get.offNamed(RouteConfig.homeScreen);
        } else {
          Utils.toastError(value.msg);
        }
      } else {
        Utils.toastError(value.msg);
      }
    });
  }

  void createCompany() {
    MyHttpClient.createCompany({
      "name": state.companyName,
      "employeeSize": state.employeeId,
      "industryId": state.companyTypeId
    }).then((value) {
      if (value.code == 200) {
        Utils.toastNotification(value.msg);
        String? refreshToken = GlobalInit.getSpData(SPFlag.refreshToken);
        // 加入公司后需要手动刷新下accessToken
        MyHttpClient.getRefreshByToken(refreshToken!).then((value) {
          if (value.code == 200) {
            GlobalInit.saveSpData(SPFlag.expiresIn, value.data!.expiresIn!);
            GlobalInit.saveSpData(SPFlag.accessToken, value.data!.accessToken!);
            _saveCompanyId();
          }
        }).onError((error, stackTrace) {
          Utils.toastError(error);
        });
      } else {
        Utils.toastError(value.msg);
      }
    });
  }
}
