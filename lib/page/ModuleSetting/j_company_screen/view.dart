import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:management/const/consts.dart';
import 'package:management/const/icon.dart';
import 'package:management/element/InputField.dart';

import 'logic.dart';

class JCompanyScreenPage extends StatelessWidget {
  const JCompanyScreenPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final logic = Get.put(JCompanyScreenLogic());
    final state = Get.find<JCompanyScreenLogic>().state;

    var controllerEmployee = ExpansionTileController();
    var controllerType = ExpansionTileController();

    return Scaffold(
      appBar: AppBar(
        title: Text(state.isJoin ? MyText.joinCompany.tr : MyText.createCompany.tr),
        actions: [
          state.isJoin
              ? IconButton(
                  onPressed: () => logic.saveJoin(),
                  icon: const Icon(Icons.save_outlined, size: 24))
              : IconButton(
                  onPressed: () => logic.createCompany(),
                  icon: const Icon(Icons.save_outlined, size: 24)),
        ],
      ),
      body: Column(
        children: [
          // Text(state.isJoin
          //     ? MyText.pleaseEnterInvitationCode
          //     : MyText.pleaseEnterYourCompanyInfo),
          const SizedBox(
            height: 30,
          ),
          MyTextField(
            isDark: Get.isDarkMode,
            hintText: state.isJoin
                ? MyText.pleaseEnterInvitationCode.tr
                : MyText.pleaseEnterYourCompanyName.tr,
            prefixIcon: state.isJoin ? Icons.code : Icons.account_balance,
            onChanged: (value) => logic.nameOrCode(value),
          ),
          !state.isJoin
              ? ExpansionTile(
                  leading: const Icon(IconBold.Iconly_Bold_Work, size: 24),
                  title: Text(
                    MyText.companyType.tr,
                    style: const TextStyle(
                        fontSize: MyDimension.medium,
                        height: 1.4,
                        letterSpacing: 0.2,
                        wordSpacing: 0.2,
                        fontWeight: FontWeight.w600),
                  ),
                  trailing: Obx(() {
                    return Text(
                      state.companyType.value,
                      style: const TextStyle(
                          fontSize: MyDimension.medium,
                          height: 1.4,
                          letterSpacing: 0.2,
                          wordSpacing: 0.2,
                          fontWeight: FontWeight.w400),
                    );
                  }),
                  controller: controllerType,
                  children: [
                    SizedBox(
                      height: 150,
                      child: ListView.builder(
                          itemCount: state.companyTypeLength.value,
                          itemBuilder: (context, index) {
                            return SizedBox(
                              child: ListTile(
                                  title: Center(
                                    child: Text(
                                      state.listCompanyType.value[index].name!,
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyMedium,
                                    ),
                                  ),
                                  onTap: () => {
                                        controllerType.collapse(),
                                        logic.chooseCompanyType(index)
                                      }),
                            );
                          }),
                    ),
                  ],
                )
              : Container(),
          !state.isJoin
              ? ExpansionTile(
                  leading: const Icon(IconBold.Iconly_Bold_3_User, size: 24),
                  title: Text(
                    MyText.companyNumSize.tr,
                    style: const TextStyle(
                        fontSize: MyDimension.medium,
                        height: 1.4,
                        letterSpacing: 0.2,
                        wordSpacing: 0.2,
                        fontWeight: FontWeight.w600),
                  ),
                  trailing: Obx(() {
                    return Text(
                      state.employeesNum.value,
                      style: const TextStyle(
                          fontSize: MyDimension.medium,
                          height: 1.4,
                          letterSpacing: 0.2,
                          wordSpacing: 0.2,
                          fontWeight: FontWeight.w400),
                    );
                  }),
                  controller: controllerEmployee,
                  children: [
                    SizedBox(
                      height: 90,
                      child: ListView.builder(
                          itemCount: state.listEmployees.length,
                          itemBuilder: (context, index) {
                            return ListTile(
                                title: Center(
                                  child: Text(
                                    state.listEmployees[index],
                                    style:
                                        Theme.of(context).textTheme.bodyMedium,
                                  ),
                                ),
                                onTap: () => {
                                      controllerEmployee.collapse(),
                                      logic.chooseNum(index)
                                    });
                          }),
                    ),
                  ],
                )
              : Container()
        ],
      ),
    );
  }
}
