import 'package:get/get.dart';
import 'package:management/data/network/entity/company/CompanyTypeInfo.dart';

class JCompanyScreenState {

  bool isJoin = false;
  String joinCode = '';
  String companyName = '';

  List<String> listEmployees = [
    '1-9',
    '10-20',
    '21-50',
    '51-100',
    '101-200',
    '201-500',
    '501-2000',
    '2000-10000'
  ];

  var listCompanyType = <Data>[].obs;
  var companyTypeLength = 0.obs;
  var employeesNum = ''.obs;
  var companyType= ''.obs;
  var companyTypeId = '';
  var employeeId = '';

  JCompanyScreenState() {
    isJoin = Get.arguments['isJoin'];
  }
}
