import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:management/const/consts.dart';
import 'package:management/const/icon.dart';
import 'package:management/generated/assets.dart';

import 'logic.dart';

class ProfileScreenPage extends StatelessWidget {
  const ProfileScreenPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final logic = Get.put(ProfileScreenLogic());
    final state = Get.find<ProfileScreenLogic>().state;

    return Scaffold(
      appBar: AppBar(
        leading: Image.asset(
          Assets.imagesLogo,
          width: 32,
          height: 32,
          fit: BoxFit.scaleDown,
        ),
        title: Text(MyText.profile.tr),
        actions: [
          IconButton(
            onPressed: () {},
            icon: const Icon(IconLight.Iconly_Light_Outline_More_Circle,
                size: 28),
          )
        ],
      ),
      body: Obx(() {
        return Container(
          padding: const EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const SizedBox(
                    height: 24,
                  ),
                  Stack(
                    alignment: AlignmentDirectional.bottomEnd,
                    children: [
                      ClipOval(
                        child: FadeInImage(
                          image: NetworkImage(state.headImage.value),
                          height: 120,
                          width: 120,
                          fit: BoxFit.fill,
                          placeholderFit: BoxFit.scaleDown,
                          placeholder:
                              const AssetImage(Assets.imagesDefaultAvatar),
                        ),
                      ),
                      GestureDetector(
                        child: const ImageIcon(
                          AssetImage(Assets.exclude),
                          size: 25,
                          color: MyColor.primary,
                        ),
                        onTap: () => logic.changeAvatar(),
                      )
                    ],
                  ),
                  const SizedBox(
                    height: 12,
                  ),
                  Text(
                    state.name.value,
                    style: Theme.of(context).textTheme.titleLarge,
                  ),
                  const SizedBox(
                    height: 6,
                  ),
                  Text(
                    state.company.value,
                    style: Theme.of(context).textTheme.bodyMedium,
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  SizedBox(
                    height: 50,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              state.projectNum.value.toString(),
                              style: Theme.of(context).textTheme.titleLarge,
                            ),
                            Text(
                              MyText.projects.tr,
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyMedium
                                  ?.copyWith(
                                      fontWeight: FontWeight.w400,
                                      color: Get.isDarkMode
                                          ? MyColor.white
                                          : MyColor.grayScale[800]),
                            ),
                          ],
                        ),
                        VerticalDivider(
                          thickness: 1,
                          width: 10,
                          indent: 1,
                          endIndent: 1,
                          color: MyColor.grayScale[200],
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              state.taskNum.value.toString(),
                              style: Theme.of(context).textTheme.titleLarge,
                            ),
                            Text(
                              MyText.tasks.tr,
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyMedium
                                  ?.copyWith(
                                      fontWeight: FontWeight.w400,
                                      color: Get.isDarkMode
                                          ? MyColor.white
                                          : MyColor.grayScale[800]),
                            ),
                          ],
                        ),
                        VerticalDivider(
                          thickness: 1,
                          width: 10,
                          indent: 1,
                          endIndent: 1,
                          color: MyColor.grayScale[200],
                        ),
                        Column(
                          children: [
                            Text(
                              state.deviceNum.value.toString(),
                              style: Theme.of(context).textTheme.titleLarge,
                            ),
                            Text(
                              MyText.devices.tr,
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyMedium
                                  ?.copyWith(
                                      fontWeight: FontWeight.w400,
                                      color: Get.isDarkMode
                                          ? MyColor.white
                                          : MyColor.grayScale[800]),
                            ),
                          ],
                        )
                      ],
                    ),
                  )
                ],
              ),
              const SizedBox(
                height: 24,
              ),
              Divider(
                thickness: 1,
                color: MyColor.grayScale[200],
              ),
              const SizedBox(
                height: 12,
              ),
              Flexible(
                child: ListView.builder(
                    itemCount: 7,
                    itemBuilder: (context, index) {
                      return SizedBox(
                        // height: 30,
                        child: ListTile(
                          onTap: () => logic.click(index),
                          leading: Icon(
                            state.listTileIcon[index],
                            size: 28,
                            color: index == 6
                                ? MyColor.error
                                : Get.isDarkMode
                                    ? MyColor.white
                                    : MyColor.grayScale[800],
                          ),
                          title: Text(
                            state.listTileTitle[index],
                            style:
                                Theme.of(context).textTheme.bodyLarge?.copyWith(
                                    fontSize: 18,
                                    color: index == 6
                                        ? MyColor.error
                                        : Get.isDarkMode
                                            ? MyColor.white
                                            : MyColor.grayScale[800]),
                          ),
                          trailing: index == 5
                              ? Switch(
                                  inactiveThumbColor: MyColor.white,
                                  inactiveTrackColor: MyColor.grayScale[200],
                                  activeColor: MyColor.white,
                                  activeTrackColor: MyColor.primary,
                                  trackOutlineWidth:
                                      MaterialStateProperty.all(14),
                                  trackOutlineColor:
                                      MaterialStateProperty.resolveWith(
                                          (states) {
                                    if (states
                                        .contains(MaterialState.selected)) {
                                      return MyColor.primary;
                                    }
                                    return MyColor.grayScale[200];
                                  }),
                                  value: Get.isDarkMode,
                                  onChanged: (state) =>
                                      logic.changeTheme(state))
                              : null,
                        ),
                      );
                    }),
              )
            ],
          ),
        );
      }),
    );
  }
}
