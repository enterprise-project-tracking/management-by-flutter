import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:management/const/consts.dart';
import 'package:management/const/icon.dart';

class ProfileScreenState {
  var headImage =
      'https://p26-passport.byteacctimg.com/img/user-avatar/d9b8526c6e8aa695935c5e68106317b1~50x50.awebp'.obs;

  var name = 'Daniel Austin'.obs;
  var company = 'Google'.obs;
  var projectNum = 0.obs;
  var taskNum = 0.obs;
  var deviceNum = 0.obs;

  final List<IconData> listTileIcon = [
    IconLight.Iconly_Light_Outline_Work,
    IconLight.Iconly_Light_Outline_Profile,
    IconLight.Iconly_Light_Outline_Setting,
    IconLight.Iconly_Light_Outline_Shield_Done,
    IconLight.Iconly_Light_Outline_Info_Square,
    IconLight.Iconly_Light_Outline_Show,
    IconLight.Iconly_Light_Outline_Logout
  ];

  final List<String> listTileTitle = [
    MyText.workspace.tr,
    MyText.editProfile.tr,
    MyText.setting.tr,
    MyText.security.tr,
    MyText.help.tr,
    MyText.darkTheme.tr,
    MyText.logout.tr
  ];

  ProfileScreenState() {
    ///Initialize variables
  }
}
