import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:management/data/network/entity/company/CompanyTypeInfo.dart';

class WorkSpaceScreenState {
  // 如果权限级别不等于2, 就可以修改公司信息
  var canEdit = 2.obs;
  var changeEditState = 0.obs;

  // 显示信息
  var listTileTitle = [const MapEntry("", "")].obs;
  // 可编辑信息
  var listCanEditTitle = [const MapEntry("", "")].obs;
  // 更新时所用的tag
  var listUpdateDataTag = [
    "name",
    "email",
    "telephone",
    "legalRepresentative",
    "employeeSize",
    "industryId",
  ];

  var employeesNum = "".obs;
  var companyType= ''.obs;
  var listCompanyType = <Data>[].obs;

  Map<String, String> listUpdateData = {"init": ""};
  // 0-(1-9)，1-(10-20),2-(21-50),3-(51-100),
  // 4-(101-200),5-(201-500),6-(501-2000),7-(>2000)
  List<String> listEmployees = [
    '1-9',
    '10-20',
    '21-50',
    '51-100',
    '101-200',
    '201-500',
    '501-2000',
    '2000-10000'
  ];

  // List<MapEntry<String, String>>
  WorkSpaceScreenState() {}
}
