import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:management/const/consts.dart';
import 'package:management/const/icon.dart';
import 'package:management/element/InputField.dart';

import 'logic.dart';

class WorkSpaceScreenPage extends StatelessWidget {
  const WorkSpaceScreenPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final logic = Get.put(WorkSpaceScreenLogic());
    final state = Get.find<WorkSpaceScreenLogic>().state;

    var controllerEmployee = ExpansionTileController();
    var controllerType = ExpansionTileController();
    // 修改信息
    Widget createList(BuildContext context, int index) {
      if (state.listCanEditTitle[index].key == MyText.companyNumSize.tr) {
        return Container(
          padding: const EdgeInsets.symmetric(horizontal: 24),
          child: ExpansionTile(
            leading: const Icon(IconBold.Iconly_Bold_3_User, size: 24),
            title: Text(
              MyText.companyNumSize.tr,
              style: const TextStyle(
                  fontSize: MyDimension.medium,
                  height: 1.4,
                  letterSpacing: 0.2,
                  wordSpacing: 0.2,
                  fontWeight: FontWeight.w600),
            ),
            trailing: Obx(() {
              return Text(
                state.employeesNum.value,
                style: const TextStyle(
                    fontSize: MyDimension.medium,
                    height: 1.4,
                    letterSpacing: 0.2,
                    wordSpacing: 0.2,
                    fontWeight: FontWeight.w400),
              );
            }),
            controller: controllerEmployee,
            children: [
              SizedBox(
                height: 90,
                child: ListView.builder(
                    itemCount: state.listEmployees.length,
                    itemBuilder: (context, index) {
                      return ListTile(
                          title: Center(
                            child: Text(
                              state.listEmployees[index],
                              style: Theme.of(context).textTheme.bodyMedium,
                            ),
                          ),
                          onTap: () => {
                                controllerEmployee.collapse(),
                                logic.chooseNum(index)
                              });
                    }),
              ),
            ],
          ),
        );
      } else if (state.listCanEditTitle[index].key == MyText.companyType.tr) {
        return Container(
          padding: const EdgeInsets.symmetric(horizontal: 24),
          child: ExpansionTile(
            leading: const Icon(IconBold.Iconly_Bold_Work, size: 24),
            title: Text(
              MyText.companyType.tr,
              style: const TextStyle(
                  fontSize: MyDimension.medium,
                  height: 1.4,
                  letterSpacing: 0.2,
                  wordSpacing: 0.2,
                  fontWeight: FontWeight.w600),
            ),
            trailing: Obx(() {
              return Text(
                state.companyType.value,
                style: const TextStyle(
                    fontSize: MyDimension.medium,
                    height: 1.4,
                    letterSpacing: 0.2,
                    wordSpacing: 0.2,
                    fontWeight: FontWeight.w400),
              );
            }),
            controller: controllerType,
            children: [
              SizedBox(
                height: 150,
                child: ListView.builder(
                    itemCount: state.listCompanyType.length,
                    itemBuilder: (context, index) {
                      return SizedBox(
                        child: ListTile(
                            title: Center(
                              child: Text(
                                state.listCompanyType.value[index].name!,
                                style: Theme.of(context).textTheme.bodyMedium,
                              ),
                            ),
                            onTap: () => {
                                  controllerType.collapse(),
                                  logic.chooseCompanyType(index)
                                }),
                      );
                    }),
              ),
            ],
          ),
        );
      } else {
        return Container(
            padding: const EdgeInsets.symmetric(horizontal: 24),
            child: MyTextField(
              isDark: Get.isDarkMode,
              hintText:
                  '${state.listCanEditTitle[index].key} : ${state.listCanEditTitle[index].value}',
              onChanged: (value) => logic.saveChange(value, index),
            ));
      }
    }

    // 信息展示
    Widget createList2(BuildContext context, int index) {
      // 雇员人数需要进行本地映射
      if (index == 4) {
        return SizedBox(
          child: ListTile(
            title: Text(
              state.listTileTitle[index].key,
              style: Theme.of(context).textTheme.bodyLarge?.copyWith(
                    fontSize: 18,
                  ),
            ),
            trailing: Text(
              state.listEmployees[int.parse(state.listTileTitle[index].value)],
              style: Theme.of(context)
                  .textTheme
                  .bodyMedium
                  ?.copyWith(color: MyColor.blue),
            ),
          ),
        );
      } else {
        return SizedBox(
          child: ListTile(
            title: Text(
              state.listTileTitle[index].key,
              style: Theme.of(context).textTheme.bodyLarge?.copyWith(
                    fontSize: 18,
                  ),
            ),
            trailing: Text(
              state.listTileTitle[index].value,
              style: Theme.of(context)
                  .textTheme
                  .bodyMedium
                  ?.copyWith(color: MyColor.blue),
            ),
          ),
        );
      }
    }

    return Obx(() {
      return Scaffold(
          appBar: AppBar(
            title: Text(MyText.workspace.tr),
            actions: state.canEdit.value != CompanyMemberPermissions.member
                ? [
                    IconButton(
                        onPressed: () => logic.shareCompany(),
                        icon: const Icon(Icons.share_outlined, size: 24)),
                    IconButton(
                      onPressed: () {
                        state.changeEditState.value++;
                        if (state.changeEditState.value >= 3) {
                          logic.updateCompany();
                        }
                      },
                      icon: state.changeEditState.value != 0
                          ? const Icon(Icons.save_outlined)
                          : const Icon(IconLight.Iconly_Light_Outline_Edit,
                              size: 24),
                    )
                  ]
                : [
                    IconButton(
                        onPressed: () => logic.shareCompany(),
                        icon: const Icon(Icons.share_outlined, size: 24))
                  ],
          ),
          body: Container(
            padding: const EdgeInsets.only(
              top: 2,
            ),
            child: state.changeEditState.value != 0
                ? Padding(
                    padding: const EdgeInsets.only(top: 10),
                    child: ListView.builder(
                        itemCount: state.listCanEditTitle.length,
                        itemBuilder: (context, index) =>
                            createList(context, index)),
                  )
                : ListView.builder(
                    itemCount: state.listTileTitle.length,
                    itemBuilder: (context, index) =>
                        createList2(context, index)),
          ));
    });
  }
}
