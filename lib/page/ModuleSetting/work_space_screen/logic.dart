import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:management/const/consts.dart';
import 'package:management/data/network/api/api.dart';
import 'package:management/element/loading.dart';
import 'package:management/element/toast.dart';
import 'package:management/main.dart';
import 'package:management/page/ModuleSetting/profile_screen/logic.dart';

import 'state.dart';

class WorkSpaceScreenLogic extends GetxController {
  final WorkSpaceScreenState state = WorkSpaceScreenState();

  @override
  void onInit() {
    super.onInit();
    _initData();
  }

  @override
  void onReady() {
    super.onReady();
    LoadingElement.loadingDialog(() async {
      _initData();
      return true;
    });
    Get.back();
  }

  void _initData() {
    MyHttpClient.getMyCompanyInfo().then((value) {
      if (value.code == 200 && value.data?.id != 0) {
        state.canEdit.value = value.data!.roleId!.toInt();
        state.listTileTitle.clear();
        state.listCanEditTitle.clear();
        state.listTileTitle.addAll([
          MapEntry(MyText.companyName, value.data!.name ?? ''),
          MapEntry(MyText.companyEmail, value.data!.email ?? ''),
          MapEntry(MyText.companyPhone, value.data!.telephone ?? ''),
          MapEntry(
              MyText.companyCorporate, value.data!.legalRepresentative ?? ''),
          MapEntry(MyText.companyNumSize, value.data!.employeeSize!.toString()),
          MapEntry(MyText.createTime, value.data!.createTime!),
          MapEntry(MyText.companyType, value.data!.industryName!),
        ]);

        state.listCanEditTitle.addAll([
          MapEntry(MyText.companyName, value.data!.name ?? ''),
          MapEntry(MyText.companyEmail, value.data!.email ?? ''),
          MapEntry(MyText.companyPhone, value.data!.telephone ?? ''),
          MapEntry(
              MyText.companyCorporate, value.data!.legalRepresentative ?? ''),
          MapEntry(MyText.companyNumSize, value.data!.employeeSize!.toString()),
          MapEntry(MyText.companyType, value.data!.industryName!),
        ]);

        state.employeesNum.value =
            state.listEmployees[value.data!.employeeSize!.toInt()];

        state.companyType.value = value.data!.industryName!;
      } else {
        Utils.toastError(MyText.errSomeThing);
      }
    });

    MyHttpClient.getCompanyType().then((value) {
      if (value.code == 200) {
        state.listCompanyType.value = value.data!;
      } else {
        Utils.toastError(MyText.errSomeThing);
      }
    });
  }

  void saveChange(String value, int index) {
    state.listUpdateData[state.listUpdateDataTag[index]] = value;
  }

  void chooseNum(int index) {
    state.employeesNum.value = state.listEmployees[index];
    state.listUpdateData["employeeSize"] = index.toString();
  }

  void chooseCompanyType(int index) {
    state.companyType.value = state.listCompanyType[index].name!;
    state.listUpdateData["industryId"] =
        state.listCompanyType[index].industryId!;
  }

  void updateCompany() {
    state.listUpdateData.removeWhere((key, value) {
      return value.isEmpty;
    });
    // 禁止用户操作
    LoadingElement.loadingDialog(() async {
      return false;
    });
    MyHttpClient.updateCompanyInfo(state.listUpdateData).then((value) {
      if (value.code == 200) {
        Utils.toastNotification(MyText.upDateSuccess);
      } else {
        Utils.toastError(MyText.errSomeThing);
      }
      state.changeEditState.value = 0;
      _initData();
      // 跳转并刷新详情页
      Get.offNamed(RouteConfig.homeScreen);
      Get.find<ProfileScreenLogic>().refreshProfileScreen();
    });
  }

  void shareCompany() {
    MyHttpClient.createJoinCode().then((value) {
      if (value.code == 200) {
        Get.bottomSheet(
            Wrap(
              children: <Widget>[
                ListTile(
                    leading: const Icon(Icons.code),
                    title: Text(
                      value.data!.code!,
                      style: Theme.of(Get.context!).textTheme.titleMedium,
                    ),
                    trailing: Text(
                      MyText.clickToCpy,
                      style: Theme.of(Get.context!)
                          .textTheme
                          .bodyMedium
                          ?.copyWith(color: MyColor.blue),
                    ),
                    onTap: () {
                      Clipboard.setData(ClipboardData(text: value.data!.code!));
                    }),
              ],
            ),
            barrierColor: Colors.transparent,
            backgroundColor: MyColor.backBlue);
      } else {
        Utils.toastError(MyText.errSomeThing);
      }
    });
  }
}
