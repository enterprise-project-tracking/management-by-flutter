import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:management/const/consts.dart';

import 'logic.dart';

class SecurityScreenPage extends StatelessWidget {
  const SecurityScreenPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final logic = Get.put(SecurityScreenLogic());
    final state = Get
        .find<SecurityScreenLogic>()
        .state;

    return Scaffold(
        appBar: AppBar(
          title: Text(MyText.security.tr),
        ),
        body: Container(
          padding: const EdgeInsets.only(top: 2,),
          child: Column(
            children: [
              Flexible(
                child: ListView.builder(
                  // padding: EdgeInsets.symmetric(horizontal: 0),
                    itemCount: 1,
                    itemBuilder: (context, index) {
                      return SizedBox(
                        // height: 30,
                        child: Obx(() {
                          return ListTile(
                            title: Text(
                              state.listTileTitle[index],
                              style: Theme
                                  .of(context)
                                  .textTheme
                                  .bodyLarge
                                  ?.copyWith(
                                fontSize: 18,
                              ),
                            ),
                            trailing: Switch(
                                inactiveThumbColor: MyColor.white,
                                inactiveTrackColor: MyColor.grayScale[200],
                                activeColor: MyColor.white,
                                activeTrackColor: MyColor.primary,
                                trackOutlineWidth: MaterialStateProperty.all(
                                    14),
                                trackOutlineColor:
                                WidgetStateProperty.resolveWith((states) {
                                  if (states.contains(WidgetState.selected)) {
                                    return MyColor.primary;
                                  }
                                  return MyColor.grayScale[200];
                                }),
                                value: state.isRemember.value,
                                onChanged: (state) =>
                                    logic.handleSwitch(state, index)),
                          );
                        }),
                      );
                    }),
              ),
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 18),
                width: double.infinity,
                height: 55,
                child: ElevatedButton(
                    style: Theme
                        .of(Get.context!)
                        .elevatedButtonTheme
                        .style
                        ?.copyWith(
                        elevation: WidgetStateProperty.all(0),
                        backgroundColor: Get.isDarkMode
                            ? WidgetStateProperty.all(MyColor.dark3)
                            : WidgetStateProperty.all(
                            MyColor.primary[200])),
                    onPressed: () => logic.changePassword(),
                    child: Text(MyText.changePwd.tr)),
              ),
              const SizedBox(height: 30,)
            ],
          ),
        ));
  }
}
