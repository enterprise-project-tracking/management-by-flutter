import 'package:get/get.dart';
import 'package:management/const/consts.dart';

class SecurityScreenState {

  final List<String> listTileTitle = [
    MyText.rememberMe
  ];

  var account = ''.obs;
  var canCreate = false.obs;
  var isRemember = false.obs;

  SecurityScreenState() {
    ///Initialize variables
  }
}
