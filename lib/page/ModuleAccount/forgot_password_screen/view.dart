import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:management/const/consts.dart';
import 'package:management/const/icon.dart';
import 'package:management/generated/assets.dart';

import 'logic.dart';

class ForgotPasswordPage extends StatelessWidget {
  const ForgotPasswordPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final logic = Get.put(ForgotPasswordLogic());

    return Scaffold(
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          title: Text(MyText.forgetPassword.tr),
        ),
        body: Container(
            padding: const EdgeInsets.symmetric(horizontal: 24),
            child: Column(children: [
              const SizedBox(
                height: 50,
              ),
              Center(
                  child: Image.asset(
                Assets.imagesLock,
                height: 240,
                width: 240,
              )),
              const SizedBox(
                height: 50,
              ),
              Text(
                MyText.howToVerify.tr,
                style: Theme.of(context)
                    .textTheme
                    .bodyLarge
                    ?.copyWith(fontWeight: FontWeight.w400),
              ),
              const SizedBox(
                height: 50,
              ),
              GestureDetector(
                onTap: () => logic.viaEMS(),
                child: Container(
                  padding: const EdgeInsets.all(24),
                  height: 130,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      border: Border.all(color: MyColor.primary, width: 3)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                          height: 80,
                          width: 80,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(100),
                              color: Get.isDarkMode
                                  ? MyColor.dark3
                                  : MyColor.primary[100]),
                          child: const Icon(
                            IconBold.Iconly_Bold_Chat,
                            size: 32,
                            color: MyColor.primary,
                          )),
                      const SizedBox(
                        width: 20,
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(MyText.viaSMS.tr,
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyMedium
                                  ?.copyWith(
                                      fontWeight: FontWeight.w500,
                                      color: MyColor.grayScale[600])),
                          Text('+86 111 ******99',
                              style: Theme.of(context).textTheme.bodyLarge),
                        ],
                      )
                    ],
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              GestureDetector(
                onTap: () => logic.viaEmail(),
                child: Container(
                  padding: const EdgeInsets.all(24),
                  height: 130,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      border: Border.all(color: MyColor.primary, width: 3)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                          height: 80,
                          width: 80,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(100),
                              color: Get.isDarkMode
                                  ? MyColor.dark3
                                  : MyColor.primary[100]),
                          child: const Icon(
                            IconBold.Iconly_Bold_Message,
                            size: 32,
                            color: MyColor.primary,
                          )),
                      const SizedBox(
                        width: 20,
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(MyText.viaEmail.tr,
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyMedium
                                  ?.copyWith(
                                      fontWeight: FontWeight.w500,
                                      color: MyColor.grayScale[600])),
                          Text('forget@example.com',
                              style: Theme.of(context).textTheme.bodyLarge),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ])));
  }
}
