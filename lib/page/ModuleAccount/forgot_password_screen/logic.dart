import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:management/const/consts.dart';
import 'package:management/const/icon.dart';
import 'package:management/element/InputField.dart';
import 'package:management/element/verifyCode.dart';
import 'package:management/main.dart';

class ForgotPasswordLogic extends GetxController {
  var _phoneNumber = ''.obs;
  var _emailAddress = ''.obs;

  void viaEMS() {
    Get.bottomSheet(Container(
        padding: const EdgeInsets.all(24),
        color: Theme.of(Get.context!).scaffoldBackgroundColor,
        child: Wrap(children: [
          Column(
            children: [
              MyTextField(
                hintText: MyText.pleaseEnterYourPhoneNumber.tr,
                isDark: Get.isDarkMode,
                inputType: 'p',
                prefixIcon: IconBold.Iconly_Bold_Chat,
                onChanged: (value) {
                  _phoneNumber.value = value.toString();
                  update();
                },
              ),
              Obx(() {
                return TextButton(
                    onPressed: () => _sendCodePhone(),
                    child: Text(
                      MyText.sendTo.tr + _phoneNumber.value,
                      style: Theme.of(Get.context!).textTheme.bodyLarge,
                    ));
              })
            ],
          ),
        ])));
  }

  void viaEmail() {
    Get.bottomSheet(Container(
        padding: const EdgeInsets.all(24),
        color: Theme.of(Get.context!).scaffoldBackgroundColor,
        child: Wrap(children: [
          Column(
            children: [
              MyTextField(
                hintText: MyText.pleaseEnterYourEmailAddress.tr,
                isDark: Get.isDarkMode,
                inputType: 'e',
                prefixIcon: IconBold.Iconly_Bold_Message,
                onChanged: (value) {
                  _emailAddress.value = value.toString();
                  update();
                },
              ),
              Obx(() {
                return TextButton(
                    onPressed: () => _sendCodeEmail(),
                    child: Text(
                      MyText.sendTo.tr + _emailAddress.value,
                      style: Theme.of(Get.context!).textTheme.bodyLarge,
                    ));
              })
            ],
          ),
        ])));
  }

  void _sendCodePhone() {
    // Get.snackbar("发送成功", "已向" + _phoneNumber.value + "发送验证码");
    if (_phoneNumber.value.isNotEmpty) {
      Get.toNamed(RouteConfig.verifyCodeScreen,
          arguments: {'isNewPassword': true, 'data': _phoneNumber.value, 'pwd': ''});
    }
  }

  void _sendCodeEmail() {
    // Get.snackbar("发送成功", "已向" + _emailAddress.value + "发送验证码");
    if (_emailAddress.value.isNotEmpty && _emailAddress.value.isEmail) {
      Get.toNamed(RouteConfig.verifyCodeScreen,
          arguments: {'isNewPassword': true, 'data': _emailAddress.value, 'pwd': ''});
    }
  }
}

// MyVerifyCode(
// hintText: "ffffff",
// isDark: Get.isDarkMode,
// countTime: _CountTime,
// onChanged: (value) => (value) {
// print(value);
// }),
