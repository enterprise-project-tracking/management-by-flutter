import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:management/const/consts.dart';
import 'package:management/const/icon.dart';
import 'package:management/element/InputField.dart';
import 'package:management/generated/assets.dart';

import 'logic.dart';

class SignUpScreenPage extends StatelessWidget {
  const SignUpScreenPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final logic = Get.put(SignUpScreenLogic());
    final state = Get.find<SignUpScreenLogic>().state;

    return Scaffold(
      appBar: AppBar(),
      body: Container(
        padding: const EdgeInsets.symmetric(horizontal: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const SizedBox(
              height: 67.5,
            ),
            SizedBox(
              width: double.infinity,
              child: Text(
                MyText.createAccent.tr,
                style: Theme.of(context).textTheme.headlineMedium,
              ),
            ),
            const SizedBox(
              height: 67.5,
            ),
            Obx(() {
              return MyTextField(
                hintText: MyText.mail.tr,
                prefixIcon: IconBold.Iconly_Bold_Message,
                isDark: Get.isDarkMode,
                errorText: state.hasError.value ? MyText.errorTextEP.tr : null,
                onChanged: (value) => logic.getEmailOrPhone(value),
              );
            }),
            MyTextField(
              hintText: MyText.password.tr,
              prefixIcon: IconBold.Iconly_Bold_Lock,
              isDark: Get.isDarkMode,
              inputType: 's',
              suffixIcon: IconBold.Iconly_Bold_Hide,
              onChanged: (value) => logic.getPassword(value),
            ),
            const Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  width: 3,
                ),
                SizedBox(
                  width: 24,
                  height: 24,
                  // child: Obx(() {
                  //   return Checkbox(
                  //       activeColor: MyColor.primary,
                  //       value: state.checkBox.value,
                  //       onChanged: (value) => logic.rememberMe(value));
                  // }),
                ),
                SizedBox(
                  width: 10,
                ),
                SizedBox(
                  height: 20,
                  // child: Text(
                  //   MyText.rememberMe,
                  //   style: Theme.of(context).textTheme.bodyMedium,
                  // ),
                ),
              ],
            ),
            const SizedBox(
              height: 24,
            ),
            SizedBox(
                width: double.infinity,
                height: 55,
                child: ElevatedButton(
                    onPressed: () => logic.submit(),
                    child: Text(MyText.signUp.tr))),
            const SizedBox(
              height: 24,
            ),
            const SizedBox(
              height: 22,
              // child: Text(MyText.forgetPassword, style: Theme.of(context).textTheme.bodyLarge?.copyWith(
              //     color: MyColor.primary
              // ),),
            ),
            const SizedBox(height: 60),
            const Divider(height: 1, thickness: 1),
            const SizedBox(height: 30),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                SizedBox(
                  width: 88,
                  height: 60,
                  child: TextButton(
                    onPressed: () => logic.signUpWithQQ(),
                    child: Image.asset(
                      Assets.imagesQQ,
                      width: 24,
                      height: 24,
                    ),
                  ),
                ),
                SizedBox(
                  width: 88,
                  height: 60,
                  child: TextButton(
                    onPressed: () => logic.signUpWithWechat(),
                    child: Image.asset(
                      Assets.imagesWechat,
                      width: 24,
                      height: 24,
                    ),
                  ),
                ),
                SizedBox(
                  width: 88,
                  height: 60,
                  child: TextButton(
                    onPressed: () => logic.signUpWithOthers(),
                    child: Image.asset(
                      Assets.imagesTwitter,
                      width: 24,
                      height: 24,
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(height: 60),
            Row(mainAxisAlignment: MainAxisAlignment.center, children: [
              Text(
                MyText.hasAccount.tr,
                style: Theme.of(context).textTheme.bodyMedium?.copyWith(
                    fontWeight: FontWeight.w400,
                    color: Get.isDarkMode
                        ? MyColor.white
                        : MyColor.grayScale[500]),
              ),
              const SizedBox(
                width: 8,
              ),
              GestureDetector(
                onTap: () => logic.toSignIn(),
                child: Text(
                  MyText.signIn.tr,
                  style: Theme.of(context)
                      .textTheme
                      .bodyMedium
                      ?.copyWith(color: MyColor.primary),
                ),
              ),
            ])
          ],
        ),
      ),
    );
  }
}
