import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:management/const/consts.dart';
import 'package:management/const/theme.dart';
import 'package:management/data/network/api/api.dart';
import 'package:management/data/network/entity/BaseResponse.dart';
import 'package:management/main.dart';

import 'state.dart';

class SignUpScreenLogic extends GetxController {
  final SignUpScreenState state = SignUpScreenState();

  void getEmailOrPhone(String value) {
    if (value.isNum && value.length == 11) {
      state.phoneNumber = value;
      // print(value + 'is phone');
      state.hasError.value = false;
    } else if (value.isEmail) {
      state.email = value;
      // print(value + 'is email');
      state.hasError.value = false;
    } else if (value.isNotEmpty) {
      state.hasError.value = true;
    } else {
      state.hasError.value = false;
    }

    _checkSubmitState();

    update();
  }

  void getPassword(String value) {
    // print(value);

    state.password = value;

    _checkSubmitState();
  }

  // Ignore this
  void rememberMe(bool? value) {
    state.checkBox.value = !state.checkBox.value;
  }

  void submit() {
    if (state.phoneNumber.isNotEmpty && state.password.isNotEmpty) {
      // Get.snackbar("注册", state.phoneNumber + state.password);
      MyHttpClient.sendVerifyCode(state.phoneNumber, MyHttpClient.register)
          .then((value) => _goToVerifyScreenByPhone(value));
    }

    if (state.email.isNotEmpty && state.password.isNotEmpty) {
      // Get.snackbar("注册", state.email + state.password);
      MyHttpClient.sendVerifyCode(state.email, MyHttpClient.register)
          .then((value) => _goToVerifyScreenByEmail(value));
    }
    return;
  }

  void signUpWithQQ() {
    Get.snackbar("敬请期待", "暂不支持此QQ功能",
        backgroundGradient: LinearGradient(
            colors: [MyColor.gradientBlue[0]!, MyColor.gradientBlue[1]!]));
    Get.changeTheme(AppTheme.lightTheme);
  }

  void signUpWithWechat() {
    Get.snackbar("敬请期待", "暂不支持此WeChat功能",
        colorText: MyColor.white,
        backgroundGradient: LinearGradient(
            colors: [MyColor.gradientGreen[0]!, MyColor.gradientGreen[1]!]));
    Get.changeTheme(AppTheme.darkTheme);
  }

  void signUpWithOthers() {}

  void toSignIn() {
    Get.offNamed(RouteConfig.signInScreen);
  }

  void _goToVerifyScreenByPhone(BaseResponse value) {
    if (value.code == 200) {
      Get.toNamed(RouteConfig.verifyCodeScreen, arguments: {
        'isNewPassword': false,
        'data': state.phoneNumber,
        'pwd': state.password
      });
    } else {
      Get.snackbar(MyText.errSomeThing.tr, MyText.errSomeThingEx.tr,
          backgroundGradient: LinearGradient(
              colors: [MyColor.gradientRed[0]!, MyColor.gradientRed[1]!]));
    }
  }

  void _goToVerifyScreenByEmail(BaseResponse value) {
    if (value.code == 200) {
      Get.toNamed(RouteConfig.verifyCodeScreen, arguments: {
        'isNewPassword': false,
        'data': state.email,
        'pwd': state.password
      });
    } else {
      Get.snackbar(MyText.errSomeThing.tr, MyText.errSomeThingEx.tr,
          backgroundGradient: LinearGradient(
              colors: [MyColor.gradientRed[0]!, MyColor.gradientRed[1]!]));
    }
  }

  void _checkSubmitState() {
    if (state.password.isNotEmpty && state.phoneNumber.isNotEmpty) {
      state.canSubmit.value = true;
    } else if (state.password.isNotEmpty && state.email.isNotEmpty) {
      state.canSubmit.value = true;
    } else {
      state.canSubmit.value = false;
    }
  }
}
