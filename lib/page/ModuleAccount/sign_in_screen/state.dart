import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SignInScreenState {
  var phoneNumber = ''.obs;
  var email = ''.obs;
  var password = ''.obs;
  var hasError = false.obs;
  var checkBox = false.obs;
  var canSubmit = false.obs;
  int countTime = 30;
  var controllerAccount;
  var controllerPwd;
  SignInScreenState() {
    controllerAccount = TextEditingController();
    controllerPwd = TextEditingController();
  }
}
