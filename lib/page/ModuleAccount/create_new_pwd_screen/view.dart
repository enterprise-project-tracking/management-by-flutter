import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:management/const/consts.dart';
import 'package:management/const/icon.dart';
import 'package:management/element/InputField.dart';
import 'package:management/generated/assets.dart';

import 'logic.dart';

class CreateNewPwdScreenPage extends StatelessWidget {
  const CreateNewPwdScreenPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final logic = Get.put(CreateNewPwdScreenLogic());

    return Scaffold(
        appBar: AppBar(
          title: Text(MyText.createNewPwd.tr),
        ),
        body: Container(
          padding: const EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            children: [
              const SizedBox(
                height: 50,
              ),
              Center(
                  child: Image.asset(
                Assets.imagesGuard,
                height: 300,
                width: 300,
              )),
              const SizedBox(
                height: 50,
              ),
              Text(
                // TODO : 方便调试, 待删除
                "${MyText.createNewPwd.tr}=>${logic.account}",
                style: Theme.of(context)
                    .textTheme
                    .bodyLarge
                    ?.copyWith(fontWeight: FontWeight.w400),
              ),
              const SizedBox(
                height: 20,
              ),
              MyTextField(
                hintText: MyText.password.tr,
                prefixIcon: IconBold.Iconly_Bold_Lock,
                isDark: Get.isDarkMode,
                inputType: 's',
                suffixIcon: IconBold.Iconly_Bold_Hide,
                onChanged: (value) => logic.getPasswordFirst(value),
              ),
              MyTextField(
                hintText: MyText.password.tr,
                prefixIcon: IconBold.Iconly_Bold_Lock,
                isDark: Get.isDarkMode,
                inputType: 's',
                suffixIcon: IconBold.Iconly_Bold_Hide,
                onChanged: (value) => logic.getPasswordTwo(value),
              ),
              const SizedBox(
                height: 24,
              ),
              SizedBox(
                  width: double.infinity,
                  height: 55,
                  child: ElevatedButton(
                      onPressed: () => logic.submit(),
                      child: Text(MyText.continues.tr))),
            ],
          ),
        ));
  }
}
