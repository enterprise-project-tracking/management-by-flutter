import 'package:get/get.dart';
import 'package:management/const/consts.dart';
import 'package:management/const/flags.dart';
import 'package:management/const/init.dart';
import 'package:management/data/network/api/api.dart';
import 'package:management/element/toast.dart';
import 'package:management/main.dart';

class CreateNewPwdScreenLogic extends GetxController {
  String _passwordOne = '';
  String _passwordTwo = '';
  String account = Get.arguments['account'];
  String code = Get.arguments['code'];

  void getPasswordFirst(String value) {
    _passwordOne = value;
  }

  void getPasswordTwo(String value) {
    _passwordTwo = value;
  }

  void submit() {
    if (_passwordOne.isEmpty || _passwordTwo.isEmpty) {
      Utils.toastWarning(MyText.pwdNotNull.tr);
      return;
    }

    if (_passwordOne != _passwordTwo) {
      Utils.toastWarning(MyText.pwdTwoEnterNotSame.tr);
      return;
    }

    // TODO : 忘记密码情况时, 接口无法获取Header参数
    if (_passwordOne.isNotEmpty &&
        _passwordTwo.isNotEmpty &&
        _passwordOne == _passwordTwo) {
      MyHttpClient.upDatePwd(
              {"account": account, "password": _passwordTwo, "code": code})
          .then((value) async {
        if (value.code == 200) {
          Utils.toastNotification(MyText.upDateSuccess.tr);
          GlobalInit.removeLoginStatus();
          Get.offAllNamed(RouteConfig.welcomeScreen);
        } else {
          Utils.toastNotification(value.msg);
        }
      }).onError((error, stackTrace) {
        Utils.toast(MyText.error.tr, error);
        Get.back();
      });
    }
  }
}
