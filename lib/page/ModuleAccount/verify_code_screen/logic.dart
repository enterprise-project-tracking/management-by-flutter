import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:management/const/consts.dart';
import 'package:management/data/network/api/api.dart';
import 'package:management/data/network/entity/BaseResponse.dart';
import 'package:management/element/toast.dart';
import 'package:management/main.dart';

class VerifyCodeScreenLogic extends GetxController {
  // 验证码 1 分钟限制
  int countTime = 60;
  var verifyCode = ''.obs;
  // 是否修改密码
  var _isNewPassword = false;
  var _account = "";
  var _pwd = '';

  @override
  void onInit() {
    super.onInit();
    _isNewPassword = Get.arguments['isNewPassword'];
    _account = Get.arguments['data'];
    _pwd = Get.arguments['pwd'];
    MyHttpClient.sendVerifyCode(_account, MyHttpClient.forget)
        .then((value) => _reSendAccount(value));
  }

  void submit(String code) {
    if (Get.arguments['isNewPassword']) {
      // 忘记密码/创建新密码
      Get.offNamed(RouteConfig.createNewPwdScreen,
          arguments: {'account':_account, 'code': code});
    } else {
      MyHttpClient.signUp(_account, _pwd, code)
          .then((value) => _createAccount(value));
    }
  }

  void reSend() {
    MyHttpClient.sendVerifyCode(_account, MyHttpClient.register)
        .then((value) => _reSendAccount(value));
  }

  void _reSendAccount(BaseResponse value) {
    if (value.code != 200) {
      Utils.toastError(MyText.errSomeThing.tr);
    } else {
      Utils.toast(MyText.sendSuccess.tr, MyText.hasSendCodeTo.tr + _account);
    }
  }

  void _createAccount(BaseResponse value) {
    if (value.code == 200) {
      Get.offNamed(RouteConfig.welcomeScreen);
      Utils.toastNotification(MyText.signUpSuccess.tr);
    } else {
      Utils.toastError(value.msg);
    }
  }
}
