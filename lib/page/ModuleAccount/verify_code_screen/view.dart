import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:management/const/consts.dart';
import 'package:management/element/verifyCode.dart';

import 'logic.dart';

class VerifyCodeScreenPage extends StatelessWidget {
  const VerifyCodeScreenPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final logic = Get.put(VerifyCodeScreenLogic());

    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        title: Text(Get.arguments['isNewPassword']
            ? MyText.forgetPassword.tr
            : MyText.createAccent.tr),
      ),
      body: Container(
        padding: const EdgeInsets.symmetric(horizontal: 24),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            MyVerifyCode(
              hintText: '${MyText.hasSendCodeTo.tr}\n${Get.arguments['data']}',
              isDark: Get.isDarkMode,
              countTime: logic.countTime,
              onSubmit: (String newValue) => logic.submit(newValue),
              onResend: () => logic.reSend(),
            ),
          ],
        ),
      ),
    );
  }
}
