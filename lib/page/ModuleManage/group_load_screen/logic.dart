import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:management/const/consts.dart';
import 'package:management/data/network/api/api.dart';
import 'package:management/data/network/entity/company/GroupLoadResponseInfo.dart';
import 'package:management/element/loading.dart';
import 'package:management/element/toast.dart';
import 'package:management/main.dart';
import 'package:management/page/ModuleManage/manage_screen/logic.dart';
import 'package:management/page/home_screen/logic.dart';

import 'state.dart';

class GroupLoadScreenLogic extends GetxController {
  final GroupLoadScreenState state = GroupLoadScreenState();

  @override
  void onInit() {
    super.onInit();
    _initData();
  }

  // 移动到 parentId 文件夹下
  void chooseMobileGroup(int parentId) {
    Get.defaultDialog(
        title: state.isGroup
            ? MyText.mobileCompanyGroup.tr
            : MyText.mobileEmployeeGroup.tr,
        titleStyle: Theme.of(Get.context!)
            .textTheme
            .titleSmall
            ?.copyWith(color: MyColor.warning),
        middleText: MyText.moveAToB.trParams(
            {'A': state.unMoveToName.value, 'B': state.parentName.value}),
        middleTextStyle: Theme.of(Get.context!).textTheme.bodyMedium,
        onCancel: () {
          Get.back();
        },
        onConfirm: () => state.isGroup
            ? _moveGroupToGroup(parentId)
            : _moveMemberToGroup(parentId));
  }

  void _initData() {
    MyHttpClient.getGroupLoadGroup(state.parentId.value.toString())
        .then((value) {
      if (value.code == 200) {
        state.data.value = value;
      } else {
        Utils.toastError(value.msg);
      }
    }).onError((error, stackTrace) {
      Utils.toastError(error);
    });
  }

  void jumpToNextGroup(Data? data) {
    if (data == null) {
      return;
    }
    if (data.id == state.unMoveToId.value) {
      Utils.toastError("not allow");
      return;
    }
    if (state.parentName.value == MyText.pmuibs.tr) {
      state.parentName.value = '';
    }
    Get.toNamed(RouteConfig.groupLoadScreen,
        arguments: {
          'parentId': data.id,
          'groupId': state.unMoveToId.value,
          'parentName': '${state.parentName.value}/${data.name}',
          'groupName': state.unMoveToName.value,
          'isGroup': state.isGroup,
          'popTimes': state.popTimes + 1
        },
        preventDuplicates: false);
  }

  void _moveGroupToGroup(int parentId) {
    LoadingElement.loadingWidget(true);
    MyHttpClient.changeCompanyGroup(null, state.unMoveToId.value.toString(),
            state.parentId.value.toString())
        .then((value) {
      if (value.code == 200) {
        Get.close(state.popTimes + 1);
        Get.find<ManageScreenLogic>().refreshManagePage();
      }
    }).onError((error, stackTrace) {
      Get.back();
    });
  }

  void _moveMemberToGroup(int parentId) {
    LoadingElement.loadingWidget(true);
    MyHttpClient.changeEmployeeInformation(state.unMoveToId.value.toString(),
            state.parentId.value.toString(), null)
        .then((value) {
      if (value.code == 200) {
        Get.close(state.popTimes + 1);
        Get.find<ManageScreenLogic>().refreshManagePage();
      }
    }).onError((error, stackTrace) {
      Get.back();
    });
  }
}
