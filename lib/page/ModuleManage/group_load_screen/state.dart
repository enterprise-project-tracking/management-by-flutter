import 'package:get/get.dart';
import 'package:management/const/consts.dart';
import 'package:management/data/network/entity/company/GroupLoadResponseInfo.dart';

class GroupLoadScreenState {
  var data = GroupLoadResponseInfo().obs;

  var unMoveToId = 0.obs;
  var parentId = 0.obs;
  var parentName = ''.obs;
  var isGroup = true;
  var unMoveToName = ''.obs;
  var popTimes = 1;

  GroupLoadScreenState() {
    ///Initialize variables
    unMoveToId.value = Get.arguments['groupId'] ?? 0;
    parentId.value = Get.arguments['parentId'] ?? 0;
    parentName.value = Get.arguments['parentName'] ?? MyText.pmuibs.tr;
    isGroup = Get.arguments['isGroup'] ?? true;
    unMoveToName.value = Get.arguments['groupName'] ?? '';
    popTimes = Get.arguments['popTimes'] ?? 1;
  }
}
