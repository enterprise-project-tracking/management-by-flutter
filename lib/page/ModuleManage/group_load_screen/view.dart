import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:management/const/consts.dart';
import 'package:get/get.dart';
import 'package:management/const/icon.dart';
import 'package:management/generated/assets.dart';

import 'logic.dart';

class GroupLoadScreenPage extends StatelessWidget {
  const GroupLoadScreenPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final logic = Get.put(GroupLoadScreenLogic());
    final state = Get.find<GroupLoadScreenLogic>().state;

    return Obx(() {
      return Scaffold(
          appBar: AppBar(
            title: Text(
              state.parentName.value,
              style: Theme.of(Get.context!).textTheme.titleSmall,
            ),
            actions: [
              IconButton(
                iconSize: 20,
                icon: const Icon(IconLight.Iconly_Light_Outline_Tick_Square),
                onPressed: () => logic.chooseMobileGroup(state.parentId.value),
              )
            ],
          ),
          body: Container(
            padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 10),
            child: ListView.builder(
                itemCount: state.data.value.data?.length ?? 0,
                itemBuilder: (context, index) {
                  return Container(
                    height: 60,
                    margin: const EdgeInsets.symmetric(vertical: 10),
                    child: ListTile(
                      onTap: () =>
                          logic.jumpToNextGroup(state.data.value.data?[index]),
                      leading: ClipOval(
                        child: FadeInImage(
                          image: const AssetImage(Assets.iconFile),
                          height: 50.w,
                          width: 50.h,
                          fit: BoxFit.fill,
                          placeholderFit: BoxFit.scaleDown,
                          placeholder: const AssetImage(Assets.iconAvator),
                        ),
                      ),
                      title: Text(
                        state.data.value.data?[index].name ?? " ",
                        style: Theme.of(context).textTheme.bodyLarge?.copyWith(
                            fontSize: 18,
                            color: index == 6
                                ? MyColor.error
                                : Get.isDarkMode
                                    ? MyColor.white
                                    : MyColor.grayScale[800]),
                      ),
                      tileColor: state.data.value.data?[index].id ==
                              state.unMoveToId.value
                          ? Get.isDarkMode
                              ? MyColor.dark3
                              : MyColor.grayScale[100]
                          : Colors.transparent,
                    ),
                  );
                }),
          ));
    });
  }
}
