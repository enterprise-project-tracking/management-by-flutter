import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:management/const/consts.dart';
import 'package:management/const/icon.dart';
import 'package:management/const/init.dart';
import 'package:management/element/InputField.dart';
import 'package:management/generated/assets.dart';

import 'logic.dart';

class ManageScreenPage extends StatelessWidget {
  const ManageScreenPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final logic = Get.put(ManageScreenLogic());
    final state = Get.find<ManageScreenLogic>().state;

    return Obx(() {
      return Scaffold(
        appBar: AppBar(
          leading: Image.asset(
            Assets.imagesLogo,
            width: 32,
            height: 32,
            fit: BoxFit.scaleDown,
          ),
          title: Text(MyText.manage.tr),
          actions: [
            state.permission.value != CompanyMemberPermissions.member.toString()
                ? IconButton(
                    style: Theme.of(context).iconButtonTheme.style,
                    onPressed: () => logic.showEditSheet(),
                    icon: const Icon(IconLight.Iconly_Light_Outline_More_Circle,
                        size: 28),
                  )
                : const SizedBox()
          ],
        ),
        body: PageView(
          onPageChanged: (value) => logic.onPageChange(value),
          children: [
            RefreshIndicator(
              onRefresh: () async {
                logic.refreshManagePage();
              },
              child: CustomScrollView(
                slivers: [
                  SliverToBoxAdapter(
                    child: Container(
                      padding: const EdgeInsets.symmetric(horizontal: 24),
                      child: Column(
                        children: [
                          const SizedBox(
                            height: 24,
                          ),
                          MyTextField(
                            isDark: Get.isDarkMode,
                            prefixIcon: IconLight.Iconly_Light_Outline_Search,
                            readOnly: true,
                            onTap: () => logic.jumpToSearch(),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Obx(() {
                    return SliverList(
                      delegate: SliverChildBuilderDelegate((context, index) {
                        if (state.listMember.value.data?[index].type ==
                            "GROUP") {
                          return Container(
                            margin: const EdgeInsets.symmetric(vertical: 16),
                            padding: const EdgeInsets.symmetric(horizontal: 12),
                            height: 60,
                            child: ListTile(
                              leading: ClipOval(
                                child: FadeInImage(
                                  image: const AssetImage(Assets.iconFile),
                                  height: 60.w,
                                  width: 60.h,
                                  fit: BoxFit.fill,
                                  placeholderFit: BoxFit.scaleDown,
                                  placeholder:
                                      const AssetImage(Assets.iconAvator),
                                ),
                              ),
                              title: Text(
                                state.listMember.value.data?[index].name ?? "",
                                style: Theme.of(context).textTheme.titleSmall,
                              ),
                              // subtitle: Text(
                              //   " ",
                              //   style: Theme.of(context).textTheme.bodyMedium,
                              // ),
                              trailing: const Icon(
                                IconLight.Iconly_Light_Outline_Login,
                                size: 24,
                              ),
                              onTap: () => logic.jumpToNextGroup(state.listMember.value.data?[index]),
                              onLongPress: () => logic.groupOperateDialog(
                                  state.listMember.value.data?[index]),
                            ),
                          );
                        } else {
                          return Container(
                            margin: const EdgeInsets.symmetric(vertical: 16),
                            padding: const EdgeInsets.symmetric(horizontal: 12),
                            height: 60,
                            child: ListTile(
                              leading: ClipOval(
                                child: FadeInImage(
                                  image: NetworkImage(state.listMember.value
                                          .data?[index].avatar ??
                                      GlobalInit.headImage),
                                  height: 60.w,
                                  width: 60.h,
                                  fit: BoxFit.fill,
                                  placeholderFit: BoxFit.scaleDown,
                                  placeholder:
                                      const AssetImage(Assets.iconAvator),
                                ),
                              ),
                              title: Text(
                                state.listMember.value.data?[index].nickname ??
                                    MyText.defaultName,
                                style: Theme.of(context).textTheme.titleSmall,
                              ),
                              // subtitle: Text(
                              //   " ",
                              //   style: Theme.of(context).textTheme.bodyMedium,
                              // ),
                              trailing: const Icon(
                                IconLight.Iconly_Light_Outline_Danger_Circle,
                                size: 24,
                              ),
                              onTap: () => logic.jumpToPeopleDetail(
                                  state.listMember.value.data?[index]),
                              onLongPress: () => logic.employeeOperateDialog(
                                  state.listMember.value.data?[index]),
                            ),
                          );
                        }
                      }, childCount: state.listMember.value.data?.length ?? 0),
                    );
                  })
                ],
              ),
            ),
            // Device
            RefreshIndicator(
              onRefresh: () => logic.refreshManagePage(),
              child: CustomScrollView(
                slivers: [
                  SliverToBoxAdapter(
                    child: Container(
                      padding: const EdgeInsets.symmetric(horizontal: 24),
                      child: Column(
                        children: [
                          const SizedBox(
                            height: 24,
                          ),
                          MyTextField(
                            isDark: Get.isDarkMode,
                            prefixIcon: IconLight.Iconly_Light_Outline_Search,
                            readOnly: true,
                            onTap: () => logic.jumpToSearch(),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Obx(() {
                    return SliverList(
                      delegate: SliverChildBuilderDelegate((context, index) {
                        return Container(
                          margin: const EdgeInsets.symmetric(vertical: 16),
                          padding: const EdgeInsets.symmetric(horizontal: 12),
                          height: 60,
                          child: ListTile(
                            leading: ClipOval(
                              child: FadeInImage(
                                image: NetworkImage(state.listDevice.value.data!
                                        .records?[index].picUrl ??
                                    GlobalInit.headImage),
                                height: 60.w,
                                width: 60.h,
                                fit: BoxFit.fill,
                                placeholderFit: BoxFit.scaleDown,
                                placeholder:
                                    const AssetImage(Assets.iconAvator),
                              ),
                            ),
                            title: Text(
                              state.listDevice.value.data!.records?[index]
                                      .name ??
                                  MyText.defaultName,
                              style: Theme.of(context).textTheme.titleSmall,
                            ),
                            subtitle: Text(
                              state.listDevice.value.data!.records?[index]
                                      .remark ??
                                  MyText.unknownPosition,
                              style: Theme.of(context).textTheme.bodyMedium,
                            ),
                            trailing: Text(
                              '${state.listDevice.value.data!.records?[index].position}\n'
                                      '${state.listDevice.value.data!.records?[index].value}¥' ??
                                  " ",
                              textAlign: TextAlign.end,
                              style: Theme.of(context).textTheme.bodyMedium,
                            ),
                            onLongPress: () => logic.deviceOperateDialog(state.listDevice.value.data!.records?[index]),
                          ),
                        );
                      },
                          childCount:
                              state.listDevice.value.data?.total?.toInt() ?? 0),
                    );
                  })
                ],
              ),
            ),
          ],
        ),
      );
    });
  }
}
