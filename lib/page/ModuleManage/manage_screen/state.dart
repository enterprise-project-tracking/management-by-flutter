import 'package:get/get.dart';
import 'package:management/const/consts.dart';
import 'package:management/data/network/entity/manage/device/ListDeviceResponse.dart';
import 'package:management/data/network/entity/manage/people/ListMemberAndGroupResponse.dart';

class ManageScreenState {
  var loadingState = true.obs;

  var listMember = ListMemberAndGroupResponse().obs;
  var listDevice = ListDeviceResponse().obs;
  var permission = '2'.obs;
  var currentPage = 0;

  var newCompanyGroupName = '';

  ManageScreenState() {
    ///Initialize variables
  }
}
