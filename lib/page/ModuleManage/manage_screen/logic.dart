import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:management/const/consts.dart';
import 'package:management/const/flags.dart';
import 'package:management/const/icon.dart';
import 'package:management/const/init.dart';
import 'package:management/data/network/api/api.dart';
import 'package:management/data/network/entity/manage/device/ListDeviceResponse.dart'
    as device_response;
import 'package:management/data/network/entity/manage/people/ListMemberAndGroupResponse.dart';
import 'package:management/element/InputField.dart';
import 'package:management/element/loading.dart';
import 'package:management/element/toast.dart';
import 'package:management/main.dart';

import 'state.dart';

class ManageScreenLogic extends GetxController {
  final ManageScreenState state = ManageScreenState();

  @override
  void onInit() {
    super.onInit();
    _initData();
    _initPermission();
  }

  void jumpToSearch() {
    Get.toNamed(RouteConfig.searchManageScreen);
  }

  Future<void> refreshManagePage() async {
    LoadingElement.loadingDialog(true);
    _initData();
  }

  void _initData() {
    String? refreshToken = GlobalInit.getSpData(SPFlag.refreshToken);
    MyHttpClient.getRefreshByToken(refreshToken!).then((value) {
      if (value.code == 200) {
        GlobalInit.saveSpData(SPFlag.expiresIn, value.data!.expiresIn!);
        GlobalInit.saveSpData(SPFlag.accessToken, value.data!.accessToken!);
        MyHttpClient.getCompanyMember("0").then((value) {
          if (value.code == 200) {
            state.listMember.value = value;
            Get.back();
          } else {
            Utils.toastError(value.msg);
            Get.back();
            Get.back();
          }
        }).onError((error, stackTrace) {
          Utils.toastError(error);
        });
        MyHttpClient.getCompanyDevice(null).then((value) {
          if (value.code == 200) {
            state.listDevice.value = value;
            Get.back();
          } else {
            Utils.toastError(value.msg);
            Get.back();
            Get.back();
          }
        }).onError((error, stackTrace) {
          Utils.toastError(error);
        });
      }
    });
  }

  void _initPermission() {
    MyHttpClient.getMyCompanyInfo().then((value) {
      if (value.code == 200) {
        state.permission.value = value.data?.roleId.toString() ??
            CompanyMemberPermissions.member.toString();
        refresh();
      } else {
        Utils.toastError(value.msg);
      }
    }).onError((error, stackTrace) {
      Utils.toastError(error);
    });
  }

  void jumpToNextGroup(Data? data) {
    Get.toNamed(RouteConfig.groupDetailScreen, arguments: {
      'id': data?.id,
      'name': data?.name,
      'permission': state.permission.value
    });
  }

  void showEditSheet() {
    Get.bottomSheet(
        Wrap(
          children: <Widget>[
            Container(
              padding: const EdgeInsets.symmetric(vertical: 2),
              width: double.infinity,
              height: 60,
              child: TextButton(
                onPressed: () => state.currentPage == 0
                    ? _createCompanyMemberGroup()
                    : _createNewDevice(),
                style: Theme.of(Get.context!).textButtonTheme.style,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      state.currentPage == 0
                          ? MyText.createCompanyGroup.tr
                          : MyText.createNewDevice.tr,
                      style: Theme.of(Get.context!).textTheme.bodyLarge,
                    ),
                  ],
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 5),
              width: double.infinity,
              height: 60,
              child: TextButton(
                onPressed: () => Get.back(),
                style: Theme.of(Get.context!).textButtonTheme.style,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      MyText.cancel.tr,
                      style: Theme.of(Get.context!)
                          .textTheme
                          .bodyLarge
                          ?.copyWith(color: MyColor.error),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
        isDismissible: false,
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(top: Radius.circular(20))),
        backgroundColor: Theme.of(Get.context!).scaffoldBackgroundColor);
  }

  void _createNewDevice() {
    Get.toNamed(RouteConfig.editDeviceScreen, arguments: {'isCreate': true});
  }

  void _createCompanyMemberGroup() {
    Get.back();
    Get.defaultDialog(
        title: MyText.createCompanyGroup.tr,
        titleStyle: Theme.of(Get.context!).textTheme.titleSmall,
        content: MyTextField(
          prefixIcon: IconLight.Iconly_Light_Outline_Paper_Plus,
          isDark: Get.isDarkMode,
          hintText: MyText.companyGroupName.tr,
          onChanged: (value) {
            state.newCompanyGroupName = value;
          },
        ),
        contentPadding: const EdgeInsets.symmetric(horizontal: 20),
        onConfirm: () {
          MyHttpClient.createCompanyGroup(state.newCompanyGroupName, null)
              .then((value) {
            if (value.code == 200) {
              Get.back();
              refreshManagePage();
            } else {
              Utils.toastError(value.msg);
            }
          }).onError((error, stackTrace) {
            Utils.toastError(error);
          });
        },
        textCancel: MyText.cancel.tr,
        textConfirm: MyText.confirm.tr);
  }

  void groupOperateDialog(Data? data) {
    if (state.permission.value == CompanyMemberPermissions.member.toString()) {
      return;
    }
    if (data == null || data.name == null) {
      return;
    }
    if (data.type != "GROUP") {
      return;
    }
    Get.bottomSheet(
        Wrap(
          children: <Widget>[
            Container(
              padding: const EdgeInsets.symmetric(vertical: 2),
              width: double.infinity,
              height: 60,
              child: TextButton(
                onPressed: () => _deleteGroup(data),
                style: Theme.of(Get.context!).textButtonTheme.style,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      MyText.deleteCompanyGroup.tr,
                      style: Theme.of(Get.context!).textTheme.bodyLarge,
                    ),
                  ],
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.symmetric(vertical: 2),
              width: double.infinity,
              height: 60,
              child: TextButton(
                onPressed: () => _changeGroupName(data),
                style: Theme.of(Get.context!).textButtonTheme.style,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      MyText.changeCompanyGroup.tr,
                      style: Theme.of(Get.context!).textTheme.bodyLarge,
                    ),
                  ],
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.symmetric(vertical: 2),
              width: double.infinity,
              height: 60,
              child: TextButton(
                onPressed: () => _moveGroupPosition(data),
                style: Theme.of(Get.context!).textButtonTheme.style,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      MyText.mobileCompanyGroup.tr,
                      style: Theme.of(Get.context!).textTheme.bodyLarge,
                    ),
                  ],
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 5),
              width: double.infinity,
              height: 60,
              child: TextButton(
                onPressed: () => Get.back(),
                style: Theme.of(Get.context!).textButtonTheme.style,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      MyText.cancel.tr,
                      style: Theme.of(Get.context!)
                          .textTheme
                          .bodyLarge
                          ?.copyWith(color: MyColor.error),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
        isDismissible: false,
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(top: Radius.circular(20))),
        backgroundColor: Theme.of(Get.context!).scaffoldBackgroundColor);
  }

  void _deleteGroup(Data data) {
    Get.back();
    Get.defaultDialog(
        title: MyText.deleteCompanyGroup.tr,
        titleStyle: Theme.of(Get.context!).textTheme.titleSmall,
        // titlePadding: const EdgeInsets.symmetric(horizontal: 20),
        content: Text(
          "${MyText.delete.tr}  ${data.name!}?",
          style: Theme.of(Get.context!).textTheme.bodyLarge,
        ),
        onConfirm: () {
          _realDeleteGroup(data);
          Get.back();
        },
        textCancel: MyText.cancel.tr,
        textConfirm: MyText.confirm.tr);
  }

  void _changeGroupName(Data data) {
    Get.back();
    Get.defaultDialog(
        title: MyText.changeCompanyGroup.tr,
        titleStyle: Theme.of(Get.context!).textTheme.titleSmall,
        content: MyTextField(
          prefixIcon: IconLight.Iconly_Light_Outline_Paper_Plus,
          isDark: Get.isDarkMode,
          hintText: MyText.companyGroupName.tr,
          onChanged: (value) {
            state.newCompanyGroupName = value;
          },
        ),
        onConfirm: () {
          MyHttpClient.changeCompanyGroup(
                  state.newCompanyGroupName, data.id.toString(), "")
              .then((value) {
            if (value.code == 200) {
              Get.back();
              refreshManagePage();
            } else {
              Utils.toastError(value.msg);
            }
          }).onError((error, stackTrace) {
            Utils.toastError(error);
          });
        },
        textCancel: MyText.cancel.tr,
        textConfirm: MyText.confirm.tr);
  }

  void _moveGroupPosition(Data data) {
    Get.back();
    Get.toNamed(RouteConfig.groupLoadScreen, arguments: {
      'groupId': data.id,
      'isGroup': true,
      'groupName': data.name
    });
  }

  void _realDeleteGroup(Data data) {
    MyHttpClient.deleteCompanyGroup(data.id.toString()).then((value) {
      if (value.code == 200) {
        refreshManagePage();
      } else {
        Utils.toastError(value.msg);
      }
    }).onError((error, stackTrace) {
      Utils.toastError(error);
    });
  }

  void jumpToPeopleDetail(Data? data) {
    Get.toNamed(RouteConfig.employeeDetailScreen,
        arguments: {'data': data?.id});
  }

  void employeeOperateDialog(Data? data) {
    if (state.permission.value == CompanyMemberPermissions.member.toString()) {
      return;
    }
    if (data == null || data.nickname == null) {
      return;
    }
    Get.bottomSheet(
        Wrap(
          children: <Widget>[
            Container(
              padding: const EdgeInsets.symmetric(vertical: 2),
              width: double.infinity,
              height: 60,
              child: TextButton(
                onPressed: () => _deleteEmployee(data),
                style: Theme.of(Get.context!).textButtonTheme.style,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      MyText.deleteEmployee.tr,
                      style: Theme.of(Get.context!).textTheme.bodyLarge,
                    ),
                  ],
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.symmetric(vertical: 2),
              width: double.infinity,
              height: 60,
              child: TextButton(
                onPressed: () => _changeEmployee(data),
                style: Theme.of(Get.context!).textButtonTheme.style,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      MyText.changeEmployee.tr,
                      style: Theme.of(Get.context!).textTheme.bodyLarge,
                    ),
                  ],
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.symmetric(vertical: 2),
              width: double.infinity,
              height: 60,
              child: TextButton(
                onPressed: () => _moveEmployeePosition(data),
                style: Theme.of(Get.context!).textButtonTheme.style,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      MyText.mobileEmployeeGroup.tr,
                      style: Theme.of(Get.context!).textTheme.bodyLarge,
                    ),
                  ],
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 5),
              width: double.infinity,
              height: 60,
              child: TextButton(
                onPressed: () => Get.back(),
                style: Theme.of(Get.context!).textButtonTheme.style,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      MyText.cancel.tr,
                      style: Theme.of(Get.context!)
                          .textTheme
                          .bodyLarge
                          ?.copyWith(color: MyColor.error),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
        isDismissible: false,
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(top: Radius.circular(20))),
        backgroundColor: Theme.of(Get.context!).scaffoldBackgroundColor);
  }

  void _deleteEmployee(Data data) {
    Get.back();
    Get.defaultDialog(
        title: MyText.deleteEmployee.tr,
        titleStyle: Theme.of(Get.context!).textTheme.titleSmall,
        // titlePadding: const EdgeInsets.symmetric(horizontal: 20),
        content: Text(
          "${MyText.fire.tr}  ${data.nickname!}?",
          style: Theme.of(Get.context!).textTheme.bodyLarge,
        ),
        onConfirm: () {
          _realDeleteEmployee(data);
          Get.back();
        },
        textCancel: MyText.cancel.tr,
        textConfirm: MyText.confirm.tr);
  }

  void _changeEmployee(Data data) {}

  void _realDeleteEmployee(Data data) {
    MyHttpClient.fireEmployee(data.id.toString()).then((value) {
      if (value.code == 200) {
        refreshManagePage();
      } else {
        Utils.toastError(value.msg);
      }
    }).onError((error, stackTrace) {
      Utils.toastError(error);
    });
  }

  void _moveEmployeePosition(Data data) {
    Get.back();
    Get.toNamed(RouteConfig.groupLoadScreen, arguments: {
      'groupId': data.id,
      'isGroup': false,
      'groupName': data.nickname
    });
  }

  void deviceOperateDialog(device_response.Records? record) {
    if (state.permission.value == CompanyMemberPermissions.member.toString()) {
      return;
    }
    if (record == null) {
      return;
    }
    Get.bottomSheet(
        Wrap(
          children: <Widget>[
            Container(
              padding: const EdgeInsets.symmetric(vertical: 2),
              width: double.infinity,
              height: 60,
              child: TextButton(
                onPressed: () => _deleteDevice(record),
                style: Theme.of(Get.context!).textButtonTheme.style,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      MyText.deleteDevice.tr,
                      style: Theme.of(Get.context!).textTheme.bodyLarge,
                    ),
                  ],
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.symmetric(vertical: 2),
              width: double.infinity,
              height: 60,
              child: TextButton(
                onPressed: () => _changeDeviceInformation(record),
                style: Theme.of(Get.context!).textButtonTheme.style,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      MyText.changeDevice.tr,
                      style: Theme.of(Get.context!).textTheme.bodyLarge,
                    ),
                  ],
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 5),
              width: double.infinity,
              height: 60,
              child: TextButton(
                onPressed: () => Get.back(),
                style: Theme.of(Get.context!).textButtonTheme.style,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      MyText.cancel.tr,
                      style: Theme.of(Get.context!)
                          .textTheme
                          .bodyLarge
                          ?.copyWith(color: MyColor.error),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
        isDismissible: false,
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(top: Radius.circular(20))),
        backgroundColor: Theme.of(Get.context!).scaffoldBackgroundColor);
  }

  void _changeDeviceInformation(device_response.Records? record) {
    Get.toNamed(RouteConfig.editDeviceScreen,
        arguments: {'isCreate': false, 'record': record});
  }

  void _deleteDevice(device_response.Records record) {
    Get.back();
    Get.defaultDialog(
        title: MyText.deleteDevice.tr,
        titleStyle: Theme.of(Get.context!).textTheme.titleSmall,
        // titlePadding: const EdgeInsets.symmetric(horizontal: 20),
        content: Text(
          "${MyText.delete.tr}  ${record.name!}?",
          style: Theme.of(Get.context!).textTheme.bodyLarge,
        ),
        onConfirm: () {
          _realDeleteDevice(record);
          Get.back();
        },
        textCancel: MyText.cancel.tr,
        textConfirm: MyText.confirm.tr);
  }

  void _realDeleteDevice(device_response.Records record) {
    MyHttpClient.deleteDevice(record.id.toString()).then((value) {
      if (value.code == 200) {
        refreshManagePage();
      } else {
        Utils.toastError(value.msg);
      }
    }).onError((error, stackTrace) {
      Utils.toastError(error);
    });
  }

  void onPageChange(int value) {
    state.currentPage = value;
  }
}
