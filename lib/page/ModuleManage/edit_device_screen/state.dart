import 'package:get/get.dart';
import 'package:management/const/init.dart';
import 'package:management/data/network/entity/manage/device/ListDeviceResponse.dart';

class EditDeviceScreenState {

  var data = Records().obs;
  // var upLoadData = Records().obs;
  var name = '';
  var value = '';
  var position = '';
  var remark = '';
  var url = ''.obs;
  var urlName = '';
  bool isCreate = true;
  EditDeviceScreenState() {
    ///Initialize variables
    data.value = Get.arguments['record'] ?? Records();
    url.value = data.value.picUrl ?? GlobalInit.headImage;
    isCreate = Get.arguments['isCreate'];
  }

}
