import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:management/const/consts.dart';
import 'package:management/data/network/api/api.dart';
import 'package:management/element/loading.dart';
import 'package:management/element/toast.dart';
import 'package:management/page/ModuleManage/manage_screen/logic.dart';

import 'state.dart';

class EditDeviceScreenLogic extends GetxController {
  final EditDeviceScreenState state = EditDeviceScreenState();

  void updateDeviceInformation() {
    if (state.value.isEmpty ||
        state.name.isEmpty ||
        state.position.isEmpty ||
        state.remark.isEmpty ||
        state.url.value.isEmpty) {
      Utils.toastWarning(MyText.ppeo.tr);
      return;
    }
    MyHttpClient.changeDeviceInformation({
      "id": state.data.value.id.toString(),
      "value": state.value,
      "name": state.name,
      "position": state.position,
      "remark": state.remark,
      "picUrl": state.url.value
    }).then((value) {
      if (value.code == 200) {
        Get.find<ManageScreenLogic>().refreshManagePage();
        Get.back();
      } else {
        Utils.toastError(value.msg);
      }
    }).onError((error, stackTrace) {
      Utils.toastError(error);
    });
  }

  void createNewDevice() {
    MyHttpClient.addNewDevice({
      "value": state.value,
      "name": state.name,
      "position": state.position,
      "remark": state.remark,
      "picUrl": state.url.value
    }).then((value) {
      if (value.code == 200) {
        Get.find<ManageScreenLogic>().refreshManagePage();
        Get.back();
      } else {
        Utils.toastError(value.msg);
      }
    }).onError((error, stackTrace) {
      Utils.toastError(error);
    });
  }

  void getAvatarUrl() {
    Get.bottomSheet(
        Wrap(
          children: <Widget>[
            Container(
              padding: const EdgeInsets.symmetric(vertical: 2),
              width: double.infinity,
              height: 60,
              child: TextButton(
                onPressed: () => _getLostData(0),
                style: Theme.of(Get.context!).textButtonTheme.style,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      MyText.picFromCamera.tr,
                      style: Theme.of(Get.context!).textTheme.bodyLarge,
                    ),
                  ],
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.symmetric(vertical: 2),
              width: double.infinity,
              height: 60,
              child: TextButton(
                onPressed: () => _getLostData(1),
                style: Theme.of(Get.context!).textButtonTheme.style,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      MyText.picFromAlbum.tr,
                      style: Theme.of(Get.context!).textTheme.bodyLarge,
                    ),
                  ],
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 5),
              width: double.infinity,
              height: 60,
              child: TextButton(
                onPressed: () => Get.back(),
                style: Theme.of(Get.context!).textButtonTheme.style,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      MyText.cancel.tr,
                      style: Theme.of(Get.context!)
                          .textTheme
                          .bodyLarge
                          ?.copyWith(color: MyColor.error),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
        isDismissible: false,
        backgroundColor: Theme.of(Get.context!).scaffoldBackgroundColor);
  }

  Future<void> _getLostData(int type) async {
    final XFile? file = await ImagePicker().pickImage(
        source: type == 0 ? ImageSource.camera : ImageSource.gallery,
        maxWidth: 120);
    if (file == null) {
      return;
    }
    LoadingElement.loadingDialog(true);
    MyHttpClient.uploadFile(file.path, file.name).then((value) {
      if (value.code == 200) {
        Get.back();
        Get.back();
        state.url.value = value.data?.url ?? '';
        state.urlName = value.data?.name ?? '';
      } else {
        Utils.toastError(value.msg);
        Get.back();
      }
    }).onError((error, stackTrace) {
      Utils.toastError(error);
      Get.back();
    });
  }
}
