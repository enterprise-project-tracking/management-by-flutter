import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:management/const/consts.dart';
import 'package:management/const/icon.dart';
import 'package:management/const/init.dart';
import 'package:management/element/InputField.dart';
import 'package:management/generated/assets.dart';

import 'logic.dart';

class EditDeviceScreenPage extends StatelessWidget {
  const EditDeviceScreenPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final logic = Get.put(EditDeviceScreenLogic());
    final state = Get.find<EditDeviceScreenLogic>().state;

    return Scaffold(
      appBar: AppBar(title: Text(MyText.editProfile.tr)),
      body: Obx(() {
        return Container(
          padding: const EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            children: [
              const SizedBox(
                height: 24,
              ),
              Stack(
                alignment: AlignmentDirectional.bottomEnd,
                children: [
                  ClipOval(
                    child: FadeInImage(
                      image: NetworkImage(state.url.value),
                      height: 120,
                      width: 120,
                      fit: BoxFit.fill,
                      placeholderFit: BoxFit.scaleDown,
                      placeholder: const AssetImage(Assets.imagesDefaultAvatar),
                    ),
                  ),
                  GestureDetector(
                    child: const ImageIcon(
                      AssetImage(Assets.exclude),
                      size: 25,
                      color: MyColor.primary,
                    ),
                    onTap: () => logic.getAvatarUrl(),
                  )
                ],
              ),
              const SizedBox(
                height: 24,
              ),
              MyTextField(
                isDark: Get.isDarkMode,
                prefixIcon: IconLight.Iconly_Light_Outline_Paper,
                hintText: state.data.value.name ?? MyText.defaultName,
                onChanged: (value) {
                  state.name = value;
                },
              ),
              MyTextField(
                isDark: Get.isDarkMode,
                prefixIcon: IconLight.Iconly_Light_Outline_Wallet,
                hintText: "¥${state.data.value.value ?? 0}",
                inputType: 'n',
                onChanged: (value) {
                  state.value = value;
                },
              ),
              MyTextField(
                prefixIcon: IconLight.Iconly_Light_Outline_Location,
                isDark: Get.isDarkMode,
                hintText: state.data.value.position ?? MyText.unknownPosition,
                onChanged: (value) {
                  state.position = value;
                },
              ),
              MyTextField(
                prefixIcon: IconLight.Iconly_Light_Outline_Edit,
                isDark: Get.isDarkMode,
                hintText: state.data.value.remark ?? '',
                onChanged: (value) {
                  state.remark = value;
                },
              ),
              SizedBox(
                width: double.infinity,
                height: 55,
                child: ElevatedButton(
                    style: Theme.of(Get.context!)
                        .elevatedButtonTheme
                        .style
                        ?.copyWith(
                            elevation: MaterialStateProperty.all(0),
                            backgroundColor: Get.isDarkMode
                                ? MaterialStateProperty.all(MyColor.dark3)
                                : MaterialStateProperty.all(
                                    MyColor.primary[200])),
                    onPressed: () => state.isCreate
                        ? logic.createNewDevice()
                        : logic.updateDeviceInformation(),
                    child: Text(state.isCreate
                        ? MyText.createNewDevice.tr
                        : MyText.update.tr)),
              ),
            ],
          ),
        );
      }),
    );
  }
}
