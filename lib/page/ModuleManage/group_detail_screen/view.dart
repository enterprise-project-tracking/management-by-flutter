import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:management/const/consts.dart';
import 'package:management/const/icon.dart';
import 'package:management/const/init.dart';
import 'package:management/generated/assets.dart';
import 'package:management/page/ModuleManage/group_detail_screen/logic.dart';

class GroupDetailScreenPage extends StatelessWidget {
  const GroupDetailScreenPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final controller = Get.put(GroupDetailScreenLogic());
    final state = Get.find<GroupDetailScreenLogic>().state;

    return Obx(() {
      return Scaffold(
          appBar: AppBar(
            title: Text(state.parentName.value),
            actions: [
              state.permission.value !=
                      CompanyMemberPermissions.member.toString()
                  ? IconButton(
                      style: Theme.of(context).iconButtonTheme.style,
                      onPressed: () => controller.showEditSheet(),
                      icon: const Icon(
                          IconLight.Iconly_Light_Outline_More_Circle,
                          size: 28),
                    )
                  : const SizedBox()
            ],
          ),
          body: RefreshIndicator(
            onRefresh: () => controller.refreshGroupDetailPage(),
            child: Obx(() {
              return CustomScrollView(
                slivers: [
                  SliverList(
                    delegate: SliverChildBuilderDelegate((context, index) {
                      if (state.listMember.value.data?[index].type == "GROUP") {
                        return Container(
                          margin: const EdgeInsets.symmetric(vertical: 16),
                          padding: const EdgeInsets.symmetric(horizontal: 12),
                          height: 60,
                          child: ListTile(
                            leading: ClipOval(
                              child: FadeInImage(
                                image: const AssetImage(Assets.iconFile),
                                height: 60.w,
                                width: 60.h,
                                fit: BoxFit.fill,
                                placeholderFit: BoxFit.scaleDown,
                                placeholder:
                                    const AssetImage(Assets.iconAvator),
                              ),
                            ),
                            title: Text(
                              state.listMember.value.data?[index].name ?? "",
                              style: Theme.of(context).textTheme.titleSmall,
                            ),
                            subtitle: Text(
                              " ",
                              style: Theme.of(context).textTheme.bodyMedium,
                            ),
                            trailing: const Icon(
                              IconLight.Iconly_Light_Outline_Login,
                              size: 24,
                            ),
                            onTap: () => controller.jumpToNextGroup(
                                state.listMember.value.data?[index]),
                            onLongPress: () => controller.groupOperateDialog(
                                state.listMember.value.data?[index]),
                          ),
                        );
                      } else {
                        return Container(
                          margin: const EdgeInsets.symmetric(vertical: 16),
                          padding: const EdgeInsets.symmetric(horizontal: 12),
                          height: 60,
                          child: ListTile(
                            leading: ClipOval(
                              child: FadeInImage(
                                image: NetworkImage(state
                                        .listMember.value.data?[index].avatar ??
                                    GlobalInit.headImage),
                                height: 60.w,
                                width: 60.h,
                                fit: BoxFit.fill,
                                placeholderFit: BoxFit.scaleDown,
                                placeholder:
                                    const AssetImage(Assets.iconAvator),
                              ),
                            ),
                            title: Text(
                              state.listMember.value.data?[index].nickname ??
                                  MyText.defaultName,
                              style: Theme.of(context).textTheme.titleSmall,
                            ),
                            subtitle: Text(
                              " ",
                              style: Theme.of(context).textTheme.bodyMedium,
                            ),
                            trailing: const Icon(
                              IconLight.Iconly_Light_Outline_Danger_Circle,
                              size: 24,
                            ),
                            onTap: () => controller.jumpToPeopleDetail(
                                state.listMember.value.data?[index]),
                            onLongPress: () => controller.employeeOperateDialog(
                                state.listMember.value.data?[index]),
                          ),
                        );
                      }
                    },
                        childCount: state.listMember.value.data?.length ?? 0),
                  ),
                ],
              );
            }),
          ));
    });
  }
}
