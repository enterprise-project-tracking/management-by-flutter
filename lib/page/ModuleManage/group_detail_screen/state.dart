import 'package:get/get.dart';
import 'package:management/data/network/entity/manage/people/ListMemberAndGroupResponse.dart';

class GroupDetailScreenState {
  var parentName = ''.obs;
  var parentId = 0.obs;
  var permission = '2'.obs;
  var listMember = ListMemberAndGroupResponse().obs;

  String newCompanyGroupName = '';

  GroupDetailScreenState() {
    parentName.value = Get.arguments['name'] ?? '';
    parentId.value = Get.arguments['id'] ?? 0;
    permission.value = Get.arguments['permission'] ?? '2';
  }
}
