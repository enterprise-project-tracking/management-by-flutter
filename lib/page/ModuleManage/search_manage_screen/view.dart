import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:management/const/consts.dart';
import 'package:management/const/icon.dart';
import 'package:management/element/InputField.dart';

import 'logic.dart';

class SearchManageScreenPage extends StatelessWidget {
  const SearchManageScreenPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final logic = Get.put(SearchManageScreenLogic());
    final state = Get.find<SearchManageScreenLogic>().state;

    return Scaffold(
      appBar: AppBar(
        title: Text(MyText.searchForPeople.tr),
        actions: [
          IconButton(
            style: Theme.of(context).iconButtonTheme.style,
            onPressed: () {},
            icon: const Icon(IconLight.Iconly_Light_Outline_More_Circle,
                size: 28),
          )
        ],
      ),
      body: Container(
        padding: const EdgeInsets.symmetric(horizontal: 24),
        child: Column(
          children: [
            const SizedBox(
              height: 24,
            ),
            MyTextField(
              isDark: Get.isDarkMode,
              prefixIcon: IconLight.Iconly_Light_Outline_Search,
              autoFocus: true,
            )
          ],
        ),
      ),
    );
  }
}
