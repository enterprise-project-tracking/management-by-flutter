import 'package:get/get.dart';
import 'package:management/data/network/api/api.dart';
import 'package:management/element/toast.dart';

import 'state.dart';

class EmployeeDetailScreenLogic extends GetxController {
  final EmployeeDetailScreenState state = EmployeeDetailScreenState();

  @override
  void onInit() {
    super.onInit();
    _initData();
  }

  void _initData() {
    MyHttpClient.getCompanyMemberDetail(state.id.toString()).then((value) {
      if (value.code == 200) {
        state.data.value = value.data!;
      } else {
        Utils.toastError(value.msg);
      }
    }).onError((error, stackTrace) {
      Utils.toastError(error);
    });
  }
}
