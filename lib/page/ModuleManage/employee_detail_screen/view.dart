import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:management/const/consts.dart';
import 'package:management/const/init.dart';
import 'package:management/generated/assets.dart';

import 'logic.dart';

class EmployeeDetailScreenPage extends StatelessWidget {
  const EmployeeDetailScreenPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final logic = Get.put(EmployeeDetailScreenLogic());
    final state = Get
        .find<EmployeeDetailScreenLogic>()
        .state;

    return Scaffold(
      appBar: AppBar(),
      body: Obx(() {
        return Container(
            padding: const EdgeInsets.symmetric(horizontal: 24),
            child: Column(children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const SizedBox(
                    height: 24,
                  ),
                  ClipOval(
                    child: FadeInImage(
                      image: NetworkImage(state.data.value.avatar ??
                          GlobalInit.headImage),
                      height: 120,
                      width: 120,
                      fit: BoxFit.fill,
                      placeholderFit: BoxFit.scaleDown,
                      placeholder: const AssetImage(Assets.imagesDefaultAvatar),
                    ),
                  ),
                  const SizedBox(
                    height: 12,
                  ),
                  Text(
                    state.data.value.nickName ?? MyText.defaultName,
                    style: Theme
                        .of(context)
                        .textTheme
                        .titleLarge,
                  ),
                  const SizedBox(
                    height: 6,
                  ),
                  Text(
                    state.data.value.jobNumber.toString(),
                    style: Theme
                        .of(context)
                        .textTheme
                        .bodyMedium,
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  SizedBox(
                    height: 50,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              state.projectNum.value.toString(),
                              style: Theme
                                  .of(context)
                                  .textTheme
                                  .titleLarge,
                            ),
                            Text(
                              MyText.projects.tr,
                              style: Theme
                                  .of(context)
                                  .textTheme
                                  .bodyMedium
                                  ?.copyWith(
                                  fontWeight: FontWeight.w400,
                                  color: Get.isDarkMode
                                      ? MyColor.white
                                      : MyColor.grayScale[800]),
                            ),
                          ],
                        ),
                        VerticalDivider(
                          thickness: 1,
                          width: 10,
                          indent: 1,
                          endIndent: 1,
                          color: MyColor.grayScale[200],
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              state.taskNum.value.toString(),
                              style: Theme
                                  .of(context)
                                  .textTheme
                                  .titleLarge,
                            ),
                            Text(
                              MyText.tasks.tr,
                              style: Theme
                                  .of(context)
                                  .textTheme
                                  .bodyMedium
                                  ?.copyWith(
                                  fontWeight: FontWeight.w400,
                                  color: Get.isDarkMode
                                      ? MyColor.white
                                      : MyColor.grayScale[800]),
                            ),
                          ],
                        ),
                        VerticalDivider(
                          thickness: 1,
                          width: 10,
                          indent: 1,
                          endIndent: 1,
                          color: MyColor.grayScale[200],
                        ),
                        Column(
                          children: [
                            Text(
                              state.deviceNum.value.toString(),
                              style: Theme
                                  .of(context)
                                  .textTheme
                                  .titleLarge,
                            ),
                            Text(
                              MyText.devices.tr,
                              style: Theme
                                  .of(context)
                                  .textTheme
                                  .bodyMedium
                                  ?.copyWith(
                                  fontWeight: FontWeight.w400,
                                  color: Get.isDarkMode
                                      ? MyColor.white
                                      : MyColor.grayScale[800]),
                            ),
                          ],
                        )
                      ],
                    ),
                  )
                ],
              ),
              const SizedBox(
                height: 24,
              ),
              Divider(
                thickness: 1,
                color: MyColor.grayScale[200],
              ),
              const SizedBox(
                height: 12,
              ),
            ]));
      }),
    );
  }
}
