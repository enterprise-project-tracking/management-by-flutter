import 'package:get/get.dart';
import 'package:management/const/consts.dart';
import 'package:management/data/network/entity/project/response/ListProjectResponse.dart'
    as project;
import 'package:management/data/network/entity/demand/response/ListDemandOrTaskForMain.dart'
    as children;
import 'package:management/generated/assets.dart';

class MainScreenState {
  num workLoadType = 0;

  var listColor = [
    MyColor.gradientBlue,
    MyColor.gradientRed,
    MyColor.gradientGreen,
    MyColor.gradientYellow
  ];

  var listBg = [
    Assets.projectBg1,
    Assets.projectBg2,
    Assets.projectBg3,
    Assets.projectBg4,
  ];

  var listDemandState = {
    -1: MyText.all.tr,
    0: MyText.notStarted.tr,
    1: MyText.inProgress.tr,
    2: MyText.completed.tr,
    3: MyText.testing.tr
  };

  var listDemandPriority = {
    0: MyText.low.tr,
    1: MyText.common.tr,
    2: MyText.high.tr,
    3: MyText.urgency.tr
  };

  var listRecentFinishProject = <project.Records>[].obs;
  var listChildDemand = <children.Records>[].obs;
  var showProject = true.obs;
  var showDemand = true.obs;
  var selectValue = 0.obs;
  MainScreenState() {
    ///Initialize variables
    selectValue.value = -1;
  }
}
