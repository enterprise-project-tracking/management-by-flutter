import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:management/const/consts.dart';
import 'package:management/const/icon.dart';
import 'package:management/const/init.dart';
import 'package:management/element/InputField.dart';
import 'package:management/element/delegate.dart';
import 'package:management/generated/assets.dart';

import 'logic.dart';

class MainScreenPage extends StatelessWidget {
  const MainScreenPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final logic = Get.put(MainScreenLogic());
    final state = Get.find<MainScreenLogic>().state;

    return Scaffold(
      appBar: AppBar(
        leading: Image.asset(
          Assets.imagesLogo,
          width: 32,
          height: 32,
          fit: BoxFit.scaleDown,
        ),
        title: Text(MyText.home.tr),
        actions: [
          IconButton(
            style: Theme.of(context).iconButtonTheme.style,
            onPressed: () => logic.jumpToUploadWorkScreen(),
            icon: const Icon(IconLight.Iconly_Light_Outline_Time_Circle,
                color: MyColor.primary, size: 28),
          )
        ],
      ),
      body: RefreshIndicator(
        onRefresh: () => logic.refreshMainPage(),
        child: Obx(() {
          return CustomScrollView(
            slivers: [
              SliverPadding(
                padding: const EdgeInsets.symmetric(horizontal: 24),
                sliver: SliverToBoxAdapter(
                  child: MyTextField(
                    isDark: Get.isDarkMode,
                    prefixIcon: IconLight.Iconly_Light_Outline_Search,
                    readOnly: true,
                    onTap: () => logic.jumpToMainSearch(),
                  ),
                ),
              ),
              SliverVisibility(
                visible: state.listRecentFinishProject.isNotEmpty,
                sliver: SliverToBoxAdapter(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 24),
                    child: Column(
                      children: [
                        const SizedBox(
                          height: 24,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              '${MyText.recentFinishProject.tr} (${state.listRecentFinishProject.length})',
                              style: Theme.of(context).textTheme.titleSmall,
                            ),
                            GestureDetector(
                              onTap: () {
                                state.showProject.value =
                                    !state.showProject.value;
                              },
                              child: Text(
                                state.showProject.value
                                    ? MyText.hidden.tr
                                    : MyText.show.tr,
                                style: Theme.of(context)
                                    .textTheme
                                    .titleSmall
                                    ?.copyWith(color: MyColor.info),
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 24,
                        ),
                        AnimatedContainer(
                          duration: const Duration(milliseconds: 500),
                          height: state.showProject.value ? 365 : 0,
                          child: AnimatedOpacity(
                            duration: const Duration(milliseconds: 500),
                            opacity: state.showProject.value ? 1 : 0,
                            child: SingleChildScrollView(
                              scrollDirection: Axis.horizontal,
                              child: Row(
                                children: List<Widget>.generate(
                                    state.listRecentFinishProject.length,
                                    (int index) {
                                  return GestureDetector(
                                    onTap: () => logic.jumpToProjectDetail(
                                        state.listRecentFinishProject[index]),
                                    child: Card(
                                      shape: const RoundedRectangleBorder(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(20))),
                                      elevation: 10,
                                      shadowColor: const Color(0x1404060f),
                                      child: Column(
                                        children: [
                                          SizedBox(
                                            height: 150,
                                            child: Stack(
                                              alignment: AlignmentDirectional
                                                  .bottomStart,
                                              children: [
                                                ClipRRect(
                                                  borderRadius:
                                                      const BorderRadius
                                                          .vertical(
                                                          top: Radius.circular(
                                                              20)),
                                                  child: Image(
                                                    image: logic.getBg(state
                                                            .listRecentFinishProject[
                                                        index]),
                                                    width: 340,
                                                    height: 150,
                                                    fit: BoxFit.fill,
                                                  ),
                                                ),
                                                Positioned(
                                                  left: 24,
                                                  bottom: 16,
                                                  child: AnimatedOpacity(
                                                    opacity: 1,
                                                    duration: const Duration(
                                                        milliseconds: 500),
                                                    child: ClipOval(
                                                      child: FadeInImage(
                                                        image: logic.getLogo(
                                                            state.listRecentFinishProject[
                                                                index]),
                                                        fit: BoxFit.fill,
                                                        width: 40,
                                                        height: 40,
                                                        placeholderFit:
                                                            BoxFit.scaleDown,
                                                        placeholder:
                                                            const AssetImage(
                                                                Assets
                                                                    .iconAvator),
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                                Positioned(
                                                  width: 28,
                                                  height: 28,
                                                  right: 76,
                                                  bottom: 22,
                                                  child: AnimatedOpacity(
                                                    opacity: 1,
                                                    duration: const Duration(
                                                        milliseconds: 500),
                                                    child: CircleAvatar(
                                                      backgroundImage:
                                                          NetworkImage(state
                                                                  .listRecentFinishProject[
                                                                      index]
                                                                  .charge
                                                                  ?.avatar ??
                                                              GlobalInit
                                                                  .headImage),
                                                      radius: 56,
                                                      backgroundColor: Theme.of(
                                                              context)
                                                          .scaffoldBackgroundColor,
                                                    ),
                                                  ),
                                                ),
                                                Positioned(
                                                  width: 28,
                                                  height: 28,
                                                  right: 59,
                                                  bottom: 22,
                                                  child: AnimatedOpacity(
                                                    opacity: 1,
                                                    duration: const Duration(
                                                        milliseconds: 500),
                                                    child: CircleAvatar(
                                                      backgroundImage:
                                                          NetworkImage(state
                                                                  .listRecentFinishProject[
                                                                      index]
                                                                  .charge
                                                                  ?.avatar ??
                                                              GlobalInit
                                                                  .headImage),
                                                      radius: 56,
                                                      backgroundColor: Theme.of(
                                                              context)
                                                          .scaffoldBackgroundColor,
                                                    ),
                                                  ),
                                                ),
                                                Positioned(
                                                  width: 28,
                                                  height: 28,
                                                  right: 41,
                                                  bottom: 22,
                                                  child: AnimatedOpacity(
                                                    opacity: 1,
                                                    duration: const Duration(
                                                        milliseconds: 500),
                                                    child: CircleAvatar(
                                                      backgroundImage:
                                                          NetworkImage(state
                                                                  .listRecentFinishProject[
                                                                      index]
                                                                  .charge
                                                                  ?.avatar ??
                                                              GlobalInit
                                                                  .headImage),
                                                      radius: 56,
                                                      backgroundColor: Theme.of(
                                                              context)
                                                          .scaffoldBackgroundColor,
                                                    ),
                                                  ),
                                                ),
                                                Positioned(
                                                  width: 28,
                                                  height: 28,
                                                  right: 24,
                                                  bottom: 22,
                                                  child: AnimatedOpacity(
                                                    opacity: 1,
                                                    duration: const Duration(
                                                        milliseconds: 500),
                                                    child: CircleAvatar(
                                                      radius: 56,
                                                      backgroundColor: Theme.of(
                                                              context)
                                                          .scaffoldBackgroundColor,
                                                      backgroundImage:
                                                          NetworkImage(state
                                                                  .listRecentFinishProject[
                                                                      index]
                                                                  .charge
                                                                  ?.avatar ??
                                                              GlobalInit
                                                                  .headImage),
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          Container(
                                            width: 340,
                                            padding: const EdgeInsets.all(28.0),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                    state
                                                            .listRecentFinishProject[
                                                                index]
                                                            .name ??
                                                        MyText.loading.tr,
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .titleMedium),
                                                const SizedBox(
                                                  height: 2,
                                                ),
                                                Text(
                                                  state
                                                          .listRecentFinishProject[
                                                              index]
                                                          .description ??
                                                      MyText.loading.tr,
                                                  maxLines: 2,
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .bodyMedium
                                                      ?.copyWith(
                                                          fontWeight:
                                                              FontWeight.w400),
                                                ),
                                                const SizedBox(
                                                  height: 10,
                                                ),
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.end,
                                                  children: [
                                                    Chip(
                                                        backgroundColor: logic
                                                            .getMainColor(state
                                                                    .listRecentFinishProject[
                                                                index]),
                                                        label: Text(
                                                            "${state.listRecentFinishProject[index].finishDemand ?? MyText.loading.tr} / ${state.listRecentFinishProject[index].totalDemand ?? MyText.loading.tr}"),
                                                        side: BorderSide.none,
                                                        labelStyle: Theme.of(
                                                                context)
                                                            .textTheme
                                                            .bodyMedium
                                                            ?.copyWith(
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w600,
                                                                color: MyColor
                                                                    .white)),
                                                    Text(
                                                      logic.getFinishDay(state
                                                              .listRecentFinishProject[
                                                          index]),
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .bodySmall
                                                          ?.copyWith(
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w400,
                                                              color: MyColor
                                                                      .grayScale[
                                                                  500]),
                                                      textAlign: TextAlign.end,
                                                    )
                                                  ],
                                                ),
                                                const SizedBox(
                                                  height: 10,
                                                ),
                                                SizedBox(
                                                  width: 340,
                                                  child:
                                                      LinearProgressIndicator(
                                                    minHeight: 6,
                                                    value: logic.getProgress(
                                                        state.listRecentFinishProject[
                                                            index]),
                                                    backgroundColor:
                                                        MyColor.grayScale[100],
                                                    color: logic.getMainColor(
                                                        state.listRecentFinishProject[
                                                            index]),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            56),
                                                  ),
                                                )
                                              ],
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  );
                                }).toList(),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              SliverToBoxAdapter(
                child: Container(
                  margin: const EdgeInsets.only(top: 10),
                  padding: const EdgeInsets.symmetric(horizontal: 24),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        '${MyText.myTask.tr} (${state.listChildDemand.length})',
                        style: Theme.of(context).textTheme.titleSmall,
                      ),
                      GestureDetector(
                        onTap: () {
                          state.showDemand.value = !state.showDemand.value;
                        },
                        child: Text(
                          state.showDemand.value
                              ? MyText.hidden.tr
                              : MyText.show.tr,
                          style: Theme.of(context)
                              .textTheme
                              .titleSmall
                              ?.copyWith(color: MyColor.info),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SliverAnimatedOpacity(
                opacity: state.showDemand.value ? 1 : 0,
                sliver: SliverPersistentHeader(
                  pinned: true,
                  delegate: MyDelegate(
                      child: Visibility(
                        visible: state.showDemand.value,
                        child: SingleChildScrollView(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 20, vertical: 20),
                          scrollDirection: Axis.horizontal,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: List<Widget>.generate(
                              5,
                              (int index) {
                                return Container(
                                  margin:
                                      const EdgeInsets.symmetric(horizontal: 6),
                                  child: ChoiceChip(
                                      label: Text(
                                        state.listDemandState[index - 1] ??
                                            MyText.loading.tr,
                                        style: Theme.of(context)
                                            .chipTheme
                                            .labelStyle
                                            ?.copyWith(
                                                color:
                                                    state.selectValue.value ==
                                                            index - 1
                                                        ? MyColor.white
                                                        : MyColor.primary),
                                      ),
                                      selected:
                                          state.selectValue.value == index - 1,
                                      onSelected: (bool selected) {
                                        state.selectValue.value =
                                            selected ? index - 1 : 0;
                                        logic.getListByStatus();
                                      }),
                                );
                              },
                            ).toList(),
                          ),
                        ),
                      ),
                      height: state.showDemand.value ? 80 : 0),
                ),
                duration: const Duration(milliseconds: 1000),
              ),
              SliverList(
                  delegate: SliverChildBuilderDelegate((context, index) {
                return GestureDetector(
                  onTap: () =>
                      logic.jumpToNextDemand(state.listChildDemand[index].id),
                  child: Container(
                    margin:
                        const EdgeInsets.symmetric(vertical: 6, horizontal: 24),
                    child: Card(
                      color: Get.isDarkMode ? MyColor.dark2 : MyColor.white,
                      elevation: 10,
                      shadowColor: const Color(0x1404060f),
                      shape: const RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(20))),
                      child: Padding(
                        padding: const EdgeInsets.all(28.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                                state.listChildDemand[index].name ??
                                    MyText.loading.tr,
                                style: Theme.of(context).textTheme.titleMedium),
                            const SizedBox(
                              height: 2,
                            ),
                            Text(
                              '${MyText.dueDate.tr} : ${state.listChildDemand[index].endTime ?? ' '}',
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyMedium
                                  ?.copyWith(fontWeight: FontWeight.w400),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  children: [
                                    CircleAvatar(
                                      radius: 14,
                                      backgroundColor: Theme.of(context)
                                          .scaffoldBackgroundColor,
                                      backgroundImage: NetworkImage(state
                                              .listChildDemand[index]
                                              .creator
                                              ?.avatar ??
                                          GlobalInit.headImage),
                                    ),
                                    const SizedBox(
                                      width: 12,
                                    ),
                                    Text(
                                      state.listChildDemand[index].creator
                                              ?.nickName ??
                                          ' ',
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyMedium
                                          ?.copyWith(
                                              color: MyColor.grayScale[500],
                                              fontWeight: FontWeight.w400),
                                    ),
                                  ],
                                ),
                                Row(
                                  children: [
                                    IconButton(
                                        onPressed: () => logic.changePriority(),
                                        icon: Icon(
                                          Icons.flag_outlined,
                                          size: 20,
                                          color: logic.getFlagPriorityColor(
                                              state.listChildDemand[index]
                                                  .priority),
                                        )),
                                    Text(
                                      state.listDemandPriority[state
                                          .listChildDemand[index].priority]!,
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyMedium
                                          ?.copyWith(
                                              color: logic.getFlagPriorityColor(
                                                  state.listChildDemand[index]
                                                      .priority),
                                              fontWeight: FontWeight.w400),
                                    ),
                                  ],
                                ),
                                IconButton(
                                    onPressed: () => logic.seeDemandLog(
                                        state.listChildDemand[index].id),
                                    icon: const Icon(
                                      IconLight.Iconly_Light_Outline_Chat,
                                      size: 20,
                                    ))
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                );
              }, childCount: state.listChildDemand.length)),
            ],
          );
        }),
      ),
    );
  }
}
