import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:management/const/consts.dart';
import 'package:management/const/init.dart';
import 'package:management/data/network/api/api.dart';
import 'package:management/data/network/entity/worklog/request/changeWorkLogRequestParams.dart';
import 'package:management/data/network/entity/worklog/request/createWorkLogRequestParams.dart';
import 'package:management/element/loading.dart';
import 'package:management/element/toast.dart';
import 'package:management/page/ModuleMain/upload_work_log_screen/logic.dart';

import 'state.dart';

class ChangeWorkLogScreenLogic extends GetxController {
  final ChangeWorkLogScreenState state = ChangeWorkLogScreenState();

  @override
  void onInit() {
    super.onInit();
    _initData();
  }

  void _initData() async {
    await MyHttpClient.getWorkLoadType().then((value) {
      if (value.code == 200) {
        state.workType.value = value.data?.workloadType?.toInt() ?? 1;
      } else {
        Get.back();
        Utils.toastError(value.msg);
        return;
      }
    }).onError((error, p) {
      Get.back();
      Utils.toastError(error);
      return;
    });
    await MyHttpClient.getMyProject(null, null, null, null, null).then((value) {
      if (value.code == 200) {
        if (value.data!.records != null) {
          state.listProject.value = value.data!.records!;
        }
      } else {
        Get.back();
        Utils.toastError(value.msg);
      }
    }).onError((error, p) {
      Get.back();
      Utils.toastError(error);
    });
  }

  void addTimeCount() {
    Get.bottomSheet(
        Flexible(
          child: Padding(
            padding: const EdgeInsets.all(24.0),
            child: Column(
              children:
                  List<Widget>.generate(state.listProject.length, (int index) {
                return ListTile(
                  leading: CircleAvatar(
                    radius: 14,
                    backgroundImage: NetworkImage(
                        state.listProject[index].logoUrl ??
                            GlobalInit.headImage),
                  ),
                  title: Text(state.listProject[index].name ?? MyText.loading),
                  titleTextStyle: Theme.of(Get.context!).textTheme.bodyLarge,
                  onTap: () {
                    for (var element in state.selectProjects) {
                      if (element.id == state.listProject[index].id) {
                        Get.back();
                        return;
                      }
                    }
                    state.selectProjects.addIf(
                        !state.selectProjects
                            .contains(state.listProject[index]),
                        state.listProject[index]);
                    state.projectTimeMap[state.listProject[index]] = '';
                    Get.back();
                  },
                );
              }).toList(),
            ),
          ),
        ),
        isDismissible: false,
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(top: Radius.circular(20))),
        backgroundColor: Theme.of(Get.context!).scaffoldBackgroundColor);
  }

  String getRawTime() {
    var date = DateTime.parse(state.rawData.value.reportTime!);
    return '${date.year}-${date.month}-${date.day}';
  }

  void upload() {
    state.workTimes.clear();
    state.projectTimeMap.forEach((k, y) {
      state.workTimes
          .add(WorkTimesAdd(projectId: k.id!, timeCount: int.parse(y)));
    });

    var params = ChangeWorkLogRequestParams(
        id: state.rawData.value.id,
        content: state.newSummary.value.isNotEmpty
            ? state.newSummary.value
            : state.rawData.value.content,
        workAhead: state.newWorkAhead.value.isNotEmpty
            ? state.newWorkAhead.value
            : state.rawData.value.workAhead,
        workAfter: state.newWorkAfternoon.value.isNotEmpty
            ? state.newWorkAfternoon.value
            : state.rawData.value.workAfter,
        reportTime: state.newReportTime.value.isNotEmpty
            ? state.newReportTime.value
            : state.rawData.value.reportTime,
        workPlan: state.newWorkPlan.value.isNotEmpty
            ? state.newWorkPlan.value
            : state.rawData.value.workPlan,
        type: state.workType.value,
        workTimesAdd: state.workTimes);
    LoadingElement.loadingDialog(true);
    MyHttpClient.changeWorkLog(params).then((value) {
      if (value.code == 200) {
        Get.back();
        Get.find<UploadWorkLogScreenLogic>().refreshWorkLogPage();
        Get.back();
      } else {
        Get.back();
        Utils.toastError(value.msg);
      }
    }).onError((error, p) {
      Get.back();
      Utils.toastError(error);
    });
  }
}
