import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker_plus/flutter_datetime_picker_plus.dart';
import 'package:get/get.dart';
import 'package:management/const/consts.dart';
import 'package:management/const/icon.dart';
import 'package:management/element/InputField.dart';

import 'logic.dart';

class ChangeWorkLogScreenPage extends StatelessWidget {
  const ChangeWorkLogScreenPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final logic = Get.put(ChangeWorkLogScreenLogic());
    final state = Get.find<ChangeWorkLogScreenLogic>().state;
    final startController = TextEditingController();

    return Scaffold(
      appBar: AppBar(
        title: Text(MyText.changeWorkLog.tr),
        actions: [
          IconButton(
            onPressed: () => logic.upload(),
            icon: const Icon(IconLight.Iconly_Light_Outline_Send,
                color: MyColor.primary, size: 28),
          )
        ],
      ),
      body: Obx(() {
        return Container(
          padding: const EdgeInsets.symmetric(horizontal: 24),
          child: SingleChildScrollView(
            child: Column(children: [
              const SizedBox(
                height: 15,
              ),
              Visibility(
                visible: false,
                child: Text(
                  state.rawData.value.id.toString(),
                ),
              ),
              MyTextField(
                labelText: MyText.reportTime.tr,
                hintText: logic.getRawTime(),
                controller: startController,
                suffixIcon: IconLight.Iconly_Light_Outline_Calendar,
                onTapSuffix: () {
                  DatePicker.showDatePicker(context,
                      showTitleActions: true,
                      minTime: DateTime(2000, 1, 1),
                      maxTime: DateTime(2099, 12, 31), onConfirm: (date) {
                    startController.text =
                        '${date.year}-${date.month}-${date.day}';
                    state.newReportTime.value = date.toIso8601String();
                  }, currentTime: DateTime.now(), locale: LocaleType.zh);
                },
              ),
              SizedBox(
                height: 150,
                child: MyTextField(
                    labelText: state.workType.value == 1
                        ? MyText.xContent.trParams({'x': MyText.morning.tr})
                        : MyText.content,
                    onChanged: (value) {
                      state.newWorkAhead.value = value;
                    },
                    controller: TextEditingController(
                        text: state.rawData.value.workAhead),
                    expands: true,
                    hintText: state.rawData.value.workAhead),
              ),
              Visibility(
                visible: state.workType.value == 1,
                child: SizedBox(
                  height: 150,
                  child: MyTextField(
                    labelText:
                        MyText.xContent.trParams({'x': MyText.afternoon.tr}),
                    onChanged: (value) {
                      state.newWorkAfternoon.value = value;
                    },
                    controller: TextEditingController(
                        text: state.rawData.value.workAfter),
                    expands: true,
                    hintText: state.rawData.value.workAfter,
                  ),
                ),
              ),
              SizedBox(
                height: 150,
                child: MyTextField(
                  labelText: MyText.summary.tr,
                  onChanged: (value) {
                    state.newSummary.value = value;
                  },
                  controller:
                      TextEditingController(text: state.rawData.value.content),
                  expands: true,
                  hintText: state.rawData.value.content,
                ),
              ),
              SizedBox(
                height: 150,
                child: MyTextField(
                  labelText: MyText.workPlan.tr,
                  onChanged: (value) {
                    state.newWorkPlan.value = value;
                  },
                  controller:
                      TextEditingController(text: state.rawData.value.workPlan),
                  expands: true,
                  hintText: state.rawData.value.workPlan,
                ),
              ),
              TextButton(
                onPressed: () => logic.addTimeCount(),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Icon(
                      Icons.add_alarm,
                      size: 18,
                      color: MyColor.primary,
                    ),
                    const SizedBox(
                      width: 12,
                    ),
                    Text(
                      MyText.workTime.tr,
                      style: Theme.of(context).textTheme.bodyLarge,
                    ),
                  ],
                ),
              ),
              Column(
                children: List<Widget>.generate(state.selectProjects.length,
                    (int index) {
                  return ListTile(
                      title: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            state.selectProjects[index].name ?? MyText.loading,
                            style: Theme.of(context)
                                .textTheme
                                .bodyMedium
                                ?.copyWith(fontWeight: FontWeight.w400),
                          ),
                          GestureDetector(
                            onTap: () {
                              state.selectProjects.removeAt(index);
                              state.projectTimeMap
                                  .remove(state.selectProjects[index]);
                            },
                            child: Text(
                              MyText.delete.tr,
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyMedium
                                  ?.copyWith(
                                      color: MyColor.error,
                                      fontWeight: FontWeight.w600),
                            ),
                          )
                        ],
                      ),
                      subtitle: Container(
                        padding:
                            const EdgeInsets.only(left: 8, right: 8, top: 10),
                        child: MyTextField(
                          labelText: MyText.workTime.tr,
                          hintText:
                              state.projectTimeMap[state.selectProjects[index]],
                          inputType: 'n',
                          onChanged: (value) {
                            state.projectTimeMap[state.selectProjects[index]] =
                                value;
                          },
                        ),
                      ));
                }).toList(),
              )
            ]),
          ),
        );
      }),
    );
  }
}
