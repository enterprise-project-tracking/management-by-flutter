import 'package:get/get.dart';
import 'package:management/data/network/entity/worklog/request/changeWorkLogRequestParams.dart';
import 'package:management/data/network/entity/worklog/response/ListWorkLogResponse.dart'
    as list_log;
import 'package:management/data/network/entity/project/response/ListProjectResponse.dart';

class ChangeWorkLogScreenState {
  var listProject = <Records>[].obs;
  var rawData = list_log.Records().obs;

  var newReportTime = ''.obs;
  var newWorkAhead = ''.obs;
  var newWorkAfternoon = ''.obs;
  var newWorkPlan = ''.obs;
  var newSummary = ''.obs;
  var workTimes = <WorkTimesAdd>[];

  var workType = 3.obs;
  var selectProjects = <Records>[].obs;
  var projectTimeMap = <Records, String>{}.obs;
  ChangeWorkLogScreenState() {
    ///Initialize variables
    rawData.value = Get.arguments['data'];
    rawData.value.workTimes?.forEach((element) {
      var selected = Records(id: element.projectId, name: element.projectName);
      selectProjects.add(selected);
      projectTimeMap[selected] = element.timeCount.toString();
    });
  }
}
