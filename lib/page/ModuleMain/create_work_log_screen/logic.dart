import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:management/const/consts.dart';
import 'package:management/const/init.dart';
import 'package:management/data/network/api/api.dart';
import 'package:management/data/network/entity/worklog/request/createWorkLogRequestParams.dart';
import 'package:management/element/loading.dart';
import 'package:management/element/toast.dart';
import 'package:management/page/ModuleMain/upload_work_log_screen/logic.dart';

import 'state.dart';

class CreateWorkLogScreenLogic extends GetxController {
  final CreateWorkLogScreenState state = CreateWorkLogScreenState();

  @override
  Future<void> onInit() async {
    super.onInit();
    _initData();
  }

  void addTimeCount() {
    Get.bottomSheet(
        Flexible(
          child: Padding(
            padding: const EdgeInsets.all(24.0),
            child: Column(
              children:
                  List<Widget>.generate(state.listProject.length, (int index) {
                return ListTile(
                  leading: CircleAvatar(
                    radius: 14,
                    backgroundImage: NetworkImage(
                        state.listProject[index].logoUrl ??
                            GlobalInit.headImage),
                  ),
                  title: Text(state.listProject[index].name ?? MyText.loading),
                  titleTextStyle: Theme.of(Get.context!).textTheme.bodyLarge,
                  onTap: () {
                    state.selectProjects.addIf(
                        !state.selectProjects
                            .contains(state.listProject[index]),
                        state.listProject[index]);
                    state.projectTimeMap[state.listProject[index]] = '';
                    Get.back();
                  },
                );
              }).toList(),
            ),
          ),
        ),
        isDismissible: false,
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(top: Radius.circular(20))),
        backgroundColor: Theme.of(Get.context!).scaffoldBackgroundColor);
  }

  void _initData() async {
    await MyHttpClient.getWorkLoadType().then((value) {
      if (value.code == 200) {
        state.workType.value = value.data?.workloadType?.toInt() ?? 1;
      } else {
        Get.back();
        Utils.toastError(value.msg);
        return;
      }
    }).onError((error, p) {
      Get.back();
      Utils.toastError(error);
      return;
    });
    await MyHttpClient.getMyProject(null, null, null, null, null).then((value) {
      if (value.code == 200) {
        if (value.data!.records != null) {
          state.listProject.value = value.data!.records!;
        }
      } else {
        Get.back();
        Utils.toastError(value.msg);
      }
    }).onError((error, p) {
      Get.back();
      Utils.toastError(error);
    });
  }

  void upload() {
    state.workTimes.clear();
    state.projectTimeMap.forEach((k, y) {
      state.workTimes.add(WorkTimes(projectId: k.id!, timeCount: int.parse(y)));
    });
    var params = CreateWorkLogRequestParams(
        content: state.summary.value,
        workAhead: state.workAhead.value,
        workAfter: state.workAfternoon.value,
        reportTime: state.reportTime.value,
        workPlan: state.workPlan.value,
        type: state.workType.value,
        workTimes: state.workTimes);
    LoadingElement.loadingDialog(true);
    MyHttpClient.createWorkLog(params).then((value) {
      if (value.code == 200) {
        Get.back();
        Get.find<UploadWorkLogScreenLogic>().refreshWorkLogPage();
        Get.back();
      } else {
        Get.back();
        Utils.toastError(value.msg);
      }
    }).onError((error, p) {
      Get.back();
      Utils.toastError(error);
    });
  }
}
