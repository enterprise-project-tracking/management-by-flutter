import 'package:get/get.dart';
import 'package:management/data/network/entity/project/response/ListProjectResponse.dart';
import 'package:management/data/network/entity/worklog/request/createWorkLogRequestParams.dart';

class CreateWorkLogScreenState {
  var listProject = <Records>[].obs;
  var workAhead = ''.obs;
  var workAfternoon = ''.obs;
  var workPlan = ''.obs;
  var reportTime = ''.obs;
  var summary = ''.obs;
  var workType = 3.obs;
  var workTimes = <WorkTimes>[];
  // 用于记录键
  var selectProjects = <Records>[].obs;
  var projectTimeMap = <Records, String>{}.obs;
  CreateWorkLogScreenState() {
    ///Initialize variables
  }
}
