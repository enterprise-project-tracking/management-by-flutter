import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker_plus/flutter_datetime_picker_plus.dart';
import 'package:get/get.dart';
import 'package:management/const/consts.dart';
import 'package:management/const/icon.dart';
import 'package:management/element/InputField.dart';

import 'logic.dart';

class CreateWorkLogScreenPage extends StatelessWidget {
  const CreateWorkLogScreenPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final logic = Get.put(CreateWorkLogScreenLogic());
    final state = Get.find<CreateWorkLogScreenLogic>().state;
    final startController = TextEditingController();

    return Scaffold(
      appBar: AppBar(
        title: Text(MyText.createWorkLog.tr),
        actions: [
          IconButton(
            onPressed: () => logic.upload(),
            icon: const Icon(IconLight.Iconly_Light_Outline_Send,
                color: MyColor.primary, size: 28),
          )
        ],
      ),
      body: Obx(() {
        return Container(
          padding: const EdgeInsets.symmetric(horizontal: 24),
          child: SingleChildScrollView(
            child: Column(children: [
              const SizedBox(
                height: 15,
              ),
              Visibility(
                visible: false,
                child: Text(
                  state.summary.value,
                ),
              ),
              MyTextField(
                labelText: MyText.reportTime.tr,
                hintText: MyText.pspxd.trParams({'x': MyText.reportTime.tr}),
                controller: startController,
                suffixIcon: IconLight.Iconly_Light_Outline_Calendar,
                onTapSuffix: () {
                  DatePicker.showDatePicker(context,
                      showTitleActions: true,
                      minTime: DateTime(2000, 1, 1),
                      maxTime: DateTime.now(), onConfirm: (date) {
                    startController.text =
                        '${date.year}-${date.month}-${date.day}';
                    state.reportTime.value = date.toIso8601String();
                  }, currentTime: DateTime.now(), locale: LocaleType.zh);
                },
              ),
              SizedBox(
                height: 150,
                child: MyTextField(
                  labelText: state.workType.value == 1
                      ? MyText.xContent.trParams({'x': MyText.morning.tr})
                      : MyText.content,
                  onChanged: (value) {
                    state.workAhead.value = value;
                  },
                  expands: true,
                  hintText: MyText.pex.trParams({
                    'x': state.workType.value == 1
                        ? MyText.xContent.trParams({'x': MyText.morning.tr})
                        : MyText.content
                  }),
                ),
              ),
              Visibility(
                visible: state.workType.value == 1,
                child: SizedBox(
                  height: 150,
                  child: MyTextField(
                    labelText:
                        MyText.xContent.trParams({'x': MyText.afternoon.tr}),
                    onChanged: (value) {
                      state.workAfternoon.value = value;
                    },
                    expands: true,
                    hintText: MyText.pex.trParams({
                      'x': MyText.xContent.trParams({'x': MyText.afternoon.tr})
                    }),
                  ),
                ),
              ),
              SizedBox(
                height: 150,
                child: MyTextField(
                  labelText: MyText.summary.tr,
                  onChanged: (value) {
                    state.summary.value = value;
                  },
                  expands: true,
                  hintText: MyText.pex.trParams({'x': MyText.summary.tr}),
                ),
              ),
              SizedBox(
                height: 150,
                child: MyTextField(
                  labelText: MyText.workPlan.tr,
                  onChanged: (value) {
                    state.workPlan.value = value;
                  },
                  expands: true,
                  hintText: MyText.pex.trParams({'x': MyText.workPlan.tr}),
                ),
              ),
              TextButton(
                onPressed: () => logic.addTimeCount(),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Icon(
                      Icons.add_alarm,
                      size: 18,
                      color: MyColor.primary,
                    ),
                    const SizedBox(
                      width: 12,
                    ),
                    Text(
                      MyText.workTime.tr,
                      style: Theme.of(context).textTheme.bodyLarge,
                    ),
                  ],
                ),
              ),
              Column(
                children: List<Widget>.generate(state.selectProjects.length,
                    (int index) {
                  return ListTile(
                      title: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            state.selectProjects[index].name ?? MyText.loading,
                            style: Theme.of(context)
                                .textTheme
                                .bodyMedium
                                ?.copyWith(fontWeight: FontWeight.w400),
                          ),
                          GestureDetector(
                            onTap: () {
                              state.selectProjects.removeAt(index);
                              state.projectTimeMap
                                  .remove(state.selectProjects[index]);
                            },
                            child: Text(
                              MyText.delete.tr,
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyMedium
                                  ?.copyWith(
                                      color: MyColor.error,
                                      fontWeight: FontWeight.w600),
                            ),
                          )
                        ],
                      ),
                      subtitle: Container(
                        padding:
                            const EdgeInsets.only(left: 8, right: 8, top: 10),
                        child: MyTextField(
                          labelText: MyText.workTime.tr,
                          hintText:
                              MyText.pex.trParams({'x': MyText.workTime.tr}),
                          inputType: 'n',
                          onChanged: (value) {
                            state.projectTimeMap[state.selectProjects[index]] =
                                value;
                          },
                        ),
                      ));
                }).toList(),
              )
            ]),
          ),
        );
      }),
    );
  }
}
