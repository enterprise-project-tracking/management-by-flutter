import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:management/const/consts.dart';

import 'logic.dart';

class UploadWorkLogScreenPage extends StatelessWidget {
  const UploadWorkLogScreenPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final logic = Get.put(UploadWorkLogScreenLogic());
    final state = Get.find<UploadWorkLogScreenLogic>().state;

    return Scaffold(
      appBar: AppBar(title: Text(MyText.myWorkLog.tr)),
      body: Obx(() {
        return RefreshIndicator(
            onRefresh: () => logic.refreshWorkLogPage(),
            child: Padding(
              padding: const EdgeInsets.all(24),
              child: Column(
                children: [
                  // RangeSlider(
                  //   values: RangeValues(0, 1),
                  //   onChanged: (value) {},
                  //   labels: RangeLabels('ggg', DateTime.now().toIso8601String()),
                  // ),
                  Flexible(
                    child: ListView.builder(
                      itemBuilder: (context, index) {
                        return GestureDetector(
                          onLongPress: () => logic
                              .showOperateWorkLog(state.listWorkLog[index]),
                          child: Card(
                            color:
                                Get.isDarkMode ? MyColor.dark2 : MyColor.white,
                            elevation: 10,
                            shadowColor: const Color(0x1404060f),
                            shape: const RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(20))),
                            child: Padding(
                              padding: const EdgeInsets.all(20),
                              child: Column(
                                children: [
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Expanded(
                                        child: Text(
                                            state.listWorkLog[index].content ??
                                                MyText.loading,
                                            maxLines: 3),
                                      ),
                                      const SizedBox(width: 5,),
                                      GestureDetector(
                                        onTap: () => logic.seeDetail(
                                            state.listWorkLog[index]),
                                        child: Text(
                                          MyText.seeAll,
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyMedium
                                              ?.copyWith(
                                                  color: MyColor.primary,
                                                  fontWeight: FontWeight.w600),
                                        ),
                                      )
                                    ],
                                  ),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                          state.listWorkLog[index].reportTime
                                                  ?.replaceAll('T', ' ') ??
                                              MyText.loading,
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyMedium
                                              ?.copyWith(
                                                  color: MyColor.grayScale[500],
                                                  fontWeight: FontWeight.w400)),
                                      Text(
                                        state.listWorkLoadTypeMap[state
                                                .listWorkLog[index].type] ??
                                            MyText.loading,
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyMedium
                                            ?.copyWith(
                                                color: MyColor.primary[200],
                                                fontWeight: FontWeight.w600),
                                      )
                                    ],
                                  )
                                ],
                              ),
                            ),
                          ),
                        );
                      },
                      itemCount: state.listWorkLog.length,
                    ),
                  ),
                ],
              ),
            ));
      }),
      floatingActionButton: FloatingActionButton(
        onPressed: () => logic.createWorkLog(),
        child: const Icon(
          Icons.add,
          color: MyColor.white,
        ),
      ),
    );
  }
}
