import 'package:get/get.dart';
import 'package:management/const/consts.dart';
import 'package:management/data/network/entity/worklog/response/ListWorkLogResponse.dart';

class UploadWorkLogScreenState {
  var listWorkLog = <Records>[].obs;
  var listWorkLoadTypeMap = {
    0: MyText.loading.tr,
    1: MyText.daily.tr,
    2: MyText.weekly.tr,
    3: MyText.monthly.tr
  };
  UploadWorkLogScreenState() {
    ///Initialize variables
  }
}
