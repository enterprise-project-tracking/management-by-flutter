import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:management/const/consts.dart';
import 'package:management/const/icon.dart';
import 'package:management/const/init.dart';
import 'package:management/element/delegate.dart';
import 'package:management/generated/assets.dart';

import 'logic.dart';

class ProjectScreenPage extends StatelessWidget {
  const ProjectScreenPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final logic = Get.put(ProjectScreenLogic());
    final state = Get.find<ProjectScreenLogic>().state;

    return Obx(() {
      return Scaffold(
        appBar: AppBar(
          leading: Image.asset(
            Assets.imagesLogo,
            width: 32,
            height: 32,
            fit: BoxFit.scaleDown,
          ),
          title: Text(MyText.project.tr),
          actions: [
            IconButton(
              focusColor: MyColor.primary,
              onPressed: () {},
              icon: const Icon(IconLight.Iconly_Light_Outline_Search, size: 28),
            ),
            IconButton(
              focusColor: MyColor.primary,
              onPressed: () {
                state.isSimple.value = false;
              },
              isSelected: state.isSimple.value == false,
              selectedIcon: const Icon(
                IconBold.Iconly_Bold_Image,
                size: 28,
                color: MyColor.primary,
              ),
              icon: const Icon(IconLight.Iconly_Light_Outline_Image, size: 28),
            ),
            IconButton(
              focusColor: MyColor.primary,
              onPressed: () {
                state.isSimple.value = true;
              },
              isSelected: state.isSimple.value == true,
              selectedIcon: const Icon(
                IconBold.Iconly_Bold_Document,
                size: 28,
                color: MyColor.primary,
              ),
              icon:
                  const Icon(IconLight.Iconly_Light_Outline_Document, size: 28),
            )
          ],
        ),
        body: RefreshIndicator(
            onRefresh: () => logic.refreshProjectPage(),
            child: Obx(() {
              return CustomScrollView(
                slivers: [
                  SliverPersistentHeader(
                    pinned: true,
                    delegate: MyDelegate(
                        child: SingleChildScrollView(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 20, vertical: 20),
                          scrollDirection: Axis.horizontal,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: List<Widget>.generate(
                              5,
                              (int index) {
                                return Container(
                                  margin:
                                      const EdgeInsets.symmetric(horizontal: 6),
                                  child: ChoiceChip(
                                      label: Text(
                                        state.listClassification[index] ??
                                            MyText.loading.tr,
                                        style: Theme.of(context)
                                            .chipTheme
                                            .labelStyle
                                            ?.copyWith(
                                                color:
                                                    state.selectValue.value ==
                                                            index
                                                        ? MyColor.white
                                                        : MyColor.primary),
                                      ),
                                      selected:
                                          state.selectValue.value == index,
                                      onSelected: (bool selected) {
                                        state.selectValue.value =
                                            selected ? index : 0;
                                        logic.getListByStatus();
                                      }),
                                );
                              },
                            ).toList(),
                          ),
                        ),
                        height: 80),
                  ),
                  SliverList(
                      delegate: SliverChildBuilderDelegate((context, index) {
                    return GestureDetector(
                      onTap: () => logic.jumpToDetail(state.listProject[index]),
                      child: Container(
                        padding: const EdgeInsets.symmetric(horizontal: 20),
                        margin: const EdgeInsets.only(bottom: 20),
                        child: Card(
                          shape: const RoundedRectangleBorder(
                              // side: BorderSide(color: Color(0x1404060f)),
                              borderRadius: BorderRadius.vertical(
                                  top: Radius.circular(20))),
                          elevation: 10,
                          shadowColor: const Color(0x1404060f),
                          child: Column(
                            children: [
                              AnimatedContainer(
                                height: !state.isSimple.value ? 150 : 0,
                                duration: const Duration(milliseconds: 500),
                                child: Stack(
                                  alignment: AlignmentDirectional.bottomStart,
                                  children: [
                                    ClipRRect(
                                      borderRadius: const BorderRadius.vertical(
                                          top: Radius.circular(20)),
                                      child: Image(
                                        image: logic
                                            .getBg(state.listProject[index]),
                                        width: double.infinity,
                                        height: 150,
                                        fit: BoxFit.fill,
                                      ),
                                    ),
                                    Positioned(
                                      left: 24,
                                      bottom: 16,
                                      child: AnimatedOpacity(
                                        opacity: !state.isSimple.value ? 1 : 0,
                                        duration:
                                            const Duration(milliseconds: 500),
                                        child: ClipOval(
                                          child: FadeInImage(
                                            image: logic.getLogo(
                                                state.listProject[index]),
                                            fit: BoxFit.fill,
                                            width: 40,
                                            height: 40,
                                            placeholderFit: BoxFit.scaleDown,
                                            placeholder: const AssetImage(
                                                Assets.iconAvator),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Positioned(
                                      width: 28,
                                      height: 28,
                                      right: 76,
                                      bottom: 22,
                                      child: AnimatedOpacity(
                                        opacity: !state.isSimple.value ? 1 : 0,
                                        duration:
                                            const Duration(milliseconds: 500),
                                        child: CircleAvatar(
                                          backgroundImage: NetworkImage(state
                                                  .listProject[index]
                                                  .charge
                                                  ?.avatar ??
                                              GlobalInit.headImage),
                                          radius: 56,
                                          backgroundColor: Theme.of(context)
                                              .scaffoldBackgroundColor,
                                        ),
                                      ),
                                    ),
                                    Positioned(
                                      width: 28,
                                      height: 28,
                                      right: 59,
                                      bottom: 22,
                                      child: AnimatedOpacity(
                                        opacity: !state.isSimple.value ? 1 : 0,
                                        duration:
                                            const Duration(milliseconds: 500),
                                        child: CircleAvatar(
                                          backgroundImage: NetworkImage(state
                                                  .listProject[index]
                                                  .charge
                                                  ?.avatar ??
                                              GlobalInit.headImage),
                                          radius: 56,
                                          backgroundColor: Theme.of(context)
                                              .scaffoldBackgroundColor,
                                        ),
                                      ),
                                    ),
                                    Positioned(
                                      width: 28,
                                      height: 28,
                                      right: 41,
                                      bottom: 22,
                                      child: AnimatedOpacity(
                                        opacity: !state.isSimple.value ? 1 : 0,
                                        duration:
                                            const Duration(milliseconds: 500),
                                        child: CircleAvatar(
                                          backgroundImage: NetworkImage(state
                                                  .listProject[index]
                                                  .charge
                                                  ?.avatar ??
                                              GlobalInit.headImage),
                                          radius: 56,
                                          backgroundColor: Theme.of(context)
                                              .scaffoldBackgroundColor,
                                        ),
                                      ),
                                    ),
                                    Positioned(
                                      width: 28,
                                      height: 28,
                                      right: 24,
                                      bottom: 22,
                                      child: AnimatedOpacity(
                                        opacity: !state.isSimple.value ? 1 : 0,
                                        duration:
                                            const Duration(milliseconds: 500),
                                        child: CircleAvatar(
                                          radius: 56,
                                          backgroundColor: Theme.of(context)
                                              .scaffoldBackgroundColor,
                                          backgroundImage: NetworkImage(state
                                                  .listProject[index]
                                                  .charge
                                                  ?.avatar ??
                                              GlobalInit.headImage),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(28.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                            state.listProject[index].name ??
                                                MyText.loading.tr,
                                            style: Theme.of(context)
                                                .textTheme
                                                .titleMedium),
                                        IconButton(
                                            onPressed: () => logic.seeMore(
                                                state.listProject[index]),
                                            icon: const Icon(
                                              IconLight
                                                  .Iconly_Light_Outline_More_Circle,
                                              size: 19,
                                            ))
                                      ],
                                    ),
                                    const SizedBox(
                                      height: 2,
                                    ),
                                    Text(
                                      state.listProject[index].description ??
                                          MyText.loading.tr,
                                      maxLines: 2,
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyMedium
                                          ?.copyWith(
                                              fontWeight: FontWeight.w400),
                                    ),
                                    const SizedBox(
                                      height: 10,
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.end,
                                      children: [
                                        Chip(
                                            backgroundColor: logic.getMainColor(
                                                state.listProject[index]),
                                            label: Text(
                                                "${state.listProject[index].finishDemand ?? MyText.loading.tr} / ${state.listProject[index].totalDemand ?? MyText.loading.tr}"),
                                            side: BorderSide.none,
                                            labelStyle: Theme.of(context)
                                                .textTheme
                                                .bodyMedium
                                                ?.copyWith(
                                                    fontWeight: FontWeight.w600,
                                                    color: MyColor.white)),
                                        Text(
                                          logic.getFinishDay(
                                              state.listProject[index]),
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodySmall
                                              ?.copyWith(
                                                  fontWeight: FontWeight.w400,
                                                  color:
                                                      MyColor.grayScale[500]),
                                          textAlign: TextAlign.end,
                                        )
                                      ],
                                    ),
                                    const SizedBox(
                                      height: 10,
                                    ),
                                    LinearProgressIndicator(
                                      minHeight: 6,
                                      value: logic.getProgress(
                                          state.listProject[index]),
                                      backgroundColor: MyColor.grayScale[100],
                                      color: logic.getMainColor(
                                          state.listProject[index]),
                                      borderRadius: BorderRadius.circular(56),
                                    )
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    );
                  }, childCount: state.listProject.length))
                ],
              );
            })),
      );
    });
  }
}
