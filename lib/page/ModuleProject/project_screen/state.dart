import 'package:get/get.dart';
import 'package:management/const/consts.dart';
import 'package:management/data/network/entity/project/response/ListProjectResponse.dart';
import 'package:management/generated/assets.dart';

class ProjectScreenState {
  //1-需求中 2-开发中 3-已交付 4-已停滞
  var listClassification = {
    0: MyText.all.tr,
    1: MyText.inDemand.tr,
    2: MyText.inDevelopment.tr,
    3: MyText.paid.tr,
    4: MyText.stopped.tr
  };

  var listColor = [
    MyColor.gradientBlue,
    MyColor.gradientRed,
    MyColor.gradientGreen,
    MyColor.gradientYellow
  ];

  var listBg = [
    Assets.projectBg1,
    Assets.projectBg2,
    Assets.projectBg3,
    Assets.projectBg4,
  ];

  var selectValue = 0.obs;

  var isSimple = false.obs;

  var listProject = <Records>[].obs;

  ProjectScreenState() {
    ///Initialize variables
  }
}
