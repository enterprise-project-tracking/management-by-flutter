import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:management/const/consts.dart';
import 'package:management/data/network/api/api.dart';
import 'package:management/data/network/entity/demand/response/ListChildDemandOrTaskResponse.dart';
import 'package:management/element/loading.dart';
import 'package:management/element/toast.dart';
import 'package:management/main.dart';

import 'state.dart';

class ShowMoreDemandScreenLogic extends GetxController {
  final ShowMoreDemandScreenState state = ShowMoreDemandScreenState();

  @override
  void onInit() {
    super.onInit();
    _initData(false);
  }

  Future<void> refreshMoreDemandPage() async {
    LoadingElement.loadingDialog(true);
    _initData(true);
  }

  void _initData(bool isRefresh) {
    MyHttpClient.getListChildDemandOrTask(state.demandId.toString())
        .then((value) {
      if (value.code == 200) {
        if (value.data != null) {
          state.listChildDemand.value = value.data!;
        }
        if (isRefresh) Get.back();
      } else {
        Get.back();
        Utils.toastError(value.msg);
      }
    }).onError((error, p) {
      Get.back();
      Utils.toastError(error);
    });
  }

  void jumpToNextDemand(num? id) {
    if (id == null) {
      return;
    }
    Get.toNamed(RouteConfig.demandOrTaskScreen,
        preventDuplicates: false, arguments: {'demandId': id});
  }

  void seeMoreDemand(Data elementAt) {
    Get.bottomSheet(
        Wrap(
          children: <Widget>[
            Container(
              padding: const EdgeInsets.symmetric(vertical: 2),
              width: double.infinity,
              height: 60,
              child: TextButton(
                onPressed: () => _deleteDemand(elementAt),
                style: Theme.of(Get.context!).textButtonTheme.style,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      MyText.deleteDemand.tr,
                      style: Theme.of(Get.context!).textTheme.bodyLarge,
                    ),
                  ],
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 5),
              width: double.infinity,
              height: 60,
              child: TextButton(
                onPressed: () => Get.back(),
                style: Theme.of(Get.context!).textButtonTheme.style,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      MyText.cancel.tr,
                      style: Theme.of(Get.context!)
                          .textTheme
                          .bodyLarge
                          ?.copyWith(color: MyColor.error),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
        isDismissible: false,
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(top: Radius.circular(20))),
        backgroundColor: Theme.of(Get.context!).scaffoldBackgroundColor);
  }

  void _deleteDemand(Data elementAt) {
    Get.back();
    Get.defaultDialog(
        title: MyText.deleteProject.tr,
        titleStyle: Theme.of(Get.context!).textTheme.titleSmall,
        middleText: "${MyText.delete.tr}  ${elementAt.name} ?",
        middleTextStyle: Theme.of(Get.context!).textTheme.bodyLarge,
        onConfirm: () {
          Get.back();
          _realDeleteDemand(elementAt);
        },
        textCancel: MyText.cancel.tr,
        textConfirm: MyText.confirm.tr);
  }

  void _realDeleteDemand(Data elementAt) {
    MyHttpClient.deleteDemandOrTask(elementAt.id.toString()).then((value) {
      if (value.code == 200) {
        refreshMoreDemandPage();
      } else {
        Utils.toastError(value.msg);
      }
    }).onError((error, stackTrace) {
      Utils.toastError(error);
    });
  }

  changePriority() {}

  getFlagPriorityColor(num? priority) {
    if (priority == null) return Colors.transparent;
    switch (priority.toInt()) {
      case 0:
        return MyColor.grayScale[200];
      case 1:
        return MyColor.lightBlue;
      case 2:
        return MyColor.lightGreen;
      case 3:
        return MyColor.red;
    }
    return Colors.transparent;
  }

  seeDemandLog() {}
}
