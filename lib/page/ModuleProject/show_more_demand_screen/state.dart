import 'package:get/get.dart';
import 'package:management/const/consts.dart';
import 'package:management/data/network/entity/demand/response/ListChildDemandOrTaskResponse.dart'
    as children;

class ShowMoreDemandScreenState {
  num demandId = 0;
  var pName = ''.obs;
  var listChildDemand = <children.Data>[].obs;
  var listDemandPriority = {
    0: MyText.low.tr,
    1: MyText.common.tr,
    2: MyText.high.tr,
    3: MyText.urgency.tr
  };
  ShowMoreDemandScreenState() {
    ///Initialize variables
    demandId = Get.arguments['demandId'];
    pName.value = Get.arguments['pName'];
  }
}
