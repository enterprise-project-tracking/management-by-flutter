import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:management/const/consts.dart';
import 'package:management/const/icon.dart';
import 'package:management/const/init.dart';

import 'logic.dart';

class ShowMoreDemandScreenPage extends StatelessWidget {
  const ShowMoreDemandScreenPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final logic = Get.put(ShowMoreDemandScreenLogic());
    final state = Get.find<ShowMoreDemandScreenLogic>().state;

    return Scaffold(
      appBar: AppBar(
        title: Text(state.pName.value),
      ),
      body: Obx(() {
        return RefreshIndicator(
            onRefresh: () => logic.refreshMoreDemandPage(),
            child: Padding(
              padding: const EdgeInsets.all(24),
              child: Flexible(
                child: ListView.builder(
                  itemBuilder: (context, index) {
                    return GestureDetector(
                      onTap: () => logic
                          .jumpToNextDemand(state.listChildDemand[index].id),
                      child: Container(
                        margin: const EdgeInsets.symmetric(vertical: 12),
                        child: Card(
                          color: Get.isDarkMode ? MyColor.dark3 : MyColor.white,
                          elevation: 10,
                          shadowColor: const Color(0x1404060f),
                          shape: const RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(20))),
                          child: Padding(
                            padding: const EdgeInsets.all(28.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                        state.listChildDemand[index].name ??
                                            MyText.loading.tr,
                                        style: Theme.of(context)
                                            .textTheme
                                            .titleMedium),
                                    IconButton(
                                        onPressed: () => logic.seeMoreDemand(
                                            state.listChildDemand[index]),
                                        icon: const Icon(
                                          IconLight
                                              .Iconly_Light_Outline_More_Circle,
                                          size: 18.5,
                                        ))
                                  ],
                                ),
                                const SizedBox(
                                  height: 2,
                                ),
                                Text(
                                  state.listChildDemand[index].endTime ?? ' ',
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyMedium
                                      ?.copyWith(fontWeight: FontWeight.w400),
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Row(
                                      children: [
                                        CircleAvatar(
                                          radius: 14,
                                          backgroundColor: Theme.of(context)
                                              .scaffoldBackgroundColor,
                                          backgroundImage: NetworkImage(state
                                                  .listChildDemand[index]
                                                  .creator
                                                  ?.avatar ??
                                              GlobalInit.headImage),
                                        ),
                                        const SizedBox(
                                          width: 12,
                                        ),
                                        Text(
                                          state.listChildDemand[index].creator
                                                  ?.nickName ??
                                              ' ',
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyMedium
                                              ?.copyWith(
                                                  color: MyColor.grayScale[500],
                                                  fontWeight: FontWeight.w400),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        IconButton(
                                            onPressed: () =>
                                                logic.changePriority(),
                                            icon: Icon(
                                              Icons.flag_outlined,
                                              size: 20,
                                              color: logic.getFlagPriorityColor(
                                                  state.listChildDemand[index]
                                                      .priority),
                                            )),
                                        Text(
                                          state.listDemandPriority[state
                                              .listChildDemand[index]
                                              .priority]!,
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyMedium
                                              ?.copyWith(
                                                  color: logic
                                                      .getFlagPriorityColor(
                                                          state
                                                              .listChildDemand[
                                                                  index]
                                                              .priority),
                                                  fontWeight: FontWeight.w400),
                                        ),
                                      ],
                                    ),
                                    IconButton(
                                        onPressed: () => logic.seeDemandLog(),
                                        icon: const Icon(
                                          IconLight.Iconly_Light_Outline_Chat,
                                          size: 20,
                                        ))
                                  ],
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                    );
                  },
                  itemCount: state.listChildDemand.length,
                ),
              ),
            ));
      }),
    );
  }
}
