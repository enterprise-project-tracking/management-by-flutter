import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:management/const/consts.dart';
import 'package:management/data/network/entity/project/request/SubmitProjectRequestParams.dart';
import 'package:management/data/network/entity/project/response/AllUserResponse.dart';

class CreateProjectScreeState {
  num projectId = 0;
  var projectName = ''.obs;
  var projectStartTime = ''.obs;
  var projectEndTime = ''.obs;
  var estimatedCost = ''.obs;
  var projectDescription = ''.obs;
  var projectStatus = 1.obs;

  final statusController = TextEditingController();

  var listClassification = {
    0: MyText.all,
    1: MyText.inDemand.tr,
    2: MyText.inDevelopment,
    3: MyText.paid,
    4: MyText.stopped
  };

  var listNumbers = <Data>[].obs;
  var listSelectNumbers = <Data>{}.obs;
  var mapSelect = <Data, bool>{}.obs;
  var chargeId = ''.obs;
  var chargeUrl = ''.obs;

  var listConferenceResolutions = <ConferenceResolutions>[].obs;
  var listProjectPlanningPaper = <ProjectPlanningPaper>[].obs;

  var listConferenceFiles = <PlatformFile>[].obs;
  var listPlanningFiles = <PlatformFile>[].obs;

  CreateProjectScreeState() {
    ///Initialize variables
  }
}
