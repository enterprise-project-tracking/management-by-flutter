import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker_plus/flutter_datetime_picker_plus.dart';
import 'package:get/get.dart';
import 'package:management/const/consts.dart';
import 'package:management/const/icon.dart';
import 'package:management/const/init.dart';
import 'package:management/element/InputField.dart';

import 'logic.dart';

class CreateProjectScreePage extends StatelessWidget {
  const CreateProjectScreePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final logic = Get.put(CreateProjectScreeLogic());
    final state = Get.find<CreateProjectScreeLogic>().state;
    final startController = TextEditingController();
    final endController = TextEditingController();

    return Scaffold(
      appBar: AppBar(
        title: Text(MyText.createProject.tr),
      ),
      body: Obx(() {
        return Container(
          padding: const EdgeInsets.symmetric(horizontal: 24),
          child: SingleChildScrollView(
            child: Column(children: [
              const SizedBox(
                height: 24,
              ),
              MyTextField(
                labelText: MyText.projectName.tr,
                onChanged: (value) {
                  state.projectName.value = value;
                },
                hintText: MyText.pex.trParams({'x': MyText.projectName.tr}),
              ),
              MyTextField(
                labelText: MyText.start.tr,
                hintText: MyText.pspxd.trParams({'x': MyText.start.tr}),
                controller: startController,
                suffixIcon: IconLight.Iconly_Light_Outline_Calendar,
                onTapSuffix: () {
                  DatePicker.showDatePicker(context,
                      showTitleActions: true,
                      minTime: DateTime(2000, 1, 1),
                      maxTime: DateTime(2099, 12, 31), onConfirm: (date) {
                    startController.text =
                        '${date.year}-${date.month}-${date.day}';
                    state.projectStartTime.value = date.toIso8601String();
                  }, currentTime: DateTime.now(), locale: LocaleType.zh);
                },
              ),
              MyTextField(
                labelText: MyText.end.tr,
                hintText: MyText.pspxd.trParams({'x': MyText.end.tr}),
                controller: endController,
                suffixIcon: IconLight.Iconly_Light_Outline_Calendar,
                onTapSuffix: () {
                  DatePicker.showDatePicker(Get.context!,
                      showTitleActions: true,
                      minTime: DateTime(2000, 1, 1),
                      maxTime: DateTime(2099, 12, 31), onConfirm: (date) {
                    endController.text =
                        '${date.year}-${date.month}-${date.day}';
                    state.projectEndTime.value = date.toIso8601String();
                  }, currentTime: DateTime.now(), locale: LocaleType.zh);
                },
              ),
              MyTextField(
                labelText: MyText.estimatedCost.tr,
                onChanged: (value) {
                  state.estimatedCost.value = value;
                },
                inputType: 'n',
                hintText: MyText.pex.trParams({'x': MyText.estimatedCost.tr}),
              ),
              MyTextField(
                labelText: MyText.projectStatus.tr,
                suffixIcon: IconLight.Iconly_Light_Outline_More_Square,
                onTapSuffix: () {
                  logic.selectStatus();
                },
                controller: state.statusController,
                hintText: MyText.pex.trParams({'x': MyText.projectStatus.tr}),
              ),
              SizedBox(
                height: 150,
                child: MyTextField(
                  labelText: MyText.projectDescription.tr,
                  onChanged: (value) {
                    state.projectDescription.value = value;
                  },
                  expands: true,
                  hintText:
                      MyText.pex.trParams({'x': MyText.projectDescription.tr}),
                ),
              ),
              Container(
                child: state.listSelectNumbers.isNotEmpty
                    ? SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Text(MyText.member.tr),
                            const SizedBox(
                              width: 50,
                            ),
                            Row(
                              children: List<Widget>.generate(
                                  state.listSelectNumbers.length, (index) {
                                return CircleAvatar(
                                  backgroundImage: NetworkImage(state
                                          .listSelectNumbers
                                          .elementAt(index)
                                          .avatar ??
                                      GlobalInit.headImage),
                                  radius: 15,
                                  backgroundColor:
                                      Theme.of(context).scaffoldBackgroundColor,
                                );
                              }),
                            ),
                            const SizedBox(
                              width: 50,
                            ),
                            IconButton(
                              onPressed: () =>
                                  logic.showMultipleSelectionInterface(),
                              icon: const Icon(
                                IconLight.Iconly_Light_Outline_Plus,
                                size: 30,
                                color: MyColor.primary,
                              ),
                            )
                          ],
                        ),
                      )
                    : TextButton(
                        onPressed: () => logic.showMultipleSelectionInterface(),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            const Icon(
                              IconLight.Iconly_Light_Outline_Plus,
                              size: 18,
                              color: MyColor.primary,
                            ),
                            const SizedBox(
                              width: 12,
                            ),
                            Text(
                              MyText.addMember.tr,
                              style: Theme.of(context).textTheme.bodyLarge,
                            ),
                          ],
                        ),
                      ),
              ),
              Container(
                child: state.chargeId.value.isNotEmpty
                    ? SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Text(MyText.charge.tr),
                            const SizedBox(
                              width: 50,
                            ),
                            CircleAvatar(
                              backgroundImage: NetworkImage(
                                  state.chargeUrl.value),
                              radius: 15,
                              backgroundColor:
                                  Theme.of(context).scaffoldBackgroundColor,
                            ),
                            const SizedBox(
                              width: 50,
                            ),
                            IconButton(
                              onPressed: () => logic.selectCharge(),
                              icon: const Icon(
                                IconLight.Iconly_Light_Outline_Plus,
                                size: 18,
                                color: MyColor.primary,
                              ),
                            )
                          ],
                        ),
                      )
                    : TextButton(
                        onPressed: () => logic.selectCharge(),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            const Icon(
                              IconLight.Iconly_Light_Outline_Plus,
                              size: 18,
                              color: MyColor.primary,
                            ),
                            const SizedBox(
                              width: 12,
                            ),
                            Text(
                              MyText.setCharge.tr,
                              style: Theme.of(context).textTheme.bodyLarge,
                            ),
                          ],
                        ),
                      ),
              ),
              TextButton(
                onPressed: () => logic.addConferenceResolutions(),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Icon(
                      IconLight.Iconly_Light_Outline_Paper_Upload,
                      size: 18,
                      color: MyColor.primary,
                    ),
                    const SizedBox(
                      width: 12,
                    ),
                    Text(
                      MyText.addConferenceResolutions.tr,
                      style: Theme.of(context).textTheme.bodyLarge,
                    ),
                  ],
                ),
              ),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: List<Widget>.generate(
                    state.listConferenceFiles.length,
                    (int index) {
                      return Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 2),
                        child: InputChip(
                          visualDensity: const VisualDensity(
                              horizontal: 0.0, vertical: -4),
                          label: Text(
                            state.listConferenceFiles[index].name,
                            style: Theme.of(context)
                                .textTheme
                                .bodyMedium
                                ?.copyWith(color: MyColor.primary),
                          ),
                          avatar: const Icon(
                            IconLight.Iconly_Light_Outline_Document,
                            size: 18,
                            color: MyColor.primary,
                          ),
                          deleteIcon: const Icon(
                            IconLight.Iconly_Light_Outline_Close_Square,
                            size: 18,
                            color: MyColor.primary,
                          ),
                          onDeleted: () {
                            state.listConferenceFiles.removeAt(index);
                          },
                        ),
                      );
                    },
                  ).toList(),
                ),
              ),
              TextButton(
                onPressed: () => logic.addProjectPlanningPaper(),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Icon(
                      IconLight.Iconly_Light_Outline_Paper_Upload,
                      size: 18,
                      color: MyColor.primary,
                    ),
                    const SizedBox(
                      width: 12,
                    ),
                    Text(
                      MyText.addProjectPlanningPaper.tr,
                      style: Theme.of(context).textTheme.bodyLarge,
                    ),
                  ],
                ),
              ),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: List<Widget>.generate(
                    state.listPlanningFiles.length,
                    (int index) {
                      return Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 2),
                        child: InputChip(
                          visualDensity: const VisualDensity(
                              horizontal: 0.0, vertical: -4),
                          label: Text(
                            state.listPlanningFiles[index].name,
                            style: Theme.of(context)
                                .textTheme
                                .bodyMedium
                                ?.copyWith(color: MyColor.primary),
                          ),
                          avatar: const Icon(
                            IconLight.Iconly_Light_Outline_Document,
                            size: 18,
                            color: MyColor.primary,
                          ),
                          deleteIcon: const Icon(
                            IconLight.Iconly_Light_Outline_Close_Square,
                            size: 18,
                            color: MyColor.primary,
                          ),
                          onDeleted: () {
                            state.listPlanningFiles.removeAt(index);
                          },
                        ),
                      );
                    },
                  ).toList(),
                ),
              ),
              SizedBox(
                width: double.maxFinite,
                child: ElevatedButton(
                    onPressed: () => logic.submit(),
                    child: Text(MyText.submit.tr)),
              ),
              const SizedBox(
                height: 10,
              )
            ]),
          ),
        );
      }),
    );
  }
}
