import 'package:get/get.dart';
import 'package:management/data/network/api/api.dart';
import 'package:management/data/network/entity/demand/response/ListLogDemandOrTaskResponse.dart';
import 'package:management/element/loading.dart';
import 'package:management/element/toast.dart';
import 'package:management/main.dart';

import 'state.dart';

class ShowMoreLogScreenLogic extends GetxController {
  final ShowMoreLogScreenState state = ShowMoreLogScreenState();

  Future<void> refreshMoreLogPage() async {
    LoadingElement.loadingDialog(true);
    _initData(true);
  }

  @override
  void onInit() {
    super.onInit();
    _initData(false);
  }

  void _initData(bool isRefresh) {
    MyHttpClient.getListLogDemandOrTask(state.demandId.toString())
        .then((value) {
      if (value.code == 200) {
        state.listLogDemand.value = value.data!;
        if (isRefresh) Get.back();
      } else {
        Get.back();
        Utils.toastError(value.msg);
      }
    }).onError((error, stackTrace) {
      Get.back();
      Utils.toastError(error);
    });
  }

  String logText1(Data listLogDemand) {
    if (listLogDemand.companyUser == null) {
      return '';
    }
    if (listLogDemand.companyUser?.nickName == null) {
      return '';
    }
    return '用户  ${listLogDemand.companyUser!.nickName!}  ${listLogDemand.content!}';
  }

  void jumpToNextDemandByLog(Data listLogDemand) {
    if (listLogDemand.content == null) return;
    if (listLogDemand.content!.startsWith('创建')) {
      Get.toNamed(RouteConfig.demandOrTaskScreen,
          preventDuplicates: false,
          arguments: {'demandId': listLogDemand.relateDemandId});
    }
  }
}
