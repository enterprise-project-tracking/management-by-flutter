import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:management/const/consts.dart';
import 'package:management/const/init.dart';
import 'package:timelines/timelines.dart';

import 'logic.dart';

class ShowMoreLogScreenPage extends StatelessWidget {
  const ShowMoreLogScreenPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final logic = Get.put(ShowMoreLogScreenLogic());
    final state = Get.find<ShowMoreLogScreenLogic>().state;

    return Scaffold(
      appBar: AppBar(
        title: Text(state.pName.value),
      ),
      body: Obx(() {
        return RefreshIndicator(
          onRefresh: () => logic.refreshMoreLogPage(),
          child: Padding(
            padding: const EdgeInsets.all(24.0),
            child: Timeline(
              theme: TimelineThemeData(
                  direction: Axis.vertical,
                  color: MyColor.primary[300],
                  nodePosition: 0.3),
              children: List.generate(state.listLogDemand.length, (int index) {
                return TimelineTile(
                  node: TimelineNode(
                    indicator: DotIndicator(
                      child: CircleAvatar(
                        radius: 13,
                        backgroundColor:
                            Theme.of(context).scaffoldBackgroundColor,
                        backgroundImage: NetworkImage(
                            state.listLogDemand[index].companyUser?.avatar ??
                                GlobalInit.headImage),
                      ),
                    ),
                    startConnector: index != 0
                        ? DecoratedLineConnector(
                            decoration: BoxDecoration(
                              gradient: LinearGradient(
                                begin: Alignment.topCenter,
                                end: Alignment.bottomCenter,
                                colors: [
                                  MyColor.gradientBlue[0]!,
                                  MyColor.gradientBlue[1]!
                                ],
                              ),
                            ),
                          )
                        : null,
                    endConnector: index == state.listLogDemand.length - 1
                        ? null
                        : DecoratedLineConnector(
                            decoration: BoxDecoration(
                              gradient: LinearGradient(
                                begin: Alignment.topCenter,
                                end: Alignment.bottomCenter,
                                colors: [
                                  MyColor.gradientBlue[1]!,
                                  MyColor.gradientBlue[0]!
                                ],
                              ),
                            ),
                          ),
                  ),
                  contents: Container(
                    margin: const EdgeInsets.only(left: 24),
                    padding: const EdgeInsets.all(8),
                    child: GestureDetector(
                      onTap: () => logic
                          .jumpToNextDemandByLog(state.listLogDemand[index]),
                      child: Card(
                        child: Column(
                          children: [
                            Text(logic.logText1(state.listLogDemand[index]),
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyMedium
                                    ?.copyWith(
                                      fontWeight: FontWeight.w400,
                                    )),
                            Text(
                                state.listLogDemand[index].relateDemandName ??
                                    '',
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyMedium
                                    ?.copyWith(
                                        fontWeight: FontWeight.w400,
                                        color: MyColor.grayScale[500])),
                          ],
                        ),
                      ),
                    ),
                  ),
                  oppositeContents: Center(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(state.listLogDemand[index].createTime ?? '',
                          style: Theme.of(context)
                              .textTheme
                              .bodyMedium
                              ?.copyWith(
                                  fontWeight: FontWeight.w400,
                                  color: MyColor.grayScale[500])),
                    ),
                  ),
                );
              }),
            ),
          ),
        );
      }),
    );
  }
}
