import 'package:get/get.dart';
import 'package:management/data/network/entity/demand/response/ListLogDemandOrTaskResponse.dart'
as log;

class ShowMoreLogScreenState {
  num demandId =0;
  var pName = ''.obs;
  var listLogDemand = <log.Data>[].obs;
  ShowMoreLogScreenState() {
    ///Initialize variables
    demandId = Get.arguments['demandId'];
    pName.value = Get.arguments['pName'];
  }
}
