import 'package:file_picker/file_picker.dart';
import 'package:get/get.dart';
import 'package:management/const/consts.dart';
import 'package:management/data/network/entity/demand/response/DemandOrTaskDetailResponse.dart';
import 'package:management/generated/assets.dart';
import 'package:management/data/network/entity/demand/response/ListChildDemandOrTaskResponse.dart'
    as children;
import 'package:management/data/network/entity/demand/response/ListLogDemandOrTaskResponse.dart'
    as log;

class DemandOrTaskDetailScreenState {
  var demandId = 0;
  var refreshIndictor = 0.obs;
  var listBg = [
    Assets.projectBg1,
    Assets.projectBg2,
    Assets.projectBg3,
    Assets.projectBg4,
  ];

  var listDemandState = {
    0: MyText.notStarted.tr,
    1: MyText.inProgress.tr,
    2: MyText.completed.tr,
    3: MyText.testing.tr
  };

  var listDemandPriority = {
    0: MyText.low.tr,
    1: MyText.common.tr,
    2: MyText.high.tr,
    3: MyText.urgency.tr
  };

  var listAttachment = <Attachments>[].obs;
  var listChildDemand = <children.Data>[].obs;
  var listLogDemand = <log.Data>[].obs;
  var taskDetail = Data().obs;
  var listAttachFiles = <PlatformFile>[].obs;
  // var description = ''.obs;
  // var name = ''.obs;
  // var status = RxNum(0);
  // var statusText = MyText.loading.obs;
  DemandOrTaskDetailScreenState() {
    ///Initialize variables
    demandId = Get.arguments['demandId'];
  }
}
