import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:management/const/consts.dart';
import 'package:management/const/icon.dart';
import 'package:management/const/init.dart';

import 'logic.dart';

class DemandOrTaskDetailScreenPage extends StatelessWidget {
  const DemandOrTaskDetailScreenPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final logic = Get.put(DemandOrTaskDetailScreenLogic());
    final state = Get.find<DemandOrTaskDetailScreenLogic>().state;

    return Scaffold(
      appBar: AppBar(
        title: Text(MyText.taskDetail.tr),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => logic.addTask(-1),
        child: const Icon(
          Icons.add,
          color: MyColor.white,
        ),
      ),
      body: Obx(() {
        return RefreshIndicator(
          onRefresh: () => logic.refreshDemandOrTaskDetailPage(),
          child: Container(
            padding: const EdgeInsets.all(24),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  ClipRRect(
                    borderRadius: const BorderRadius.all(Radius.circular(20)),
                    child: Image(
                      isAntiAlias: true,
                      image: logic.getBg(),
                      height: 150,
                      fit: BoxFit.fill,
                    ),
                  ),
                  const SizedBox(
                    height: 24,
                  ),
                  GestureDetector(
                    onTap: () => logic.changeDescription(),
                    child: Text(
                      state.taskDetail.value.description ?? MyText.loading,
                      style: Theme.of(context)
                          .textTheme
                          .bodyLarge
                          ?.copyWith(fontWeight: FontWeight.w400),
                    ),
                  ),
                  const SizedBox(
                    height: 12,
                  ),
                  Row(
                    children: [
                      SizedBox(
                        width: 120,
                        child: Row(
                          children: [
                            const Icon(
                              IconLight.Iconly_Light_Outline_Discount,
                              size: 18.5,
                            ),
                            const SizedBox(
                              width: 8,
                            ),
                            Text(
                              MyText.name.tr,
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyMedium
                                  ?.copyWith(
                                      color: Get.isDarkMode
                                          ? MyColor.white
                                          : MyColor.grayScale[700],
                                      fontWeight: FontWeight.w500),
                            ),
                          ],
                        ),
                      ),
                      Row(
                        children: [
                          Text(
                            state.taskDetail.value.name ?? MyText.loading,
                            style: Theme.of(context)
                                .textTheme
                                .bodyLarge
                                ?.copyWith(fontWeight: FontWeight.w500),
                          ),
                          IconButton(
                            onPressed: () => logic.changeName(),
                            icon: const Icon(
                              IconLight.Iconly_Light_Outline_Edit,
                              size: 20,
                              color: MyColor.primary,
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 12,
                  ),
                  Row(
                    children: [
                      SizedBox(
                        width: 120,
                        child: Row(
                          children: [
                            const Icon(
                              IconLight.Iconly_Light_Outline_Profile,
                              size: 18.5,
                            ),
                            const SizedBox(
                              width: 8,
                            ),
                            Text(
                              MyText.charge.tr,
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyMedium
                                  ?.copyWith(
                                      color: Get.isDarkMode
                                          ? MyColor.white
                                          : MyColor.grayScale[700],
                                      fontWeight: FontWeight.w500),
                            ),
                          ],
                        ),
                      ),
                      GestureDetector(
                        onTap: () => logic.changeCharge(),
                        child: Row(
                          children: [
                            CircleAvatar(
                              backgroundImage: NetworkImage(
                                  state.taskDetail.value.charge?.avatar ??
                                      GlobalInit.headImage),
                              radius: 14,
                              backgroundColor:
                                  Theme.of(context).scaffoldBackgroundColor,
                            ),
                            const SizedBox(
                              width: 8,
                            ),
                            Text(
                              state.taskDetail.value.charge?.nickName ??
                                  MyText.loading.tr,
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyLarge
                                  ?.copyWith(fontWeight: FontWeight.w500),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 12,
                  ),
                  Row(
                    children: [
                      SizedBox(
                        width: 120,
                        child: Row(
                          children: [
                            const Icon(
                              IconLight.Iconly_Light_Outline_Tick_Square,
                              size: 18.5,
                            ),
                            const SizedBox(
                              width: 8,
                            ),
                            Text(
                              MyText.status.tr,
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyMedium
                                  ?.copyWith(
                                      color: Get.isDarkMode
                                          ? MyColor.white
                                          : MyColor.grayScale[700],
                                      fontWeight: FontWeight.w500),
                            ),
                          ],
                        ),
                      ),
                      InputChip(
                        label: Text(state.listDemandState[
                                state.taskDetail.value.status] ??
                            MyText.loading),
                        onPressed: () => logic.changeStatus(),
                      )
                    ],
                  ),
                  const SizedBox(
                    height: 12,
                  ),
                  Row(
                    children: [
                      SizedBox(
                        width: 120,
                        child: Row(
                          children: [
                            const Icon(
                              IconLight.Iconly_Light_Outline_Swap,
                              size: 18.5,
                            ),
                            const SizedBox(
                              width: 8,
                            ),
                            Text(
                              MyText.priority.tr,
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyMedium
                                  ?.copyWith(
                                      color: Get.isDarkMode
                                          ? MyColor.white
                                          : MyColor.grayScale[700],
                                      fontWeight: FontWeight.w500),
                            ),
                          ],
                        ),
                      ),
                      InputChip(
                        label: Text(state.listDemandPriority[
                                state.taskDetail.value.priority] ??
                            MyText.loading),
                        onPressed: () => logic.changePriority(),
                      )
                    ],
                  ),
                  const SizedBox(
                    height: 12,
                  ),
                  Row(
                    children: [
                      SizedBox(
                        width: 120,
                        child: Row(
                          children: [
                            const Icon(
                              IconLight.Iconly_Light_Outline_Calendar,
                              size: 18.5,
                            ),
                            const SizedBox(
                              width: 8,
                            ),
                            Text(
                              MyText.dueDate.tr,
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyMedium
                                  ?.copyWith(
                                      color: Get.isDarkMode
                                          ? MyColor.white
                                          : MyColor.grayScale[700],
                                      fontWeight: FontWeight.w500),
                            ),
                          ],
                        ),
                      ),
                      Row(
                        children: [
                          Text(
                            logic.getEndTime(state.taskDetail.value.endTime),
                            style: Theme.of(context).textTheme.bodyLarge,
                          ),
                          IconButton(
                            onPressed: () => logic.changeEndTime(),
                            icon: const Icon(
                              IconLight.Iconly_Light_Outline_Edit,
                              size: 20,
                              color: MyColor.primary,
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                  const SizedBox(
                    height: 12,
                  ),
                  Row(
                    children: [
                      SizedBox(
                        width: 130,
                        child: Row(
                          children: [
                            const Icon(
                              IconLight.Iconly_Light_Outline_Document,
                              size: 18.5,
                            ),
                            const SizedBox(
                              width: 8,
                            ),
                            Text(
                              MyText.attachments.tr,
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyMedium
                                  ?.copyWith(
                                      color: Get.isDarkMode
                                          ? MyColor.white
                                          : MyColor.grayScale[700],
                                      fontWeight: FontWeight.w500),
                            ),
                          ],
                        ),
                      ),
                      IconButton(
                          onPressed: () => logic.addFiles(),
                          icon: const Icon(
                            IconLight.Iconly_Light_Outline_Paper_Plus,
                            size: 20,
                            color: MyColor.primary,
                          ))
                    ],
                  ),
                  const SizedBox(
                    height: 6,
                  ),
                  SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: List<Widget>.generate(
                            state.listAttachment.length, (index) {
                          return Container(
                            margin: const EdgeInsets.only(right: 8),
                            child: InputChip(
                              label:
                                  Text(state.listAttachment[index].name ?? ''),
                              onDeleted: () => logic
                                  .removeFile(state.listAttachment[index].id),
                              avatar: const Icon(
                                IconLight.Iconly_Light_Outline_Document,
                                size: 18,
                                color: MyColor.primary,
                              ),
                              deleteIcon: const Icon(
                                IconLight.Iconly_Light_Outline_Close_Square,
                                size: 18,
                                color: MyColor.primary,
                              ),
                            ),
                          );
                        })),
                  ),
                  const SizedBox(
                    height: 12,
                  ),
                  Visibility(
                    visible: state.listChildDemand.isNotEmpty,
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              '${MyText.subTask.tr}(${state.listChildDemand.length})',
                              style: Theme.of(context).textTheme.titleSmall,
                            ),
                            Visibility(
                              visible: state.listChildDemand.length > 5,
                              child: GestureDetector(
                                onTap: () => logic
                                    .seeAllDemand(state.taskDetail.value.name),
                                child: Text(
                                  MyText.seeAll.tr,
                                  style: Theme.of(context)
                                      .textTheme
                                      .titleSmall
                                      ?.copyWith(color: MyColor.info),
                                ),
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 12,
                        ),
                        Column(
                          children: List<Widget>.generate(
                              state.listChildDemand.length <= 5
                                  ? state.listChildDemand.length
                                  : 5, (index) {
                            return GestureDetector(
                              onTap: () => logic.jumpToNextDemand(
                                  state.listChildDemand[index].id),
                              child: Container(
                                margin:
                                    const EdgeInsets.symmetric(vertical: 12),
                                child: Card(
                                  color: Get.isDarkMode
                                      ? MyColor.dark3
                                      : MyColor.white,
                                  elevation: 10,
                                  shadowColor: const Color(0x1404060f),
                                  shape: const RoundedRectangleBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(20))),
                                  child: Padding(
                                    padding: const EdgeInsets.all(28.0),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                                state.listChildDemand[index]
                                                        .name ??
                                                    MyText.loading.tr,
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .titleMedium),
                                            IconButton(
                                                onPressed: () =>
                                                    logic.seeMoreDemand(
                                                        state.listChildDemand[
                                                            index]),
                                                icon: const Icon(
                                                  IconLight
                                                      .Iconly_Light_Outline_More_Circle,
                                                  size: 18.5,
                                                ))
                                          ],
                                        ),
                                        const SizedBox(
                                          height: 2,
                                        ),
                                        Text(
                                          state.listChildDemand[index]
                                                  .endTime ??
                                              ' ',
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyMedium
                                              ?.copyWith(
                                                  fontWeight: FontWeight.w400),
                                        ),
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Row(
                                              children: [
                                                CircleAvatar(
                                                  radius: 14,
                                                  backgroundColor: Theme.of(
                                                          context)
                                                      .scaffoldBackgroundColor,
                                                  backgroundImage: NetworkImage(
                                                      state
                                                              .listChildDemand[
                                                                  index]
                                                              .creator
                                                              ?.avatar ??
                                                          GlobalInit.headImage),
                                                ),
                                                const SizedBox(
                                                  width: 12,
                                                ),
                                                Text(
                                                  state.listChildDemand[index]
                                                          .creator?.nickName ??
                                                      ' ',
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .bodyMedium
                                                      ?.copyWith(
                                                          color: MyColor
                                                              .grayScale[500],
                                                          fontWeight:
                                                              FontWeight.w400),
                                                ),
                                              ],
                                            ),
                                            Row(
                                              children: [
                                                IconButton(
                                                    onPressed: () =>
                                                        logic.changePriority(),
                                                    icon: Icon(
                                                      Icons.flag_outlined,
                                                      size: 20,
                                                      color: logic
                                                          .getFlagPriorityColor(
                                                              state
                                                                  .listChildDemand[
                                                                      index]
                                                                  .priority),
                                                    )),
                                                Text(
                                                  state.listDemandPriority[state
                                                      .listChildDemand[index]
                                                      .priority]!,
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .bodyMedium
                                                      ?.copyWith(
                                                          color: logic
                                                              .getFlagPriorityColor(state
                                                                  .listChildDemand[
                                                                      index]
                                                                  .priority),
                                                          fontWeight:
                                                              FontWeight.w400),
                                                ),
                                              ],
                                            ),
                                            IconButton(
                                                onPressed: () =>
                                                    logic.seeDemandLog(state
                                                        .listChildDemand[index]
                                                        .id),
                                                icon: const Icon(
                                                  IconLight
                                                      .Iconly_Light_Outline_Chat,
                                                  size: 20,
                                                ))
                                          ],
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            );
                          }),
                        )
                      ],
                    ),
                  ),
                  Visibility(
                    visible: state.listLogDemand.isNotEmpty,
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              '${MyText.operationLog.tr}  (${state.listLogDemand.length})',
                              style: Theme.of(context).textTheme.titleSmall,
                            ),
                            Visibility(
                              visible: state.listLogDemand.length > 4,
                              child: GestureDetector(
                                onTap: () => logic
                                    .seeAllLog(state.taskDetail.value.name),
                                child: Text(
                                  MyText.seeAll.tr,
                                  style: Theme.of(context)
                                      .textTheme
                                      .titleSmall
                                      ?.copyWith(color: MyColor.info),
                                ),
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 12,
                        ),
                        Column(
                          children: List<Widget>.generate(
                              state.listLogDemand.length <= 4
                                  ? state.listLogDemand.length
                                  : 4, (index) {
                            return GestureDetector(
                              onTap: () => logic.jumpToNextDemandByLog(
                                  state.listLogDemand[index]),
                              child: Container(
                                margin:
                                    const EdgeInsets.symmetric(vertical: 12),
                                child: Card(
                                  color: Get.isDarkMode
                                      ? MyColor.dark3
                                      : MyColor.white,
                                  shadowColor: const Color(0x1404060f),
                                  elevation: 10,
                                  shape: const RoundedRectangleBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(20))),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      ListTile(
                                        title: Text(
                                            logic.logText1(
                                                state.listLogDemand[index]),
                                            style: Theme.of(context)
                                                .textTheme
                                                .bodyMedium
                                                ?.copyWith(
                                                  fontWeight: FontWeight.w400,
                                                )),
                                        subtitle: Text(
                                            logic.logText2(
                                                state.listLogDemand[index]),
                                            style: Theme.of(context)
                                                .textTheme
                                                .bodyMedium
                                                ?.copyWith(
                                                    fontWeight: FontWeight.w400,
                                                    color: MyColor
                                                        .grayScale[500])),
                                        leading: CircleAvatar(
                                          radius: 24,
                                          backgroundColor: Theme.of(context)
                                              .scaffoldBackgroundColor,
                                          backgroundImage: NetworkImage(state
                                                  .listLogDemand[index]
                                                  .companyUser
                                                  ?.avatar ??
                                              GlobalInit.headImage),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            );
                          }),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      }),
    );
  }
}
