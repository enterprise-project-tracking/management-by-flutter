import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:management/const/consts.dart';
import 'package:management/data/network/entity/demand/request/CreateDemandOrTaskRequestParams.dart';
import 'package:management/data/network/entity/project/response/AllUserResponse.dart';

class CreateDemandScreenState {
  var status = -1;
  num projectId = 0;
  num pid = 0;

  var listDemandState = {
    0: MyText.notStarted.tr,
    1: MyText.inProgress.tr,
    2: MyText.completed.tr,
    3: MyText.testing.tr
  };

  var listDemandPriority = {
    0: MyText.low.tr,
    1: MyText.common.tr,
    2: MyText.high.tr,
    3: MyText.urgency.tr
  };

  var taskTitle = ''.obs;
  var taskStartTime = ''.obs;
  var taskEndTime = ''.obs;
  var demandDescription = ''.obs;
  var chargeId = ''.obs;
  var chargeUrl = ''.obs;
  var listAttachFiles = <PlatformFile>[].obs;
  var listAttachmentsParams = <Attachments>[].obs;
  var demandStatus = 1.obs;
  var demandPriority = 1.obs;

  final statusController = TextEditingController();
  final priorityStatusController = TextEditingController();

  var listNumbers = <Data>[].obs;

  CreateDemandScreenState() {
    ///Initialize variables
    status = Get.arguments['demandStatus'] ?? 1;
    projectId = Get.arguments['projectId'] ?? 0;
    pid = Get.arguments['pid'] ?? 0;
  }
}
