import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker_plus/flutter_datetime_picker_plus.dart';
import 'package:get/get.dart';
import 'package:management/const/consts.dart';
import 'package:management/const/icon.dart';
import 'package:management/element/InputField.dart';

import 'logic.dart';

class CreateDemandScreenPage extends StatelessWidget {
  const CreateDemandScreenPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final logic = Get.put(CreateDemandScreenLogic());
    final state = Get.find<CreateDemandScreenLogic>().state;
    final startController = TextEditingController();
    final endController = TextEditingController();

    return Scaffold(
      appBar: AppBar(
        title: Text(MyText.addDemand.tr),
      ),
      body: Obx(() {
        return Container(
          padding: const EdgeInsets.symmetric(horizontal: 24),
          child: SingleChildScrollView(
            child: Column(children: [
              const SizedBox(
                height: 24,
              ),
              MyTextField(
                labelText: MyText.taskTitle.tr,
                onChanged: (value) {
                  state.taskTitle.value = value;
                },
                hintText: MyText.pex.trParams({'x': MyText.taskTitle.tr}),
              ),
              MyTextField(
                labelText: MyText.start.tr,
                hintText: MyText.pspxd.trParams({'x': MyText.start.tr}),
                controller: startController,
                suffixIcon: IconLight.Iconly_Light_Outline_Calendar,
                onTapSuffix: () {
                  DatePicker.showDatePicker(context,
                      showTitleActions: true,
                      minTime: DateTime(2000, 1, 1),
                      maxTime: DateTime(2099, 12, 31), onConfirm: (date) {
                    startController.text =
                        '${date.year}-${date.month}-${date.day}';
                    state.taskStartTime.value = date.toIso8601String();
                  }, currentTime: DateTime.now(), locale: LocaleType.zh);
                },
              ),
              MyTextField(
                labelText: MyText.end.tr,
                hintText: MyText.pspxd.trParams({'x': MyText.end.tr}),
                controller: endController,
                suffixIcon: IconLight.Iconly_Light_Outline_Calendar,
                onTapSuffix: () {
                  DatePicker.showDatePicker(Get.context!,
                      showTitleActions: true,
                      minTime: DateTime(2000, 1, 1),
                      maxTime: DateTime(2099, 12, 31), onConfirm: (date) {
                    endController.text =
                        '${date.year}-${date.month}-${date.day}';
                    state.taskEndTime.value = date.toIso8601String();
                  }, currentTime: DateTime.now(), locale: LocaleType.zh);
                },
              ),
              MyTextField(
                labelText: MyText.demandStatus.tr,
                suffixIcon: IconLight.Iconly_Light_Outline_More_Square,
                onTapSuffix: () {
                  logic.selectStatus();
                },
                controller: state.statusController,
                hintText: MyText.pex.trParams({'x': MyText.demandStatus.tr}),
              ),
              MyTextField(
                labelText: MyText.demandPriority.tr,
                suffixIcon: IconLight.Iconly_Light_Outline_More_Square,
                onTapSuffix: () {
                  logic.selectPriority();
                },
                controller: state.priorityStatusController,
                hintText: MyText.pex.trParams({'x': MyText.demandPriority.tr}),
              ),
              SizedBox(
                height: 150,
                child: MyTextField(
                  labelText: MyText.demandDescription.tr,
                  onChanged: (value) {
                    state.demandDescription.value = value;
                  },
                  expands: true,
                  hintText:
                      MyText.pex.trParams({'x': MyText.demandDescription.tr}),
                ),
              ),
              Container(
                child: state.chargeId.value.isNotEmpty
                    ? SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Text(MyText.charge.tr),
                            const SizedBox(
                              width: 50,
                            ),
                            CircleAvatar(
                              backgroundImage:
                                  NetworkImage(state.chargeUrl.value),
                              radius: 15,
                              backgroundColor:
                                  Theme.of(context).scaffoldBackgroundColor,
                            ),
                            const SizedBox(
                              width: 50,
                            ),
                            IconButton(
                              onPressed: () => logic.selectCharge(),
                              icon: const Icon(
                                IconLight.Iconly_Light_Outline_Plus,
                                size: 18,
                                color: MyColor.primary,
                              ),
                            )
                          ],
                        ),
                      )
                    : TextButton(
                        onPressed: () => logic.selectCharge(),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            const Icon(
                              IconLight.Iconly_Light_Outline_Plus,
                              size: 18,
                              color: MyColor.primary,
                            ),
                            const SizedBox(
                              width: 12,
                            ),
                            Text(
                              MyText.setCharge.tr,
                              style: Theme.of(context).textTheme.bodyLarge,
                            ),
                          ],
                        ),
                      ),
              ),
              TextButton(
                onPressed: () => logic.addAttachments(),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Icon(
                      IconLight.Iconly_Light_Outline_Paper_Upload,
                      size: 18,
                      color: MyColor.primary,
                    ),
                    const SizedBox(
                      width: 12,
                    ),
                    Text(
                      MyText.addAttachments.tr,
                      style: Theme.of(context).textTheme.bodyLarge,
                    ),
                  ],
                ),
              ),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: List<Widget>.generate(
                    state.listAttachFiles.length,
                    (int index) {
                      return Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 2),
                        child: InputChip(
                          visualDensity: const VisualDensity(
                              horizontal: 0.0, vertical: -4),
                          label: Text(
                            state.listAttachFiles[index].name,
                            style: Theme.of(context)
                                .textTheme
                                .bodyMedium
                                ?.copyWith(color: MyColor.primary),
                          ),
                          avatar: const Icon(
                            IconLight.Iconly_Light_Outline_Document,
                            size: 18,
                            color: MyColor.primary,
                          ),
                          deleteIcon: const Icon(
                            IconLight.Iconly_Light_Outline_Close_Square,
                            size: 18,
                            color: MyColor.primary,
                          ),
                          onDeleted: () {
                            state.listAttachFiles.removeAt(index);
                          },
                        ),
                      );
                    },
                  ).toList(),
                ),
              ),
              SizedBox(
                width: double.maxFinite,
                child: ElevatedButton(
                    onPressed: () => logic.submit(),
                    child: Text(MyText.submit.tr)),
              ),
              const SizedBox(
                height: 10,
              )
            ]),
          ),
        );
      }),
    );
  }
}
