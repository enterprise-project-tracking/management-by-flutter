import 'package:get/get.dart';
import 'package:management/const/consts.dart';
import 'package:management/data/network/entity/project/response/ProjectDetailResponse.dart'
    as detail;
import 'package:management/data/network/entity/demand/response/ListProjectDemandResponse.dart'
    as demand_list;
import 'package:management/generated/assets.dart';

class ProjectDetailScreenState {
  var projectId = 0;

  var listColor = [
    MyColor.gradientBlue,
    MyColor.gradientRed,
    MyColor.gradientGreen,
    MyColor.gradientYellow
  ];

  var listBg = [
    Assets.projectBg1,
    Assets.projectBg2,
    Assets.projectBg3,
    Assets.projectBg4,
  ];

  var listDemandState = {
    0: MyText.notStarted.tr,
    1: MyText.inProgress.tr,
    2: MyText.completed.tr,
    3: MyText.testing.tr
  };

  var listDemandStateCount = {0: 0, 1: 0, 2: 0, 3: 0}.obs;
  var listDemandByState = {
    0: <demand_list.Records>[],
    1: <demand_list.Records>[],
    2: <demand_list.Records>[],
    3: <demand_list.Records>[],
  }.obs;

  var isConclusion = false;

  var projectDetail = detail.Data().obs;
  var demandList = demand_list.Data().obs;

  ProjectDetailScreenState() {
    ///Initialize variables
    projectId = Get.arguments['projectId'];
  }
}
