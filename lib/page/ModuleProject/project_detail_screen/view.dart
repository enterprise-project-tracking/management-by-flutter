import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:management/const/consts.dart';
import 'package:management/const/icon.dart';
import 'package:management/const/init.dart';
import 'package:management/generated/assets.dart';
import 'dart:math';

import 'logic.dart';

class ProjectDetailScreenPage extends StatelessWidget {
  const ProjectDetailScreenPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final logic = Get.put(ProjectDetailScreenLogic());
    final state = Get.find<ProjectDetailScreenLogic>().state;

    return Obx(() {
      return Scaffold(
        body: RefreshIndicator(
          onRefresh: () => logic.refreshProjectDetailPage(),
          child: CustomScrollView(
            slivers: [
              SliverAppBar(
                stretch: true,
                floating: true,
                expandedHeight: 200,
                flexibleSpace: FlexibleSpaceBar(
                  background: Stack(
                    alignment: AlignmentDirectional.bottomStart,
                    children: [
                      Image(
                        isAntiAlias: true,
                        image: logic.getBg(),
                        width: double.infinity,
                        height: 200 + kToolbarHeight,
                        fit: BoxFit.fill,
                      ),
                      Positioned(
                        left: 24,
                        bottom: 24,
                        child: ClipOval(
                          child: FadeInImage(
                            image: logic.getLogo(),
                            fit: BoxFit.fill,
                            width: 40,
                            height: 40,
                            placeholderFit: BoxFit.scaleDown,
                            placeholder: const AssetImage(Assets.iconAvator),
                          ),
                        ),
                      ),
                      Positioned(
                        width: 28,
                        height: 28,
                        right: 76,
                        bottom: 30,
                        child: CircleAvatar(
                          backgroundImage: NetworkImage(GlobalInit.headImage),
                          radius: 56,
                          backgroundColor:
                              Theme.of(context).scaffoldBackgroundColor,
                        ),
                      ),
                      Positioned(
                        width: 28,
                        height: 28,
                        right: 59,
                        bottom: 30,
                        child: CircleAvatar(
                          backgroundImage: NetworkImage(GlobalInit.headImage),
                          radius: 56,
                          backgroundColor:
                              Theme.of(context).scaffoldBackgroundColor,
                        ),
                      ),
                      Positioned(
                        width: 28,
                        height: 28,
                        right: 41,
                        bottom: 30,
                        child: CircleAvatar(
                          backgroundImage: NetworkImage(GlobalInit.headImage),
                          radius: 56,
                          backgroundColor:
                              Theme.of(context).scaffoldBackgroundColor,
                        ),
                      ),
                      Positioned(
                        width: 28,
                        height: 28,
                        right: 24,
                        bottom: 30,
                        child: CircleAvatar(
                          radius: 56,
                          backgroundColor:
                              Theme.of(context).scaffoldBackgroundColor,
                          backgroundImage: NetworkImage(
                              state.projectDetail.value.charge?.avatar ??
                                  GlobalInit.headImage),
                        ),
                      ),
                    ],
                  ),
                ),
                actions: [
                  IconButton(
                    color: MyColor.white,
                    focusColor: MyColor.primary,
                    onPressed: () {},
                    icon: const Icon(IconLight.Iconly_Light_Outline_Search,
                        size: 28),
                  ),
                  IconButton(
                    color: MyColor.white,
                    focusColor: MyColor.primary,
                    onPressed: () => logic.seeMore(),
                    icon: const Icon(IconLight.Iconly_Light_Outline_More_Circle,
                        size: 28),
                  ),
                ],
                onStretchTrigger: () => logic.refreshProjectDetailPage(),
              ),
              SliverPadding(
                padding: const EdgeInsets.all(28.0),
                sliver: SliverToBoxAdapter(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                              state.projectDetail.value.name ??
                                  MyText.loading.tr,
                              style: Theme.of(context).textTheme.titleMedium),
                        ],
                      ),
                      const SizedBox(
                        height: 2,
                      ),
                      Text(
                        state.projectDetail.value.description ??
                            MyText.loading.tr,
                        style: Theme.of(context)
                            .textTheme
                            .bodyMedium
                            ?.copyWith(fontWeight: FontWeight.w400),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Chip(
                              backgroundColor: logic.getMainColor(),
                              label: Text(
                                  "${state.projectDetail.value.finishDemand ?? MyText.loading.tr} / ${state.projectDetail.value.totalDemand ?? MyText.loading.tr}"),
                              side: BorderSide.none,
                              labelStyle: Theme.of(context)
                                  .textTheme
                                  .bodyMedium
                                  ?.copyWith(
                                      fontWeight: FontWeight.w600,
                                      color: MyColor.white)),
                          Text(
                            logic.getFinishDay(),
                            style: Theme.of(context)
                                .textTheme
                                .bodySmall
                                ?.copyWith(
                                    fontWeight: FontWeight.w400,
                                    color: MyColor.grayScale[500]),
                            textAlign: TextAlign.end,
                          )
                        ],
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      LinearProgressIndicator(
                        minHeight: 6,
                        value: logic.getProgress(),
                        backgroundColor: MyColor.grayScale[100],
                        color: logic.getMainColor(),
                        borderRadius: BorderRadius.circular(56),
                      )
                    ],
                  ),
                ),
              ),
              SliverToBoxAdapter(
                  child: SingleChildScrollView(
                padding: const EdgeInsets.only(left: 24),
                scrollDirection: Axis.horizontal,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: List<Widget>.generate(4, (rowIndex) {
                    return Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color:
                            Get.isDarkMode ? MyColor.dark2 : MyColor.backBlue,
                      ),
                      width: 320,
                      margin: const EdgeInsets.only(right: 24),
                      child: Column(
                        children: [
                          const SizedBox(
                            height: 24,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Row(
                                children: [
                                  Text(
                                    state.listDemandState[rowIndex]!,
                                    style:
                                        Theme.of(context).textTheme.titleSmall,
                                  ),
                                  const SizedBox(
                                    width: 3,
                                  ),
                                  Chip(
                                    label: Text(state
                                        .listDemandStateCount[rowIndex]
                                        .toString()),
                                    materialTapTargetSize:
                                        MaterialTapTargetSize.shrinkWrap,
                                  ),
                                ],
                              ),
                              IconButton(
                                  onPressed: () => logic.addTask(rowIndex),
                                  icon: const Icon(
                                      IconLight.Iconly_Light_Outline_Plus))
                            ],
                          ),
                          SingleChildScrollView(
                            child: Column(
                              children: List<Widget>.generate(
                                  state.listDemandStateCount[rowIndex]!,
                                  (index) {
                                return GestureDetector(
                                  onTap: () => logic.jumpToDemandDetail(state
                                      .listDemandByState[rowIndex]
                                      ?.elementAt(index)
                                      .id),
                                  child: Container(
                                    decoration: index == 0
                                        ? BoxDecoration(
                                            color: Get.isDarkMode
                                                ? MyColor.dark3
                                                : MyColor.primary[100],
                                            borderRadius:
                                                BorderRadius.circular(20))
                                        : null,
                                    margin: const EdgeInsets.symmetric(
                                        horizontal: 24, vertical: 12),
                                    // height: index == 0 ? 326 : null,
                                    child: Transform.rotate(
                                      angle: index == 0 ? 3.14 / 30 : 0,
                                      alignment: Alignment.bottomRight,
                                      child: Card(
                                        color: Get.isDarkMode
                                            ? MyColor.dark3
                                            : MyColor.white,
                                        elevation: index == 0 ? 6 : 3,
                                        shadowColor: const Color(0x0d04060f),
                                        shape: const RoundedRectangleBorder(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(20))),
                                        child: Padding(
                                          padding: const EdgeInsets.all(28.0),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: [
                                                  Text(
                                                      state.listDemandByState[
                                                                  rowIndex]
                                                              ?.elementAt(index)
                                                              .name ??
                                                          MyText.loading.tr,
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .titleMedium),
                                                  IconButton(
                                                      onPressed: () => logic
                                                          .seeMoreDemand(state
                                                              .listDemandByState[
                                                                  rowIndex]
                                                              ?.elementAt(
                                                                  index)),
                                                      icon: const Icon(
                                                        IconLight
                                                            .Iconly_Light_Outline_More_Circle,
                                                        size: 18.5,
                                                      ))
                                                ],
                                              ),
                                              const SizedBox(
                                                height: 2,
                                              ),
                                              Text(
                                                '${MyText.dueDate.tr}: ${state.listDemandByState[rowIndex]?.elementAt(index).endTime ?? ' '}',
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .bodyMedium
                                                    ?.copyWith(
                                                        fontWeight:
                                                            FontWeight.w400),
                                              ),
                                              index == 0
                                                  ? Container(
                                                      margin:
                                                          const EdgeInsets.only(
                                                              top: 8),
                                                      child: ClipRRect(
                                                        borderRadius:
                                                            const BorderRadius
                                                                .all(
                                                                Radius.circular(
                                                                    20)),
                                                        child: Image(
                                                          isAntiAlias: true,
                                                          image: logic.getBg(),
                                                          height: 150,
                                                          fit: BoxFit.fill,
                                                        ),
                                                      ),
                                                    )
                                                  : Container(),
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: [
                                                  CircleAvatar(
                                                    radius: 14,
                                                    backgroundColor: Theme.of(
                                                            context)
                                                        .scaffoldBackgroundColor,
                                                    backgroundImage: NetworkImage(
                                                        state.listDemandByState[
                                                                    rowIndex]
                                                                ?.elementAt(
                                                                    index)
                                                                .charge
                                                                ?.avatar ??
                                                            GlobalInit
                                                                .headImage),
                                                  ),
                                                  Text(
                                                    state.listDemandByState[
                                                                rowIndex]
                                                            ?.elementAt(index)
                                                            .charge
                                                            ?.nickName ??
                                                        ' ',
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .bodyMedium
                                                        ?.copyWith(
                                                            color: MyColor
                                                                .grayScale[500],
                                                            fontWeight:
                                                                FontWeight
                                                                    .w400),
                                                  ),
                                                  IconButton(
                                                      onPressed: () => logic
                                                          .changePriority(),
                                                      icon: Icon(
                                                        Icons.flag_outlined,
                                                        size: 20,
                                                        color: logic
                                                            .getFlagPriorityColor(state
                                                                .listDemandByState[
                                                                    rowIndex]
                                                                ?.elementAt(
                                                                    index)
                                                                .priority),
                                                      )),
                                                  IconButton(
                                                      onPressed: () =>
                                                          logic.seeDemandLog(state
                                                              .listDemandByState[
                                                          rowIndex]
                                                              ?.elementAt(
                                                              index).id),
                                                      icon: const Icon(
                                                        IconLight
                                                            .Iconly_Light_Outline_Chat,
                                                        size: 20,
                                                      ))
                                                ],
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                );
                              }),
                            ),
                          ),
                          const SizedBox(
                            height: 24,
                          ),
                        ],
                      ),
                    );
                  }).toList(),
                ),
              ))
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () => logic.addTask(-1),
          child: const Icon(
            Icons.add,
            color: MyColor.white,
          ),
        ),
      );
    });
  }
}
