import 'package:file_picker/file_picker.dart';
import 'package:get/get.dart';
import 'package:management/data/network/entity/project/request/ProjectConclusionRequestParams.dart';
import 'package:management/data/network/entity/project/response/AllUserResponse.dart';
import 'package:management/data/network/entity/project/response/ProjectConclusionResponse.dart'
    as detail;

class ProjectConclusionScreenState {
  var projectId = 0;
  var isConclusion = false.obs;
  var legalPersonId = ''.obs;
  var applyCompanyUserId = ''.obs;
  var legalPersonUrl = ''.obs;
  var applyCompanyUserUrl = ''.obs;
  var projectOutcome = ''.obs;
  var listConclusionFile = <ConclusionFiles>[].obs;
  var listConclusionPlatformFiles = <PlatformFile>[].obs;
  var listNumbers = <Data>[].obs;
  var conclusionDetail = detail.Data().obs;
  ProjectConclusionScreenState() {
    ///Initialize variables
    projectId = Get.arguments['projectId'];
    isConclusion.value = Get.arguments['isConclusion'];
  }
}
