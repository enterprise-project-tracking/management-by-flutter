import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:management/const/consts.dart';
import 'package:management/const/icon.dart';
import 'package:management/const/init.dart';
import 'package:management/element/InputField.dart';
import 'package:management/element/toast.dart';

import 'logic.dart';

class ProjectConclusionScreenPage extends StatelessWidget {
  const ProjectConclusionScreenPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final logic = Get.put(ProjectConclusionScreenLogic());
    final state = Get.find<ProjectConclusionScreenLogic>().state;

    return Obx(() {
      return Scaffold(
        appBar: AppBar(
          title: Text(state.isConclusion.value
              ? MyText.viewProjectResults.tr
              : MyText.finalProject.tr),
        ),
        body: Obx(() {
          return state.isConclusion.value
              ? Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 24, vertical: 24),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            MyText.projectName.tr,
                            style: Theme.of(context)
                                .textTheme
                                .bodyMedium
                                ?.copyWith(
                                    color: Get.isDarkMode
                                        ? MyColor.white
                                        : MyColor.grayScale[700],
                                    fontWeight: FontWeight.w500),
                          ),
                          Text(
                            state.conclusionDetail.value.projectName ??
                                MyText.loading.tr,
                            style: Theme.of(context)
                                .textTheme
                                .bodyLarge
                                ?.copyWith(fontWeight: FontWeight.w500),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            MyText.applyCompanyUser.tr,
                            style: Theme.of(context)
                                .textTheme
                                .bodyMedium
                                ?.copyWith(
                                    color: Get.isDarkMode
                                        ? MyColor.white
                                        : MyColor.grayScale[700],
                                    fontWeight: FontWeight.w500),
                          ),
                          Row(
                            children: [
                              CircleAvatar(
                                backgroundImage: NetworkImage(state
                                        .conclusionDetail
                                        .value
                                        .applyCompanyUser
                                        ?.avatar ??
                                    GlobalInit.headImage),
                                radius: 15,
                                backgroundColor:
                                    Theme.of(context).scaffoldBackgroundColor,
                              ),
                              const SizedBox(
                                width: 15,
                              ),
                              Text(
                                state.conclusionDetail.value.applyCompanyUser
                                        ?.nickName ??
                                    MyText.loading.tr,
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyLarge
                                    ?.copyWith(fontWeight: FontWeight.w500),
                              ),
                            ],
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            MyText.legalPerson.tr,
                            style: Theme.of(context)
                                .textTheme
                                .bodyMedium
                                ?.copyWith(
                                    color: Get.isDarkMode
                                        ? MyColor.white
                                        : MyColor.grayScale[700],
                                    fontWeight: FontWeight.w500),
                          ),
                          Row(
                            children: [
                              CircleAvatar(
                                backgroundImage: NetworkImage(state
                                        .conclusionDetail
                                        .value
                                        .legalPerson
                                        ?.avatar ??
                                    GlobalInit.headImage),
                                radius: 15,
                                backgroundColor:
                                    Theme.of(context).scaffoldBackgroundColor,
                              ),
                              const SizedBox(
                                width: 15,
                              ),
                              Text(
                                state.conclusionDetail.value.legalPerson
                                        ?.nickName ??
                                    MyText.loading.tr,
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyLarge
                                    ?.copyWith(fontWeight: FontWeight.w500),
                              ),
                            ],
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            MyText.projectOutcome.tr,
                            style: Theme.of(context)
                                .textTheme
                                .bodyMedium
                                ?.copyWith(
                                    color: Get.isDarkMode
                                        ? MyColor.white
                                        : MyColor.grayScale[700],
                                    fontWeight: FontWeight.w500),
                          ),
                          Text(
                            state.conclusionDetail.value.projectOutcome ??
                                MyText.loading.tr,
                            style: Theme.of(context)
                                .textTheme
                                .bodyLarge
                                ?.copyWith(fontWeight: FontWeight.w500),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            MyText.end.tr,
                            style: Theme.of(context)
                                .textTheme
                                .bodyMedium
                                ?.copyWith(
                                    color: Get.isDarkMode
                                        ? MyColor.white
                                        : MyColor.grayScale[700],
                                    fontWeight: FontWeight.w500),
                          ),
                          Text(
                            state.conclusionDetail.value.createTime ??
                                MyText.loading.tr,
                            style: Theme.of(context)
                                .textTheme
                                .bodyLarge
                                ?.copyWith(fontWeight: FontWeight.w500),
                          ),
                        ],
                      ),
                      Center(
                        child: Text(
                          MyText.files.tr,
                          style: Theme.of(context)
                              .textTheme
                              .bodyMedium
                              ?.copyWith(
                                  color: Get.isDarkMode
                                      ? MyColor.white
                                      : MyColor.grayScale[700],
                                  fontWeight: FontWeight.w500),
                        ),
                      ),
                      SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: List<Widget>.generate(
                            state.conclusionDetail.value.conclusionFiles
                                    ?.length ??
                                0,
                            (int index) {
                              return Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 2),
                                child: InputChip(
                                  visualDensity: const VisualDensity(
                                      horizontal: 0.0, vertical: -4),
                                  label: Text(
                                    state.conclusionDetail.value
                                            .conclusionFiles?[index].name ??
                                        MyText.loadingFailure.tr,
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodyMedium
                                        ?.copyWith(color: MyColor.primary),
                                  ),
                                  avatar: const Icon(
                                    IconLight.Iconly_Light_Outline_Document,
                                    size: 18,
                                    color: MyColor.primary,
                                  ),
                                  onPressed: () {
                                    Utils.toastWarning("暂不支持查看");
                                  },
                                ),
                              );
                            },
                          ).toList(),
                        ),
                      ),
                    ],
                  ),
                )
              : Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 24, vertical: 24),
                  child: Column(
                    children: [
                      SizedBox(
                        height: 200,
                        child: MyTextField(
                          labelText: MyText.projectOutcome.tr,
                          onChanged: (value) {
                            state.projectOutcome.value = value;
                          },
                          expands: true,
                          hintText: MyText.pex
                              .trParams({'x': MyText.projectOutcome.tr}),
                        ),
                      ),
                      Container(
                        child: state.legalPersonId.value.isNotEmpty
                            ? SingleChildScrollView(
                                scrollDirection: Axis.horizontal,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Text(MyText.legalPerson.tr),
                                    const SizedBox(
                                      width: 50,
                                    ),
                                    CircleAvatar(
                                      backgroundImage: NetworkImage(
                                          state.legalPersonUrl.value),
                                      radius: 15,
                                      backgroundColor: Theme.of(context)
                                          .scaffoldBackgroundColor,
                                    ),
                                    const SizedBox(
                                      width: 50,
                                    ),
                                    IconButton(
                                      onPressed: () =>
                                          logic.selectLegalPerson(),
                                      icon: const Icon(
                                        IconLight.Iconly_Light_Outline_Plus,
                                        size: 18,
                                        color: MyColor.primary,
                                      ),
                                    )
                                  ],
                                ),
                              )
                            : TextButton(
                                onPressed: () => logic.selectLegalPerson(),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    const Icon(
                                      IconLight.Iconly_Light_Outline_Plus,
                                      size: 18,
                                      color: MyColor.primary,
                                    ),
                                    const SizedBox(
                                      width: 12,
                                    ),
                                    Text(
                                      MyText.setLegalPerson.tr,
                                      style:
                                          Theme.of(context).textTheme.bodyLarge,
                                    ),
                                  ],
                                ),
                              ),
                      ),
                      Container(
                        child: state.applyCompanyUserId.value.isNotEmpty
                            ? SingleChildScrollView(
                                scrollDirection: Axis.horizontal,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Text(MyText.applyCompanyUser.tr),
                                    const SizedBox(
                                      width: 50,
                                    ),
                                    CircleAvatar(
                                      backgroundImage: NetworkImage(
                                          state.applyCompanyUserUrl.value),
                                      radius: 15,
                                      backgroundColor: Theme.of(context)
                                          .scaffoldBackgroundColor,
                                    ),
                                    const SizedBox(
                                      width: 50,
                                    ),
                                    IconButton(
                                      onPressed: () =>
                                          logic.selectApplyCompanyUser(),
                                      icon: const Icon(
                                        IconLight.Iconly_Light_Outline_Plus,
                                        size: 18,
                                        color: MyColor.primary,
                                      ),
                                    )
                                  ],
                                ),
                              )
                            : TextButton(
                                onPressed: () => logic.selectApplyCompanyUser(),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    const Icon(
                                      IconLight.Iconly_Light_Outline_Plus,
                                      size: 18,
                                      color: MyColor.primary,
                                    ),
                                    const SizedBox(
                                      width: 12,
                                    ),
                                    Text(
                                      MyText.setApplyCompanyUser.tr,
                                      style:
                                          Theme.of(context).textTheme.bodyLarge,
                                    ),
                                  ],
                                ),
                              ),
                      ),
                      TextButton(
                        onPressed: () => logic.addConclusionFiles(),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            const Icon(
                              IconLight.Iconly_Light_Outline_Paper_Upload,
                              size: 18,
                              color: MyColor.primary,
                            ),
                            const SizedBox(
                              width: 12,
                            ),
                            Text(
                              MyText.addConferenceResolutions.tr,
                              style: Theme.of(context).textTheme.bodyLarge,
                            ),
                          ],
                        ),
                      ),
                      SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: List<Widget>.generate(
                            state.listConclusionPlatformFiles.length,
                            (int index) {
                              return Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 2),
                                child: InputChip(
                                  visualDensity: const VisualDensity(
                                      horizontal: 0.0, vertical: -4),
                                  label: Text(
                                    state.listConclusionPlatformFiles[index]
                                        .name,
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodyMedium
                                        ?.copyWith(color: MyColor.primary),
                                  ),
                                  avatar: const Icon(
                                    IconLight.Iconly_Light_Outline_Document,
                                    size: 18,
                                    color: MyColor.primary,
                                  ),
                                  deleteIcon: const Icon(
                                    IconLight.Iconly_Light_Outline_Close_Square,
                                    size: 18,
                                    color: MyColor.primary,
                                  ),
                                  onDeleted: () {
                                    state.listConclusionPlatformFiles
                                        .removeAt(index);
                                  },
                                ),
                              );
                            },
                          ).toList(),
                        ),
                      ),
                      SizedBox(
                        width: double.maxFinite,
                        child: ElevatedButton(
                            onPressed: () => logic.submit(),
                            child: Text(MyText.submit.tr)),
                      ),
                    ],
                  ),
                );
        }),
      );
    });
  }
}
