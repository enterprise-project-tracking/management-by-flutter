import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:management/const/consts.dart';
import 'package:management/const/theme.dart';
import 'package:management/main.dart';

class WelcomeScreenLogic extends GetxController {
  void continueWithQQ() {
    // var locale = const Locale('zh', 'CN');
    // Get.updateLocale(locale);
    // Get.offAllNamed('/');
    Get.snackbar("敬请期待", "暂不支持此QQ功能",
        backgroundGradient: LinearGradient(
            colors: [MyColor.gradientBlue[0]!, MyColor.gradientBlue[1]!]));
    Get.changeTheme(AppTheme.lightTheme);
  }

  void continueWithWeChat() {
    // var locale = const Locale('en', 'US');
    // Get.updateLocale(locale);
    // Get.offAllNamed('/');
    Get.snackbar("敬请期待", "暂不支持此WeChat功能",
        colorText: MyColor.white,
        backgroundGradient: LinearGradient(
            colors: [MyColor.gradientGreen[0]!, MyColor.gradientGreen[1]!]));
    Get.changeTheme(AppTheme.darkTheme);
  }

  void continueWithPassword() {
    Get.toNamed(RouteConfig.signInScreen);
  }

  void signUp() {
    Get.toNamed(RouteConfig.signUpScreen);
  }
}
