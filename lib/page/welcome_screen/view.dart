import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:management/const/consts.dart';
import 'package:management/generated/assets.dart';

import 'logic.dart';

class WelcomeScreenPage extends StatelessWidget {
  const WelcomeScreenPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final logic = Get.put(WelcomeScreenLogic());

    return Scaffold(
      appBar: AppBar(
        title: const Text(" "),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const SizedBox(
            height: 100,
          ),
          SizedBox(
              width: double.infinity,
              child: Text(
                MyText.welcomeTitle.tr,
                style: Theme.of(context).textTheme.headlineLarge,
                textAlign: TextAlign.center,
              )),
          const SizedBox(
            height: 99,
          ),
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 24),
            width: double.infinity,
            height: 60,
            child: TextButton(
              onPressed: () => logic.continueWithQQ(),
              style: Theme.of(context).textButtonTheme.style,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset(
                    Assets.imagesQQ,
                    width: 24,
                    height: 24,
                  ),
                  const SizedBox(
                    width: 12,
                  ),
                  Text(
                    MyText.continueWithQQ.tr,
                    style: Theme.of(context).textTheme.bodyLarge,
                  ),
                ],
              ),
            ),
          ),
          const SizedBox(
            height: 16,
          ),
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 24),
            width: double.infinity,
            height: 60,
            child: TextButton(
              onPressed: () => logic.continueWithWeChat(),
              style: Theme.of(context).textButtonTheme.style,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset(
                    Assets.imagesWechat,
                    width: 24,
                    height: 24,
                  ),
                  const SizedBox(
                    width: 12,
                  ),
                  Text(
                    MyText.continueWithWechat.tr,
                    style: Theme.of(context).textTheme.bodyLarge,
                  ),
                ],
              ),
            ),
          ),
          const SizedBox(
            height: 40,
          ),
          const Divider(height: 1, thickness: 1, indent: 24, endIndent: 24),
          const SizedBox(
            height: 40,
          ),
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 24),
            width: double.infinity,
            height: 55,
            child: ElevatedButton(
              onPressed: () => logic.continueWithPassword(),
              style: Theme.of(context).elevatedButtonTheme.style,
              child: Text(MyText.continueWithPassword.tr),
            ),
          ),
          const SizedBox(height: 99,),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
              children: [
            Text(
              MyText.notAccount.tr,
              style: Theme.of(context).textTheme.bodyMedium?.copyWith(
                  fontWeight: FontWeight.w400, color: Get.isDarkMode ? MyColor.white : MyColor.grayScale[500]),
            ),
            const SizedBox(width: 8,),
            GestureDetector(
              onTap: () => logic.signUp(),
              child: Text(
                MyText.signUp.tr,
                style: Theme.of(context).textTheme.bodyMedium?.copyWith(
                    color: MyColor.primary),
              ),
            ),
          ])
        ],
      ),
    );
  }
}
