import 'package:isar/isar.dart';

part 'ListMemberAndGroup.g.dart';

@Collection()
class ListMemberAndGroupData {
  @Name("id")
  Id modelId = Isar.autoIncrement;
  String? _name;
  String? _type;
  String? _avatar;
  String? _nickname;
  @Name("itemId")
  int? _id;
  int? _userId;
}
