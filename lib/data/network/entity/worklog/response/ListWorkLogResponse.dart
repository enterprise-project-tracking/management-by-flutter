/// code : 200
/// msg : "ok"
/// data : {"records":[{"id":1782273913975779330,"content":"cupidatat labore elit","createTime":"2024-04-22T13:03:09","reportTime":"2002-11-14T05:56:12","workAhead":"culpa Duis Excepteur minim amet","workAfter":"culpa reprehenderit et","workPlan":"consequat","type":3,"workTimes":[{"id":1782273914130968577,"projectId":1777693640121073666,"projectName":"科技型项目跟踪管理系统","timeCount":9}]},{"id":1782274068208726017,"content":"ipsum nisi in pariatur","createTime":"2024-04-22T13:03:46","reportTime":"1999-06-01T20:18:39","workAhead":"consectetur","workAfter":"in","workPlan":"in et deserunt","type":3,"workTimes":[{"id":1782274068296806401,"projectId":1777693640121073666,"projectName":"科技型项目跟踪管理系统","timeCount":16}]}],"total":2,"size":10,"current":1}

class ListWorkLogResponse {
  ListWorkLogResponse({
      num? code, 
      String? msg, 
      Data? data,}){
    _code = code;
    _msg = msg;
    _data = data;
}

  ListWorkLogResponse.fromJson(dynamic json) {
    _code = json['code'];
    _msg = json['msg'];
    _data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }
  num? _code;
  String? _msg;
  Data? _data;
ListWorkLogResponse copyWith({  num? code,
  String? msg,
  Data? data,
}) => ListWorkLogResponse(  code: code ?? _code,
  msg: msg ?? _msg,
  data: data ?? _data,
);
  num? get code => _code;
  String? get msg => _msg;
  Data? get data => _data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['code'] = _code;
    map['msg'] = _msg;
    if (_data != null) {
      map['data'] = _data?.toJson();
    }
    return map;
  }

}

/// records : [{"id":1782273913975779330,"content":"cupidatat labore elit","createTime":"2024-04-22T13:03:09","reportTime":"2002-11-14T05:56:12","workAhead":"culpa Duis Excepteur minim amet","workAfter":"culpa reprehenderit et","workPlan":"consequat","type":3,"workTimes":[{"id":1782273914130968577,"projectId":1777693640121073666,"projectName":"科技型项目跟踪管理系统","timeCount":9}]},{"id":1782274068208726017,"content":"ipsum nisi in pariatur","createTime":"2024-04-22T13:03:46","reportTime":"1999-06-01T20:18:39","workAhead":"consectetur","workAfter":"in","workPlan":"in et deserunt","type":3,"workTimes":[{"id":1782274068296806401,"projectId":1777693640121073666,"projectName":"科技型项目跟踪管理系统","timeCount":16}]}]
/// total : 2
/// size : 10
/// current : 1

class Data {
  Data({
      List<Records>? records, 
      num? total, 
      num? size, 
      num? current,}){
    _records = records;
    _total = total;
    _size = size;
    _current = current;
}

  Data.fromJson(dynamic json) {
    if (json['records'] != null) {
      _records = [];
      json['records'].forEach((v) {
        _records?.add(Records.fromJson(v));
      });
    }
    _total = json['total'];
    _size = json['size'];
    _current = json['current'];
  }
  List<Records>? _records;
  num? _total;
  num? _size;
  num? _current;
Data copyWith({  List<Records>? records,
  num? total,
  num? size,
  num? current,
}) => Data(  records: records ?? _records,
  total: total ?? _total,
  size: size ?? _size,
  current: current ?? _current,
);
  List<Records>? get records => _records;
  num? get total => _total;
  num? get size => _size;
  num? get current => _current;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_records != null) {
      map['records'] = _records?.map((v) => v.toJson()).toList();
    }
    map['total'] = _total;
    map['size'] = _size;
    map['current'] = _current;
    return map;
  }

}

/// id : 1782273913975779330
/// content : "cupidatat labore elit"
/// createTime : "2024-04-22T13:03:09"
/// reportTime : "2002-11-14T05:56:12"
/// workAhead : "culpa Duis Excepteur minim amet"
/// workAfter : "culpa reprehenderit et"
/// workPlan : "consequat"
/// type : 3
/// workTimes : [{"id":1782273914130968577,"projectId":1777693640121073666,"projectName":"科技型项目跟踪管理系统","timeCount":9}]

class Records {
  Records({
      num? id, 
      String? content, 
      String? createTime, 
      String? reportTime, 
      String? workAhead, 
      String? workAfter, 
      String? workPlan, 
      num? type, 
      List<WorkTimes>? workTimes,}){
    _id = id;
    _content = content;
    _createTime = createTime;
    _reportTime = reportTime;
    _workAhead = workAhead;
    _workAfter = workAfter;
    _workPlan = workPlan;
    _type = type;
    _workTimes = workTimes;
}

  Records.fromJson(dynamic json) {
    _id = json['id'];
    _content = json['content'];
    _createTime = json['createTime'];
    _reportTime = json['reportTime'];
    _workAhead = json['workAhead'];
    _workAfter = json['workAfter'];
    _workPlan = json['workPlan'];
    _type = json['type'];
    if (json['workTimes'] != null) {
      _workTimes = [];
      json['workTimes'].forEach((v) {
        _workTimes?.add(WorkTimes.fromJson(v));
      });
    }
  }
  num? _id;
  String? _content;
  String? _createTime;
  String? _reportTime;
  String? _workAhead;
  String? _workAfter;
  String? _workPlan;
  num? _type;
  List<WorkTimes>? _workTimes;
Records copyWith({  num? id,
  String? content,
  String? createTime,
  String? reportTime,
  String? workAhead,
  String? workAfter,
  String? workPlan,
  num? type,
  List<WorkTimes>? workTimes,
}) => Records(  id: id ?? _id,
  content: content ?? _content,
  createTime: createTime ?? _createTime,
  reportTime: reportTime ?? _reportTime,
  workAhead: workAhead ?? _workAhead,
  workAfter: workAfter ?? _workAfter,
  workPlan: workPlan ?? _workPlan,
  type: type ?? _type,
  workTimes: workTimes ?? _workTimes,
);
  num? get id => _id;
  String? get content => _content;
  String? get createTime => _createTime;
  String? get reportTime => _reportTime;
  String? get workAhead => _workAhead;
  String? get workAfter => _workAfter;
  String? get workPlan => _workPlan;
  num? get type => _type;
  List<WorkTimes>? get workTimes => _workTimes;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['content'] = _content;
    map['createTime'] = _createTime;
    map['reportTime'] = _reportTime;
    map['workAhead'] = _workAhead;
    map['workAfter'] = _workAfter;
    map['workPlan'] = _workPlan;
    map['type'] = _type;
    if (_workTimes != null) {
      map['workTimes'] = _workTimes?.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// id : 1782273914130968577
/// projectId : 1777693640121073666
/// projectName : "科技型项目跟踪管理系统"
/// timeCount : 9

class WorkTimes {
  WorkTimes({
      num? id, 
      num? projectId, 
      String? projectName, 
      num? timeCount,}){
    _id = id;
    _projectId = projectId;
    _projectName = projectName;
    _timeCount = timeCount;
}

  WorkTimes.fromJson(dynamic json) {
    _id = json['id'];
    _projectId = json['projectId'];
    _projectName = json['projectName'];
    _timeCount = json['timeCount'];
  }
  num? _id;
  num? _projectId;
  String? _projectName;
  num? _timeCount;
WorkTimes copyWith({  num? id,
  num? projectId,
  String? projectName,
  num? timeCount,
}) => WorkTimes(  id: id ?? _id,
  projectId: projectId ?? _projectId,
  projectName: projectName ?? _projectName,
  timeCount: timeCount ?? _timeCount,
);
  num? get id => _id;
  num? get projectId => _projectId;
  String? get projectName => _projectName;
  num? get timeCount => _timeCount;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['projectId'] = _projectId;
    map['projectName'] = _projectName;
    map['timeCount'] = _timeCount;
    return map;
  }

}