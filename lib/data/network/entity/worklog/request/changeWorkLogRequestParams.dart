/// id : 46
/// content : "adipisicing ad aute ut fugiat"
/// workAhead : "ullamco"
/// reportTime : "1979-07-29 04:08:59"
/// workAfter : "ea quis et"
/// workPlan : "sed nostrud qui"
/// type : 42
/// workTimesAdd : [{"projectId":79,"timeCount":335488043603}]
/// workTimesDeleteIds : [26]

class ChangeWorkLogRequestParams {
  ChangeWorkLogRequestParams({
      num? id, 
      String? content, 
      String? workAhead, 
      String? reportTime, 
      String? workAfter, 
      String? workPlan, 
      num? type, 
      List<WorkTimesAdd>? workTimesAdd, 
      List<num>? workTimesDeleteIds,}){
    _id = id;
    _content = content;
    _workAhead = workAhead;
    _reportTime = reportTime;
    _workAfter = workAfter;
    _workPlan = workPlan;
    _type = type;
    _workTimesAdd = workTimesAdd;
    _workTimesDeleteIds = workTimesDeleteIds;
}

  ChangeWorkLogRequestParams.fromJson(dynamic json) {
    _id = json['id'];
    _content = json['content'];
    _workAhead = json['workAhead'];
    _reportTime = json['reportTime'];
    _workAfter = json['workAfter'];
    _workPlan = json['workPlan'];
    _type = json['type'];
    if (json['workTimesAdd'] != null) {
      _workTimesAdd = [];
      json['workTimesAdd'].forEach((v) {
        _workTimesAdd?.add(WorkTimesAdd.fromJson(v));
      });
    }
    _workTimesDeleteIds = json['workTimesDeleteIds'] != null ? json['workTimesDeleteIds'].cast<num>() : [];
  }
  num? _id;
  String? _content;
  String? _workAhead;
  String? _reportTime;
  String? _workAfter;
  String? _workPlan;
  num? _type;
  List<WorkTimesAdd>? _workTimesAdd;
  List<num>? _workTimesDeleteIds;
ChangeWorkLogRequestParams copyWith({  num? id,
  String? content,
  String? workAhead,
  String? reportTime,
  String? workAfter,
  String? workPlan,
  num? type,
  List<WorkTimesAdd>? workTimesAdd,
  List<num>? workTimesDeleteIds,
}) => ChangeWorkLogRequestParams(  id: id ?? _id,
  content: content ?? _content,
  workAhead: workAhead ?? _workAhead,
  reportTime: reportTime ?? _reportTime,
  workAfter: workAfter ?? _workAfter,
  workPlan: workPlan ?? _workPlan,
  type: type ?? _type,
  workTimesAdd: workTimesAdd ?? _workTimesAdd,
  workTimesDeleteIds: workTimesDeleteIds ?? _workTimesDeleteIds,
);
  num? get id => _id;
  String? get content => _content;
  String? get workAhead => _workAhead;
  String? get reportTime => _reportTime;
  String? get workAfter => _workAfter;
  String? get workPlan => _workPlan;
  num? get type => _type;
  List<WorkTimesAdd>? get workTimesAdd => _workTimesAdd;
  List<num>? get workTimesDeleteIds => _workTimesDeleteIds;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['content'] = _content;
    map['workAhead'] = _workAhead;
    map['reportTime'] = _reportTime;
    map['workAfter'] = _workAfter;
    map['workPlan'] = _workPlan;
    map['type'] = _type;
    if (_workTimesAdd != null) {
      map['workTimesAdd'] = _workTimesAdd?.map((v) => v.toJson()).toList();
    }
    map['workTimesDeleteIds'] = _workTimesDeleteIds;
    return map;
  }

}

/// projectId : 79
/// timeCount : 335488043603

class WorkTimesAdd {
  WorkTimesAdd({
      num? projectId, 
      num? timeCount,}){
    _projectId = projectId;
    _timeCount = timeCount;
}

  WorkTimesAdd.fromJson(dynamic json) {
    _projectId = json['projectId'];
    _timeCount = json['timeCount'];
  }
  num? _projectId;
  num? _timeCount;
WorkTimesAdd copyWith({  num? projectId,
  num? timeCount,
}) => WorkTimesAdd(  projectId: projectId ?? _projectId,
  timeCount: timeCount ?? _timeCount,
);
  num? get projectId => _projectId;
  num? get timeCount => _timeCount;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['projectId'] = _projectId;
    map['timeCount'] = _timeCount;
    return map;
  }

}