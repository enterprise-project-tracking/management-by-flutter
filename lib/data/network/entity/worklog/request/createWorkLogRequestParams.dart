/// content : "ipsum nisi in pariatur"
/// workAhead : "consectetur"
/// reportTime : "1999-06-01T20:18:39"
/// workAfter : "in"
/// workPlan : "in et deserunt"
/// type : 3
/// workTimes : [{"projectId":1777693640121073666,"timeCount":16}]

class CreateWorkLogRequestParams {
  CreateWorkLogRequestParams({
    String? content,
    String? workAhead,
    String? reportTime,
    String? workAfter,
    String? workPlan,
    num? type,
    List<WorkTimes>? workTimes,
  }) {
    _content = content;
    _workAhead = workAhead;
    _reportTime = reportTime;
    _workAfter = workAfter;
    _workPlan = workPlan;
    _type = type;
    _workTimes = workTimes;
  }

  CreateWorkLogRequestParams.fromJson(dynamic json) {
    _content = json['content'];
    _workAhead = json['workAhead'];
    _reportTime = json['reportTime'];
    _workAfter = json['workAfter'];
    _workPlan = json['workPlan'];
    _type = json['type'];
    if (json['workTimes'] != null) {
      _workTimes = [];
      json['workTimes'].forEach((v) {
        _workTimes?.add(WorkTimes.fromJson(v));
      });
    }
  }
  String? _content;
  String? _workAhead;
  String? _reportTime;
  String? _workAfter;
  String? _workPlan;
  num? _type;
  List<WorkTimes>? _workTimes;
  CreateWorkLogRequestParams copyWith({
    String? content,
    String? workAhead,
    String? reportTime,
    String? workAfter,
    String? workPlan,
    num? type,
    List<WorkTimes>? workTimes,
  }) =>
      CreateWorkLogRequestParams(
        content: content ?? _content,
        workAhead: workAhead ?? _workAhead,
        reportTime: reportTime ?? _reportTime,
        workAfter: workAfter ?? _workAfter,
        workPlan: workPlan ?? _workPlan,
        type: type ?? _type,
        workTimes: workTimes ?? _workTimes,
      );
  String? get content => _content;
  String? get workAhead => _workAhead;
  String? get reportTime => _reportTime;
  String? get workAfter => _workAfter;
  String? get workPlan => _workPlan;
  num? get type => _type;
  List<WorkTimes>? get workTimes => _workTimes;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['content'] = _content;
    map['workAhead'] = _workAhead;
    map['reportTime'] = _reportTime;
    map['workAfter'] = _workAfter;
    map['workPlan'] = _workPlan;
    map['type'] = _type;
    if (_workTimes != null) {
      map['workTimes'] = _workTimes?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// projectId : 1777693640121073666
/// timeCount : 16

class WorkTimes {
  WorkTimes({
    num? projectId,
    num? timeCount,
  }) {
    _projectId = projectId;
    _timeCount = timeCount;
  }

  WorkTimes.fromJson(dynamic json) {
    _projectId = json['projectId'];
    _timeCount = json['timeCount'];
  }
  num? _projectId;
  num? _timeCount;
  WorkTimes copyWith({
    num? projectId,
    num? timeCount,
  }) =>
      WorkTimes(
        projectId: projectId ?? _projectId,
        timeCount: timeCount ?? _timeCount,
      );
  num? get projectId => _projectId;
  num? get timeCount => _timeCount;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['projectId'] = _projectId;
    map['timeCount'] = _timeCount;
    return map;
  }
}
