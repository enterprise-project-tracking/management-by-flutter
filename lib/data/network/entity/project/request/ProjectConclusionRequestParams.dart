/// projectId : 90
/// legalPersonId : 83
/// applyCompanyUserId : 20
/// projectOutcome : "et exercitation"
/// conclusionFiles : [{"filename":"行走精设","url":"http://obolwm.ye/inw"}]

class ProjectConclusionRequestParams {
  ProjectConclusionRequestParams({
      num? projectId, 
      num? legalPersonId, 
      num? applyCompanyUserId, 
      String? projectOutcome, 
      List<ConclusionFiles>? conclusionFiles,}){
    _projectId = projectId;
    _legalPersonId = legalPersonId;
    _applyCompanyUserId = applyCompanyUserId;
    _projectOutcome = projectOutcome;
    _conclusionFiles = conclusionFiles;
}

  ProjectConclusionRequestParams.fromJson(dynamic json) {
    _projectId = json['projectId'];
    _legalPersonId = json['legalPersonId'];
    _applyCompanyUserId = json['applyCompanyUserId'];
    _projectOutcome = json['projectOutcome'];
    if (json['conclusionFiles'] != null) {
      _conclusionFiles = [];
      json['conclusionFiles'].forEach((v) {
        _conclusionFiles?.add(ConclusionFiles.fromJson(v));
      });
    }
  }
  num? _projectId;
  num? _legalPersonId;
  num? _applyCompanyUserId;
  String? _projectOutcome;
  List<ConclusionFiles>? _conclusionFiles;
ProjectConclusionRequestParams copyWith({  num? projectId,
  num? legalPersonId,
  num? applyCompanyUserId,
  String? projectOutcome,
  List<ConclusionFiles>? conclusionFiles,
}) => ProjectConclusionRequestParams(  projectId: projectId ?? _projectId,
  legalPersonId: legalPersonId ?? _legalPersonId,
  applyCompanyUserId: applyCompanyUserId ?? _applyCompanyUserId,
  projectOutcome: projectOutcome ?? _projectOutcome,
  conclusionFiles: conclusionFiles ?? _conclusionFiles,
);
  num? get projectId => _projectId;
  num? get legalPersonId => _legalPersonId;
  num? get applyCompanyUserId => _applyCompanyUserId;
  String? get projectOutcome => _projectOutcome;
  List<ConclusionFiles>? get conclusionFiles => _conclusionFiles;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['projectId'] = _projectId;
    map['legalPersonId'] = _legalPersonId;
    map['applyCompanyUserId'] = _applyCompanyUserId;
    map['projectOutcome'] = _projectOutcome;
    if (_conclusionFiles != null) {
      map['conclusionFiles'] = _conclusionFiles?.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// filename : "行走精设"
/// url : "http://obolwm.ye/inw"

class ConclusionFiles {
  ConclusionFiles({
      String? filename, 
      String? url,}){
    _filename = filename;
    _url = url;
}

  ConclusionFiles.fromJson(dynamic json) {
    _filename = json['filename'];
    _url = json['url'];
  }
  String? _filename;
  String? _url;
ConclusionFiles copyWith({  String? filename,
  String? url,
}) => ConclusionFiles(  filename: filename ?? _filename,
  url: url ?? _url,
);
  String? get filename => _filename;
  String? get url => _url;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['filename'] = _filename;
    map['url'] = _url;
    return map;
  }

}