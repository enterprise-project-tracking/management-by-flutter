/// id : 49
/// companyUserIds : [92]
/// name : "必示收他"
/// beginTime : "1988-05-31 01:07:23"
/// endTime : "2004-07-30 05:08:20"
/// estimateCost : 16
/// description : "运其化并用圆联当出二才律对领到空深。战始红号火经面及原省军越要。本儿空体眼派专应步济低成做之。式水共门生物少品之那应众划圆来。"
/// status : 27
/// chargeId : 6
/// conferenceResolutions : [{"filename":"这局酸放王重","url":"http://fkdjzxl.io/fxhjghsj"}]
/// projectPlanningPaper : [{"filename":"少知民始相员局","url":"http://vcytbparej.sh/knt"}]

class SubmitProjectRequestParams {
  SubmitProjectRequestParams({
    num? id,
    List<num>? companyUserIds,
    String? name,
    String? beginTime,
    String? endTime,
    num? estimateCost,
    String? description,
    num? status,
    num? chargeId,
    List<ConferenceResolutions>? conferenceResolutions,
    List<ProjectPlanningPaper>? projectPlanningPaper,
  }) {
    _id = id;
    _companyUserIds = companyUserIds;
    _name = name;
    _beginTime = beginTime;
    _endTime = endTime;
    _estimateCost = estimateCost;
    _description = description;
    _status = status;
    _chargeId = chargeId;
    _conferenceResolutions = conferenceResolutions;
    _projectPlanningPaper = projectPlanningPaper;
  }

  SubmitProjectRequestParams.fromJson(dynamic json) {
    _id = json['id'];
    _companyUserIds = json['companyUserIds'] != null
        ? json['companyUserIds'].cast<num>()
        : [];
    _name = json['name'];
    _beginTime = json['beginTime'];
    _endTime = json['endTime'];
    _estimateCost = json['estimateCost'];
    _description = json['description'];
    _status = json['status'];
    _chargeId = json['chargeId'];
    if (json['conferenceResolutions'] != null) {
      _conferenceResolutions = [];
      json['conferenceResolutions'].forEach((v) {
        _conferenceResolutions?.add(ConferenceResolutions.fromJson(v));
      });
    }
    if (json['projectPlanningPaper'] != null) {
      _projectPlanningPaper = [];
      json['projectPlanningPaper'].forEach((v) {
        _projectPlanningPaper?.add(ProjectPlanningPaper.fromJson(v));
      });
    }
  }
  num? _id;
  List<num>? _companyUserIds;
  String? _name;
  String? _beginTime;
  String? _endTime;
  num? _estimateCost;
  String? _description;
  num? _status;
  num? _chargeId;
  List<ConferenceResolutions>? _conferenceResolutions;
  List<ProjectPlanningPaper>? _projectPlanningPaper;
  SubmitProjectRequestParams copyWith({
    num? id,
    List<num>? companyUserIds,
    String? name,
    String? beginTime,
    String? endTime,
    num? estimateCost,
    String? description,
    num? status,
    num? chargeId,
    List<ConferenceResolutions>? conferenceResolutions,
    List<ProjectPlanningPaper>? projectPlanningPaper,
  }) =>
      SubmitProjectRequestParams(
        id: id ?? _id,
        companyUserIds: companyUserIds ?? _companyUserIds,
        name: name ?? _name,
        beginTime: beginTime ?? _beginTime,
        endTime: endTime ?? _endTime,
        estimateCost: estimateCost ?? _estimateCost,
        description: description ?? _description,
        status: status ?? _status,
        chargeId: chargeId ?? _chargeId,
        conferenceResolutions: conferenceResolutions ?? _conferenceResolutions,
        projectPlanningPaper: projectPlanningPaper ?? _projectPlanningPaper,
      );
  num? get id => _id;
  List<num>? get companyUserIds => _companyUserIds;
  String? get name => _name;
  String? get beginTime => _beginTime;
  String? get endTime => _endTime;
  num? get estimateCost => _estimateCost;
  String? get description => _description;
  num? get status => _status;
  num? get chargeId => _chargeId;
  List<ConferenceResolutions>? get conferenceResolutions =>
      _conferenceResolutions;
  List<ProjectPlanningPaper>? get projectPlanningPaper => _projectPlanningPaper;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['companyUserIds'] = _companyUserIds;
    map['name'] = _name;
    map['beginTime'] = _beginTime;
    map['endTime'] = _endTime;
    map['estimateCost'] = _estimateCost;
    map['description'] = _description;
    map['status'] = _status;
    map['chargeId'] = _chargeId;
    if (_conferenceResolutions != null) {
      map['conferenceResolutions'] =
          _conferenceResolutions?.map((v) => v.toJson()).toList();
    }
    if (_projectPlanningPaper != null) {
      map['projectPlanningPaper'] =
          _projectPlanningPaper?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// filename : "少知民始相员局"
/// url : "http://vcytbparej.sh/knt"

class ProjectPlanningPaper {
  ProjectPlanningPaper({
    String? filename,
    String? url,
  }) {
    _filename = filename;
    _url = url;
  }

  ProjectPlanningPaper.fromJson(dynamic json) {
    _filename = json['filename'];
    _url = json['url'];
  }
  String? _filename;
  String? _url;
  ProjectPlanningPaper copyWith({
    String? filename,
    String? url,
  }) =>
      ProjectPlanningPaper(
        filename: filename ?? _filename,
        url: url ?? _url,
      );
  String? get filename => _filename;
  String? get url => _url;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['filename'] = _filename;
    map['url'] = _url;
    return map;
  }
}

/// filename : "这局酸放王重"
/// url : "http://fkdjzxl.io/fxhjghsj"

class ConferenceResolutions {
  ConferenceResolutions({
    String? filename,
    String? url,
  }) {
    _filename = filename;
    _url = url;
  }

  ConferenceResolutions.fromJson(dynamic json) {
    _filename = json['filename'];
    _url = json['url'];
  }
  String? _filename;
  String? _url;
  ConferenceResolutions copyWith({
    String? filename,
    String? url,
  }) =>
      ConferenceResolutions(
        filename: filename ?? _filename,
        url: url ?? _url,
      );
  String? get filename => _filename;
  String? get url => _url;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['filename'] = _filename;
    map['url'] = _url;
    return map;
  }
}
