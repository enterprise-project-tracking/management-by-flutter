/// id : 99
/// companyUserAddIds : [54]
/// companyUserDeleteIds : [98]
/// name : "龙圆题感段算代"
/// beginTime : "2016-02-07 23:25:44"
/// endTime : "2005-01-29 10:24:03"
/// estimateCost : 57
/// description : "素学派活周影家五较育口有指示强。以改动整备后山命拉开设再江六。离由人务必国合较专三并格内。理级切样应两之清种平置界基国保和出。一关总家东族级花米水问非。"
/// status : 89
/// chargeId : 66
/// conferenceResolutionsAdd : [{"filename":"老成然","url":"http://hehmkam.sg/ehx"}]
/// conferenceResolutionsDeleteIds : [94]
/// projectPlanningPaperAdd : [{"filename":"把点样","url":"http://vnxmgi.ad/srh"}]
/// projectPlanningPaperDeleteIds : [96]

class UpdateProjectRequestParams {
  UpdateProjectRequestParams({
      num? id, 
      List<num>? companyUserAddIds, 
      List<num>? companyUserDeleteIds, 
      String? name, 
      String? beginTime, 
      String? endTime, 
      num? estimateCost, 
      String? description, 
      num? status, 
      num? chargeId, 
      List<ConferenceResolutionsAdd>? conferenceResolutionsAdd, 
      List<num>? conferenceResolutionsDeleteIds, 
      List<ProjectPlanningPaperAdd>? projectPlanningPaperAdd, 
      List<num>? projectPlanningPaperDeleteIds,}){
    _id = id;
    _companyUserAddIds = companyUserAddIds;
    _companyUserDeleteIds = companyUserDeleteIds;
    _name = name;
    _beginTime = beginTime;
    _endTime = endTime;
    _estimateCost = estimateCost;
    _description = description;
    _status = status;
    _chargeId = chargeId;
    _conferenceResolutionsAdd = conferenceResolutionsAdd;
    _conferenceResolutionsDeleteIds = conferenceResolutionsDeleteIds;
    _projectPlanningPaperAdd = projectPlanningPaperAdd;
    _projectPlanningPaperDeleteIds = projectPlanningPaperDeleteIds;
}

  UpdateProjectRequestParams.fromJson(dynamic json) {
    _id = json['id'];
    _companyUserAddIds = json['companyUserAddIds'] != null ? json['companyUserAddIds'].cast<num>() : [];
    _companyUserDeleteIds = json['companyUserDeleteIds'] != null ? json['companyUserDeleteIds'].cast<num>() : [];
    _name = json['name'];
    _beginTime = json['beginTime'];
    _endTime = json['endTime'];
    _estimateCost = json['estimateCost'];
    _description = json['description'];
    _status = json['status'];
    _chargeId = json['chargeId'];
    if (json['conferenceResolutionsAdd'] != null) {
      _conferenceResolutionsAdd = [];
      json['conferenceResolutionsAdd'].forEach((v) {
        _conferenceResolutionsAdd?.add(ConferenceResolutionsAdd.fromJson(v));
      });
    }
    _conferenceResolutionsDeleteIds = json['conferenceResolutionsDeleteIds'] != null ? json['conferenceResolutionsDeleteIds'].cast<num>() : [];
    if (json['projectPlanningPaperAdd'] != null) {
      _projectPlanningPaperAdd = [];
      json['projectPlanningPaperAdd'].forEach((v) {
        _projectPlanningPaperAdd?.add(ProjectPlanningPaperAdd.fromJson(v));
      });
    }
    _projectPlanningPaperDeleteIds = json['projectPlanningPaperDeleteIds'] != null ? json['projectPlanningPaperDeleteIds'].cast<num>() : [];
  }
  num? _id;
  List<num>? _companyUserAddIds;
  List<num>? _companyUserDeleteIds;
  String? _name;
  String? _beginTime;
  String? _endTime;
  num? _estimateCost;
  String? _description;
  num? _status;
  num? _chargeId;
  List<ConferenceResolutionsAdd>? _conferenceResolutionsAdd;
  List<num>? _conferenceResolutionsDeleteIds;
  List<ProjectPlanningPaperAdd>? _projectPlanningPaperAdd;
  List<num>? _projectPlanningPaperDeleteIds;
UpdateProjectRequestParams copyWith({  num? id,
  List<num>? companyUserAddIds,
  List<num>? companyUserDeleteIds,
  String? name,
  String? beginTime,
  String? endTime,
  num? estimateCost,
  String? description,
  num? status,
  num? chargeId,
  List<ConferenceResolutionsAdd>? conferenceResolutionsAdd,
  List<num>? conferenceResolutionsDeleteIds,
  List<ProjectPlanningPaperAdd>? projectPlanningPaperAdd,
  List<num>? projectPlanningPaperDeleteIds,
}) => UpdateProjectRequestParams(  id: id ?? _id,
  companyUserAddIds: companyUserAddIds ?? _companyUserAddIds,
  companyUserDeleteIds: companyUserDeleteIds ?? _companyUserDeleteIds,
  name: name ?? _name,
  beginTime: beginTime ?? _beginTime,
  endTime: endTime ?? _endTime,
  estimateCost: estimateCost ?? _estimateCost,
  description: description ?? _description,
  status: status ?? _status,
  chargeId: chargeId ?? _chargeId,
  conferenceResolutionsAdd: conferenceResolutionsAdd ?? _conferenceResolutionsAdd,
  conferenceResolutionsDeleteIds: conferenceResolutionsDeleteIds ?? _conferenceResolutionsDeleteIds,
  projectPlanningPaperAdd: projectPlanningPaperAdd ?? _projectPlanningPaperAdd,
  projectPlanningPaperDeleteIds: projectPlanningPaperDeleteIds ?? _projectPlanningPaperDeleteIds,
);
  num? get id => _id;
  List<num>? get companyUserAddIds => _companyUserAddIds;
  List<num>? get companyUserDeleteIds => _companyUserDeleteIds;
  String? get name => _name;
  String? get beginTime => _beginTime;
  String? get endTime => _endTime;
  num? get estimateCost => _estimateCost;
  String? get description => _description;
  num? get status => _status;
  num? get chargeId => _chargeId;
  List<ConferenceResolutionsAdd>? get conferenceResolutionsAdd => _conferenceResolutionsAdd;
  List<num>? get conferenceResolutionsDeleteIds => _conferenceResolutionsDeleteIds;
  List<ProjectPlanningPaperAdd>? get projectPlanningPaperAdd => _projectPlanningPaperAdd;
  List<num>? get projectPlanningPaperDeleteIds => _projectPlanningPaperDeleteIds;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['companyUserAddIds'] = _companyUserAddIds;
    map['companyUserDeleteIds'] = _companyUserDeleteIds;
    map['name'] = _name;
    map['beginTime'] = _beginTime;
    map['endTime'] = _endTime;
    map['estimateCost'] = _estimateCost;
    map['description'] = _description;
    map['status'] = _status;
    map['chargeId'] = _chargeId;
    if (_conferenceResolutionsAdd != null) {
      map['conferenceResolutionsAdd'] = _conferenceResolutionsAdd?.map((v) => v.toJson()).toList();
    }
    map['conferenceResolutionsDeleteIds'] = _conferenceResolutionsDeleteIds;
    if (_projectPlanningPaperAdd != null) {
      map['projectPlanningPaperAdd'] = _projectPlanningPaperAdd?.map((v) => v.toJson()).toList();
    }
    map['projectPlanningPaperDeleteIds'] = _projectPlanningPaperDeleteIds;
    return map;
  }

}

/// filename : "把点样"
/// url : "http://vnxmgi.ad/srh"

class ProjectPlanningPaperAdd {
  ProjectPlanningPaperAdd({
      String? filename, 
      String? url,}){
    _filename = filename;
    _url = url;
}

  ProjectPlanningPaperAdd.fromJson(dynamic json) {
    _filename = json['filename'];
    _url = json['url'];
  }
  String? _filename;
  String? _url;
ProjectPlanningPaperAdd copyWith({  String? filename,
  String? url,
}) => ProjectPlanningPaperAdd(  filename: filename ?? _filename,
  url: url ?? _url,
);
  String? get filename => _filename;
  String? get url => _url;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['filename'] = _filename;
    map['url'] = _url;
    return map;
  }

}

/// filename : "老成然"
/// url : "http://hehmkam.sg/ehx"

class ConferenceResolutionsAdd {
  ConferenceResolutionsAdd({
      String? filename, 
      String? url,}){
    _filename = filename;
    _url = url;
}

  ConferenceResolutionsAdd.fromJson(dynamic json) {
    _filename = json['filename'];
    _url = json['url'];
  }
  String? _filename;
  String? _url;
ConferenceResolutionsAdd copyWith({  String? filename,
  String? url,
}) => ConferenceResolutionsAdd(  filename: filename ?? _filename,
  url: url ?? _url,
);
  String? get filename => _filename;
  String? get url => _url;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['filename'] = _filename;
    map['url'] = _url;
    return map;
  }

}