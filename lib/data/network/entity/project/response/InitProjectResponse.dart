/// code : 200
/// msg : "ok"
/// data : {"id":1779011321853423618}

class InitProjectResponse {
  InitProjectResponse({
      num? code, 
      String? msg, 
      Data? data,}){
    _code = code;
    _msg = msg;
    _data = data;
}

  InitProjectResponse.fromJson(dynamic json) {
    _code = json['code'];
    _msg = json['msg'];
    _data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }
  num? _code;
  String? _msg;
  Data? _data;
InitProjectResponse copyWith({  num? code,
  String? msg,
  Data? data,
}) => InitProjectResponse(  code: code ?? _code,
  msg: msg ?? _msg,
  data: data ?? _data,
);
  num? get code => _code;
  String? get msg => _msg;
  Data? get data => _data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['code'] = _code;
    map['msg'] = _msg;
    if (_data != null) {
      map['data'] = _data?.toJson();
    }
    return map;
  }

}

/// id : 1779011321853423618

class Data {
  Data({
      num? id,}){
    _id = id;
}

  Data.fromJson(dynamic json) {
    _id = json['id'];
  }
  num? _id;
Data copyWith({  num? id,
}) => Data(  id: id ?? _id,
);
  num? get id => _id;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    return map;
  }

}