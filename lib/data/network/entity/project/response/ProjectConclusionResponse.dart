/// code : 0
/// msg : "string"
/// data : {"projectId":0,"name":"string","id":0,"applyCompanyUser":{"id":0,"nickName":"string","avatar":"string","realName":"string"},"legalPerson":{"id":0,"nickName":"string","avatar":"string","realName":"string"},"projectOutcome":"string","projectName":"string","updateTime":"2019-08-24T14:15:22Z","createTime":"2019-08-24T14:15:22Z","conclusionFiles":[{"id":0,"name":"string","suffix":"string","url":"string","createTime":"2019-08-24T14:15:22Z","fileType":0,"creator":{"id":0,"nickName":"string","avatar":"string","realName":"string"}}]}

class ProjectConclusionResponse {
  ProjectConclusionResponse({
      num? code, 
      String? msg, 
      Data? data,}){
    _code = code;
    _msg = msg;
    _data = data;
}

  ProjectConclusionResponse.fromJson(dynamic json) {
    _code = json['code'];
    _msg = json['msg'];
    _data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }
  num? _code;
  String? _msg;
  Data? _data;
ProjectConclusionResponse copyWith({  num? code,
  String? msg,
  Data? data,
}) => ProjectConclusionResponse(  code: code ?? _code,
  msg: msg ?? _msg,
  data: data ?? _data,
);
  num? get code => _code;
  String? get msg => _msg;
  Data? get data => _data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['code'] = _code;
    map['msg'] = _msg;
    if (_data != null) {
      map['data'] = _data?.toJson();
    }
    return map;
  }

}

/// projectId : 0
/// name : "string"
/// id : 0
/// applyCompanyUser : {"id":0,"nickName":"string","avatar":"string","realName":"string"}
/// legalPerson : {"id":0,"nickName":"string","avatar":"string","realName":"string"}
/// projectOutcome : "string"
/// projectName : "string"
/// updateTime : "2019-08-24T14:15:22Z"
/// createTime : "2019-08-24T14:15:22Z"
/// conclusionFiles : [{"id":0,"name":"string","suffix":"string","url":"string","createTime":"2019-08-24T14:15:22Z","fileType":0,"creator":{"id":0,"nickName":"string","avatar":"string","realName":"string"}}]

class Data {
  Data({
      num? projectId, 
      String? name, 
      num? id, 
      ApplyCompanyUser? applyCompanyUser, 
      LegalPerson? legalPerson, 
      String? projectOutcome, 
      String? projectName, 
      String? updateTime, 
      String? createTime, 
      List<ConclusionFiles>? conclusionFiles,}){
    _projectId = projectId;
    _name = name;
    _id = id;
    _applyCompanyUser = applyCompanyUser;
    _legalPerson = legalPerson;
    _projectOutcome = projectOutcome;
    _projectName = projectName;
    _updateTime = updateTime;
    _createTime = createTime;
    _conclusionFiles = conclusionFiles;
}

  Data.fromJson(dynamic json) {
    _projectId = json['projectId'];
    _name = json['name'];
    _id = json['id'];
    _applyCompanyUser = json['applyCompanyUser'] != null ? ApplyCompanyUser.fromJson(json['applyCompanyUser']) : null;
    _legalPerson = json['legalPerson'] != null ? LegalPerson.fromJson(json['legalPerson']) : null;
    _projectOutcome = json['projectOutcome'];
    _projectName = json['projectName'];
    _updateTime = json['updateTime'];
    _createTime = json['createTime'];
    if (json['conclusionFiles'] != null) {
      _conclusionFiles = [];
      json['conclusionFiles'].forEach((v) {
        _conclusionFiles?.add(ConclusionFiles.fromJson(v));
      });
    }
  }
  num? _projectId;
  String? _name;
  num? _id;
  ApplyCompanyUser? _applyCompanyUser;
  LegalPerson? _legalPerson;
  String? _projectOutcome;
  String? _projectName;
  String? _updateTime;
  String? _createTime;
  List<ConclusionFiles>? _conclusionFiles;
Data copyWith({  num? projectId,
  String? name,
  num? id,
  ApplyCompanyUser? applyCompanyUser,
  LegalPerson? legalPerson,
  String? projectOutcome,
  String? projectName,
  String? updateTime,
  String? createTime,
  List<ConclusionFiles>? conclusionFiles,
}) => Data(  projectId: projectId ?? _projectId,
  name: name ?? _name,
  id: id ?? _id,
  applyCompanyUser: applyCompanyUser ?? _applyCompanyUser,
  legalPerson: legalPerson ?? _legalPerson,
  projectOutcome: projectOutcome ?? _projectOutcome,
  projectName: projectName ?? _projectName,
  updateTime: updateTime ?? _updateTime,
  createTime: createTime ?? _createTime,
  conclusionFiles: conclusionFiles ?? _conclusionFiles,
);
  num? get projectId => _projectId;
  String? get name => _name;
  num? get id => _id;
  ApplyCompanyUser? get applyCompanyUser => _applyCompanyUser;
  LegalPerson? get legalPerson => _legalPerson;
  String? get projectOutcome => _projectOutcome;
  String? get projectName => _projectName;
  String? get updateTime => _updateTime;
  String? get createTime => _createTime;
  List<ConclusionFiles>? get conclusionFiles => _conclusionFiles;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['projectId'] = _projectId;
    map['name'] = _name;
    map['id'] = _id;
    if (_applyCompanyUser != null) {
      map['applyCompanyUser'] = _applyCompanyUser?.toJson();
    }
    if (_legalPerson != null) {
      map['legalPerson'] = _legalPerson?.toJson();
    }
    map['projectOutcome'] = _projectOutcome;
    map['projectName'] = _projectName;
    map['updateTime'] = _updateTime;
    map['createTime'] = _createTime;
    if (_conclusionFiles != null) {
      map['conclusionFiles'] = _conclusionFiles?.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// id : 0
/// name : "string"
/// suffix : "string"
/// url : "string"
/// createTime : "2019-08-24T14:15:22Z"
/// fileType : 0
/// creator : {"id":0,"nickName":"string","avatar":"string","realName":"string"}

class ConclusionFiles {
  ConclusionFiles({
      num? id, 
      String? name, 
      String? suffix, 
      String? url, 
      String? createTime, 
      num? fileType, 
      Creator? creator,}){
    _id = id;
    _name = name;
    _suffix = suffix;
    _url = url;
    _createTime = createTime;
    _fileType = fileType;
    _creator = creator;
}

  ConclusionFiles.fromJson(dynamic json) {
    _id = json['id'];
    _name = json['name'];
    _suffix = json['suffix'];
    _url = json['url'];
    _createTime = json['createTime'];
    _fileType = json['fileType'];
    _creator = json['creator'] != null ? Creator.fromJson(json['creator']) : null;
  }
  num? _id;
  String? _name;
  String? _suffix;
  String? _url;
  String? _createTime;
  num? _fileType;
  Creator? _creator;
ConclusionFiles copyWith({  num? id,
  String? name,
  String? suffix,
  String? url,
  String? createTime,
  num? fileType,
  Creator? creator,
}) => ConclusionFiles(  id: id ?? _id,
  name: name ?? _name,
  suffix: suffix ?? _suffix,
  url: url ?? _url,
  createTime: createTime ?? _createTime,
  fileType: fileType ?? _fileType,
  creator: creator ?? _creator,
);
  num? get id => _id;
  String? get name => _name;
  String? get suffix => _suffix;
  String? get url => _url;
  String? get createTime => _createTime;
  num? get fileType => _fileType;
  Creator? get creator => _creator;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['name'] = _name;
    map['suffix'] = _suffix;
    map['url'] = _url;
    map['createTime'] = _createTime;
    map['fileType'] = _fileType;
    if (_creator != null) {
      map['creator'] = _creator?.toJson();
    }
    return map;
  }

}

/// id : 0
/// nickName : "string"
/// avatar : "string"
/// realName : "string"

class Creator {
  Creator({
      num? id, 
      String? nickName, 
      String? avatar, 
      String? realName,}){
    _id = id;
    _nickName = nickName;
    _avatar = avatar;
    _realName = realName;
}

  Creator.fromJson(dynamic json) {
    _id = json['id'];
    _nickName = json['nickName'];
    _avatar = json['avatar'];
    _realName = json['realName'];
  }
  num? _id;
  String? _nickName;
  String? _avatar;
  String? _realName;
Creator copyWith({  num? id,
  String? nickName,
  String? avatar,
  String? realName,
}) => Creator(  id: id ?? _id,
  nickName: nickName ?? _nickName,
  avatar: avatar ?? _avatar,
  realName: realName ?? _realName,
);
  num? get id => _id;
  String? get nickName => _nickName;
  String? get avatar => _avatar;
  String? get realName => _realName;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['nickName'] = _nickName;
    map['avatar'] = _avatar;
    map['realName'] = _realName;
    return map;
  }

}

/// id : 0
/// nickName : "string"
/// avatar : "string"
/// realName : "string"

class LegalPerson {
  LegalPerson({
      num? id, 
      String? nickName, 
      String? avatar, 
      String? realName,}){
    _id = id;
    _nickName = nickName;
    _avatar = avatar;
    _realName = realName;
}

  LegalPerson.fromJson(dynamic json) {
    _id = json['id'];
    _nickName = json['nickName'];
    _avatar = json['avatar'];
    _realName = json['realName'];
  }
  num? _id;
  String? _nickName;
  String? _avatar;
  String? _realName;
LegalPerson copyWith({  num? id,
  String? nickName,
  String? avatar,
  String? realName,
}) => LegalPerson(  id: id ?? _id,
  nickName: nickName ?? _nickName,
  avatar: avatar ?? _avatar,
  realName: realName ?? _realName,
);
  num? get id => _id;
  String? get nickName => _nickName;
  String? get avatar => _avatar;
  String? get realName => _realName;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['nickName'] = _nickName;
    map['avatar'] = _avatar;
    map['realName'] = _realName;
    return map;
  }

}

/// id : 0
/// nickName : "string"
/// avatar : "string"
/// realName : "string"

class ApplyCompanyUser {
  ApplyCompanyUser({
      num? id, 
      String? nickName, 
      String? avatar, 
      String? realName,}){
    _id = id;
    _nickName = nickName;
    _avatar = avatar;
    _realName = realName;
}

  ApplyCompanyUser.fromJson(dynamic json) {
    _id = json['id'];
    _nickName = json['nickName'];
    _avatar = json['avatar'];
    _realName = json['realName'];
  }
  num? _id;
  String? _nickName;
  String? _avatar;
  String? _realName;
ApplyCompanyUser copyWith({  num? id,
  String? nickName,
  String? avatar,
  String? realName,
}) => ApplyCompanyUser(  id: id ?? _id,
  nickName: nickName ?? _nickName,
  avatar: avatar ?? _avatar,
  realName: realName ?? _realName,
);
  num? get id => _id;
  String? get nickName => _nickName;
  String? get avatar => _avatar;
  String? get realName => _realName;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['nickName'] = _nickName;
    map['avatar'] = _avatar;
    map['realName'] = _realName;
    return map;
  }

}