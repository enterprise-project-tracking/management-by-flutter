/// code : 200
/// msg : "ok"
/// data : {"records":[{"id":1777693640121073666,"name":"科技型项目跟踪管理系统","beginTime":"2014-05-20 20:33:35","endTime":"2001-03-18 06:18:43","description":"深从也目断次道型便好产成来两较。厂造空争标格议毛特院后问题转则照。近都解数使万受品么更从她标真压。同级立使第则容状展量山农近群。","status":1,"charge":{"id":1770851265893519361,"nickName":"wwww","avatar":null,"realName":"eeee"},"totalDemand":10,"finishDemand":1},{"id":1778776741942702081,"name":"转间见社律音","beginTime":"1995-03-02 20:48:06","endTime":"2006-07-06 07:17:39","description":"为同内共技而它切劳始高表事。易地还几县算北同动标九量下统新。热接地界员加高打片位查长系受军。书照如里平统制三文也究实。家也只青东号便眼部统器具认照阶看。号实离据想清查率利称作从使说精米热属。","status":1,"charge":{"id":1764677020037554177,"nickName":"rrrr","avatar":"https://oss.sshine.ltd/tracking/public/20240407/a988416c0a0042549e61e9e6b2e9620e-scaled_IMG_20240407_113104.jpg","realName":"wwwww"},"totalDemand":10,"finishDemand":1},{"id":1778777786206957570,"name":"项目2","beginTime":"1995-03-02 20:48:06","endTime":"2006-07-06 07:17:39","description":"为同内共技而它切劳始高表事。易地还几县算北同动标九量下统新。热接地界员加高打片位查长系受军。书照如里平统制三文也究实。家也只青东号便眼部统器具认照阶看。号实离据想清查率利称作从使说精米热属。","status":1,"charge":{"id":1764677020037554177,"nickName":"rrrr","avatar":"https://oss.sshine.ltd/tracking/public/20240407/a988416c0a0042549e61e9e6b2e9620e-scaled_IMG_20240407_113104.jpg","realName":"wwwww"},"totalDemand":10,"finishDemand":1}],"total":3,"size":10,"current":1}

class ListProjectResponse {
  ListProjectResponse({
    num? code,
    String? msg,
    Data? data,
  }) {
    _code = code;
    _msg = msg;
    _data = data;
  }

  ListProjectResponse.fromJson(dynamic json) {
    _code = json['code'];
    _msg = json['msg'];
    _data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }
  num? _code;
  String? _msg;
  Data? _data;
  ListProjectResponse copyWith({
    num? code,
    String? msg,
    Data? data,
  }) =>
      ListProjectResponse(
        code: code ?? _code,
        msg: msg ?? _msg,
        data: data ?? _data,
      );
  num? get code => _code;
  String? get msg => _msg;
  Data? get data => _data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['code'] = _code;
    map['msg'] = _msg;
    if (_data != null) {
      map['data'] = _data?.toJson();
    }
    return map;
  }
}

/// records : [{"id":1777693640121073666,"name":"科技型项目跟踪管理系统","beginTime":"2014-05-20 20:33:35","endTime":"2001-03-18 06:18:43","description":"深从也目断次道型便好产成来两较。厂造空争标格议毛特院后问题转则照。近都解数使万受品么更从她标真压。同级立使第则容状展量山农近群。","status":1,"charge":{"id":1770851265893519361,"nickName":"wwww","avatar":null,"realName":"eeee"},"totalDemand":10,"finishDemand":1},{"id":1778776741942702081,"name":"转间见社律音","beginTime":"1995-03-02 20:48:06","endTime":"2006-07-06 07:17:39","description":"为同内共技而它切劳始高表事。易地还几县算北同动标九量下统新。热接地界员加高打片位查长系受军。书照如里平统制三文也究实。家也只青东号便眼部统器具认照阶看。号实离据想清查率利称作从使说精米热属。","status":1,"charge":{"id":1764677020037554177,"nickName":"rrrr","avatar":"https://oss.sshine.ltd/tracking/public/20240407/a988416c0a0042549e61e9e6b2e9620e-scaled_IMG_20240407_113104.jpg","realName":"wwwww"},"totalDemand":10,"finishDemand":1},{"id":1778777786206957570,"name":"项目2","beginTime":"1995-03-02 20:48:06","endTime":"2006-07-06 07:17:39","description":"为同内共技而它切劳始高表事。易地还几县算北同动标九量下统新。热接地界员加高打片位查长系受军。书照如里平统制三文也究实。家也只青东号便眼部统器具认照阶看。号实离据想清查率利称作从使说精米热属。","status":1,"charge":{"id":1764677020037554177,"nickName":"rrrr","avatar":"https://oss.sshine.ltd/tracking/public/20240407/a988416c0a0042549e61e9e6b2e9620e-scaled_IMG_20240407_113104.jpg","realName":"wwwww"},"totalDemand":10,"finishDemand":1}]
/// total : 3
/// size : 10
/// current : 1

class Data {
  Data({
    List<Records>? records,
    num? total,
    num? size,
    num? current,
  }) {
    _records = records;
    _total = total;
    _size = size;
    _current = current;
  }

  Data.fromJson(dynamic json) {
    if (json['records'] != null) {
      _records = [];
      json['records'].forEach((v) {
        _records?.add(Records.fromJson(v));
      });
    }
    _total = json['total'];
    _size = json['size'];
    _current = json['current'];
  }
  List<Records>? _records;
  num? _total;
  num? _size;
  num? _current;
  Data copyWith({
    List<Records>? records,
    num? total,
    num? size,
    num? current,
  }) =>
      Data(
        records: records ?? _records,
        total: total ?? _total,
        size: size ?? _size,
        current: current ?? _current,
      );
  List<Records>? get records => _records;
  num? get total => _total;
  num? get size => _size;
  num? get current => _current;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_records != null) {
      map['records'] = _records?.map((v) => v.toJson()).toList();
    }
    map['total'] = _total;
    map['size'] = _size;
    map['current'] = _current;
    return map;
  }
}

/// id : 1777693640121073666
/// name : "科技型项目跟踪管理系统"
/// beginTime : "2014-05-20 20:33:35"
/// endTime : "2001-03-18 06:18:43"
/// description : "深从也目断次道型便好产成来两较。厂造空争标格议毛特院后问题转则照。近都解数使万受品么更从她标真压。同级立使第则容状展量山农近群。"
/// status : 1
/// charge : {"id":1770851265893519361,"nickName":"wwww","avatar":null,"realName":"eeee"}
/// totalDemand : 10
/// finishDemand : 1

/// id : 1777693640121073666
/// name : "科技型项目跟踪管理系统"
/// beginTime : "2014-05-20 20:33:35"
/// endTime : "2001-03-18 06:18:43"
/// description : "深从也目断次道型便好产成来两较。厂造空争标格议毛特院后问题转则照。近都解数使万受品么更从她标真压。同级立使第则容状展量山农近群。"
/// status : 1
/// charge : {"id":1770851265893519361,"nickName":"wwww","avatar":null,"realName":"eeee"}
/// totalDemand : 10
/// finishDemand : 1
/// logoUrl : ""
/// backgroundUrl : ""

class Records {
  Records({
    num? id,
    String? name,
    String? beginTime,
    String? endTime,
    String? description,
    num? status,
    Charge? charge,
    num? totalDemand,
    num? finishDemand,
    String? logoUrl,
    String? backgroundUrl,
  }) {
    _id = id;
    _name = name;
    _beginTime = beginTime;
    _endTime = endTime;
    _description = description;
    _status = status;
    _charge = charge;
    _totalDemand = totalDemand;
    _finishDemand = finishDemand;
    _logoUrl = logoUrl;
    _backgroundUrl = backgroundUrl;
  }

  Records.fromJson(dynamic json) {
    _id = json['id'];
    _name = json['name'];
    _beginTime = json['beginTime'];
    _endTime = json['endTime'];
    _description = json['description'];
    _status = json['status'];
    _charge = json['charge'] != null ? Charge.fromJson(json['charge']) : null;
    _totalDemand = json['totalDemand'];
    _finishDemand = json['finishDemand'];
    _logoUrl = json['logoUrl'];
    _backgroundUrl = json['backgroundUrl'];
  }
  num? _id;
  String? _name;
  String? _beginTime;
  String? _endTime;
  String? _description;
  num? _status;
  Charge? _charge;
  num? _totalDemand;
  num? _finishDemand;
  String? _logoUrl;
  String? _backgroundUrl;
  Records copyWith({
    num? id,
    String? name,
    String? beginTime,
    String? endTime,
    String? description,
    num? status,
    Charge? charge,
    num? totalDemand,
    num? finishDemand,
    String? logoUrl,
    String? backgroundUrl,
  }) =>
      Records(
        id: id ?? _id,
        name: name ?? _name,
        beginTime: beginTime ?? _beginTime,
        endTime: endTime ?? _endTime,
        description: description ?? _description,
        status: status ?? _status,
        charge: charge ?? _charge,
        totalDemand: totalDemand ?? _totalDemand,
        finishDemand: finishDemand ?? _finishDemand,
        logoUrl: logoUrl ?? _logoUrl,
        backgroundUrl: backgroundUrl ?? _backgroundUrl,
      );
  num? get id => _id;
  String? get name => _name;
  String? get beginTime => _beginTime;
  String? get endTime => _endTime;
  String? get description => _description;
  num? get status => _status;
  Charge? get charge => _charge;
  num? get totalDemand => _totalDemand;
  num? get finishDemand => _finishDemand;
  String? get logoUrl => _logoUrl;
  String? get backgroundUrl => _backgroundUrl;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['name'] = _name;
    map['beginTime'] = _beginTime;
    map['endTime'] = _endTime;
    map['description'] = _description;
    map['status'] = _status;
    if (_charge != null) {
      map['charge'] = _charge?.toJson();
    }
    map['totalDemand'] = _totalDemand;
    map['finishDemand'] = _finishDemand;
    map['logoUrl'] = _logoUrl;
    map['backgroundUrl'] = _backgroundUrl;
    return map;
  }
}

/// id : 1770851265893519361
/// nickName : "wwww"
/// avatar : null
/// realName : "eeee"

class Charge {
  Charge({
    num? id,
    String? nickName,
    dynamic avatar,
    String? realName,
  }) {
    _id = id;
    _nickName = nickName;
    _avatar = avatar;
    _realName = realName;
  }

  Charge.fromJson(dynamic json) {
    _id = json['id'];
    _nickName = json['nickName'];
    _avatar = json['avatar'];
    _realName = json['realName'];
  }
  num? _id;
  String? _nickName;
  dynamic _avatar;
  String? _realName;
  Charge copyWith({
    num? id,
    String? nickName,
    dynamic avatar,
    String? realName,
  }) =>
      Charge(
        id: id ?? _id,
        nickName: nickName ?? _nickName,
        avatar: avatar ?? _avatar,
        realName: realName ?? _realName,
      );
  num? get id => _id;
  String? get nickName => _nickName;
  dynamic get avatar => _avatar;
  String? get realName => _realName;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['nickName'] = _nickName;
    map['avatar'] = _avatar;
    map['realName'] = _realName;
    return map;
  }
}
