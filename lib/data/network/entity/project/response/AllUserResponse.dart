/// code : 200
/// msg : "ok"
/// data : [{"type":"USER","avatar":"https://oss.sshine.ltd/tracking/public/20240407/a988416c0a0042549e61e9e6b2e9620e-scaled_IMG_20240407_113104.jpg","nickname":"rrrr","id":1764677020037554177,"email":"1742137309@qq.com","phone":null,"jobNumber":null,"createTime":"2024-03-24T18:08:05","roleId":0,"realName":"wwwww","groupId":0},{"type":"USER","avatar":null,"nickname":"wwww","id":1770851265893519361,"email":"174213730959@qq.com","phone":null,"jobNumber":null,"createTime":"2024-03-24T18:08:05","roleId":2,"realName":"eeee","groupId":0},{"type":"USER","avatar":"http://dummyimage.com/200x200","nickname":"孔艳","id":1771578963453849602,"email":"174213730996@qq.com","phone":null,"jobNumber":null,"createTime":"2024-03-24T18:08:05","roleId":0,"realName":"eeee","groupId":1775525325651898370}]

class AllUserResponse {
  AllUserResponse({
      num? code, 
      String? msg, 
      List<Data>? data,}){
    _code = code;
    _msg = msg;
    _data = data;
}

  AllUserResponse.fromJson(dynamic json) {
    _code = json['code'];
    _msg = json['msg'];
    if (json['data'] != null) {
      _data = [];
      json['data'].forEach((v) {
        _data?.add(Data.fromJson(v));
      });
    }
  }
  num? _code;
  String? _msg;
  List<Data>? _data;
AllUserResponse copyWith({  num? code,
  String? msg,
  List<Data>? data,
}) => AllUserResponse(  code: code ?? _code,
  msg: msg ?? _msg,
  data: data ?? _data,
);
  num? get code => _code;
  String? get msg => _msg;
  List<Data>? get data => _data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['code'] = _code;
    map['msg'] = _msg;
    if (_data != null) {
      map['data'] = _data?.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// type : "USER"
/// avatar : "https://oss.sshine.ltd/tracking/public/20240407/a988416c0a0042549e61e9e6b2e9620e-scaled_IMG_20240407_113104.jpg"
/// nickname : "rrrr"
/// id : 1764677020037554177
/// email : "1742137309@qq.com"
/// phone : null
/// jobNumber : null
/// createTime : "2024-03-24T18:08:05"
/// roleId : 0
/// realName : "wwwww"
/// groupId : 0

class Data {
  Data({
      String? type, 
      String? avatar, 
      String? nickname, 
      num? id, 
      String? email, 
      dynamic phone, 
      dynamic jobNumber, 
      String? createTime, 
      num? roleId, 
      String? realName, 
      num? groupId,}){
    _type = type;
    _avatar = avatar;
    _nickname = nickname;
    _id = id;
    _email = email;
    _phone = phone;
    _jobNumber = jobNumber;
    _createTime = createTime;
    _roleId = roleId;
    _realName = realName;
    _groupId = groupId;
}

  Data.fromJson(dynamic json) {
    _type = json['type'];
    _avatar = json['avatar'];
    _nickname = json['nickname'];
    _id = json['id'];
    _email = json['email'];
    _phone = json['phone'];
    _jobNumber = json['jobNumber'];
    _createTime = json['createTime'];
    _roleId = json['roleId'];
    _realName = json['realName'];
    _groupId = json['groupId'];
  }
  String? _type;
  String? _avatar;
  String? _nickname;
  num? _id;
  String? _email;
  dynamic _phone;
  dynamic _jobNumber;
  String? _createTime;
  num? _roleId;
  String? _realName;
  num? _groupId;
Data copyWith({  String? type,
  String? avatar,
  String? nickname,
  num? id,
  String? email,
  dynamic phone,
  dynamic jobNumber,
  String? createTime,
  num? roleId,
  String? realName,
  num? groupId,
}) => Data(  type: type ?? _type,
  avatar: avatar ?? _avatar,
  nickname: nickname ?? _nickname,
  id: id ?? _id,
  email: email ?? _email,
  phone: phone ?? _phone,
  jobNumber: jobNumber ?? _jobNumber,
  createTime: createTime ?? _createTime,
  roleId: roleId ?? _roleId,
  realName: realName ?? _realName,
  groupId: groupId ?? _groupId,
);
  String? get type => _type;
  String? get avatar => _avatar;
  String? get nickname => _nickname;
  num? get id => _id;
  String? get email => _email;
  dynamic get phone => _phone;
  dynamic get jobNumber => _jobNumber;
  String? get createTime => _createTime;
  num? get roleId => _roleId;
  String? get realName => _realName;
  num? get groupId => _groupId;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['type'] = _type;
    map['avatar'] = _avatar;
    map['nickname'] = _nickname;
    map['id'] = _id;
    map['email'] = _email;
    map['phone'] = _phone;
    map['jobNumber'] = _jobNumber;
    map['createTime'] = _createTime;
    map['roleId'] = _roleId;
    map['realName'] = _realName;
    map['groupId'] = _groupId;
    return map;
  }

}