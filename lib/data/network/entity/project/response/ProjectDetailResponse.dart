/// code : 200
/// msg : "ok"
/// data : {"id":1777693640121073666,"companyUsers":[{"id":1764677020037554177,"nickName":"rrrr","avatar":"https://oss.sshine.ltd/tracking/public/20240407/a988416c0a0042549e61e9e6b2e9620e-scaled_IMG_20240407_113104.jpg","realName":"wwwww"}],"name":"科技型项目跟踪管理系统","beginTime":"2014-05-20 20:33:35","endTime":"2001-03-18 06:18:43","estimateCost":666,"description":"深从也目断次道型便好产成来两较。厂造空争标格议毛特院后问题转则照。近都解数使万受品么更从她标真压。同级立使第则容状展量山农近群。","status":1,"charge":{"id":1770851265893519361,"nickName":"wwww","avatar":null,"realName":"eeee"},"conferenceResolutions":[{"id":1777693735138836482,"name":"飞义器号","suffix":"docx","url":"http://fonmyhvet.中国/mfdm","createTime":"2024-04-09T21:43:09","fileType":1,"creator":{"id":1770851265893519361,"nickName":"wwww","avatar":null,"realName":"eeee"}}],"projectPlanningPaper":[{"id":1777693735403077634,"name":"飞义器号2","suffix":"docx","url":"http://ocgulmcx.ga/texkqm","createTime":"2024-04-09T21:43:09","fileType":2,"creator":{"id":1770851265893519361,"nickName":"wwww","avatar":null,"realName":"eeee"}}],"totalDemand":7,"finishDemand":0,"logoUrl":"","backgroundUrl":""}

class ProjectDetailResponse {
  ProjectDetailResponse({
    num? code,
    String? msg,
    Data? data,
  }) {
    _code = code;
    _msg = msg;
    _data = data;
  }

  ProjectDetailResponse.fromJson(dynamic json) {
    _code = json['code'];
    _msg = json['msg'];
    _data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }
  num? _code;
  String? _msg;
  Data? _data;
  ProjectDetailResponse copyWith({
    num? code,
    String? msg,
    Data? data,
  }) =>
      ProjectDetailResponse(
        code: code ?? _code,
        msg: msg ?? _msg,
        data: data ?? _data,
      );
  num? get code => _code;
  String? get msg => _msg;
  Data? get data => _data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['code'] = _code;
    map['msg'] = _msg;
    if (_data != null) {
      map['data'] = _data?.toJson();
    }
    return map;
  }
}

/// id : 1777693640121073666
/// companyUsers : [{"id":1764677020037554177,"nickName":"rrrr","avatar":"https://oss.sshine.ltd/tracking/public/20240407/a988416c0a0042549e61e9e6b2e9620e-scaled_IMG_20240407_113104.jpg","realName":"wwwww"}]
/// name : "科技型项目跟踪管理系统"
/// beginTime : "2014-05-20 20:33:35"
/// endTime : "2001-03-18 06:18:43"
/// estimateCost : 666
/// description : "深从也目断次道型便好产成来两较。厂造空争标格议毛特院后问题转则照。近都解数使万受品么更从她标真压。同级立使第则容状展量山农近群。"
/// status : 1
/// charge : {"id":1770851265893519361,"nickName":"wwww","avatar":null,"realName":"eeee"}
/// conferenceResolutions : [{"id":1777693735138836482,"name":"飞义器号","suffix":"docx","url":"http://fonmyhvet.中国/mfdm","createTime":"2024-04-09T21:43:09","fileType":1,"creator":{"id":1770851265893519361,"nickName":"wwww","avatar":null,"realName":"eeee"}}]
/// projectPlanningPaper : [{"id":1777693735403077634,"name":"飞义器号2","suffix":"docx","url":"http://ocgulmcx.ga/texkqm","createTime":"2024-04-09T21:43:09","fileType":2,"creator":{"id":1770851265893519361,"nickName":"wwww","avatar":null,"realName":"eeee"}}]
/// totalDemand : 7
/// finishDemand : 0
/// logoUrl : ""
/// backgroundUrl : ""

class Data {
  Data({
    num? id,
    List<CompanyUsers>? companyUsers,
    String? name,
    String? beginTime,
    String? endTime,
    num? estimateCost,
    String? description,
    num? status,
    Charge? charge,
    List<ConferenceResolutions>? conferenceResolutions,
    List<ProjectPlanningPaper>? projectPlanningPaper,
    num? totalDemand,
    num? finishDemand,
    String? logoUrl,
    String? backgroundUrl,
  }) {
    _id = id;
    _companyUsers = companyUsers;
    _name = name;
    _beginTime = beginTime;
    _endTime = endTime;
    _estimateCost = estimateCost;
    _description = description;
    _status = status;
    _charge = charge;
    _conferenceResolutions = conferenceResolutions;
    _projectPlanningPaper = projectPlanningPaper;
    _totalDemand = totalDemand;
    _finishDemand = finishDemand;
    _logoUrl = logoUrl;
    _backgroundUrl = backgroundUrl;
  }

  Data.fromJson(dynamic json) {
    _id = json['id'];
    if (json['companyUsers'] != null) {
      _companyUsers = [];
      json['companyUsers'].forEach((v) {
        _companyUsers?.add(CompanyUsers.fromJson(v));
      });
    }
    _name = json['name'];
    _beginTime = json['beginTime'];
    _endTime = json['endTime'];
    _estimateCost = json['estimateCost'];
    _description = json['description'];
    _status = json['status'];
    _charge = json['charge'] != null ? Charge.fromJson(json['charge']) : null;
    if (json['conferenceResolutions'] != null) {
      _conferenceResolutions = [];
      json['conferenceResolutions'].forEach((v) {
        _conferenceResolutions?.add(ConferenceResolutions.fromJson(v));
      });
    }
    if (json['projectPlanningPaper'] != null) {
      _projectPlanningPaper = [];
      json['projectPlanningPaper'].forEach((v) {
        _projectPlanningPaper?.add(ProjectPlanningPaper.fromJson(v));
      });
    }
    _totalDemand = json['totalDemand'];
    _finishDemand = json['finishDemand'];
    _logoUrl = json['logoUrl'];
    _backgroundUrl = json['backgroundUrl'];
  }
  num? _id;
  List<CompanyUsers>? _companyUsers;
  String? _name;
  String? _beginTime;
  String? _endTime;
  num? _estimateCost;
  String? _description;
  num? _status;
  Charge? _charge;
  List<ConferenceResolutions>? _conferenceResolutions;
  List<ProjectPlanningPaper>? _projectPlanningPaper;
  num? _totalDemand;
  num? _finishDemand;
  String? _logoUrl;
  String? _backgroundUrl;
  Data copyWith({
    num? id,
    List<CompanyUsers>? companyUsers,
    String? name,
    String? beginTime,
    String? endTime,
    num? estimateCost,
    String? description,
    num? status,
    Charge? charge,
    List<ConferenceResolutions>? conferenceResolutions,
    List<ProjectPlanningPaper>? projectPlanningPaper,
    num? totalDemand,
    num? finishDemand,
    String? logoUrl,
    String? backgroundUrl,
  }) =>
      Data(
        id: id ?? _id,
        companyUsers: companyUsers ?? _companyUsers,
        name: name ?? _name,
        beginTime: beginTime ?? _beginTime,
        endTime: endTime ?? _endTime,
        estimateCost: estimateCost ?? _estimateCost,
        description: description ?? _description,
        status: status ?? _status,
        charge: charge ?? _charge,
        conferenceResolutions: conferenceResolutions ?? _conferenceResolutions,
        projectPlanningPaper: projectPlanningPaper ?? _projectPlanningPaper,
        totalDemand: totalDemand ?? _totalDemand,
        finishDemand: finishDemand ?? _finishDemand,
        logoUrl: logoUrl ?? _logoUrl,
        backgroundUrl: backgroundUrl ?? _backgroundUrl,
      );
  num? get id => _id;
  List<CompanyUsers>? get companyUsers => _companyUsers;
  String? get name => _name;
  String? get beginTime => _beginTime;
  String? get endTime => _endTime;
  num? get estimateCost => _estimateCost;
  String? get description => _description;
  num? get status => _status;
  Charge? get charge => _charge;
  List<ConferenceResolutions>? get conferenceResolutions =>
      _conferenceResolutions;
  List<ProjectPlanningPaper>? get projectPlanningPaper => _projectPlanningPaper;
  num? get totalDemand => _totalDemand;
  num? get finishDemand => _finishDemand;
  String? get logoUrl => _logoUrl;
  String? get backgroundUrl => _backgroundUrl;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    if (_companyUsers != null) {
      map['companyUsers'] = _companyUsers?.map((v) => v.toJson()).toList();
    }
    map['name'] = _name;
    map['beginTime'] = _beginTime;
    map['endTime'] = _endTime;
    map['estimateCost'] = _estimateCost;
    map['description'] = _description;
    map['status'] = _status;
    if (_charge != null) {
      map['charge'] = _charge?.toJson();
    }
    if (_conferenceResolutions != null) {
      map['conferenceResolutions'] =
          _conferenceResolutions?.map((v) => v.toJson()).toList();
    }
    if (_projectPlanningPaper != null) {
      map['projectPlanningPaper'] =
          _projectPlanningPaper?.map((v) => v.toJson()).toList();
    }
    map['totalDemand'] = _totalDemand;
    map['finishDemand'] = _finishDemand;
    map['logoUrl'] = _logoUrl;
    map['backgroundUrl'] = _backgroundUrl;
    return map;
  }
}

/// id : 1777693735403077634
/// name : "飞义器号2"
/// suffix : "docx"
/// url : "http://ocgulmcx.ga/texkqm"
/// createTime : "2024-04-09T21:43:09"
/// fileType : 2
/// creator : {"id":1770851265893519361,"nickName":"wwww","avatar":null,"realName":"eeee"}

class ProjectPlanningPaper {
  ProjectPlanningPaper({
    num? id,
    String? name,
    String? suffix,
    String? url,
    String? createTime,
    num? fileType,
    Creator? creator,
  }) {
    _id = id;
    _name = name;
    _suffix = suffix;
    _url = url;
    _createTime = createTime;
    _fileType = fileType;
    _creator = creator;
  }

  ProjectPlanningPaper.fromJson(dynamic json) {
    _id = json['id'];
    _name = json['name'];
    _suffix = json['suffix'];
    _url = json['url'];
    _createTime = json['createTime'];
    _fileType = json['fileType'];
    _creator =
        json['creator'] != null ? Creator.fromJson(json['creator']) : null;
  }
  num? _id;
  String? _name;
  String? _suffix;
  String? _url;
  String? _createTime;
  num? _fileType;
  Creator? _creator;
  ProjectPlanningPaper copyWith({
    num? id,
    String? name,
    String? suffix,
    String? url,
    String? createTime,
    num? fileType,
    Creator? creator,
  }) =>
      ProjectPlanningPaper(
        id: id ?? _id,
        name: name ?? _name,
        suffix: suffix ?? _suffix,
        url: url ?? _url,
        createTime: createTime ?? _createTime,
        fileType: fileType ?? _fileType,
        creator: creator ?? _creator,
      );
  num? get id => _id;
  String? get name => _name;
  String? get suffix => _suffix;
  String? get url => _url;
  String? get createTime => _createTime;
  num? get fileType => _fileType;
  Creator? get creator => _creator;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['name'] = _name;
    map['suffix'] = _suffix;
    map['url'] = _url;
    map['createTime'] = _createTime;
    map['fileType'] = _fileType;
    if (_creator != null) {
      map['creator'] = _creator?.toJson();
    }
    return map;
  }
}

/// id : 1770851265893519361
/// nickName : "wwww"
/// avatar : null
/// realName : "eeee"

class Creator {
  Creator({
    num? id,
    String? nickName,
    dynamic avatar,
    String? realName,
  }) {
    _id = id;
    _nickName = nickName;
    _avatar = avatar;
    _realName = realName;
  }

  Creator.fromJson(dynamic json) {
    _id = json['id'];
    _nickName = json['nickName'];
    _avatar = json['avatar'];
    _realName = json['realName'];
  }
  num? _id;
  String? _nickName;
  dynamic _avatar;
  String? _realName;
  Creator copyWith({
    num? id,
    String? nickName,
    dynamic avatar,
    String? realName,
  }) =>
      Creator(
        id: id ?? _id,
        nickName: nickName ?? _nickName,
        avatar: avatar ?? _avatar,
        realName: realName ?? _realName,
      );
  num? get id => _id;
  String? get nickName => _nickName;
  dynamic get avatar => _avatar;
  String? get realName => _realName;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['nickName'] = _nickName;
    map['avatar'] = _avatar;
    map['realName'] = _realName;
    return map;
  }
}

/// id : 1777693735138836482
/// name : "飞义器号"
/// suffix : "docx"
/// url : "http://fonmyhvet.中国/mfdm"
/// createTime : "2024-04-09T21:43:09"
/// fileType : 1
/// creator : {"id":1770851265893519361,"nickName":"wwww","avatar":null,"realName":"eeee"}

class ConferenceResolutions {
  ConferenceResolutions({
    num? id,
    String? name,
    String? suffix,
    String? url,
    String? createTime,
    num? fileType,
    Creator? creator,
  }) {
    _id = id;
    _name = name;
    _suffix = suffix;
    _url = url;
    _createTime = createTime;
    _fileType = fileType;
    _creator = creator;
  }

  ConferenceResolutions.fromJson(dynamic json) {
    _id = json['id'];
    _name = json['name'];
    _suffix = json['suffix'];
    _url = json['url'];
    _createTime = json['createTime'];
    _fileType = json['fileType'];
    _creator =
        json['creator'] != null ? Creator.fromJson(json['creator']) : null;
  }
  num? _id;
  String? _name;
  String? _suffix;
  String? _url;
  String? _createTime;
  num? _fileType;
  Creator? _creator;
  ConferenceResolutions copyWith({
    num? id,
    String? name,
    String? suffix,
    String? url,
    String? createTime,
    num? fileType,
    Creator? creator,
  }) =>
      ConferenceResolutions(
        id: id ?? _id,
        name: name ?? _name,
        suffix: suffix ?? _suffix,
        url: url ?? _url,
        createTime: createTime ?? _createTime,
        fileType: fileType ?? _fileType,
        creator: creator ?? _creator,
      );
  num? get id => _id;
  String? get name => _name;
  String? get suffix => _suffix;
  String? get url => _url;
  String? get createTime => _createTime;
  num? get fileType => _fileType;
  Creator? get creator => _creator;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['name'] = _name;
    map['suffix'] = _suffix;
    map['url'] = _url;
    map['createTime'] = _createTime;
    map['fileType'] = _fileType;
    if (_creator != null) {
      map['creator'] = _creator?.toJson();
    }
    return map;
  }
}

/// id : 1770851265893519361
/// nickName : "wwww"
/// avatar : null
/// realName : "eeee"

// class Creator {
//   Creator({
//     num? id,
//     String? nickName,
//     dynamic avatar,
//     String? realName,
//   }) {
//     _id = id;
//     _nickName = nickName;
//     _avatar = avatar;
//     _realName = realName;
//   }
//
//   Creator.fromJson(dynamic json) {
//     _id = json['id'];
//     _nickName = json['nickName'];
//     _avatar = json['avatar'];
//     _realName = json['realName'];
//   }
//   num? _id;
//   String? _nickName;
//   dynamic _avatar;
//   String? _realName;
//   Creator copyWith({
//     num? id,
//     String? nickName,
//     dynamic avatar,
//     String? realName,
//   }) =>
//       Creator(
//         id: id ?? _id,
//         nickName: nickName ?? _nickName,
//         avatar: avatar ?? _avatar,
//         realName: realName ?? _realName,
//       );
//   num? get id => _id;
//   String? get nickName => _nickName;
//   dynamic get avatar => _avatar;
//   String? get realName => _realName;
//
//   Map<String, dynamic> toJson() {
//     final map = <String, dynamic>{};
//     map['id'] = _id;
//     map['nickName'] = _nickName;
//     map['avatar'] = _avatar;
//     map['realName'] = _realName;
//     return map;
//   }
// }

/// id : 1770851265893519361
/// nickName : "wwww"
/// avatar : null
/// realName : "eeee"

class Charge {
  Charge({
    num? id,
    String? nickName,
    dynamic avatar,
    String? realName,
  }) {
    _id = id;
    _nickName = nickName;
    _avatar = avatar;
    _realName = realName;
  }

  Charge.fromJson(dynamic json) {
    _id = json['id'];
    _nickName = json['nickName'];
    _avatar = json['avatar'];
    _realName = json['realName'];
  }
  num? _id;
  String? _nickName;
  dynamic _avatar;
  String? _realName;
  Charge copyWith({
    num? id,
    String? nickName,
    dynamic avatar,
    String? realName,
  }) =>
      Charge(
        id: id ?? _id,
        nickName: nickName ?? _nickName,
        avatar: avatar ?? _avatar,
        realName: realName ?? _realName,
      );
  num? get id => _id;
  String? get nickName => _nickName;
  dynamic get avatar => _avatar;
  String? get realName => _realName;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['nickName'] = _nickName;
    map['avatar'] = _avatar;
    map['realName'] = _realName;
    return map;
  }
}

/// id : 1764677020037554177
/// nickName : "rrrr"
/// avatar : "https://oss.sshine.ltd/tracking/public/20240407/a988416c0a0042549e61e9e6b2e9620e-scaled_IMG_20240407_113104.jpg"
/// realName : "wwwww"

class CompanyUsers {
  CompanyUsers({
    num? id,
    String? nickName,
    String? avatar,
    String? realName,
  }) {
    _id = id;
    _nickName = nickName;
    _avatar = avatar;
    _realName = realName;
  }

  CompanyUsers.fromJson(dynamic json) {
    _id = json['id'];
    _nickName = json['nickName'];
    _avatar = json['avatar'];
    _realName = json['realName'];
  }
  num? _id;
  String? _nickName;
  String? _avatar;
  String? _realName;
  CompanyUsers copyWith({
    num? id,
    String? nickName,
    String? avatar,
    String? realName,
  }) =>
      CompanyUsers(
        id: id ?? _id,
        nickName: nickName ?? _nickName,
        avatar: avatar ?? _avatar,
        realName: realName ?? _realName,
      );
  num? get id => _id;
  String? get nickName => _nickName;
  String? get avatar => _avatar;
  String? get realName => _realName;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['nickName'] = _nickName;
    map['avatar'] = _avatar;
    map['realName'] = _realName;
    return map;
  }
}
