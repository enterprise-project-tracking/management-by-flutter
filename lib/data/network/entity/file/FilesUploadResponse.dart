/// code : 200
/// msg : "ok"
/// data : [{"url":"https://oss.sshine.ltd/tracking/public/20240416/f600efa6fb7146f7ac80c96535331cd9-HostKeyDB.txt","name":"HostKeyDB.txt"},{"url":"https://oss.sshine.ltd/tracking/public/20240416/d4a9972f95ff44f9a0239f7b70c9776e-Picture1_爱奇艺_爱奇艺.jpg","name":"Picture1_爱奇艺_爱奇艺.jpg"}]

class FilesUploadResponse {
  FilesUploadResponse({
      num? code, 
      String? msg, 
      List<Data>? data,}){
    _code = code;
    _msg = msg;
    _data = data;
}

  FilesUploadResponse.fromJson(dynamic json) {
    _code = json['code'];
    _msg = json['msg'];
    if (json['data'] != null) {
      _data = [];
      json['data'].forEach((v) {
        _data?.add(Data.fromJson(v));
      });
    }
  }
  num? _code;
  String? _msg;
  List<Data>? _data;
FilesUploadResponse copyWith({  num? code,
  String? msg,
  List<Data>? data,
}) => FilesUploadResponse(  code: code ?? _code,
  msg: msg ?? _msg,
  data: data ?? _data,
);
  num? get code => _code;
  String? get msg => _msg;
  List<Data>? get data => _data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['code'] = _code;
    map['msg'] = _msg;
    if (_data != null) {
      map['data'] = _data?.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// url : "https://oss.sshine.ltd/tracking/public/20240416/f600efa6fb7146f7ac80c96535331cd9-HostKeyDB.txt"
/// name : "HostKeyDB.txt"

class Data {
  Data({
      String? url, 
      String? name,}){
    _url = url;
    _name = name;
}

  Data.fromJson(dynamic json) {
    _url = json['url'];
    _name = json['name'];
  }
  String? _url;
  String? _name;
Data copyWith({  String? url,
  String? name,
}) => Data(  url: url ?? _url,
  name: name ?? _name,
);
  String? get url => _url;
  String? get name => _name;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['url'] = _url;
    map['name'] = _name;
    return map;
  }

}