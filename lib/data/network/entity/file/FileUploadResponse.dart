/// code : 200
/// msg : "ok"
/// data : {"url":"https://oss.sshine.ltd/tracking/public/20240405/8a0c7f293772474fb8f8bb69b6c6f751-屏幕截图 2022-08-28 234420.png","name":"屏幕截图 2022-08-28 234420.png"}

class FileUploadResponse {
  FileUploadResponse({
    num? code,
    String? msg,
    Data? data,
  }) {
    _code = code;
    _msg = msg;
    _data = data;
  }

  FileUploadResponse.fromJson(dynamic json) {
    _code = json['code'];
    _msg = json['msg'];
    _data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }
  num? _code;
  String? _msg;
  Data? _data;
  FileUploadResponse copyWith({
    num? code,
    String? msg,
    Data? data,
  }) =>
      FileUploadResponse(
        code: code ?? _code,
        msg: msg ?? _msg,
        data: data ?? _data,
      );
  num? get code => _code;
  String? get msg => _msg;
  Data? get data => _data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['code'] = _code;
    map['msg'] = _msg;
    if (_data != null) {
      map['data'] = _data?.toJson();
    }
    return map;
  }
}

/// url : "https://oss.sshine.ltd/tracking/public/20240405/8a0c7f293772474fb8f8bb69b6c6f751-屏幕截图 2022-08-28 234420.png"
/// name : "屏幕截图 2022-08-28 234420.png"

class Data {
  Data({
    String? url,
    String? name,
  }) {
    _url = url;
    _name = name;
  }

  Data.fromJson(dynamic json) {
    _url = json['url'];
    _name = json['name'];
  }
  String? _url;
  String? _name;
  Data copyWith({
    String? url,
    String? name,
  }) =>
      Data(
        url: url ?? _url,
        name: name ?? _name,
      );
  String? get url => _url;
  String? get name => _name;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['url'] = _url;
    map['name'] = _name;
    return map;
  }
}
