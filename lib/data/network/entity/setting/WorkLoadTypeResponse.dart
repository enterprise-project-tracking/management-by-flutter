/// code : 200
/// msg : "ok"
/// data : {"workloadType":1}

class WorkLoadTypeResponse {
  WorkLoadTypeResponse({
      num? code, 
      String? msg, 
      Data? data,}){
    _code = code;
    _msg = msg;
    _data = data;
}

  WorkLoadTypeResponse.fromJson(dynamic json) {
    _code = json['code'];
    _msg = json['msg'];
    _data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }
  num? _code;
  String? _msg;
  Data? _data;
WorkLoadTypeResponse copyWith({  num? code,
  String? msg,
  Data? data,
}) => WorkLoadTypeResponse(  code: code ?? _code,
  msg: msg ?? _msg,
  data: data ?? _data,
);
  num? get code => _code;
  String? get msg => _msg;
  Data? get data => _data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['code'] = _code;
    map['msg'] = _msg;
    if (_data != null) {
      map['data'] = _data?.toJson();
    }
    return map;
  }

}

/// workloadType : 1

class Data {
  Data({
      num? workloadType,}){
    _workloadType = workloadType;
}

  Data.fromJson(dynamic json) {
    _workloadType = json['workloadType'];
  }
  num? _workloadType;
Data copyWith({  num? workloadType,
}) => Data(  workloadType: workloadType ?? _workloadType,
);
  num? get workloadType => _workloadType;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['workloadType'] = _workloadType;
    return map;
  }

}