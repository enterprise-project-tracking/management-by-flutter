/// code : 200
/// msg : "ok"
/// data : [{"name":"测试","type":"USER","avatar":"http://dummyimage.com/100x100","nickname":"rrrr","id":1764677020037554177,"userId":1763201736960532482},{"type":"USER","avatar":null,"nickname":"wwww","id":1770851265893519361,"userId":1769025445860397058},{"type":"USER","avatar":null,"nickname":"ddd","id":1771578963453849602,"userId":1769025485064556546},{"type":"USER","avatar":null,"nickname":"97","id":1771581470808780801,"userId":1769025486033440769}]

class ListMemberAndGroupResponse {
  ListMemberAndGroupResponse({
    num? code,
    String? msg,
    List<Data>? data,
  }) {
    _code = code;
    _msg = msg;
    _data = data;
  }

  ListMemberAndGroupResponse.fromJson(dynamic json) {
    _code = json['code'];
    _msg = json['msg'];
    if (json['data'] != null) {
      _data = [];
      json['data'].forEach((v) {
        _data?.add(Data.fromJson(v));
      });
    }
  }
  num? _code;
  String? _msg;
  List<Data>? _data;
  ListMemberAndGroupResponse copyWith({
    num? code,
    String? msg,
    List<Data>? data,
  }) =>
      ListMemberAndGroupResponse(
        code: code ?? _code,
        msg: msg ?? _msg,
        data: data ?? _data,
      );
  num? get code => _code;
  String? get msg => _msg;
  List<Data>? get data => _data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['code'] = _code;
    map['msg'] = _msg;
    if (_data != null) {
      map['data'] = _data?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// name : "测试"
/// type : "USER"
/// avatar : "http://dummyimage.com/100x100"
/// nickname : "rrrr"
/// id : 1764677020037554177
/// userId : 1763201736960532482

class Data {
  Data({
    String? name,
    String? type,
    String? avatar,
    String? nickname,
    num? id,
    num? userId,
  }) {
    _name = name;
    _type = type;
    _avatar = avatar;
    _nickname = nickname;
    _id = id;
    _userId = userId;
  }

  Data.fromJson(dynamic json) {
    _name = json['name'];
    _type = json['type'];
    _avatar = json['avatar'];
    _nickname = json['nickname'];
    _id = json['id'];
    _userId = json['userId'];
  }
  String? _name;
  String? _type;
  String? _avatar;
  String? _nickname;
  num? _id;
  num? _userId;
  Data copyWith({
    String? name,
    String? type,
    String? avatar,
    String? nickname,
    num? id,
    num? userId,
  }) =>
      Data(
        name: name ?? _name,
        type: type ?? _type,
        avatar: avatar ?? _avatar,
        nickname: nickname ?? _nickname,
        id: id ?? _id,
        userId: userId ?? _userId,
      );
  String? get name => _name;
  String? get type => _type;
  String? get avatar => _avatar;
  String? get nickname => _nickname;
  num? get id => _id;
  num? get userId => _userId;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['name'] = _name;
    map['type'] = _type;
    map['avatar'] = _avatar;
    map['nickname'] = _nickname;
    map['id'] = _id;
    map['userId'] = _userId;
    return map;
  }
}
