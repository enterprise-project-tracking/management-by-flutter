/// code : 200
/// msg : "ok"
/// data : {"id":1764677020037554177,"jobNumber":null,"userId":1763201736960532482,"createTime":"2024-03-24T18:08:05","roleId":0,"avatar":"https://oss.sshine.ltd/tracking/public/20240407/a988416c0a0042549e61e9e6b2e9620e-scaled_IMG_20240407_113104.jpg","nickName":"rrrr"}

class CompanyMemberDetailResponse {
  CompanyMemberDetailResponse({
    num? code,
    String? msg,
    Data? data,
  }) {
    _code = code;
    _msg = msg;
    _data = data;
  }

  CompanyMemberDetailResponse.fromJson(dynamic json) {
    _code = json['code'];
    _msg = json['msg'];
    _data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }
  num? _code;
  String? _msg;
  Data? _data;
  CompanyMemberDetailResponse copyWith({
    num? code,
    String? msg,
    Data? data,
  }) =>
      CompanyMemberDetailResponse(
        code: code ?? _code,
        msg: msg ?? _msg,
        data: data ?? _data,
      );
  num? get code => _code;
  String? get msg => _msg;
  Data? get data => _data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['code'] = _code;
    map['msg'] = _msg;
    if (_data != null) {
      map['data'] = _data?.toJson();
    }
    return map;
  }
}

/// id : 1764677020037554177
/// jobNumber : null
/// userId : 1763201736960532482
/// createTime : "2024-03-24T18:08:05"
/// roleId : 0
/// avatar : "https://oss.sshine.ltd/tracking/public/20240407/a988416c0a0042549e61e9e6b2e9620e-scaled_IMG_20240407_113104.jpg"
/// nickName : "rrrr"

class Data {
  Data({
    num? id,
    dynamic jobNumber,
    num? userId,
    String? createTime,
    num? roleId,
    String? avatar,
    String? nickName,
  }) {
    _id = id;
    _jobNumber = jobNumber;
    _userId = userId;
    _createTime = createTime;
    _roleId = roleId;
    _avatar = avatar;
    _nickName = nickName;
  }

  Data.fromJson(dynamic json) {
    _id = json['id'];
    _jobNumber = json['jobNumber'];
    _userId = json['userId'];
    _createTime = json['createTime'];
    _roleId = json['roleId'];
    _avatar = json['avatar'];
    _nickName = json['nickName'];
  }
  num? _id;
  dynamic _jobNumber;
  num? _userId;
  String? _createTime;
  num? _roleId;
  String? _avatar;
  String? _nickName;
  Data copyWith({
    num? id,
    dynamic jobNumber,
    num? userId,
    String? createTime,
    num? roleId,
    String? avatar,
    String? nickName,
  }) =>
      Data(
        id: id ?? _id,
        jobNumber: jobNumber ?? _jobNumber,
        userId: userId ?? _userId,
        createTime: createTime ?? _createTime,
        roleId: roleId ?? _roleId,
        avatar: avatar ?? _avatar,
        nickName: nickName ?? _nickName,
      );
  num? get id => _id;
  dynamic get jobNumber => _jobNumber;
  num? get userId => _userId;
  String? get createTime => _createTime;
  num? get roleId => _roleId;
  String? get avatar => _avatar;
  String? get nickName => _nickName;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['jobNumber'] = _jobNumber;
    map['userId'] = _userId;
    map['createTime'] = _createTime;
    map['roleId'] = _roleId;
    map['avatar'] = _avatar;
    map['nickName'] = _nickName;
    return map;
  }
}
