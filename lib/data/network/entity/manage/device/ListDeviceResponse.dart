/// code : 200
/// msg : "ok"
/// data : {"records":[{"id":1768288023862235138,"remark":"aute","picUrl":"http://dummyimage.com/400x400","value":7,"name":"由较认面素都","position":"consectetur","createTime":"2024-03-14T22:48:13","updateTime":"2024-03-14T22:48:13"},{"id":1768289041832411137,"remark":"sunt ut","picUrl":"http://dummyimage.com/400x400","value":96,"name":"很形将自交","position":null,"createTime":"2024-03-14T22:52:15","updateTime":"2024-03-14T22:52:15"}],"total":2,"size":10,"current":1}

class ListDeviceResponse {
  ListDeviceResponse({
    num? code,
    String? msg,
    Data? data,
  }) {
    _code = code;
    _msg = msg;
    _data = data;
  }

  ListDeviceResponse.fromJson(dynamic json) {
    _code = json['code'];
    _msg = json['msg'];
    _data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }
  num? _code;
  String? _msg;
  Data? _data;
  ListDeviceResponse copyWith({
    num? code,
    String? msg,
    Data? data,
  }) =>
      ListDeviceResponse(
        code: code ?? _code,
        msg: msg ?? _msg,
        data: data ?? _data,
      );
  num? get code => _code;
  String? get msg => _msg;
  Data? get data => _data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['code'] = _code;
    map['msg'] = _msg;
    if (_data != null) {
      map['data'] = _data?.toJson();
    }
    return map;
  }
}

/// records : [{"id":1768288023862235138,"remark":"aute","picUrl":"http://dummyimage.com/400x400","value":7,"name":"由较认面素都","position":"consectetur","createTime":"2024-03-14T22:48:13","updateTime":"2024-03-14T22:48:13"},{"id":1768289041832411137,"remark":"sunt ut","picUrl":"http://dummyimage.com/400x400","value":96,"name":"很形将自交","position":null,"createTime":"2024-03-14T22:52:15","updateTime":"2024-03-14T22:52:15"}]
/// total : 2
/// size : 10
/// current : 1

class Data {
  Data({
    List<Records>? records,
    num? total,
    num? size,
    num? current,
  }) {
    _records = records;
    _total = total;
    _size = size;
    _current = current;
  }

  Data.fromJson(dynamic json) {
    if (json['records'] != null) {
      _records = [];
      json['records'].forEach((v) {
        _records?.add(Records.fromJson(v));
      });
    }
    _total = json['total'];
    _size = json['size'];
    _current = json['current'];
  }
  List<Records>? _records;
  num? _total;
  num? _size;
  num? _current;
  Data copyWith({
    List<Records>? records,
    num? total,
    num? size,
    num? current,
  }) =>
      Data(
        records: records ?? _records,
        total: total ?? _total,
        size: size ?? _size,
        current: current ?? _current,
      );
  List<Records>? get records => _records;
  num? get total => _total;
  num? get size => _size;
  num? get current => _current;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_records != null) {
      map['records'] = _records?.map((v) => v.toJson()).toList();
    }
    map['total'] = _total;
    map['size'] = _size;
    map['current'] = _current;
    return map;
  }
}

/// id : 1768288023862235138
/// remark : "aute"
/// picUrl : "http://dummyimage.com/400x400"
/// value : 7
/// name : "由较认面素都"
/// position : "consectetur"
/// createTime : "2024-03-14T22:48:13"
/// updateTime : "2024-03-14T22:48:13"

class Records {
  Records({
    num? id,
    String? remark,
    String? picUrl,
    num? value,
    String? name,
    String? position,
    String? createTime,
    String? updateTime,
  }) {
    _id = id;
    _remark = remark;
    _picUrl = picUrl;
    _value = value;
    _name = name;
    _position = position;
    _createTime = createTime;
    _updateTime = updateTime;
  }

  Records.fromJson(dynamic json) {
    _id = json['id'];
    _remark = json['remark'];
    _picUrl = json['picUrl'];
    _value = json['value'];
    _name = json['name'];
    _position = json['position'];
    _createTime = json['createTime'];
    _updateTime = json['updateTime'];
  }
  num? _id;
  String? _remark;
  String? _picUrl;
  num? _value;
  String? _name;
  String? _position;
  String? _createTime;
  String? _updateTime;
  Records copyWith({
    num? id,
    String? remark,
    String? picUrl,
    num? value,
    String? name,
    String? position,
    String? createTime,
    String? updateTime,
  }) =>
      Records(
        id: id ?? _id,
        remark: remark ?? _remark,
        picUrl: picUrl ?? _picUrl,
        value: value ?? _value,
        name: name ?? _name,
        position: position ?? _position,
        createTime: createTime ?? _createTime,
        updateTime: updateTime ?? _updateTime,
      );
  num? get id => _id;
  String? get remark => _remark;
  String? get picUrl => _picUrl;
  num? get value => _value;
  String? get name => _name;
  String? get position => _position;
  String? get createTime => _createTime;
  String? get updateTime => _updateTime;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['remark'] = _remark;
    map['picUrl'] = _picUrl;
    map['value'] = _value;
    map['name'] = _name;
    map['position'] = _position;
    map['createTime'] = _createTime;
    map['updateTime'] = _updateTime;
    return map;
  }
}
