/// code : 0
/// msg : "string"
/// data : {}

class BaseResponse {
  BaseResponse({
    num? code,
    String? msg,
    dynamic data,
  }) {
    _code = code;
    _msg = msg;
    _data = data;
  }

  BaseResponse.fromJson(dynamic json) {
    _code = json['code'];
    _msg = json['msg'];
    _data = json['data'];
  }
  num? _code;
  String? _msg;
  dynamic _data;
  BaseResponse copyWith({
    num? code,
    String? msg,
    dynamic data,
  }) =>
      BaseResponse(
        code: code ?? _code,
        msg: msg ?? _msg,
        data: data ?? _data,
      );
  num? get code => _code;
  String? get msg => _msg;
  dynamic get data => _data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['code'] = _code;
    map['msg'] = _msg;
    map['data'] = _data;
    return map;
  }
}
