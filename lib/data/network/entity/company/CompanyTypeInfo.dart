/// code : 200
/// msg : "ok"
/// data : [{"industryId":"01","name":"农业","parentId":"A"},{"industryId":"02","name":"林业","parentId":"A"},{"industryId":"03","name":"畜牧业","parentId":"A"},{"industryId":"04","name":"渔业","parentId":"A"},{"industryId":"05","name":"农、林、牧、渔专业及辅助性活动","parentId":"A"},{"industryId":"06","name":"煤炭开采和洗选业","parentId":"B"},{"industryId":"07","name":"石油和天然气开采业","parentId":"B"},{"industryId":"08","name":"黑色金属矿采选业","parentId":"B"},{"industryId":"09","name":"有色金属矿采选业","parentId":"B"},{"industryId":"10","name":"非金属矿采选业","parentId":"B"},{"industryId":"11","name":"开采专业及辅助性活动","parentId":"B"},{"industryId":"12","name":"其他采矿业","parentId":"B"},{"industryId":"13","name":"农副食品加工业","parentId":"C"},{"industryId":"14","name":"食品制造业","parentId":"C"},{"industryId":"15","name":"酒、饮料和精制茶制造业","parentId":"C"},{"industryId":"16","name":"烟草制品业","parentId":"C"},{"industryId":"17","name":"纺织业","parentId":"C"},{"industryId":"18","name":"纺织服装、服饰业","parentId":"C"},{"industryId":"19","name":"皮革、毛皮、羽毛及其制品和制鞋业","parentId":"C"},{"industryId":"20","name":"木材加工和木、竹、藤、棕、草制品业","parentId":"C"},{"industryId":"21","name":"家具制造业","parentId":"C"},{"industryId":"22","name":"造纸和纸制品业","parentId":"C"},{"industryId":"23","name":"印刷和记录媒介复制业","parentId":"C"},{"industryId":"24","name":"文教、工美、体育和娱乐用品制造业","parentId":"C"},{"industryId":"25","name":"石油、煤炭及其他燃料加工业","parentId":"C"},{"industryId":"26","name":"化学原料和化学制品制造业","parentId":"C"},{"industryId":"27","name":"医药制造业","parentId":"C"},{"industryId":"28","name":"化学纤维制造业","parentId":"C"},{"industryId":"29","name":"橡胶和塑料制品业","parentId":"C"},{"industryId":"30","name":"非金属矿物制品业","parentId":"C"},{"industryId":"31","name":"黑色金属冶炼和压延加工业","parentId":"C"},{"industryId":"32","name":"有色金属冶炼和压延加工业","parentId":"C"},{"industryId":"33","name":"金属制品业","parentId":"C"},{"industryId":"34","name":"通用设备制造业","parentId":"C"},{"industryId":"35","name":"专用设备制造业","parentId":"C"},{"industryId":"36","name":"汽车制造业","parentId":"C"},{"industryId":"37","name":"铁路运输设备制造","parentId":"C"},{"industryId":"38","name":"电气机械和器材制造业","parentId":"C"},{"industryId":"39","name":"计算机、通信和其他电子设备制造业","parentId":"C"},{"industryId":"40","name":"仪器仪表制造业","parentId":"C"},{"industryId":"41","name":"其他制造业","parentId":"C"},{"industryId":"42","name":"废弃资源综合利用业","parentId":"C"},{"industryId":"43","name":"金属制品、机械和设备修理业","parentId":"C"},{"industryId":"44","name":"电力、热力生产和供应业","parentId":"D"},{"industryId":"45","name":"燃气生产和供应业","parentId":"D"},{"industryId":"46","name":"水的生产和供应业","parentId":"D"},{"industryId":"47","name":"房屋建筑业","parentId":"E"},{"industryId":"48","name":"土木工程建筑业","parentId":"E"},{"industryId":"49","name":"建筑安装业","parentId":"E"},{"industryId":"50","name":"建筑装饰、装修和其他建筑业","parentId":"E"},{"industryId":"51","name":"批发业","parentId":"F"},{"industryId":"52","name":"零售业","parentId":"F"},{"industryId":"53","name":"铁路运输业","parentId":"G"},{"industryId":"54","name":"道路运输业","parentId":"G"},{"industryId":"55","name":"水上运输业","parentId":"G"},{"industryId":"56","name":"航空运输业","parentId":"G"},{"industryId":"57","name":"管道运输业","parentId":"G"},{"industryId":"58","name":"多式联运和运输代理业","parentId":"G"},{"industryId":"59","name":"装卸搬运和仓储业","parentId":"G"},{"industryId":"60","name":"邮政业","parentId":"G"},{"industryId":"61","name":"住宿业","parentId":"H"},{"industryId":"62","name":"餐饮业","parentId":"H"},{"industryId":"63","name":"电信、广播电视和卫星传输服务","parentId":"I"},{"industryId":"64","name":"互联网和相关服务","parentId":"I"},{"industryId":"65","name":"软件和信息技术服务业","parentId":"I"},{"industryId":"66","name":"货币金融服务","parentId":"J"},{"industryId":"67","name":"资本市场服务","parentId":"J"},{"industryId":"68","name":"保险业","parentId":"J"},{"industryId":"69","name":"其他金融业","parentId":"J"},{"industryId":"70","name":"房地产业","parentId":"K"},{"industryId":"71","name":"租赁业","parentId":"L"},{"industryId":"72","name":"商务服务业","parentId":"L"},{"industryId":"73","name":"研究和试验发展","parentId":"M"},{"industryId":"74","name":"专业技术服务业","parentId":"M"},{"industryId":"75","name":"科技推广和应用服务业","parentId":"M"},{"industryId":"76","name":"水利管理业","parentId":"N"},{"industryId":"77","name":"生态保护和环境治理业","parentId":"N"},{"industryId":"78","name":"公共设施管理业","parentId":"N"},{"industryId":"79","name":"土地管理业","parentId":"N"},{"industryId":"80","name":"居民服务业","parentId":"O"},{"industryId":"81","name":"机动车、电子产品和日用产品修理业","parentId":"O"},{"industryId":"82","name":"其他服务业","parentId":"O"},{"industryId":"83","name":"教育","parentId":"P"},{"industryId":"85","name":"社会工作","parentId":"Q"},{"industryId":"86","name":"新闻和出版业","parentId":"R"},{"industryId":"87","name":"广播、电视、电影和录音制作业","parentId":"R"},{"industryId":"88","name":"文化艺术业","parentId":"R"},{"industryId":"89","name":"体育","parentId":"R"},{"industryId":"90","name":"娱乐业","parentId":"R"},{"industryId":"91","name":"中国共产党机关","parentId":"S"},{"industryId":"92","name":"国家机构","parentId":"S"},{"industryId":"93","name":"人民政协、民主党派","parentId":"S"},{"industryId":"94","name":"社会保障","parentId":"S"},{"industryId":"95","name":"群众团体、社会团体和其他成员组织","parentId":"S"},{"industryId":"96","name":"基层群众自治组织及其他组织","parentId":"S"},{"industryId":"97","name":"国际组织","parentId":"T"}]

class CompanyTypeInfo {
  CompanyTypeInfo({
    num? code,
    String? msg,
    List<Data>? data,
  }) {
    _code = code;
    _msg = msg;
    _data = data;
  }

  CompanyTypeInfo.fromJson(dynamic json) {
    _code = json['code'];
    _msg = json['msg'];
    if (json['data'] != null) {
      _data = [];
      json['data'].forEach((v) {
        _data?.add(Data.fromJson(v));
      });
    }
  }
  num? _code;
  String? _msg;
  List<Data>? _data;
  CompanyTypeInfo copyWith({
    num? code,
    String? msg,
    List<Data>? data,
  }) =>
      CompanyTypeInfo(
        code: code ?? _code,
        msg: msg ?? _msg,
        data: data ?? _data,
      );
  num? get code => _code;
  String? get msg => _msg;
  List<Data>? get data => _data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['code'] = _code;
    map['msg'] = _msg;
    if (_data != null) {
      map['data'] = _data?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// industryId : "01"
/// name : "农业"
/// parentId : "A"

class Data {
  Data({
    String? industryId,
    String? name,
    String? parentId,
  }) {
    _industryId = industryId;
    _name = name;
    _parentId = parentId;
  }

  Data.fromJson(dynamic json) {
    _industryId = json['industryId'];
    _name = json['name'];
    _parentId = json['parentId'];
  }
  String? _industryId;
  String? _name;
  String? _parentId;
  Data copyWith({
    String? industryId,
    String? name,
    String? parentId,
  }) =>
      Data(
        industryId: industryId ?? _industryId,
        name: name ?? _name,
        parentId: parentId ?? _parentId,
      );
  String? get industryId => _industryId;
  String? get name => _name;
  String? get parentId => _parentId;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['industryId'] = _industryId;
    map['name'] = _name;
    map['parentId'] = _parentId;
    return map;
  }
}
