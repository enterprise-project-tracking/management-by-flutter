/// code : 200
/// msg : "ok"

class ChangeCompanyGroupInfo {
  ChangeCompanyGroupInfo({
      num? code, 
      String? msg,}){
    _code = code;
    _msg = msg;
}

  ChangeCompanyGroupInfo.fromJson(dynamic json) {
    _code = json['code'];
    _msg = json['msg'];
  }
  num? _code;
  String? _msg;
ChangeCompanyGroupInfo copyWith({  num? code,
  String? msg,
}) => ChangeCompanyGroupInfo(  code: code ?? _code,
  msg: msg ?? _msg,
);
  num? get code => _code;
  String? get msg => _msg;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['code'] = _code;
    map['msg'] = _msg;
    return map;
  }

}