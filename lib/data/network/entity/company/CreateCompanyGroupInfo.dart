/// code : 500
/// msg : "不存在的父组"

class CreateCompanyGroupInfo {
  CreateCompanyGroupInfo({
      num? code, 
      String? msg,}){
    _code = code;
    _msg = msg;
}

  CreateCompanyGroupInfo.fromJson(dynamic json) {
    _code = json['code'];
    _msg = json['msg'];
  }
  num? _code;
  String? _msg;
CreateCompanyGroupInfo copyWith({  num? code,
  String? msg,
}) => CreateCompanyGroupInfo(  code: code ?? _code,
  msg: msg ?? _msg,
);
  num? get code => _code;
  String? get msg => _msg;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['code'] = _code;
    map['msg'] = _msg;
    return map;
  }

}