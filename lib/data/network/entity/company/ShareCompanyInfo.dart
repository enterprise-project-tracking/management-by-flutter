/// code : 200
/// msg : "ok"
/// data : {"code":"c0c93616-6aae-4856-b8ec-e5d0e551bcfb"}

class ShareCompanyInfo {
  ShareCompanyInfo({
      num? code, 
      String? msg, 
      Data? data,}){
    _code = code;
    _msg = msg;
    _data = data;
}

  ShareCompanyInfo.fromJson(dynamic json) {
    _code = json['code'];
    _msg = json['msg'];
    _data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }
  num? _code;
  String? _msg;
  Data? _data;
ShareCompanyInfo copyWith({  num? code,
  String? msg,
  Data? data,
}) => ShareCompanyInfo(  code: code ?? _code,
  msg: msg ?? _msg,
  data: data ?? _data,
);
  num? get code => _code;
  String? get msg => _msg;
  Data? get data => _data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['code'] = _code;
    map['msg'] = _msg;
    if (_data != null) {
      map['data'] = _data?.toJson();
    }
    return map;
  }

}

/// code : "c0c93616-6aae-4856-b8ec-e5d0e551bcfb"

class Data {
  Data({
      String? code,}){
    _code = code;
}

  Data.fromJson(dynamic json) {
    _code = json['code'];
  }
  String? _code;
Data copyWith({  String? code,
}) => Data(  code: code ?? _code,
);
  String? get code => _code;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['code'] = _code;
    return map;
  }

}