/// code : 200
/// msg : "ok"
/// data : [{"type":"GROUP","name":"group 1","id":1775525325651898370},{"type":"GROUP","name":"num1","id":1777356604634624002},{"type":"GROUP","name":"num3","id":1777356703062355969},{"type":"GROUP","name":"numr","id":1777558418969653249},{"type":"GROUP","name":"nnn","id":1777589274564161538}]

class AllCompanyGroupInfo {
  AllCompanyGroupInfo({
      num? code, 
      String? msg, 
      List<Data>? data,}){
    _code = code;
    _msg = msg;
    _data = data;
}

  AllCompanyGroupInfo.fromJson(dynamic json) {
    _code = json['code'];
    _msg = json['msg'];
    if (json['data'] != null) {
      _data = [];
      json['data'].forEach((v) {
        _data?.add(Data.fromJson(v));
      });
    }
  }
  num? _code;
  String? _msg;
  List<Data>? _data;
AllCompanyGroupInfo copyWith({  num? code,
  String? msg,
  List<Data>? data,
}) => AllCompanyGroupInfo(  code: code ?? _code,
  msg: msg ?? _msg,
  data: data ?? _data,
);
  num? get code => _code;
  String? get msg => _msg;
  List<Data>? get data => _data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['code'] = _code;
    map['msg'] = _msg;
    if (_data != null) {
      map['data'] = _data?.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// type : "GROUP"
/// name : "group 1"
/// id : 1775525325651898370

class Data {
  Data({
      String? type, 
      String? name, 
      num? id,}){
    _type = type;
    _name = name;
    _id = id;
}

  Data.fromJson(dynamic json) {
    _type = json['type'];
    _name = json['name'];
    _id = json['id'];
  }
  String? _type;
  String? _name;
  num? _id;
Data copyWith({  String? type,
  String? name,
  num? id,
}) => Data(  type: type ?? _type,
  name: name ?? _name,
  id: id ?? _id,
);
  String? get type => _type;
  String? get name => _name;
  num? get id => _id;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['type'] = _type;
    map['name'] = _name;
    map['id'] = _id;
    return map;
  }

}