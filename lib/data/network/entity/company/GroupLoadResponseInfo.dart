/// code : 200
/// msg : "ok"
/// data : [{"type":"GROUP","name":"group 1","id":1775525325651898370,"pid":1764677019395825665},{"type":"GROUP","name":"num1","id":1777356604634624002,"pid":1764677019395825665}]

class GroupLoadResponseInfo {
  GroupLoadResponseInfo({
      num? code, 
      String? msg, 
      List<Data>? data,}){
    _code = code;
    _msg = msg;
    _data = data;
}

  GroupLoadResponseInfo.fromJson(dynamic json) {
    _code = json['code'];
    _msg = json['msg'];
    if (json['data'] != null) {
      _data = [];
      json['data'].forEach((v) {
        _data?.add(Data.fromJson(v));
      });
    }
  }
  num? _code;
  String? _msg;
  List<Data>? _data;
GroupLoadResponseInfo copyWith({  num? code,
  String? msg,
  List<Data>? data,
}) => GroupLoadResponseInfo(  code: code ?? _code,
  msg: msg ?? _msg,
  data: data ?? _data,
);
  num? get code => _code;
  String? get msg => _msg;
  List<Data>? get data => _data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['code'] = _code;
    map['msg'] = _msg;
    if (_data != null) {
      map['data'] = _data?.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// type : "GROUP"
/// name : "group 1"
/// id : 1775525325651898370
/// pid : 1764677019395825665

class Data {
  Data({
      String? type, 
      String? name, 
      num? id, 
      num? pid,}){
    _type = type;
    _name = name;
    _id = id;
    _pid = pid;
}

  Data.fromJson(dynamic json) {
    _type = json['type'];
    _name = json['name'];
    _id = json['id'];
    _pid = json['pid'];
  }
  String? _type;
  String? _name;
  num? _id;
  num? _pid;
Data copyWith({  String? type,
  String? name,
  num? id,
  num? pid,
}) => Data(  type: type ?? _type,
  name: name ?? _name,
  id: id ?? _id,
  pid: pid ?? _pid,
);
  String? get type => _type;
  String? get name => _name;
  num? get id => _id;
  num? get pid => _pid;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['type'] = _type;
    map['name'] = _name;
    map['id'] = _id;
    map['pid'] = _pid;
    return map;
  }

}