/// code : 500
/// msg : "不存在的父组"

class ChangeEmployeeResponseInfo {
  ChangeEmployeeResponseInfo({
      num? code, 
      String? msg,}){
    _code = code;
    _msg = msg;
}

  ChangeEmployeeResponseInfo.fromJson(dynamic json) {
    _code = json['code'];
    _msg = json['msg'];
  }
  num? _code;
  String? _msg;
ChangeEmployeeResponseInfo copyWith({  num? code,
  String? msg,
}) => ChangeEmployeeResponseInfo(  code: code ?? _code,
  msg: msg ?? _msg,
);
  num? get code => _code;
  String? get msg => _msg;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['code'] = _code;
    map['msg'] = _msg;
    return map;
  }

}