/// code : 200
/// msg : "ok"

class DeleteCompanyGroupInfo {
  DeleteCompanyGroupInfo({
      num? code, 
      String? msg,}){
    _code = code;
    _msg = msg;
}

  DeleteCompanyGroupInfo.fromJson(dynamic json) {
    _code = json['code'];
    _msg = json['msg'];
  }
  num? _code;
  String? _msg;
DeleteCompanyGroupInfo copyWith({  num? code,
  String? msg,
}) => DeleteCompanyGroupInfo(  code: code ?? _code,
  msg: msg ?? _msg,
);
  num? get code => _code;
  String? get msg => _msg;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['code'] = _code;
    map['msg'] = _msg;
    return map;
  }

}