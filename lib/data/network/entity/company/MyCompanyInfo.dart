/// code : 200
/// msg : "ok"
/// data : {"id":1764677019395825665,"name":"商队量质提六","email":"d.edduqmhpae@qq.com","telephone":"19894214328","legalRepresentative":"潘祖果","employeeSize":4,"createTime":"2024-03-13T22:41:40","industryId":"A","industryName":"农、林、牧、渔业","roleId":0}

class MyCompanyInfo {
  MyCompanyInfo({
      num? code, 
      String? msg, 
      Data? data,}){
    _code = code;
    _msg = msg;
    _data = data;
}

  MyCompanyInfo.fromJson(dynamic json) {
    _code = json['code'];
    _msg = json['msg'];
    _data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }
  num? _code;
  String? _msg;
  Data? _data;
MyCompanyInfo copyWith({  num? code,
  String? msg,
  Data? data,
}) => MyCompanyInfo(  code: code ?? _code,
  msg: msg ?? _msg,
  data: data ?? _data,
);
  num? get code => _code;
  String? get msg => _msg;
  Data? get data => _data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['code'] = _code;
    map['msg'] = _msg;
    if (_data != null) {
      map['data'] = _data?.toJson();
    }
    return map;
  }

}

/// id : 1764677019395825665
/// name : "商队量质提六"
/// email : "d.edduqmhpae@qq.com"
/// telephone : "19894214328"
/// legalRepresentative : "潘祖果"
/// employeeSize : 4
/// createTime : "2024-03-13T22:41:40"
/// industryId : "A"
/// industryName : "农、林、牧、渔业"
/// roleId : 0

class Data {
  Data({
      num? id, 
      String? name, 
      String? email, 
      String? telephone, 
      String? legalRepresentative, 
      num? employeeSize, 
      String? createTime, 
      String? industryId, 
      String? industryName, 
      num? roleId,}){
    _id = id;
    _name = name;
    _email = email;
    _telephone = telephone;
    _legalRepresentative = legalRepresentative;
    _employeeSize = employeeSize;
    _createTime = createTime;
    _industryId = industryId;
    _industryName = industryName;
    _roleId = roleId;
}

  Data.fromJson(dynamic json) {
    _id = json['id'];
    _name = json['name'];
    _email = json['email'];
    _telephone = json['telephone'];
    _legalRepresentative = json['legalRepresentative'];
    _employeeSize = json['employeeSize'];
    _createTime = json['createTime'];
    _industryId = json['industryId'];
    _industryName = json['industryName'];
    _roleId = json['roleId'];
  }
  num? _id;
  String? _name;
  String? _email;
  String? _telephone;
  String? _legalRepresentative;
  num? _employeeSize;
  String? _createTime;
  String? _industryId;
  String? _industryName;
  num? _roleId;

  get code => null;
Data copyWith({  num? id,
  String? name,
  String? email,
  String? telephone,
  String? legalRepresentative,
  num? employeeSize,
  String? createTime,
  String? industryId,
  String? industryName,
  num? roleId,
}) => Data(  id: id ?? _id,
  name: name ?? _name,
  email: email ?? _email,
  telephone: telephone ?? _telephone,
  legalRepresentative: legalRepresentative ?? _legalRepresentative,
  employeeSize: employeeSize ?? _employeeSize,
  createTime: createTime ?? _createTime,
  industryId: industryId ?? _industryId,
  industryName: industryName ?? _industryName,
  roleId: roleId ?? _roleId,
);
  num? get id => _id;
  String? get name => _name;
  String? get email => _email;
  String? get telephone => _telephone;
  String? get legalRepresentative => _legalRepresentative;
  num? get employeeSize => _employeeSize;
  String? get createTime => _createTime;
  String? get industryId => _industryId;
  String? get industryName => _industryName;
  num? get roleId => _roleId;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['name'] = _name;
    map['email'] = _email;
    map['telephone'] = _telephone;
    map['legalRepresentative'] = _legalRepresentative;
    map['employeeSize'] = _employeeSize;
    map['createTime'] = _createTime;
    map['industryId'] = _industryId;
    map['industryName'] = _industryName;
    map['roleId'] = _roleId;
    return map;
  }

}