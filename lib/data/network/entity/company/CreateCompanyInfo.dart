/// code : 200
/// msg : "ok"

class CreateCompanyInfo {
  CreateCompanyInfo({
      num? code, 
      String? msg,}){
    _code = code;
    _msg = msg;
}

  CreateCompanyInfo.fromJson(dynamic json) {
    _code = json['code'];
    _msg = json['msg'];
  }
  num? _code;
  String? _msg;
CreateCompanyInfo copyWith({  num? code,
  String? msg,
}) => CreateCompanyInfo(  code: code ?? _code,
  msg: msg ?? _msg,
);
  num? get code => _code;
  String? get msg => _msg;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['code'] = _code;
    map['msg'] = _msg;
    return map;
  }

}