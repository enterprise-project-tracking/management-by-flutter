/// code : 200
/// msg : "ok"
/// data : {"id":1781318733910290434,"projectId":1777693640121073666,"projectName":"科技型项目跟踪管理系统","priority":3,"name":"test","description":"1781308291703943170","startTime":"2024-01-19T00:00:00","endTime":"2024-05-19T00:00:00","status":0,"charge":{"id":1771578963453849602,"nickName":"孔艳","avatar":"http://dummyimage.com/200x200","realName":"eeee"},"creator":{"id":1764677020037554177,"nickName":"rrrr","avatar":"https://oss.sshine.ltd/tracking/public/20240407/a988416c0a0042549e61e9e6b2e9620e-scaled_IMG_20240407_113104.jpg","realName":"wwwww"},"type":0,"attachments":[{"id":1781318734094839809,"name":"新建 Text Document.txt","suffix":"txt","url":"https://oss.sshine.ltd/tracking/public/20240419/7c6a89aa367347c89d7ee8ca0aa1c7b9-新建 Text Document.txt","createTime":"2024-04-19T21:47:36","fileType":3,"creator":{"id":1764677020037554177,"nickName":"rrrr","avatar":"https://oss.sshine.ltd/tracking/public/20240407/a988416c0a0042549e61e9e6b2e9620e-scaled_IMG_20240407_113104.jpg","realName":"wwwww"}},{"id":1781318734191308801,"name":"文稿.md","suffix":"md","url":"https://oss.sshine.ltd/tracking/public/20240419/3af59e5f98da4dc585a06c0c12302407-文稿.md","createTime":"2024-04-19T21:47:36","fileType":3,"creator":{"id":1764677020037554177,"nickName":"rrrr","avatar":"https://oss.sshine.ltd/tracking/public/20240407/a988416c0a0042549e61e9e6b2e9620e-scaled_IMG_20240407_113104.jpg","realName":"wwwww"}}],"finishDemand":0,"totalDemand":0}

class DemandOrTaskDetailResponse {
  DemandOrTaskDetailResponse({
    num? code,
    String? msg,
    Data? data,
  }) {
    _code = code;
    _msg = msg;
    _data = data;
  }

  DemandOrTaskDetailResponse.fromJson(dynamic json) {
    _code = json['code'];
    _msg = json['msg'];
    _data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }
  num? _code;
  String? _msg;
  Data? _data;
  DemandOrTaskDetailResponse copyWith({
    num? code,
    String? msg,
    Data? data,
  }) =>
      DemandOrTaskDetailResponse(
        code: code ?? _code,
        msg: msg ?? _msg,
        data: data ?? _data,
      );
  num? get code => _code;
  String? get msg => _msg;
  Data? get data => _data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['code'] = _code;
    map['msg'] = _msg;
    if (_data != null) {
      map['data'] = _data?.toJson();
    }
    return map;
  }
}

/// id : 1781318733910290434
/// projectId : 1777693640121073666
/// projectName : "科技型项目跟踪管理系统"
/// priority : 3
/// name : "test"
/// description : "1781308291703943170"
/// startTime : "2024-01-19T00:00:00"
/// endTime : "2024-05-19T00:00:00"
/// status : 0
/// charge : {"id":1771578963453849602,"nickName":"孔艳","avatar":"http://dummyimage.com/200x200","realName":"eeee"}
/// creator : {"id":1764677020037554177,"nickName":"rrrr","avatar":"https://oss.sshine.ltd/tracking/public/20240407/a988416c0a0042549e61e9e6b2e9620e-scaled_IMG_20240407_113104.jpg","realName":"wwwww"}
/// type : 0
/// attachments : [{"id":1781318734094839809,"name":"新建 Text Document.txt","suffix":"txt","url":"https://oss.sshine.ltd/tracking/public/20240419/7c6a89aa367347c89d7ee8ca0aa1c7b9-新建 Text Document.txt","createTime":"2024-04-19T21:47:36","fileType":3,"creator":{"id":1764677020037554177,"nickName":"rrrr","avatar":"https://oss.sshine.ltd/tracking/public/20240407/a988416c0a0042549e61e9e6b2e9620e-scaled_IMG_20240407_113104.jpg","realName":"wwwww"}},{"id":1781318734191308801,"name":"文稿.md","suffix":"md","url":"https://oss.sshine.ltd/tracking/public/20240419/3af59e5f98da4dc585a06c0c12302407-文稿.md","createTime":"2024-04-19T21:47:36","fileType":3,"creator":{"id":1764677020037554177,"nickName":"rrrr","avatar":"https://oss.sshine.ltd/tracking/public/20240407/a988416c0a0042549e61e9e6b2e9620e-scaled_IMG_20240407_113104.jpg","realName":"wwwww"}}]
/// finishDemand : 0
/// totalDemand : 0

class Data {
  Data({
    num? id,
    num? projectId,
    String? projectName,
    num? priority,
    String? name,
    String? description,
    String? startTime,
    String? endTime,
    num? status,
    Charge? charge,
    Creator? creator,
    num? type,
    List<Attachments>? attachments,
    num? finishDemand,
    num? totalDemand,
  }) {
    _id = id;
    _projectId = projectId;
    _projectName = projectName;
    _priority = priority;
    _name = name;
    _description = description;
    _startTime = startTime;
    _endTime = endTime;
    _status = status;
    _charge = charge;
    _creator = creator;
    _type = type;
    _attachments = attachments;
    _finishDemand = finishDemand;
    _totalDemand = totalDemand;
  }

  Data.fromJson(dynamic json) {
    _id = json['id'];
    _projectId = json['projectId'];
    _projectName = json['projectName'];
    _priority = json['priority'];
    _name = json['name'];
    _description = json['description'];
    _startTime = json['startTime'];
    _endTime = json['endTime'];
    _status = json['status'];
    _charge = json['charge'] != null ? Charge.fromJson(json['charge']) : null;
    _creator =
        json['creator'] != null ? Creator.fromJson(json['creator']) : null;
    _type = json['type'];
    if (json['attachments'] != null) {
      _attachments = [];
      json['attachments'].forEach((v) {
        _attachments?.add(Attachments.fromJson(v));
      });
    }
    _finishDemand = json['finishDemand'];
    _totalDemand = json['totalDemand'];
  }
  num? _id;
  num? _projectId;
  String? _projectName;
  num? _priority;

  set id(num? value) {
    _id = value;
  }

  String? _name;
  String? _description;
  String? _startTime;
  String? _endTime;
  num? _status;
  Charge? _charge;
  Creator? _creator;
  num? _type;
  List<Attachments>? _attachments;
  num? _finishDemand;
  num? _totalDemand;
  Data copyWith({
    num? id,
    num? projectId,
    String? projectName,
    num? priority,
    String? name,
    String? description,
    String? startTime,
    String? endTime,
    num? status,
    Charge? charge,
    Creator? creator,
    num? type,
    List<Attachments>? attachments,
    num? finishDemand,
    num? totalDemand,
  }) =>
      Data(
        id: id ?? _id,
        projectId: projectId ?? _projectId,
        projectName: projectName ?? _projectName,
        priority: priority ?? _priority,
        name: name ?? _name,
        description: description ?? _description,
        startTime: startTime ?? _startTime,
        endTime: endTime ?? _endTime,
        status: status ?? _status,
        charge: charge ?? _charge,
        creator: creator ?? _creator,
        type: type ?? _type,
        attachments: attachments ?? _attachments,
        finishDemand: finishDemand ?? _finishDemand,
        totalDemand: totalDemand ?? _totalDemand,
      );
  num? get id => _id;
  num? get projectId => _projectId;
  String? get projectName => _projectName;
  num? get priority => _priority;
  String? get name => _name;
  String? get description => _description;
  String? get startTime => _startTime;
  String? get endTime => _endTime;
  num? get status => _status;
  Charge? get charge => _charge;
  Creator? get creator => _creator;
  num? get type => _type;
  List<Attachments>? get attachments => _attachments;
  num? get finishDemand => _finishDemand;
  num? get totalDemand => _totalDemand;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['projectId'] = _projectId;
    map['projectName'] = _projectName;
    map['priority'] = _priority;
    map['name'] = _name;
    map['description'] = _description;
    map['startTime'] = _startTime;
    map['endTime'] = _endTime;
    map['status'] = _status;
    if (_charge != null) {
      map['charge'] = _charge?.toJson();
    }
    if (_creator != null) {
      map['creator'] = _creator?.toJson();
    }
    map['type'] = _type;
    if (_attachments != null) {
      map['attachments'] = _attachments?.map((v) => v.toJson()).toList();
    }
    map['finishDemand'] = _finishDemand;
    map['totalDemand'] = _totalDemand;
    return map;
  }

  set projectId(num? value) {
    _projectId = value;
  }

  set projectName(String? value) {
    _projectName = value;
  }

  set priority(num? value) {
    _priority = value;
  }

  set name(String? value) {
    _name = value;
  }

  set description(String? value) {
    _description = value;
  }

  set startTime(String? value) {
    _startTime = value;
  }

  set endTime(String? value) {
    _endTime = value;
  }

  set status(num? value) {
    _status = value;
  }

  set charge(Charge? value) {
    _charge = value;
  }

  set creator(Creator? value) {
    _creator = value;
  }

  set type(num? value) {
    _type = value;
  }

  set attachments(List<Attachments>? value) {
    _attachments = value;
  }

  set finishDemand(num? value) {
    _finishDemand = value;
  }

  set totalDemand(num? value) {
    _totalDemand = value;
  }
}

/// id : 1781318734094839809
/// name : "新建 Text Document.txt"
/// suffix : "txt"
/// url : "https://oss.sshine.ltd/tracking/public/20240419/7c6a89aa367347c89d7ee8ca0aa1c7b9-新建 Text Document.txt"
/// createTime : "2024-04-19T21:47:36"
/// fileType : 3
/// creator : {"id":1764677020037554177,"nickName":"rrrr","avatar":"https://oss.sshine.ltd/tracking/public/20240407/a988416c0a0042549e61e9e6b2e9620e-scaled_IMG_20240407_113104.jpg","realName":"wwwww"}

class Attachments {
  Attachments({
    num? id,
    String? name,
    String? suffix,
    String? url,
    String? createTime,
    num? fileType,
    Creator? creator,
  }) {
    _id = id;
    _name = name;
    _suffix = suffix;
    _url = url;
    _createTime = createTime;
    _fileType = fileType;
    _creator = creator;
  }

  Attachments.fromJson(dynamic json) {
    _id = json['id'];
    _name = json['name'];
    _suffix = json['suffix'];
    _url = json['url'];
    _createTime = json['createTime'];
    _fileType = json['fileType'];
    _creator =
        json['creator'] != null ? Creator.fromJson(json['creator']) : null;
  }
  num? _id;
  String? _name;
  String? _suffix;
  String? _url;
  String? _createTime;
  num? _fileType;
  Creator? _creator;
  Attachments copyWith({
    num? id,
    String? name,
    String? suffix,
    String? url,
    String? createTime,
    num? fileType,
    Creator? creator,
  }) =>
      Attachments(
        id: id ?? _id,
        name: name ?? _name,
        suffix: suffix ?? _suffix,
        url: url ?? _url,
        createTime: createTime ?? _createTime,
        fileType: fileType ?? _fileType,
        creator: creator ?? _creator,
      );
  num? get id => _id;
  String? get name => _name;
  String? get suffix => _suffix;
  String? get url => _url;
  String? get createTime => _createTime;
  num? get fileType => _fileType;
  Creator? get creator => _creator;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['name'] = _name;
    map['suffix'] = _suffix;
    map['url'] = _url;
    map['createTime'] = _createTime;
    map['fileType'] = _fileType;
    if (_creator != null) {
      map['creator'] = _creator?.toJson();
    }
    return map;
  }
}

/// id : 1764677020037554177
/// nickName : "rrrr"
/// avatar : "https://oss.sshine.ltd/tracking/public/20240407/a988416c0a0042549e61e9e6b2e9620e-scaled_IMG_20240407_113104.jpg"
/// realName : "wwwww"

class Creator {
  Creator({
    num? id,
    String? nickName,
    String? avatar,
    String? realName,
  }) {
    _id = id;
    _nickName = nickName;
    _avatar = avatar;
    _realName = realName;
  }

  Creator.fromJson(dynamic json) {
    _id = json['id'];
    _nickName = json['nickName'];
    _avatar = json['avatar'];
    _realName = json['realName'];
  }
  num? _id;
  String? _nickName;
  String? _avatar;
  String? _realName;
  Creator copyWith({
    num? id,
    String? nickName,
    String? avatar,
    String? realName,
  }) =>
      Creator(
        id: id ?? _id,
        nickName: nickName ?? _nickName,
        avatar: avatar ?? _avatar,
        realName: realName ?? _realName,
      );
  num? get id => _id;
  String? get nickName => _nickName;
  String? get avatar => _avatar;
  String? get realName => _realName;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['nickName'] = _nickName;
    map['avatar'] = _avatar;
    map['realName'] = _realName;
    return map;
  }
}

/// id : 1764677020037554177
/// nickName : "rrrr"
/// avatar : "https://oss.sshine.ltd/tracking/public/20240407/a988416c0a0042549e61e9e6b2e9620e-scaled_IMG_20240407_113104.jpg"
/// realName : "wwwww"

// class Creator {
//   Creator({
//       num? id,
//       String? nickName,
//       String? avatar,
//       String? realName,}){
//     _id = id;
//     _nickName = nickName;
//     _avatar = avatar;
//     _realName = realName;
// }
//
//   Creator.fromJson(dynamic json) {
//     _id = json['id'];
//     _nickName = json['nickName'];
//     _avatar = json['avatar'];
//     _realName = json['realName'];
//   }
//   num? _id;
//   String? _nickName;
//   String? _avatar;
//   String? _realName;
// Creator copyWith({  num? id,
//   String? nickName,
//   String? avatar,
//   String? realName,
// }) => Creator(  id: id ?? _id,
//   nickName: nickName ?? _nickName,
//   avatar: avatar ?? _avatar,
//   realName: realName ?? _realName,
// );
//   num? get id => _id;
//   String? get nickName => _nickName;
//   String? get avatar => _avatar;
//   String? get realName => _realName;
//
//   Map<String, dynamic> toJson() {
//     final map = <String, dynamic>{};
//     map['id'] = _id;
//     map['nickName'] = _nickName;
//     map['avatar'] = _avatar;
//     map['realName'] = _realName;
//     return map;
//   }
//
// }

/// id : 1771578963453849602
/// nickName : "孔艳"
/// avatar : "http://dummyimage.com/200x200"
/// realName : "eeee"

class Charge {
  Charge({
    num? id,
    String? nickName,
    String? avatar,
    String? realName,
  }) {
    _id = id;
    _nickName = nickName;
    _avatar = avatar;
    _realName = realName;
  }

  Charge.fromJson(dynamic json) {
    _id = json['id'];
    _nickName = json['nickName'];
    _avatar = json['avatar'];
    _realName = json['realName'];
  }
  num? _id;
  String? _nickName;
  String? _avatar;
  String? _realName;
  Charge copyWith({
    num? id,
    String? nickName,
    String? avatar,
    String? realName,
  }) =>
      Charge(
        id: id ?? _id,
        nickName: nickName ?? _nickName,
        avatar: avatar ?? _avatar,
        realName: realName ?? _realName,
      );
  num? get id => _id;
  String? get nickName => _nickName;
  String? get avatar => _avatar;
  String? get realName => _realName;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['nickName'] = _nickName;
    map['avatar'] = _avatar;
    map['realName'] = _realName;
    return map;
  }
}
