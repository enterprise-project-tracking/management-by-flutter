/// code : 200
/// msg : "ok"
/// data : [{"id":1781547634230079490,"companyUser":{"id":1764677020037554177,"nickName":"rrrr","avatar":"https://oss.sshine.ltd/tracking/public/20240407/a988416c0a0042549e61e9e6b2e9620e-scaled_IMG_20240407_113104.jpg","realName":"wwwww"},"content":"创建了需求","demandId":1781318733910290434,"relateDemandId":1781547633902923777,"relateDemandName":"vvv","createTime":"2024-04-20 12:57:10"},{"id":1781556312966475777,"companyUser":{"id":1764677020037554177,"nickName":"rrrr","avatar":"https://oss.sshine.ltd/tracking/public/20240407/a988416c0a0042549e61e9e6b2e9620e-scaled_IMG_20240407_113104.jpg","realName":"wwwww"},"content":"创建了需求","demandId":1781318733910290434,"relateDemandId":1781556312731594753,"relateDemandName":"uu","createTime":"2024-04-20 13:31:39"}]

class ListLogDemandOrTaskResponse {
  ListLogDemandOrTaskResponse({
    num? code,
    String? msg,
    List<Data>? data,
  }) {
    _code = code;
    _msg = msg;
    _data = data;
  }

  ListLogDemandOrTaskResponse.fromJson(dynamic json) {
    _code = json['code'];
    _msg = json['msg'];
    if (json['data'] != null) {
      _data = [];
      json['data'].forEach((v) {
        _data?.add(Data.fromJson(v));
      });
    }
  }
  num? _code;
  String? _msg;
  List<Data>? _data;
  ListLogDemandOrTaskResponse copyWith({
    num? code,
    String? msg,
    List<Data>? data,
  }) =>
      ListLogDemandOrTaskResponse(
        code: code ?? _code,
        msg: msg ?? _msg,
        data: data ?? _data,
      );
  num? get code => _code;
  String? get msg => _msg;
  List<Data>? get data => _data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['code'] = _code;
    map['msg'] = _msg;
    if (_data != null) {
      map['data'] = _data?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// id : 1781547634230079490
/// companyUser : {"id":1764677020037554177,"nickName":"rrrr","avatar":"https://oss.sshine.ltd/tracking/public/20240407/a988416c0a0042549e61e9e6b2e9620e-scaled_IMG_20240407_113104.jpg","realName":"wwwww"}
/// content : "创建了需求"
/// demandId : 1781318733910290434
/// relateDemandId : 1781547633902923777
/// relateDemandName : "vvv"
/// createTime : "2024-04-20 12:57:10"

class Data {
  Data({
    num? id,
    CompanyUser? companyUser,
    String? content,
    num? demandId,
    num? relateDemandId,
    String? relateDemandName,
    String? createTime,
  }) {
    _id = id;
    _companyUser = companyUser;
    _content = content;
    _demandId = demandId;
    _relateDemandId = relateDemandId;
    _relateDemandName = relateDemandName;
    _createTime = createTime;
  }

  Data.fromJson(dynamic json) {
    _id = json['id'];
    _companyUser = json['companyUser'] != null
        ? CompanyUser.fromJson(json['companyUser'])
        : null;
    _content = json['content'];
    _demandId = json['demandId'];
    _relateDemandId = json['relateDemandId'];
    _relateDemandName = json['relateDemandName'];
    _createTime = json['createTime'];
  }
  num? _id;
  CompanyUser? _companyUser;
  String? _content;
  num? _demandId;
  num? _relateDemandId;
  String? _relateDemandName;
  String? _createTime;
  Data copyWith({
    num? id,
    CompanyUser? companyUser,
    String? content,
    num? demandId,
    num? relateDemandId,
    String? relateDemandName,
    String? createTime,
  }) =>
      Data(
        id: id ?? _id,
        companyUser: companyUser ?? _companyUser,
        content: content ?? _content,
        demandId: demandId ?? _demandId,
        relateDemandId: relateDemandId ?? _relateDemandId,
        relateDemandName: relateDemandName ?? _relateDemandName,
        createTime: createTime ?? _createTime,
      );
  num? get id => _id;
  CompanyUser? get companyUser => _companyUser;
  String? get content => _content;
  num? get demandId => _demandId;
  num? get relateDemandId => _relateDemandId;
  String? get relateDemandName => _relateDemandName;
  String? get createTime => _createTime;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    if (_companyUser != null) {
      map['companyUser'] = _companyUser?.toJson();
    }
    map['content'] = _content;
    map['demandId'] = _demandId;
    map['relateDemandId'] = _relateDemandId;
    map['relateDemandName'] = _relateDemandName;
    map['createTime'] = _createTime;
    return map;
  }
}

/// id : 1764677020037554177
/// nickName : "rrrr"
/// avatar : "https://oss.sshine.ltd/tracking/public/20240407/a988416c0a0042549e61e9e6b2e9620e-scaled_IMG_20240407_113104.jpg"
/// realName : "wwwww"

class CompanyUser {
  CompanyUser({
    num? id,
    String? nickName,
    String? avatar,
    String? realName,
  }) {
    _id = id;
    _nickName = nickName;
    _avatar = avatar;
    _realName = realName;
  }

  CompanyUser.fromJson(dynamic json) {
    _id = json['id'];
    _nickName = json['nickName'];
    _avatar = json['avatar'];
    _realName = json['realName'];
  }
  num? _id;
  String? _nickName;
  String? _avatar;
  String? _realName;
  CompanyUser copyWith({
    num? id,
    String? nickName,
    String? avatar,
    String? realName,
  }) =>
      CompanyUser(
        id: id ?? _id,
        nickName: nickName ?? _nickName,
        avatar: avatar ?? _avatar,
        realName: realName ?? _realName,
      );
  num? get id => _id;
  String? get nickName => _nickName;
  String? get avatar => _avatar;
  String? get realName => _realName;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['nickName'] = _nickName;
    map['avatar'] = _avatar;
    map['realName'] = _realName;
    return map;
  }
}
