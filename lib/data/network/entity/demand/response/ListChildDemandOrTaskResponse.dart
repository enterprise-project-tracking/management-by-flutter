/// code : 200
/// msg : "ok"
/// data : [{"id":1781547633902923777,"priority":2,"name":"vvv","startTime":"2024-03-20 00:00:00","endTime":"2024-06-20 00:00:00","status":2,"charge":{"id":1771578963453849602,"nickName":"孔艳","avatar":"http://dummyimage.com/200x200","realName":"eeee"},"creator":{"id":1764677020037554177,"nickName":"rrrr","avatar":"https://oss.sshine.ltd/tracking/public/20240407/a988416c0a0042549e61e9e6b2e9620e-scaled_IMG_20240407_113104.jpg","realName":"wwwww"},"type":0,"finishDemand":1,"totalDemand":1}]

class ListChildDemandOrTaskResponse {
  ListChildDemandOrTaskResponse({
    num? code,
    String? msg,
    List<Data>? data,
  }) {
    _code = code;
    _msg = msg;
    _data = data;
  }

  ListChildDemandOrTaskResponse.fromJson(dynamic json) {
    _code = json['code'];
    _msg = json['msg'];
    if (json['data'] != null) {
      _data = [];
      json['data'].forEach((v) {
        _data?.add(Data.fromJson(v));
      });
    }
  }
  num? _code;
  String? _msg;
  List<Data>? _data;
  ListChildDemandOrTaskResponse copyWith({
    num? code,
    String? msg,
    List<Data>? data,
  }) =>
      ListChildDemandOrTaskResponse(
        code: code ?? _code,
        msg: msg ?? _msg,
        data: data ?? _data,
      );
  num? get code => _code;
  String? get msg => _msg;
  List<Data>? get data => _data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['code'] = _code;
    map['msg'] = _msg;
    if (_data != null) {
      map['data'] = _data?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// id : 1781547633902923777
/// priority : 2
/// name : "vvv"
/// startTime : "2024-03-20 00:00:00"
/// endTime : "2024-06-20 00:00:00"
/// status : 2
/// charge : {"id":1771578963453849602,"nickName":"孔艳","avatar":"http://dummyimage.com/200x200","realName":"eeee"}
/// creator : {"id":1764677020037554177,"nickName":"rrrr","avatar":"https://oss.sshine.ltd/tracking/public/20240407/a988416c0a0042549e61e9e6b2e9620e-scaled_IMG_20240407_113104.jpg","realName":"wwwww"}
/// type : 0
/// finishDemand : 1
/// totalDemand : 1

class Data {
  Data({
    num? id,
    num? priority,
    String? name,
    String? startTime,
    String? endTime,
    num? status,
    Charge? charge,
    Creator? creator,
    num? type,
    num? finishDemand,
    num? totalDemand,
  }) {
    _id = id;
    _priority = priority;
    _name = name;
    _startTime = startTime;
    _endTime = endTime;
    _status = status;
    _charge = charge;
    _creator = creator;
    _type = type;
    _finishDemand = finishDemand;
    _totalDemand = totalDemand;
  }

  Data.fromJson(dynamic json) {
    _id = json['id'];
    _priority = json['priority'];
    _name = json['name'];
    _startTime = json['startTime'];
    _endTime = json['endTime'];
    _status = json['status'];
    _charge = json['charge'] != null ? Charge.fromJson(json['charge']) : null;
    _creator =
        json['creator'] != null ? Creator.fromJson(json['creator']) : null;
    _type = json['type'];
    _finishDemand = json['finishDemand'];
    _totalDemand = json['totalDemand'];
  }
  num? _id;
  num? _priority;
  String? _name;
  String? _startTime;
  String? _endTime;
  num? _status;
  Charge? _charge;
  Creator? _creator;
  num? _type;
  num? _finishDemand;
  num? _totalDemand;
  Data copyWith({
    num? id,
    num? priority,
    String? name,
    String? startTime,
    String? endTime,
    num? status,
    Charge? charge,
    Creator? creator,
    num? type,
    num? finishDemand,
    num? totalDemand,
  }) =>
      Data(
        id: id ?? _id,
        priority: priority ?? _priority,
        name: name ?? _name,
        startTime: startTime ?? _startTime,
        endTime: endTime ?? _endTime,
        status: status ?? _status,
        charge: charge ?? _charge,
        creator: creator ?? _creator,
        type: type ?? _type,
        finishDemand: finishDemand ?? _finishDemand,
        totalDemand: totalDemand ?? _totalDemand,
      );
  num? get id => _id;
  num? get priority => _priority;
  String? get name => _name;
  String? get startTime => _startTime;
  String? get endTime => _endTime;
  num? get status => _status;
  Charge? get charge => _charge;
  Creator? get creator => _creator;
  num? get type => _type;
  num? get finishDemand => _finishDemand;
  num? get totalDemand => _totalDemand;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['priority'] = _priority;
    map['name'] = _name;
    map['startTime'] = _startTime;
    map['endTime'] = _endTime;
    map['status'] = _status;
    if (_charge != null) {
      map['charge'] = _charge?.toJson();
    }
    if (_creator != null) {
      map['creator'] = _creator?.toJson();
    }
    map['type'] = _type;
    map['finishDemand'] = _finishDemand;
    map['totalDemand'] = _totalDemand;
    return map;
  }
}

/// id : 1764677020037554177
/// nickName : "rrrr"
/// avatar : "https://oss.sshine.ltd/tracking/public/20240407/a988416c0a0042549e61e9e6b2e9620e-scaled_IMG_20240407_113104.jpg"
/// realName : "wwwww"

class Creator {
  Creator({
    num? id,
    String? nickName,
    String? avatar,
    String? realName,
  }) {
    _id = id;
    _nickName = nickName;
    _avatar = avatar;
    _realName = realName;
  }

  Creator.fromJson(dynamic json) {
    _id = json['id'];
    _nickName = json['nickName'];
    _avatar = json['avatar'];
    _realName = json['realName'];
  }
  num? _id;
  String? _nickName;
  String? _avatar;
  String? _realName;
  Creator copyWith({
    num? id,
    String? nickName,
    String? avatar,
    String? realName,
  }) =>
      Creator(
        id: id ?? _id,
        nickName: nickName ?? _nickName,
        avatar: avatar ?? _avatar,
        realName: realName ?? _realName,
      );
  num? get id => _id;
  String? get nickName => _nickName;
  String? get avatar => _avatar;
  String? get realName => _realName;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['nickName'] = _nickName;
    map['avatar'] = _avatar;
    map['realName'] = _realName;
    return map;
  }
}

/// id : 1771578963453849602
/// nickName : "孔艳"
/// avatar : "http://dummyimage.com/200x200"
/// realName : "eeee"

class Charge {
  Charge({
    num? id,
    String? nickName,
    String? avatar,
    String? realName,
  }) {
    _id = id;
    _nickName = nickName;
    _avatar = avatar;
    _realName = realName;
  }

  Charge.fromJson(dynamic json) {
    _id = json['id'];
    _nickName = json['nickName'];
    _avatar = json['avatar'];
    _realName = json['realName'];
  }
  num? _id;
  String? _nickName;
  String? _avatar;
  String? _realName;
  Charge copyWith({
    num? id,
    String? nickName,
    String? avatar,
    String? realName,
  }) =>
      Charge(
        id: id ?? _id,
        nickName: nickName ?? _nickName,
        avatar: avatar ?? _avatar,
        realName: realName ?? _realName,
      );
  num? get id => _id;
  String? get nickName => _nickName;
  String? get avatar => _avatar;
  String? get realName => _realName;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['nickName'] = _nickName;
    map['avatar'] = _avatar;
    map['realName'] = _realName;
    return map;
  }
}
