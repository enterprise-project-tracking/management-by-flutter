/// code : 200
/// msg : "ok"
/// data : {"records":[{"projectName":"科技型项目跟踪管理系统","projectId":"1777693640121073666","id":1781308291703943170,"priority":2,"name":"yyy","startTime":"2024-06-19","endTime":"2024-06-19","status":0,"charge":{"id":1771578963453849602,"nickName":"孔艳","avatar":"http://dummyimage.com/200x200","realName":"eeee"},"creator":{"id":1764677020037554177,"nickName":"rrrr","avatar":"https://oss.sshine.ltd/tracking/public/20240407/a988416c0a0042549e61e9e6b2e9620e-scaled_IMG_20240407_113104.jpg","realName":"wwwww"},"type":0}],"total":8,"size":10,"current":1}

class ListProjectDemandResponse {
  ListProjectDemandResponse({
      num? code, 
      String? msg, 
      Data? data,}){
    _code = code;
    _msg = msg;
    _data = data;
}

  ListProjectDemandResponse.fromJson(dynamic json) {
    _code = json['code'];
    _msg = json['msg'];
    _data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }
  num? _code;
  String? _msg;
  Data? _data;
ListProjectDemandResponse copyWith({  num? code,
  String? msg,
  Data? data,
}) => ListProjectDemandResponse(  code: code ?? _code,
  msg: msg ?? _msg,
  data: data ?? _data,
);
  num? get code => _code;
  String? get msg => _msg;
  Data? get data => _data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['code'] = _code;
    map['msg'] = _msg;
    if (_data != null) {
      map['data'] = _data?.toJson();
    }
    return map;
  }

}

/// records : [{"projectName":"科技型项目跟踪管理系统","projectId":"1777693640121073666","id":1781308291703943170,"priority":2,"name":"yyy","startTime":"2024-06-19","endTime":"2024-06-19","status":0,"charge":{"id":1771578963453849602,"nickName":"孔艳","avatar":"http://dummyimage.com/200x200","realName":"eeee"},"creator":{"id":1764677020037554177,"nickName":"rrrr","avatar":"https://oss.sshine.ltd/tracking/public/20240407/a988416c0a0042549e61e9e6b2e9620e-scaled_IMG_20240407_113104.jpg","realName":"wwwww"},"type":0}]
/// total : 8
/// size : 10
/// current : 1

class Data {
  Data({
      List<Records>? records, 
      num? total, 
      num? size, 
      num? current,}){
    _records = records;
    _total = total;
    _size = size;
    _current = current;
}

  Data.fromJson(dynamic json) {
    if (json['records'] != null) {
      _records = [];
      json['records'].forEach((v) {
        _records?.add(Records.fromJson(v));
      });
    }
    _total = json['total'];
    _size = json['size'];
    _current = json['current'];
  }
  List<Records>? _records;
  num? _total;
  num? _size;
  num? _current;
Data copyWith({  List<Records>? records,
  num? total,
  num? size,
  num? current,
}) => Data(  records: records ?? _records,
  total: total ?? _total,
  size: size ?? _size,
  current: current ?? _current,
);
  List<Records>? get records => _records;
  num? get total => _total;
  num? get size => _size;
  num? get current => _current;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_records != null) {
      map['records'] = _records?.map((v) => v.toJson()).toList();
    }
    map['total'] = _total;
    map['size'] = _size;
    map['current'] = _current;
    return map;
  }

}

/// projectName : "科技型项目跟踪管理系统"
/// projectId : "1777693640121073666"
/// id : 1781308291703943170
/// priority : 2
/// name : "yyy"
/// startTime : "2024-06-19"
/// endTime : "2024-06-19"
/// status : 0
/// charge : {"id":1771578963453849602,"nickName":"孔艳","avatar":"http://dummyimage.com/200x200","realName":"eeee"}
/// creator : {"id":1764677020037554177,"nickName":"rrrr","avatar":"https://oss.sshine.ltd/tracking/public/20240407/a988416c0a0042549e61e9e6b2e9620e-scaled_IMG_20240407_113104.jpg","realName":"wwwww"}
/// type : 0

class Records {
  Records({
      String? projectName, 
      String? projectId, 
      num? id, 
      num? priority, 
      String? name, 
      String? startTime, 
      String? endTime, 
      num? status, 
      Charge? charge, 
      Creator? creator, 
      num? type,}){
    _projectName = projectName;
    _projectId = projectId;
    _id = id;
    _priority = priority;
    _name = name;
    _startTime = startTime;
    _endTime = endTime;
    _status = status;
    _charge = charge;
    _creator = creator;
    _type = type;
}

  Records.fromJson(dynamic json) {
    _projectName = json['projectName'];
    _projectId = json['projectId'];
    _id = json['id'];
    _priority = json['priority'];
    _name = json['name'];
    _startTime = json['startTime'];
    _endTime = json['endTime'];
    _status = json['status'];
    _charge = json['charge'] != null ? Charge.fromJson(json['charge']) : null;
    _creator = json['creator'] != null ? Creator.fromJson(json['creator']) : null;
    _type = json['type'];
  }
  String? _projectName;
  String? _projectId;
  num? _id;
  num? _priority;
  String? _name;
  String? _startTime;
  String? _endTime;
  num? _status;
  Charge? _charge;
  Creator? _creator;
  num? _type;
Records copyWith({  String? projectName,
  String? projectId,
  num? id,
  num? priority,
  String? name,
  String? startTime,
  String? endTime,
  num? status,
  Charge? charge,
  Creator? creator,
  num? type,
}) => Records(  projectName: projectName ?? _projectName,
  projectId: projectId ?? _projectId,
  id: id ?? _id,
  priority: priority ?? _priority,
  name: name ?? _name,
  startTime: startTime ?? _startTime,
  endTime: endTime ?? _endTime,
  status: status ?? _status,
  charge: charge ?? _charge,
  creator: creator ?? _creator,
  type: type ?? _type,
);
  String? get projectName => _projectName;
  String? get projectId => _projectId;
  num? get id => _id;
  num? get priority => _priority;
  String? get name => _name;
  String? get startTime => _startTime;
  String? get endTime => _endTime;
  num? get status => _status;
  Charge? get charge => _charge;
  Creator? get creator => _creator;
  num? get type => _type;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['projectName'] = _projectName;
    map['projectId'] = _projectId;
    map['id'] = _id;
    map['priority'] = _priority;
    map['name'] = _name;
    map['startTime'] = _startTime;
    map['endTime'] = _endTime;
    map['status'] = _status;
    if (_charge != null) {
      map['charge'] = _charge?.toJson();
    }
    if (_creator != null) {
      map['creator'] = _creator?.toJson();
    }
    map['type'] = _type;
    return map;
  }

}

/// id : 1764677020037554177
/// nickName : "rrrr"
/// avatar : "https://oss.sshine.ltd/tracking/public/20240407/a988416c0a0042549e61e9e6b2e9620e-scaled_IMG_20240407_113104.jpg"
/// realName : "wwwww"

class Creator {
  Creator({
      num? id, 
      String? nickName, 
      String? avatar, 
      String? realName,}){
    _id = id;
    _nickName = nickName;
    _avatar = avatar;
    _realName = realName;
}

  Creator.fromJson(dynamic json) {
    _id = json['id'];
    _nickName = json['nickName'];
    _avatar = json['avatar'];
    _realName = json['realName'];
  }
  num? _id;
  String? _nickName;
  String? _avatar;
  String? _realName;
Creator copyWith({  num? id,
  String? nickName,
  String? avatar,
  String? realName,
}) => Creator(  id: id ?? _id,
  nickName: nickName ?? _nickName,
  avatar: avatar ?? _avatar,
  realName: realName ?? _realName,
);
  num? get id => _id;
  String? get nickName => _nickName;
  String? get avatar => _avatar;
  String? get realName => _realName;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['nickName'] = _nickName;
    map['avatar'] = _avatar;
    map['realName'] = _realName;
    return map;
  }

}

/// id : 1771578963453849602
/// nickName : "孔艳"
/// avatar : "http://dummyimage.com/200x200"
/// realName : "eeee"

class Charge {
  Charge({
      num? id, 
      String? nickName, 
      String? avatar, 
      String? realName,}){
    _id = id;
    _nickName = nickName;
    _avatar = avatar;
    _realName = realName;
}

  Charge.fromJson(dynamic json) {
    _id = json['id'];
    _nickName = json['nickName'];
    _avatar = json['avatar'];
    _realName = json['realName'];
  }
  num? _id;
  String? _nickName;
  String? _avatar;
  String? _realName;
Charge copyWith({  num? id,
  String? nickName,
  String? avatar,
  String? realName,
}) => Charge(  id: id ?? _id,
  nickName: nickName ?? _nickName,
  avatar: avatar ?? _avatar,
  realName: realName ?? _realName,
);
  num? get id => _id;
  String? get nickName => _nickName;
  String? get avatar => _avatar;
  String? get realName => _realName;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['nickName'] = _nickName;
    map['avatar'] = _avatar;
    map['realName'] = _realName;
    return map;
  }

}