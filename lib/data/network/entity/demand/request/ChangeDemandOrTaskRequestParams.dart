/// id : 34
/// priority : 3
/// name : "物理表"
/// description : "社给米京于美求省得南打派容七观低那。等图系理前都住下状合温传如。太响方各听或但识效圆条大段自界。"
/// startTime : "2006-01-13 05:39:17"
/// endTime : "2014-01-08 06:38:53"
/// status : 80
/// chargeId : 93
/// attachmentsAdd : [{"filename":"事约保","url":"http://eqxpkl.ru/oikb"}]
/// attachmentsDelete : [10]

class ChangeDemandOrTaskRequestParams {
  ChangeDemandOrTaskRequestParams({
      num? id, 
      num? priority, 
      String? name, 
      String? description, 
      String? startTime, 
      String? endTime, 
      num? status, 
      num? chargeId, 
      List<AttachmentsAdd>? attachmentsAdd, 
      List<num>? attachmentsDelete,}){
    _id = id;
    _priority = priority;
    _name = name;
    _description = description;
    _startTime = startTime;
    _endTime = endTime;
    _status = status;
    _chargeId = chargeId;
    _attachmentsAdd = attachmentsAdd;
    _attachmentsDelete = attachmentsDelete;
}

  ChangeDemandOrTaskRequestParams.fromJson(dynamic json) {
    _id = json['id'];
    _priority = json['priority'];
    _name = json['name'];
    _description = json['description'];
    _startTime = json['startTime'];
    _endTime = json['endTime'];
    _status = json['status'];
    _chargeId = json['chargeId'];
    if (json['attachmentsAdd'] != null) {
      _attachmentsAdd = [];
      json['attachmentsAdd'].forEach((v) {
        _attachmentsAdd?.add(AttachmentsAdd.fromJson(v));
      });
    }
    _attachmentsDelete = json['attachmentsDelete'] != null ? json['attachmentsDelete'].cast<num>() : [];
  }
  num? _id;
  num? _priority;
  String? _name;
  String? _description;
  String? _startTime;
  String? _endTime;
  num? _status;
  num? _chargeId;
  List<AttachmentsAdd>? _attachmentsAdd;
  List<num>? _attachmentsDelete;
ChangeDemandOrTaskRequestParams copyWith({  num? id,
  num? priority,
  String? name,
  String? description,
  String? startTime,
  String? endTime,
  num? status,
  num? chargeId,
  List<AttachmentsAdd>? attachmentsAdd,
  List<num>? attachmentsDelete,
}) => ChangeDemandOrTaskRequestParams(  id: id ?? _id,
  priority: priority ?? _priority,
  name: name ?? _name,
  description: description ?? _description,
  startTime: startTime ?? _startTime,
  endTime: endTime ?? _endTime,
  status: status ?? _status,
  chargeId: chargeId ?? _chargeId,
  attachmentsAdd: attachmentsAdd ?? _attachmentsAdd,
  attachmentsDelete: attachmentsDelete ?? _attachmentsDelete,
);
  num? get id => _id;
  num? get priority => _priority;
  String? get name => _name;
  String? get description => _description;
  String? get startTime => _startTime;
  String? get endTime => _endTime;
  num? get status => _status;
  num? get chargeId => _chargeId;
  List<AttachmentsAdd>? get attachmentsAdd => _attachmentsAdd;
  List<num>? get attachmentsDelete => _attachmentsDelete;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['priority'] = _priority;
    map['name'] = _name;
    map['description'] = _description;
    map['startTime'] = _startTime;
    map['endTime'] = _endTime;
    map['status'] = _status;
    map['chargeId'] = _chargeId;
    if (_attachmentsAdd != null) {
      map['attachmentsAdd'] = _attachmentsAdd?.map((v) => v.toJson()).toList();
    }
    map['attachmentsDelete'] = _attachmentsDelete;
    return map;
  }

}

/// filename : "事约保"
/// url : "http://eqxpkl.ru/oikb"

class AttachmentsAdd {
  AttachmentsAdd({
      String? filename, 
      String? url,}){
    _filename = filename;
    _url = url;
}

  AttachmentsAdd.fromJson(dynamic json) {
    _filename = json['filename'];
    _url = json['url'];
  }
  String? _filename;
  String? _url;
AttachmentsAdd copyWith({  String? filename,
  String? url,
}) => AttachmentsAdd(  filename: filename ?? _filename,
  url: url ?? _url,
);
  String? get filename => _filename;
  String? get url => _url;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['filename'] = _filename;
    map['url'] = _url;
    return map;
  }

}