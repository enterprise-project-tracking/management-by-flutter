/// projectId : 2
/// pid : 49
/// priority : 0
/// name : "没者书"
/// description : "九真发化节保准土生质人求同里原动。还需听月每于场太品江离志问活。社信生级观回传布活圆状快电备把步。"
/// startTime : "1993-08-26 02:48:47"
/// endTime : "2012-12-14 02:33:00"
/// status : 56
/// chargeId : 11
/// type : 28
/// attachments : [{"filename":"质克前却","url":"http://kcvtpcps.mq/ivqivtrz"}]

class CreateDemandOrTaskRequestParams {
  CreateDemandOrTaskRequestParams({
      num? projectId, 
      num? pid, 
      num? priority, 
      String? name, 
      String? description, 
      String? startTime, 
      String? endTime, 
      num? status, 
      num? chargeId, 
      num? type, 
      List<Attachments>? attachments,}){
    _projectId = projectId;
    _pid = pid;
    _priority = priority;
    _name = name;
    _description = description;
    _startTime = startTime;
    _endTime = endTime;
    _status = status;
    _chargeId = chargeId;
    _type = type;
    _attachments = attachments;
}

  CreateDemandOrTaskRequestParams.fromJson(dynamic json) {
    _projectId = json['projectId'];
    _pid = json['pid'];
    _priority = json['priority'];
    _name = json['name'];
    _description = json['description'];
    _startTime = json['startTime'];
    _endTime = json['endTime'];
    _status = json['status'];
    _chargeId = json['chargeId'];
    _type = json['type'];
    if (json['attachments'] != null) {
      _attachments = [];
      json['attachments'].forEach((v) {
        _attachments?.add(Attachments.fromJson(v));
      });
    }
  }
  num? _projectId;
  num? _pid;
  num? _priority;
  String? _name;
  String? _description;
  String? _startTime;
  String? _endTime;
  num? _status;
  num? _chargeId;
  num? _type;
  List<Attachments>? _attachments;
CreateDemandOrTaskRequestParams copyWith({  num? projectId,
  num? pid,
  num? priority,
  String? name,
  String? description,
  String? startTime,
  String? endTime,
  num? status,
  num? chargeId,
  num? type,
  List<Attachments>? attachments,
}) => CreateDemandOrTaskRequestParams(  projectId: projectId ?? _projectId,
  pid: pid ?? _pid,
  priority: priority ?? _priority,
  name: name ?? _name,
  description: description ?? _description,
  startTime: startTime ?? _startTime,
  endTime: endTime ?? _endTime,
  status: status ?? _status,
  chargeId: chargeId ?? _chargeId,
  type: type ?? _type,
  attachments: attachments ?? _attachments,
);
  num? get projectId => _projectId;
  num? get pid => _pid;
  num? get priority => _priority;
  String? get name => _name;
  String? get description => _description;
  String? get startTime => _startTime;
  String? get endTime => _endTime;
  num? get status => _status;
  num? get chargeId => _chargeId;
  num? get type => _type;
  List<Attachments>? get attachments => _attachments;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['projectId'] = _projectId;
    map['pid'] = _pid;
    map['priority'] = _priority;
    map['name'] = _name;
    map['description'] = _description;
    map['startTime'] = _startTime;
    map['endTime'] = _endTime;
    map['status'] = _status;
    map['chargeId'] = _chargeId;
    map['type'] = _type;
    if (_attachments != null) {
      map['attachments'] = _attachments?.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// filename : "质克前却"
/// url : "http://kcvtpcps.mq/ivqivtrz"

class Attachments {
  Attachments({
      String? filename, 
      String? url,}){
    _filename = filename;
    _url = url;
}

  Attachments.fromJson(dynamic json) {
    _filename = json['filename'];
    _url = json['url'];
  }
  String? _filename;
  String? _url;
Attachments copyWith({  String? filename,
  String? url,
}) => Attachments(  filename: filename ?? _filename,
  url: url ?? _url,
);
  String? get filename => _filename;
  String? get url => _url;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['filename'] = _filename;
    map['url'] = _url;
    return map;
  }

}