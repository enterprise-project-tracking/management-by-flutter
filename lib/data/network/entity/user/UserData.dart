/// code : 200
/// msg : "ok"
/// data : {"id":1763201736960532482,"nickName":"靓仔","email":"1742137309@qq.com","avatar":"http://dummyimage.com/100x100","phone":null,"realName":"潘祖果"}

class UserData {
  UserData({
    num? code,
    String? msg,
    Data? data,
  }) {
    _code = code;
    _msg = msg;
    _data = data;
  }

  UserData.fromJson(dynamic json) {
    _code = json['code'];
    _msg = json['msg'];
    _data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }
  num? _code;
  String? _msg;
  Data? _data;
  UserData copyWith({
    num? code,
    String? msg,
    Data? data,
  }) =>
      UserData(
        code: code ?? _code,
        msg: msg ?? _msg,
        data: data ?? _data,
      );
  num? get code => _code;
  String? get msg => _msg;
  Data? get data => _data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['code'] = _code;
    map['msg'] = _msg;
    if (_data != null) {
      map['data'] = _data?.toJson();
    }
    return map;
  }
}

/// id : 1763201736960532482
/// nickName : "靓仔"
/// email : "1742137309@qq.com"
/// avatar : "http://dummyimage.com/100x100"
/// phone : null
/// realName : "潘祖果"

class Data {
  Data({
    num? id,
    String? nickName,
    String? email,
    String? avatar,
    dynamic phone,
    String? realName,
  }) {
    _id = id;
    _nickName = nickName;
    _email = email;
    _avatar = avatar;
    _phone = phone;
    _realName = realName;
  }

  Data.fromJson(dynamic json) {
    _id = json['id'];
    _nickName = json['nickName'];
    _email = json['email'];
    _avatar = json['avatar'];
    _phone = json['phone'];
    _realName = json['realName'];
  }
  num? _id;
  String? _nickName;
  String? _email;
  String? _avatar;
  dynamic _phone;
  String? _realName;
  Data copyWith({
    num? id,
    String? nickName,
    String? email,
    String? avatar,
    dynamic phone,
    String? realName,
  }) =>
      Data(
        id: id ?? _id,
        nickName: nickName ?? _nickName,
        email: email ?? _email,
        avatar: avatar ?? _avatar,
        phone: phone ?? _phone,
        realName: realName ?? _realName,
      );
  num? get id => _id;
  String? get nickName => _nickName;
  String? get email => _email;
  String? get avatar => _avatar;
  dynamic get phone => _phone;
  String? get realName => _realName;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['nickName'] = _nickName;
    map['email'] = _email;
    map['avatar'] = _avatar;
    map['phone'] = _phone;
    map['realName'] = _realName;
    return map;
  }
}
