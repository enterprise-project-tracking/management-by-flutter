/// code : 500
/// msg : "验证码不正确"

class ChangePwdResponse {
  ChangePwdResponse({
      num? code, 
      String? msg,}){
    _code = code;
    _msg = msg;
}

  ChangePwdResponse.fromJson(dynamic json) {
    _code = json['code'];
    _msg = json['msg'];
  }
  num? _code;
  String? _msg;
ChangePwdResponse copyWith({  num? code,
  String? msg,
}) => ChangePwdResponse(  code: code ?? _code,
  msg: msg ?? _msg,
);
  num? get code => _code;
  String? get msg => _msg;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['code'] = _code;
    map['msg'] = _msg;
    return map;
  }

}