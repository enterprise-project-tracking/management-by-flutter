/// code : 200
/// msg : "ok"
/// data : {"access_token":"eyJraWQiOiIxMTgyYTA4Yy0yM2JjLTQ2NDctYmFjYy05OTkwMjViZTRlNzYiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiIxNzQyMTM3MzA5QHFxLmNvbSIsImNvbXBhbnlVc2VySWQiOjE3NjQ2NzcwMjAwMzc1NTQxNzcsImlzcyI6Imh0dHBzOi8vdHJhY2tpbmcuc3NoaW5lLmx0ZCIsInVzZXJJZCI6MTc2MzIwMTczNjk2MDUzMjQ4MiwiYXV0aG9yaXRpZXMiOlsiUk9MRV91c2VyIl0sImF1ZCI6ImFwcCIsImNvbXBhbnlJZCI6MTc2NDY3NzAxOTM5NTgyNTY2NSwibmJmIjoxNzA5OTkzMjc1LCJzY29wZSI6WyJvcGVuaWQiXSwiZXhwIjoxNzA5OTkzNTc1LCJpYXQiOjE3MDk5OTMyNzUsImp0aSI6ImU3OTZjNjI4LTAxODgtNDk3Zi04NWFmLWI4YTA2ZTI4NWU1NSIsInVzZXJuYW1lIjoiMTc0MjEzNzMwOUBxcS5jb20ifQ.LxbyahReyTRFa8p3U2wv4yt0ylpYdPn8KAjylkia8i2AALysK17iKRkrMCswNLg449UsBO2V3kwIBU75Rg4FPs1G-1d2Zf0Z6BbetAA3O94hBwFESHfyr1Fj-xhSmC0IykIbUEMGuvnopbezVg0EeRtLTtAz8lF2vM63l1RfkbNLIpYxxG5i03WDg3QS8TRNutRske_XDrCQBdq-erYUe_ACvjTSlmOnl_vmjf2Qm5KEPCTXXciuPN1-9xIWb5cwuYnOfUB4_pEizELGg9w_ZqNHTfwoT21fg3kXr6Ii7hF2ReyM2wZr0JLekcttZS5V5vNzzx7V5hJToJrkXTGZ7w","refresh_token":"yE0s8aGJy11wDMpzwSWlRdPrvov-gJH96C0B4AlyzeqzOzZbgXIiSbC8UElFCZF70ki1K4U5C43kKKMJfVI5U6niW2tcMPK9SPXasovLfHxxARmZmHQVEAN19rcF1Nzr","id_token":"eyJraWQiOiIxMTgyYTA4Yy0yM2JjLTQ2NDctYmFjYy05OTkwMjViZTRlNzYiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiIxNzQyMTM3MzA5QHFxLmNvbSIsImF1ZCI6ImFwcCIsImF6cCI6ImFwcCIsImlzcyI6Imh0dHBzOi8vdHJhY2tpbmcuc3NoaW5lLmx0ZCIsImV4cCI6MTcwOTk5NTA3NSwiaWF0IjoxNzA5OTkzMjc1LCJqdGkiOiJiMDUwNGUwOC0zYzUxLTRmNWUtOWUyMy01NGMwOTE2M2Q2NmUifQ.qCncjPfHJAaUTnWbOGZQUgJk5QGOfTqv_x4DoULNvhXy9JkzZKUuAJUc33aIyAPjf7KsSXCAX74K1WycdmqZDHEBtzG7a4dTfIK-BPrH1hpJI4hY7J1eFTIfXmBukm3Lx2WgPYL2T6vZ2VsJ25EvXzkI7OILjIeVU5ZAGFpwyYyhcfKoFKB9QlkUpwq8GvzsmF9INujcBc0Lhiah0DGl0PGhFtVgIeuwfh14ao7xXhE21Tfn-0LPHN_CArF5_h41xuqV3Vn9P8JVFEQ3cxmORSVBrbd6zZ5D3lhTOEbcdImV9CORPHbvXI14f3rXgwZjYzfMYplN54OcaIH0bbezSA","token_type":"Bearer","expires_in":"299"}

class LoginResponse {
  LoginResponse({
    num? code,
    String? msg,
    Data? data,
  }) {
    _code = code;
    _msg = msg;
    _data = data;
  }

  LoginResponse.fromJson(dynamic json) {
    _code = json['code'];
    _msg = json['msg'];
    _data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }
  num? _code;
  String? _msg;
  Data? _data;
  LoginResponse copyWith({
    num? code,
    String? msg,
    Data? data,
  }) =>
      LoginResponse(
        code: code ?? _code,
        msg: msg ?? _msg,
        data: data ?? _data,
      );
  num? get code => _code;
  String? get msg => _msg;
  Data? get data => _data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['code'] = _code;
    map['msg'] = _msg;
    if (_data != null) {
      map['data'] = _data?.toJson();
    }
    return map;
  }
}

/// access_token : "eyJraWQiOiIxMTgyYTA4Yy0yM2JjLTQ2NDctYmFjYy05OTkwMjViZTRlNzYiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiIxNzQyMTM3MzA5QHFxLmNvbSIsImNvbXBhbnlVc2VySWQiOjE3NjQ2NzcwMjAwMzc1NTQxNzcsImlzcyI6Imh0dHBzOi8vdHJhY2tpbmcuc3NoaW5lLmx0ZCIsInVzZXJJZCI6MTc2MzIwMTczNjk2MDUzMjQ4MiwiYXV0aG9yaXRpZXMiOlsiUk9MRV91c2VyIl0sImF1ZCI6ImFwcCIsImNvbXBhbnlJZCI6MTc2NDY3NzAxOTM5NTgyNTY2NSwibmJmIjoxNzA5OTkzMjc1LCJzY29wZSI6WyJvcGVuaWQiXSwiZXhwIjoxNzA5OTkzNTc1LCJpYXQiOjE3MDk5OTMyNzUsImp0aSI6ImU3OTZjNjI4LTAxODgtNDk3Zi04NWFmLWI4YTA2ZTI4NWU1NSIsInVzZXJuYW1lIjoiMTc0MjEzNzMwOUBxcS5jb20ifQ.LxbyahReyTRFa8p3U2wv4yt0ylpYdPn8KAjylkia8i2AALysK17iKRkrMCswNLg449UsBO2V3kwIBU75Rg4FPs1G-1d2Zf0Z6BbetAA3O94hBwFESHfyr1Fj-xhSmC0IykIbUEMGuvnopbezVg0EeRtLTtAz8lF2vM63l1RfkbNLIpYxxG5i03WDg3QS8TRNutRske_XDrCQBdq-erYUe_ACvjTSlmOnl_vmjf2Qm5KEPCTXXciuPN1-9xIWb5cwuYnOfUB4_pEizELGg9w_ZqNHTfwoT21fg3kXr6Ii7hF2ReyM2wZr0JLekcttZS5V5vNzzx7V5hJToJrkXTGZ7w"
/// refresh_token : "yE0s8aGJy11wDMpzwSWlRdPrvov-gJH96C0B4AlyzeqzOzZbgXIiSbC8UElFCZF70ki1K4U5C43kKKMJfVI5U6niW2tcMPK9SPXasovLfHxxARmZmHQVEAN19rcF1Nzr"
/// id_token : "eyJraWQiOiIxMTgyYTA4Yy0yM2JjLTQ2NDctYmFjYy05OTkwMjViZTRlNzYiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiIxNzQyMTM3MzA5QHFxLmNvbSIsImF1ZCI6ImFwcCIsImF6cCI6ImFwcCIsImlzcyI6Imh0dHBzOi8vdHJhY2tpbmcuc3NoaW5lLmx0ZCIsImV4cCI6MTcwOTk5NTA3NSwiaWF0IjoxNzA5OTkzMjc1LCJqdGkiOiJiMDUwNGUwOC0zYzUxLTRmNWUtOWUyMy01NGMwOTE2M2Q2NmUifQ.qCncjPfHJAaUTnWbOGZQUgJk5QGOfTqv_x4DoULNvhXy9JkzZKUuAJUc33aIyAPjf7KsSXCAX74K1WycdmqZDHEBtzG7a4dTfIK-BPrH1hpJI4hY7J1eFTIfXmBukm3Lx2WgPYL2T6vZ2VsJ25EvXzkI7OILjIeVU5ZAGFpwyYyhcfKoFKB9QlkUpwq8GvzsmF9INujcBc0Lhiah0DGl0PGhFtVgIeuwfh14ao7xXhE21Tfn-0LPHN_CArF5_h41xuqV3Vn9P8JVFEQ3cxmORSVBrbd6zZ5D3lhTOEbcdImV9CORPHbvXI14f3rXgwZjYzfMYplN54OcaIH0bbezSA"
/// token_type : "Bearer"
/// expires_in : "299"

class Data {
  Data({
    String? accessToken,
    String? refreshToken,
    String? idToken,
    String? tokenType,
    String? expiresIn,
  }) {
    _accessToken = accessToken;
    _refreshToken = refreshToken;
    _idToken = idToken;
    _tokenType = tokenType;
    _expiresIn = expiresIn;
  }

  Data.fromJson(dynamic json) {
    _accessToken = json['access_token'];
    _refreshToken = json['refresh_token'];
    _idToken = json['id_token'];
    _tokenType = json['token_type'];
    _expiresIn = json['expires_in'];
  }
  String? _accessToken;
  String? _refreshToken;
  String? _idToken;
  String? _tokenType;
  String? _expiresIn;
  Data copyWith({
    String? accessToken,
    String? refreshToken,
    String? idToken,
    String? tokenType,
    String? expiresIn,
  }) =>
      Data(
        accessToken: accessToken ?? _accessToken,
        refreshToken: refreshToken ?? _refreshToken,
        idToken: idToken ?? _idToken,
        tokenType: tokenType ?? _tokenType,
        expiresIn: expiresIn ?? _expiresIn,
      );
  String? get accessToken => _accessToken;
  String? get refreshToken => _refreshToken;
  String? get idToken => _idToken;
  String? get tokenType => _tokenType;
  String? get expiresIn => _expiresIn;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['access_token'] = _accessToken;
    map['refresh_token'] = _refreshToken;
    map['id_token'] = _idToken;
    map['token_type'] = _tokenType;
    map['expires_in'] = _expiresIn;
    return map;
  }
}
