import 'dart:math';

import 'package:dio/dio.dart';
import 'package:management/const/init.dart';
import 'package:management/data/network/entity/BaseResponse.dart';
import 'package:management/data/network/entity/company/AllCompanyGroupInfo.dart';
import 'package:management/data/network/entity/company/ChangeCompanyGroupInfo.dart';
import 'package:management/data/network/entity/company/ChangeEmployeeResponseInfo.dart';
import 'package:management/data/network/entity/company/CompanyTypeInfo.dart';
import 'package:management/data/network/entity/company/CreateCompanyGroupInfo.dart';
import 'package:management/data/network/entity/company/CreateCompanyInfo.dart';
import 'package:management/data/network/entity/company/DeleteCompanyGroupInfo.dart';
import 'package:management/data/network/entity/company/FireEmployeeInfo.dart';
import 'package:management/data/network/entity/company/GroupLoadResponseInfo.dart';
import 'package:management/data/network/entity/company/JoinCompanyInfo.dart';
import 'package:management/data/network/entity/company/MyCompanyInfo.dart';
import 'package:management/data/network/entity/company/ShareCompanyInfo.dart';
import 'package:management/data/network/entity/demand/request/ChangeDemandOrTaskRequestParams.dart';
import 'package:management/data/network/entity/demand/request/CreateDemandOrTaskRequestParams.dart';
import 'package:management/data/network/entity/demand/response/DemandOrTaskDetailResponse.dart';
import 'package:management/data/network/entity/demand/response/ListChildDemandOrTaskResponse.dart';
import 'package:management/data/network/entity/demand/response/ListDemandOrTaskForMain.dart';
import 'package:management/data/network/entity/demand/response/ListLogDemandOrTaskResponse.dart';
import 'package:management/data/network/entity/demand/response/ListProjectDemandResponse.dart';
import 'package:management/data/network/entity/file/FileUploadResponse.dart';
import 'package:management/data/network/entity/file/FilesUploadResponse.dart';
import 'package:management/data/network/entity/manage/device/AddDeviceResponse.dart';
import 'package:management/data/network/entity/manage/device/ListDeviceResponse.dart';
import 'package:management/data/network/entity/manage/people/CompanyMemberDetailResponse.dart';
import 'package:management/data/network/entity/manage/people/ListMemberAndGroupResponse.dart';
import 'package:management/data/network/entity/project/request/ProjectConclusionRequestParams.dart';
import 'package:management/data/network/entity/project/request/SubmitProjectRequestParams.dart';
import 'package:management/data/network/entity/project/request/UpdateProjectRequestParams.dart';
import 'package:management/data/network/entity/project/response/AllUserResponse.dart';
import 'package:management/data/network/entity/project/response/InitProjectResponse.dart';
import 'package:management/data/network/entity/project/response/ListProjectResponse.dart';
import 'package:management/data/network/entity/project/response/ProjectConclusionResponse.dart';
import 'package:management/data/network/entity/project/response/ProjectDetailResponse.dart';
import 'package:management/data/network/entity/setting/WorkLoadTypeResponse.dart';
import 'package:management/data/network/entity/user/ChangePwdResponse.dart';
import 'package:management/data/network/entity/user/LoginResponse.dart';
import 'package:management/data/network/entity/user/UpdateUserData.dart';
import 'package:management/data/network/entity/user/UserData.dart';
import 'package:management/data/network/entity/worklog/request/changeWorkLogRequestParams.dart';
import 'package:management/data/network/entity/worklog/request/createWorkLogRequestParams.dart';
import 'package:management/data/network/entity/worklog/response/ListWorkLogResponse.dart';
import 'package:management/data/network/netCenter.dart';

class MyHttpClient {
  static String register = 'REGISTER';
  static String forget = 'FORGET';
  static String login = 'LOGIN';

  MyHttpClient._();
  // 获取验证码
  static Future<BaseResponse> sendVerifyCode(
      String account, String captchaOption) async {
    var response = await NetCenter.request('/auth/captcha/send',
        method: NetCenter.POST,
        params: {"account": account, "captchaOption": captchaOption});
    final result = BaseResponse.fromJson(response);
    return result;
  }

  // 用户注册
  static Future<BaseResponse> signUp(
      String account, String password, String code) async {
    var response = await NetCenter.request('/system/user/register',
        method: NetCenter.POST,
        params: {"account": account, "password": password, "code": code});
    final result = BaseResponse.fromJson(response);
    return result;
  }

  // 用户登录
  static Future<LoginResponse> signIn(String account, String password) async {
    var response = await NetCenter.request('/auth/login',
        method: NetCenter.POST,
        params: {"username": account, "password": password});
    final result = LoginResponse.fromJson(response);
    return result;
  }

  // 获取刷新Token, 访问Token等
  static Future<LoginResponse> getRefreshByToken(String refreshToken) async {
    var response = await NetCenter.request('/auth/refresh_token',
        method: NetCenter.POST, params: {"refreshToken": refreshToken});
    final result = LoginResponse.fromJson(response);
    return result;
  }

  // 获取用户信息
  static Future<UserData> getUserData() async {
    var response = await NetCenter.request('/system/user/info',
        method: NetCenter.GET,
        headers: {"Authorization": 'Bearer ${GlobalInit.getAccessToken()}'});
    final result = UserData.fromJson(response);
    return result;
  }

  // 更新用户信息
  static Future<UpdateInformationResponse> updateUserData(
      Map<String, dynamic> params) async {
    var response = await NetCenter.request('/system/user/info',
        method: NetCenter.PUT,
        headers: {"Authorization": 'Bearer ${GlobalInit.getAccessToken()}'},
        params: params);
    final result = UpdateInformationResponse.fromJson(response);
    return result;
  }

  static Future<ChangePwdResponse> upDatePwd(Map<String, String> params) async {
    var response = await NetCenter.request('/system/user/update-password',
        // headers: {"Authorization": 'Bearer ${GlobalInit.getAccessToken()}'},
        method: NetCenter.PUT,
        params: params);
    final result = ChangePwdResponse.fromJson(response);
    return result;
  }

  // 获取公司信息
  static Future<MyCompanyInfo> getMyCompanyInfo() async {
    var response = await NetCenter.request('/project/company/my',
        method: NetCenter.GET,
        headers: {"Authorization": 'Bearer ${GlobalInit.getAccessToken()}'});
    final result = MyCompanyInfo.fromJson(response);
    return result;
  }

  // 更新公司信息
  static Future<UpdateInformationResponse> updateCompanyInfo(
      Map<String, String> params) async {
    var response = await NetCenter.request('/project/company/admin/update',
        headers: {"Authorization": 'Bearer ${GlobalInit.getAccessToken()}'},
        method: NetCenter.POST,
        params: params);
    final result = UpdateInformationResponse.fromJson(response);
    return result;
  }

  // 创建邀请码
  static Future<ShareCompanyInfo> createJoinCode() async {
    var response = await NetCenter.request(
        '/project/company/admin/create-join-code',
        headers: {"Authorization": 'Bearer ${GlobalInit.getAccessToken()}'},
        method: NetCenter.POST);
    final result = ShareCompanyInfo.fromJson(response);
    return result;
  }

  // 获取行业类型
  static Future<CompanyTypeInfo> getCompanyType() async {
    var response = await NetCenter.request('/project/industry/list',
        headers: {"Authorization": 'Bearer ${GlobalInit.getAccessToken()}'},
        method: NetCenter.GET);
    final result = CompanyTypeInfo.fromJson(response);
    return result;
  }

  // 加入公司
  static Future<JoinCompanyInfo> joinCompany(String code) async {
    var response = await NetCenter.request('/project/company/join',
        headers: {"Authorization": 'Bearer ${GlobalInit.getAccessToken()}'},
        params: {"code": code},
        method: NetCenter.POST);
    final result = JoinCompanyInfo.fromJson(response);
    return result;
  }

  // 创建公司
  static Future<CreateCompanyInfo> createCompany(
      Map<String, String> params) async {
    var response = await NetCenter.request('/project/company/create',
        headers: {"Authorization": 'Bearer ${GlobalInit.getAccessToken()}'},
        params: params,
        method: NetCenter.POST);
    final result = CreateCompanyInfo.fromJson(response);
    return result;
  }

  ///////// 管理模块
  // 获取公司成员
  static Future<ListMemberAndGroupResponse> getCompanyMember(
      String groupId) async {
    var response = await NetCenter.request('/project/company/user/list',
        headers: {"Authorization": 'Bearer ${GlobalInit.getAccessToken()}'},
        params: {'groupId': groupId},
        method: NetCenter.GET);
    final result = ListMemberAndGroupResponse.fromJson(response);
    return result;
  }

  // 创建用户组
  static Future<CreateCompanyGroupInfo> createCompanyGroup(
      String groupName, int? parentId) async {
    var response = await NetCenter.request(
        '/project/company/user/admin/create-group',
        headers: {"Authorization": 'Bearer ${GlobalInit.getAccessToken()}'},
        params: {"name": groupName, "pid": parentId},
        method: NetCenter.POST);
    final result = CreateCompanyGroupInfo.fromJson(response);
    return result;
  }

  // 删除用户组
  static Future<DeleteCompanyGroupInfo> deleteCompanyGroup(
      String groupId) async {
    var response = await NetCenter.request(
        '/project/company/user/admin/delete-group',
        headers: {"Authorization": 'Bearer ${GlobalInit.getAccessToken()}'},
        params: {"companyUserGroupId": groupId},
        method: NetCenter.DELETE);
    final result = DeleteCompanyGroupInfo.fromJson(response);
    return result;
  }

  // 修改用户组名
  static Future<ChangeCompanyGroupInfo> changeCompanyGroup(
      String? newGroupName, String groupId, String? parentId) async {
    var response = await NetCenter.request(
        '/project/company/user/admin/update-group',
        headers: {"Authorization": 'Bearer ${GlobalInit.getAccessToken()}'},
        params: {"id": groupId, "name": newGroupName, "pid": parentId},
        method: NetCenter.PUT);
    final result = ChangeCompanyGroupInfo.fromJson(response);
    return result;
  }

  // 获取全部的组
  static Future<AllCompanyGroupInfo> getAllCompanyGroup() async {
    var response = await NetCenter.request('/project/company/user/groups-all',
        headers: {"Authorization": 'Bearer ${GlobalInit.getAccessToken()}'},
        method: NetCenter.GET);
    final result = AllCompanyGroupInfo.fromJson(response);
    return result;
  }

  // 获取组内的组
  static Future<GroupLoadResponseInfo> getGroupLoadGroup(String id) async {
    var response = await NetCenter.request('/project/company/user/groups',
        headers: {"Authorization": 'Bearer ${GlobalInit.getAccessToken()}'},
        params: {"groupId": id},
        method: NetCenter.GET);
    final result = GroupLoadResponseInfo.fromJson(response);
    return result;
  }

  // 修改员工信息
  static Future<ChangeEmployeeResponseInfo> changeEmployeeInformation(
      String id, String? parentId, String? jobNumber) async {
    var response = await NetCenter.request('/project/company/user/admin/update',
        headers: {"Authorization": 'Bearer ${GlobalInit.getAccessToken()}'},
        params: {"id": id, "jobNumber": jobNumber, "groupId": parentId},
        method: NetCenter.PUT);
    final result = ChangeEmployeeResponseInfo.fromJson(response);
    return result;
  }

  // 解雇员工
  static Future<FireEmployeeInfo> fireEmployee(String companyUserId) async {
    var response = await NetCenter.request('/project/company/user/admin/leave',
        headers: {"Authorization": 'Bearer ${GlobalInit.getAccessToken()}'},
        params: {"companyUserId": companyUserId},
        method: NetCenter.DELETE);
    final result = FireEmployeeInfo.fromJson(response);
    return result;
  }

  // 获取公司设备
  static Future<ListDeviceResponse> getCompanyDevice(String? keyWord) async {
    var response = await NetCenter.request('/project/company/device/page',
        headers: {"Authorization": 'Bearer ${GlobalInit.getAccessToken()}'},
        params: {"keyword": keyWord},
        method: NetCenter.GET);
    final result = ListDeviceResponse.fromJson(response);
    return result;
  }

  // {
  // "value": 61,
  // "name": "商教要指老海",
  // "position": "velit mollit in exercitation",
  // "remark": "laborum id aliqua",
  // "picUrl": "http://dummyimage.com/400x400"
  // }
  static Future<AddDeviceResponse> addNewDevice(
      Map<String, String> params) async {
    var response = await NetCenter.request('/project/company/device/add',
        headers: {"Authorization": 'Bearer ${GlobalInit.getAccessToken()}'},
        params: params,
        method: NetCenter.POST);
    final result = AddDeviceResponse.fromJson(response);
    return result;
  }

  // 删除设备
  static Future<AddDeviceResponse> deleteDevice(String deviceId) async {
    var response = await NetCenter.request('/project/company/device/delete',
        headers: {"Authorization": 'Bearer ${GlobalInit.getAccessToken()}'},
        params: {"deviceId": deviceId},
        method: NetCenter.DELETE);
    final result = AddDeviceResponse.fromJson(response);
    return result;
  }

  // 更新设备信息
  static Future<AddDeviceResponse> changeDeviceInformation(
      Map<String, String> params) async {
    var response = await NetCenter.request('/project/company/device/update',
        headers: {"Authorization": 'Bearer ${GlobalInit.getAccessToken()}'},
        params: params,
        method: NetCenter.PUT);
    final result = AddDeviceResponse.fromJson(response);
    return result;
  }

  //获取人员详情
  static Future<CompanyMemberDetailResponse> getCompanyMemberDetail(
      String companyUserId) async {
    var response = await NetCenter.request('/project/company/user/details',
        headers: {"Authorization": 'Bearer ${GlobalInit.getAccessToken()}'},
        params: {"companyUserId": companyUserId},
        method: NetCenter.GET);
    final result = CompanyMemberDetailResponse.fromJson(response);
    return result;
  }

  // 初始化项目
  static Future<InitProjectResponse> initProject() async {
    var response = await NetCenter.request('/project/project/init',
        headers: {"Authorization": 'Bearer ${GlobalInit.getAccessToken()}'},
        params: null,
        method: NetCenter.POST);
    final result = InitProjectResponse.fromJson(response);
    return result;
  }

  // 提交立项
  static Future<BaseResponse> submitProject(
      SubmitProjectRequestParams params) async {
    var response = await NetCenter.request('/project/project/submit',
        headers: {"Authorization": 'Bearer ${GlobalInit.getAccessToken()}'},
        params: params.toJson(),
        method: NetCenter.POST);
    final result = BaseResponse.fromJson(response);
    return result;
  }

  // 修改项目信息
  static Future<BaseResponse> updateProject(
      UpdateProjectRequestParams params) async {
    var response = await NetCenter.request('/project/project/update',
        headers: {"Authorization": 'Bearer ${GlobalInit.getAccessToken()}'},
        params: params.toJson(),
        method: NetCenter.PUT);
    final result = BaseResponse.fromJson(response);
    return result;
  }

  // 项目结题
  static Future<BaseResponse> projectConclusion(
      ProjectConclusionRequestParams params) async {
    var response = await NetCenter.request(
        '/project/project/conclusion-project',
        headers: {"Authorization": 'Bearer ${GlobalInit.getAccessToken()}'},
        params: params.toJson(),
        method: NetCenter.POST);
    final result = BaseResponse.fromJson(response);
    return result;
  }

  // 删除项目
  static Future<BaseResponse> deleteProject(num projectId) async {
    var response = await NetCenter.request('/project/project/delete',
        headers: {"Authorization": 'Bearer ${GlobalInit.getAccessToken()}'},
        params: {'projectId': projectId},
        method: NetCenter.DELETE);
    final result = BaseResponse.fromJson(response);
    return result;
  }

  // 获取我的项目
  static Future<ListProjectResponse> getMyProject(String? keyword, String? size,
      String? current, String? status, String? isCharge) async {
    var response = await NetCenter.request('/project/project/my-project',
        headers: {"Authorization": 'Bearer ${GlobalInit.getAccessToken()}'},
        params: {
          'keyword': keyword,
          'size': 100,
          'current': current,
          'status': status,
          'isCharge': isCharge
        },
        method: NetCenter.GET);
    final result = ListProjectResponse.fromJson(response);
    return result;
  }

  // 获取全部用户
  static Future<AllUserResponse> getAllCompanyUser() async {
    var response = await NetCenter.request('/project/company/user/all-user',
        headers: {"Authorization": 'Bearer ${GlobalInit.getAccessToken()}'},
        method: NetCenter.GET);
    final result = AllUserResponse.fromJson(response);
    return result;
  }

  // 获取项目详情
  static Future<ProjectDetailResponse> getProjectDetail(
      String projectId) async {
    var response = await NetCenter.request('/project/project/details',
        headers: {"Authorization": 'Bearer ${GlobalInit.getAccessToken()}'},
        params: {"projectId": projectId},
        method: NetCenter.GET);
    final result = ProjectDetailResponse.fromJson(response);
    return result;
  }

  // 获取项目需求列表
  static Future<ListProjectDemandResponse> getProjectDemandList(
      String projectId, String? keyword, int? status) async {
    var response = await NetCenter.request('/project/demand/demand',
        headers: {"Authorization": 'Bearer ${GlobalInit.getAccessToken()}'},
        params: {
          "projectId": projectId,
          "keyword": keyword,
          'size': '100',
          'current': '1',
          'status': status
        },
        method: NetCenter.GET);
    final result = ListProjectDemandResponse.fromJson(response);
    return result;
  }

  // 查看项目结题
  static Future<ProjectConclusionResponse> getConclusionProject(
      String projectId) async {
    var response =
        await NetCenter.request('/project/project/conclusion-details',
            headers: {"Authorization": 'Bearer ${GlobalInit.getAccessToken()}'},
            params: {
              "projectId": projectId,
            },
            method: NetCenter.GET);
    final result = ProjectConclusionResponse.fromJson(response);
    return result;
  }

  // 创建需求or任务
  static Future<BaseResponse> createDemandOrTask(
      CreateDemandOrTaskRequestParams params) async {
    var response = await NetCenter.request('/project/demand/create',
        headers: {"Authorization": 'Bearer ${GlobalInit.getAccessToken()}'},
        params: params.toJson(),
        method: NetCenter.POST);
    final result = BaseResponse.fromJson(response);
    return result;
  }

  // 删除需求或任务
  static Future<BaseResponse> deleteDemandOrTask(String demandId) async {
    var response = await NetCenter.request('/project/demand/delete',
        headers: {"Authorization": 'Bearer ${GlobalInit.getAccessToken()}'},
        params: {'demandId': demandId},
        method: NetCenter.DELETE);
    final result = BaseResponse.fromJson(response);
    return result;
  }

  // 修改需求或任务
  static Future<BaseResponse> changeDemandOrTask(
      ChangeDemandOrTaskRequestParams params) async {
    var response = await NetCenter.request('/project/demand/update',
        headers: {"Authorization": 'Bearer ${GlobalInit.getAccessToken()}'},
        params: params.toJson(),
        method: NetCenter.PUT);
    final result = BaseResponse.fromJson(response);
    return result;
  }

  // 需求或任务详情

  static Future<DemandOrTaskDetailResponse> getDemandOrTaskDetail(
      String demandId) async {
    var response = await NetCenter.request('/project/demand/details',
        headers: {"Authorization": 'Bearer ${GlobalInit.getAccessToken()}'},
        params: {'demandId': demandId},
        method: NetCenter.GET);
    final result = DemandOrTaskDetailResponse.fromJson(response);
    return result;
  }

  // 子需求或任务列表
  static Future<ListChildDemandOrTaskResponse> getListChildDemandOrTask(
      String demandId) async {
    var response = await NetCenter.request('/project/demand/children',
        headers: {"Authorization": 'Bearer ${GlobalInit.getAccessToken()}'},
        params: {'demandId': demandId},
        method: NetCenter.GET);
    final result = ListChildDemandOrTaskResponse.fromJson(response);
    return result;
  }

  // 需求或任务日志
  static Future<ListLogDemandOrTaskResponse> getListLogDemandOrTask(
      String demandId) async {
    var response = await NetCenter.request('/project/demand/log',
        headers: {"Authorization": 'Bearer ${GlobalInit.getAccessToken()}'},
        params: {'demandId': demandId},
        method: NetCenter.GET);
    final result = ListLogDemandOrTaskResponse.fromJson(response);
    return result;
  }

  // 获取系统设置
  static Future<WorkLoadTypeResponse> getWorkLoadType() async {
    var response = await NetCenter.request('/project/company/setting',
        headers: {"Authorization": 'Bearer ${GlobalInit.getAccessToken()}'},
        method: NetCenter.GET);
    final result = WorkLoadTypeResponse.fromJson(response);
    return result;
  }

  // 修改系统设置
  static Future<BaseResponse> changeWordLoadType(int type) async {
    var response = await NetCenter.request('/project/company/setting',
        headers: {"Authorization": 'Bearer ${GlobalInit.getAccessToken()}'},
        params: {"workloadType": type},
        method: NetCenter.PUT);
    final result = BaseResponse.fromJson(response);
    return result;
  }

  // 我的任务
  static Future<ListDemandOrTaskForMain> getListDemandOrTaskForMain(
      String? keyword, String? status, String? all) async {
    var response = await NetCenter.request('/project/demand/task',
        headers: {"Authorization": 'Bearer ${GlobalInit.getAccessToken()}'},
        params: {
          "keyword": keyword,
          'size': 100,
          'status': status,
          'all': false
        },
        method: NetCenter.GET);
    final result = ListDemandOrTaskForMain.fromJson(response);
    return result;
  }

  // 我的工作日志
  static Future<ListWorkLogResponse> getListWorkLog(
      String? startTime, String? endTime) async {
    var response = await NetCenter.request('/project/workload/my',
        headers: {"Authorization": 'Bearer ${GlobalInit.getAccessToken()}'},
        params: {
          "startTime": startTime,
          'size': 100,
          'endTime': endTime,
        },
        method: NetCenter.GET);
    final result = ListWorkLogResponse.fromJson(response);
    return result;
  }

  // 创建工作日志
  static Future<BaseResponse> createWorkLog(
      CreateWorkLogRequestParams params) async {
    var response = await NetCenter.request('/project/workload/create',
        headers: {"Authorization": 'Bearer ${GlobalInit.getAccessToken()}'},
        params: {
          "content": params.content,
          "workAhead": params.workAhead,
          "reportTime": params.reportTime,
          "workAfter": params.workAfter,
          "workPlan": params.workPlan,
          "type": params.type,
          "workTimes": params.workTimes
        },
        method: NetCenter.POST);
    final result = BaseResponse.fromJson(response);
    return result;
  }

  // 删除工作日志
  static Future<BaseResponse> deleteWorkLog(String workloadId) async {
    var response = await NetCenter.request('/project/workload',
        headers: {"Authorization": 'Bearer ${GlobalInit.getAccessToken()}'},
        params: {
          "workloadId": workloadId,
        },
        method: NetCenter.DELETE);
    final result = BaseResponse.fromJson(response);
    return result;
  }

  //修改工作日志
  static Future<BaseResponse> changeWorkLog(
      ChangeWorkLogRequestParams params) async {
    var response = await NetCenter.request('/project/workload',
        headers: {"Authorization": 'Bearer ${GlobalInit.getAccessToken()}'},
        params: params.toJson(),
        method: NetCenter.PUT);
    final result = BaseResponse.fromJson(response);
    return result;
  }

  //上传文件
  static Future<FileUploadResponse> uploadFile(String path, String name) async {
    var response = await NetCenter.request('/system/file/public/upload',
        headers: {"Authorization": 'Bearer ${GlobalInit.getAccessToken()}'},
        fileData: FormData.fromMap({
          'name': 'dio$name',
          'date': DateTime.now().toIso8601String(),
          'file': await MultipartFile.fromFile(path, filename: name),
        }),
        method: NetCenter.FILE);
    final result = FileUploadResponse.fromJson(response);
    return result;
  }

  //上传多个文件
  static Future<FilesUploadResponse> uploadFiles(
      List<MultipartFile> filePaths) async {
    var response = await NetCenter.request('/system/file/public/upload/batch',
        headers: {"Authorization": 'Bearer ${GlobalInit.getAccessToken()}'},
        fileData: FormData.fromMap({
          'name': 'dio${Random().nextInt(9)}',
          'date': DateTime.now().toIso8601String(),
          'file': filePaths
        }),
        method: NetCenter.FILE);
    final result = FilesUploadResponse.fromJson(response);
    return result;
  }
}
