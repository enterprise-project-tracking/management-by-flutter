import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:management/const/consts.dart';
import 'package:management/const/theme.dart';
import 'package:management/page/ModuleAccount/create_new_pwd_screen/view.dart';
import 'package:management/page/ModuleAccount/forgot_password_screen/view.dart';
import 'package:management/page/ModuleAccount/sign_in_screen/view.dart';
import 'package:management/page/ModuleAccount/sign_up_screen/view.dart';
import 'package:management/page/ModuleAccount/verify_code_screen/view.dart';
import 'package:management/page/ModuleMain/change_work_log_screen/logic.dart';
import 'package:management/page/ModuleMain/change_work_log_screen/view.dart';
import 'package:management/page/ModuleMain/create_work_log_screen/view.dart';
import 'package:management/page/ModuleMain/main_screen/view.dart';
import 'package:management/page/ModuleMain/upload_work_log_screen/view.dart';
import 'package:management/page/ModuleManage/edit_device_screen/view.dart';
import 'package:management/page/ModuleManage/employee_detail_screen/view.dart';
import 'package:management/page/ModuleManage/group_detail_screen/logic.dart';
import 'package:management/page/ModuleManage/group_detail_screen/view.dart';
import 'package:management/page/ModuleManage/group_load_screen/logic.dart';
import 'package:management/page/ModuleManage/group_load_screen/view.dart';
import 'package:management/page/ModuleManage/manage_screen/view.dart';
import 'package:management/page/ModuleManage/search_manage_screen/view.dart';
import 'package:management/page/ModuleProject/create_demand_screen/view.dart';
import 'package:management/page/ModuleProject/create_project_scree/view.dart';
import 'package:management/page/ModuleProject/demand_or_task_detail_screen/logic.dart';
import 'package:management/page/ModuleProject/demand_or_task_detail_screen/view.dart';
import 'package:management/page/ModuleProject/project_conclusion_screen/view.dart';
import 'package:management/page/ModuleProject/project_detail_screen/view.dart';
import 'package:management/page/ModuleProject/project_screen/view.dart';
import 'package:management/page/ModuleProject/show_more_demand_screen/view.dart';
import 'package:management/page/ModuleProject/show_more_log_screen/view.dart';
import 'package:management/page/ModuleSetting/edit_profile_screen/view.dart';
import 'package:management/page/ModuleSetting/j_company_screen/view.dart';
import 'package:management/page/ModuleSetting/notification_screen/view.dart';
import 'package:management/page/ModuleSetting/profile_screen/view.dart';
import 'package:management/page/ModuleSetting/security_screen/view.dart';
import 'package:management/page/ModuleSetting/work_space_screen/view.dart';

import 'package:management/page/home_screen/view.dart';

import 'package:management/page/spalshPage.dart';

import 'package:management/page/welcome_screen/view.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: const Size(428, 926),
      minTextAdapt: true,
      splitScreenMode: true,
      builder: (BuildContext context, Widget? child) => GetMaterialApp(
          title: 'Flutter Demo',
          translations: Messages(),
          locale: Get.deviceLocale,
          theme: AppTheme.lightTheme,
          darkTheme: AppTheme.darkTheme,
          initialRoute: '/',
          supportedLocales: const [Locale('zh', 'CN'), Locale('en', 'US')],
          fallbackLocale: const Locale('zh', 'CN'),
          getPages: RouteConfig.getPages,
          defaultTransition: Transition.fadeIn,
          home: const SplashScreen(),
          localizationsDelegates: const [
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
          ],
          builder: (context, widget) {
            return MediaQuery(
              //设置文字大小不随系统设置改变
              data: MediaQuery.of(context).copyWith(
                textScaler: const TextScaler.linear(1.0),
              ),
              child: FlutterEasyLoading(
                child: widget,
              ),
            );
          },
          routingCallback: (routing) {
            if (routing?.current == RouteConfig.homeScreen) {}
          }),
    );
  }
}

class RouteConfig {
  RouteConfig._();
  static const String homeScreen = "/homeScreen";
  static const String welcomeScreen = "/welcomeScreen";

  /// ModuleAccount
  static const String signInScreen = '/loginScreen';
  static const String signUpScreen = '/signUpScreen';
  static const String forgetPasswordScreen = '/forgetPasswordScreen';
  static const String verifyCodeScreen = '/verifyCodeScreen';
  static const String createNewPwdScreen = '/createNewPwdScreen';

  ///ModuleSetting
  static const String profileScreen = '/profileScreen';
  static const String editProfileScreen = '/editProfileScreen';
  static const String notificationScreen = '/notificationScreen';
  static const String securityScreen = '/securityScreen';
  static const String workSpaceScreen = '/workSpaceScreen';
  static const String jCompanyScreen = '/jCompanyScreen';

  ///ModuleManage
  static const String manageScreen = '/manageScreen';
  static const String searchManageScreen = '/searchManageScreen';
  static const String editDeviceScreen = '/editDeviceScreen';
  static const String employeeDetailScreen = '/employeeDetailScreen';
  static const String groupDetailScreen = '/groupDetailScreen';
  static const String groupLoadScreen = '/groupLoadScreen';

  ///ModuleProject
  static const String projectScreen = '/projectScreen';
  static const String createProjectScreen = '/createProjectScreen';
  static const String projectDetailScreen = '/projectDetailScreen';
  static const String projectConclusionScreen = '/projectConclusionScreen';
  static const String createDemandScreen = '/createDemandScreen';
  static const String demandOrTaskScreen = '/demandOrTaskScreen';
  static const String showMoreDemandScreen = '/showMoreDemandScreen';
  static const String showMoreLogScreen = '/showMoreLogScreen';

  ///ModuleMain
  static const String mainScreen = '/mainScreen';
  static const String uploadWorkLogScreen = '/uploadWorkLogScreen';
  static const String createWorkLogScreen = '/createWorkLogScreen';
  static const String changeWorkLogScreen = '/changeWorkLogScreen';

  static final List<GetPage> getPages = [
    GetPage(
      name: homeScreen,
      page: () => const HomeScreenPage(),
    ),
    GetPage(
      name: welcomeScreen,
      page: () => const WelcomeScreenPage(),
    ),

    /// ModuleAccount
    GetPage(
      name: signInScreen,
      page: () => const SignInScreenPage(),
    ),
    GetPage(
      name: signUpScreen,
      page: () => const SignUpScreenPage(),
    ),
    GetPage(name: forgetPasswordScreen, page: () => const ForgotPasswordPage()),
    GetPage(name: verifyCodeScreen, page: () => const VerifyCodeScreenPage()),
    GetPage(
        name: createNewPwdScreen, page: () => const CreateNewPwdScreenPage()),

    ///ModuleSetting
    GetPage(name: profileScreen, page: () => const ProfileScreenPage()),
    GetPage(name: editProfileScreen, page: () => const EditProfileScreenPage()),
    GetPage(
        name: notificationScreen, page: () => const NotificationScreenPage()),
    GetPage(name: securityScreen, page: () => const SecurityScreenPage()),
    GetPage(name: securityScreen, page: () => const SecurityScreenPage()),
    GetPage(name: workSpaceScreen, page: () => const WorkSpaceScreenPage()),
    GetPage(name: jCompanyScreen, page: () => const JCompanyScreenPage()),

    ///ModuleManage
    GetPage(name: manageScreen, page: () => const ManageScreenPage()),
    GetPage(
        name: searchManageScreen, page: () => const SearchManageScreenPage()),
    GetPage(name: editDeviceScreen, page: () => const EditDeviceScreenPage()),
    GetPage(
        name: employeeDetailScreen,
        page: () => const EmployeeDetailScreenPage()),
    GetPage(
        name: groupDetailScreen,
        page: () => const GroupDetailScreenPage(),
        transition: Transition.rightToLeft,
        binding: BindingsBuilder(() {
          Get.create<GroupDetailScreenLogic>(() => GroupDetailScreenLogic());
        })),
    GetPage(
        name: groupLoadScreen,
        page: () => const GroupLoadScreenPage(),
        transition: Transition.downToUp,
        binding: BindingsBuilder(() {
          Get.create<GroupLoadScreenLogic>(() => GroupLoadScreenLogic());
        })),

    ///ModuleProject
    GetPage(name: profileScreen, page: () => const ProjectScreenPage()),
    GetPage(
      name: createProjectScreen,
      page: () => const CreateProjectScreePage(),
      transition: Transition.downToUp,
    ),
    GetPage(
        name: projectDetailScreen, page: () => const ProjectDetailScreenPage()),
    GetPage(
      name: projectConclusionScreen,
      page: () => const ProjectConclusionScreenPage(),
      transition: Transition.downToUp,
    ),
    GetPage(
      name: createDemandScreen,
      page: () => const CreateDemandScreenPage(),
      transition: Transition.downToUp,
    ),
    GetPage(
        name: demandOrTaskScreen,
        page: () => const DemandOrTaskDetailScreenPage(),
        binding: BindingsBuilder(() {
          Get.create<DemandOrTaskDetailScreenLogic>(
              () => DemandOrTaskDetailScreenLogic());
        })),
    GetPage(
        name: showMoreDemandScreen,
        page: () => const ShowMoreDemandScreenPage()),
    GetPage(name: showMoreLogScreen, page: () => const ShowMoreLogScreenPage()),

    ///ModuleMain
    GetPage(name: mainScreen, page: () => const MainScreenPage()),
    GetPage(
        name: uploadWorkLogScreen, page: () => const UploadWorkLogScreenPage()),
    GetPage(
        name: createWorkLogScreen, page: () => const CreateWorkLogScreenPage()),
    GetPage(
        name: changeWorkLogScreen, page: () => const ChangeWorkLogScreenPage())
  ];
}
